# Dockerfile for DeepBlue Server
# Felipe Albrecht - 03.01.2022

FROM ubuntu:21.04
LABEL Name=DeepBlue-Server Version=1.5.0

ENV DEBIAN_FRONTEND=noninteractive
ENV LD_LIBRARY_PATH=/DeepBlue/server/third_party/luajit-2.0/src/

RUN apt-get update
RUN apt-get install --yes \
      build-essential  \
      cmake            \
      git              \
      libboost-all-dev \
      libbson-dev      \
      libbz2-dev       \
      libjemalloc-dev  \
      libmongoc-dev    \
      tar              \
      unzip            \
      wget

### Build Mongo CXX Driver:
RUN wget https://github.com/mongodb/mongo-cxx-driver/archive/refs/tags/r3.6.6.tar.gz && \
    tar -zxvf r3.6.6.tar.gz

WORKDIR mongo-cxx-driver-r3.6.6/build

RUN mkdir /opt/mongo-cxx-driver && \
    cmake ..                                            \
        -DCMAKE_BUILD_TYPE=Release                      \
        -DBSONCXX_POLY_USE_BOOST=1                      \
        -DBUILD_VERSION=3.6.6                           \
        -DCMAKE_INSTALL_PREFIX=/opt/mongo-cxx-driver && \
    cmake --build . -j 16                            && \
    cmake --build . --target install


### Build FMT lib
WORKDIR /
RUN wget https://github.com/fmtlib/fmt/releases/download/7.1.3/fmt-7.1.3.zip && \
    unzip fmt-7.1.3.zip
RUN mkdir -p fmt-7.1.3/build
WORKDIR fmt-7.1.3/build
RUN cmake .. && \
    make -j 16    && \
    make install


# Checkout DeepBlue Server code and libs
WORKDIR /
ADD https://gitlab.com/api/v4/projects/19328004/repository/commits?ref_name=master version.json
RUN git clone https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/DeepBlue.git && \
    cd DeepBlue && \
    git submodule init && \
    git submodule update

## Build LuaJIT
WORKDIR  /DeepBlue/server/third_party/luajit-2.0/
RUN make
WORKDIR  /DeepBlue/server/third_party/luajit-2.0/src
RUN ln -s libluajit.so libluajit-5.1.so.2

## Build EXPAT
WORKDIR  /DeepBlue/server/third_party/expat
RUN make -j 16 && cp .libs/libexpat.a ../libexpat_linux.a

## Build DeepBlue Server
WORKDIR /DeepBlue/server/src/
RUN make -j 16

### Execute DeepBlue
FROM ubuntu:21.04

RUN apt-get update
RUN apt-get install --yes \
      libboost-filesystem1.74.0 \
      libboost-thread1.74.0 \
      libboost-system1.74.0 \
      libboost-regex1.74.0 \
      libboost-log1.74.0 \
      libboost-timer1.74.0 \
      libboost-program-options1.74.0 \
      libboost-iostreams1.74.0 \
      libboost-serialization1.74.0 \
      libjemalloc2 \
      libbson-1.0-0 \
      libmongoc-1.0-0

WORKDIR /DeepBlue
COPY --from=0 /DeepBlue/server/third_party/luajit-2.0/src/libluajit.so .
RUN ln -s libluajit.so libluajit-5.1.so.2

COPY --from=0 /opt/mongo-cxx-driver/lib/* /DeepBlue/

COPY --from=0 /DeepBlue/server/src/server .

ENV LD_LIBRARY_PATH=/DeepBlue/
ENTRYPOINT ["./server"]
CMD ["--help"]