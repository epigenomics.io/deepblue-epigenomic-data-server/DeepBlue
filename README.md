DeepBlue Epigenomic Data Server
========

Copyright (c) 2019,2020,2021 Felipe Albrecht. All rights reserved.
Copyright (c) 2014,2015,2016,2017,2018 Max Planck Institute for Informatics. All rights reserved.

Author: Felipe Albrecht (felipe.albrecht@gmail.com)

DeepBlue Epigenomic Data Server is a data server for epigenomic data that allows users to find, operate, and retrieve epigenomic data.

DeepBlue imports data from ENCODE, Roadmap, BLUEPRINT, and DEEP epigenomic consortia (the populator module will be make public ASAP)

For a complete description of DeepBlue Ecosystem: [Felipe Albrecht's PhD Thesis](https://publikationen.sulb.uni-saarland.de/handle/20.500.11880/28473)

For a short description of DeepBlue: [it's original manuscript](https://academic.oup.com/bioinformatics/article/33/13/2063/3045024?login=true) and [it's R package](https://academic.oup.com/nar/article/44/W1/W581/2499295?login=true)

For more usage information, please check:
  * [Main Page](http://deepblue.mpi-inf.mpg.de)
  * [Examples](http://deepblue.mpi-inf.mpg.de/examples.php)
  * [Manual](http://deepblue.mpi-inf.mpg.de/manual/)
  * [API](http://deepblue.mpi-inf.mpg.de/api.php)


For building and executing instructions: [build.md](https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/DeepBlue/-/blob/master/server/src/build.md)
  
For citing DeepBlue Epigenomc Data Server:

> Felipe Albrecht, Markus List, Christoph Bock, Thomas Lengauer, DeepBlueR: large-scale epigenomic analysis in R, _Bioinformatics_, Volume 33, Issue 13, 1 July 2017, Pages 2063–2064, https://doi.org/10.1093/bioinformatics/btx099


Bibtex:
```
@article{10.1093/bioinformatics/btx099,
    author = {Albrecht, Felipe and List, Markus and Bock, Christoph and Lengauer, Thomas},
    title = "{DeepBlueR: large-scale epigenomic analysis in R}",
    journal = {Bioinformatics},
    volume = {33},
    number = {13},
    pages = {2063-2064},
    year = {2017},
    month = {02},
    abstract = "{While large amounts of epigenomic data are publicly available, their retrieval in a form suitable for downstream analysis is a bottleneck in current research. The DeepBlue Epigenomic Data Server provides a powerful interface and API for filtering, transforming, aggregating and downloading data from several epigenomic consortia.To make public epigenomic data conveniently available for analysis in R, we developed an R/Bioconductor package that connects to the DeepBlue Epigenomic Data Server, enabling users to quickly gather and transform epigenomic data from selected experiments for analysis in the Bioconductor ecosystem.http://deepblue.mpi-inf.mpg.de/R.R 3.3, Bioconductor 3.4.Supplementary data are available at Bioinformatics online. }",
    issn = {1367-4803},
    doi = {10.1093/bioinformatics/btx099},
    url = {https://doi.org/10.1093/bioinformatics/btx099},
    eprint = {https://academic.oup.com/bioinformatics/article-pdf/33/13/2063/25155821/btx099.pdf},
}
```


