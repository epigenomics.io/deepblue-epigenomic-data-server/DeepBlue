//
//  experiments.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 11.11.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <limits>
#include <regex>
#include <string>

#include "../datatypes/column_types_def.hpp"
#include "../datatypes/regions.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/database.hpp"

#include "../errors.hpp"

#include "collections.hpp"
#include "experiments.hpp"
#include "exists.hpp"
#include "info.hpp"
#include "helpers.hpp"
#include "queries.hpp"

namespace epidb {
  namespace dba {
    namespace experiments {

      MaybeDocument by_name(const std::string &name)
      {
        const std::string norm_name = utils::normalize_name(name);
        return helpers::get_one_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("norm_name", norm_name));
      }

      MaybeDocument by_id(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::EXPERIMENTS(), id);
      }

      MaybeDocument by_hash(const std::string &id)
      {
        if (id.empty()) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, id);
        }

        auto _new_id = id;
        _new_id.erase(0, 1);
        return helpers::get_one_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("extra_metadata.md5sum", _new_id));
      }

      std::experimental::optional<std::string> get_genome(const std::string &norm_name)
      {
        auto maybe_experiment = by_name(norm_name);
        if (!maybe_experiment) {
          return std::experimental::nullopt;
        }

        auto view = maybe_experiment->view();
        return bson_utils::get_string(view, "norm_genome");
      }

      std::pair<std::string, std::string> get_experiment_name(const std::string &name_id)
      {
        if (name_id.empty()) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, name_id);
        }

        std::string name;
        std::string norm_name;

        if (utils::is_id(name_id, "e")) {
          auto maybe_document = by_id(name_id);
          if (!maybe_document) {
            throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, name_id);
          }
          auto experiment = maybe_document->view();
          name = bson_utils::get_string(experiment, "name");
          norm_name = utils::normalize_name(name);

        } else if (name_id[0] == '#') {
          auto maybe_document = by_hash(name_id);
          if (!maybe_document) {
            throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, name_id);
          }
          auto experiment = maybe_document->view();
          name = bson_utils::get_string(experiment, "name");
          norm_name = utils::normalize_name(name);

        } else {
          name = name_id;
          norm_name = utils::normalize_name(name_id);
        }
        return std::make_pair(name, norm_name);
      }

      std::pair<std::vector<std::string>, std::vector<std::string>> get_experiments_names(const std::vector<std::string> &names_ids)
      {
        std::vector<std::string> names;
        std::vector<std::string> norm_names;
        for (const auto &name_id : names_ids) {
          std::string name;
          std::string norm_name;

          auto experiment_names = get_experiment_name(name_id);
          names.push_back(std::move(experiment_names.first));
          norm_names.push_back(experiment_names.second);
        }

        return std::make_pair(names, norm_names);
      }

      Document build_metadata(const std::string &name, const std::string &norm_name,
                          const std::string &genome, const std::string &norm_genome,
                          const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                          const std::string &sample_id, const std::string &technique, const std::string &norm_technique,
                          const std::string &project, const std::string &norm_project,
                          const std::string &description, const std::string &norm_description,
                          const View &extra_metadata_obj,
                          const std::string &ip,
                          const parser::FileFormat &format,
                          int &dataset_id,
                          std::string &experiment_id)
      {
        dataset_id = helpers::get_incremente_counter_and_notify("datasets");
        return build_metadata_with_dataset(name, norm_name,
                                         genome, norm_genome,
                                         epigenetic_mark, norm_epigenetic_mark,
                                         sample_id, technique, norm_technique,
                                         project, norm_project,
                                         description, norm_description,
                                         extra_metadata_obj,
                                         ip,
                                         format,
                                         dataset_id,
                                         experiment_id);
      }


      Document build_metadata_with_dataset(const std::string &name, const std::string &norm_name,
                                       const std::string &genome, const std::string &norm_genome,
                                       const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                                       const std::string &sample_id,
                                       const std::string &technique, const std::string &norm_technique,
                                       const std::string &project, const std::string &norm_project,
                                       const std::string &description, const std::string &norm_description,
                                       const View &extra_metadata_obj,
                                       const std::string &ip,
                                       const parser::FileFormat &format,
                                       const int dataset_id,
                                       std::string &experiment_id)
      {
        int e_id = helpers::get_incremente_counter_and_notify(Collections::EXPERIMENTS());
        experiment_id = "e" + utils::integer_to_string(e_id);

        DocumentBuilder experiment_data_builder{};
        experiment_data_builder.append(KVP("_id", experiment_id));
        experiment_data_builder.append(KVP(KeyMapper::DATASET(), dataset_id));
        experiment_data_builder.append(KVP("name", name));
        experiment_data_builder.append(KVP("norm_name", norm_name));
        experiment_data_builder.append(KVP("genome", genome));
        experiment_data_builder.append(KVP("norm_genome", norm_genome));
        experiment_data_builder.append(KVP("epigenetic_mark", epigenetic_mark));
        experiment_data_builder.append(KVP("norm_epigenetic_mark", norm_epigenetic_mark));
        experiment_data_builder.append(KVP("sample_id", sample_id));
        experiment_data_builder.append(KVP("technique", technique));
        experiment_data_builder.append(KVP("norm_technique", norm_technique));
        experiment_data_builder.append(KVP("project", project));
        experiment_data_builder.append(KVP("norm_project", norm_project));
        experiment_data_builder.append(KVP("description", description));
        experiment_data_builder.append(KVP("norm_description", norm_description));
        experiment_data_builder.append(KVP("format", format.format()));
        experiment_data_builder.append(KVP("columns", format.to_array()));
        experiment_data_builder.append(KVP("extra_metadata", extra_metadata_obj));

        std::map<std::string, std::string> sample_data = info::get_sample_by_id(sample_id, true);
        DocumentBuilder sample_builder{};
        std::map<std::string, std::string>::iterator it;
        for (const auto& kv: sample_data) {
          if ((kv.first != "_id") && (kv.first != "user")) {
            sample_builder.append(KVP(kv.first, kv.second));
          }
        }
        experiment_data_builder.append(KVP("sample_info", sample_builder.extract()));

        return experiment_data_builder.extract();
      }

      std::string create_experiment_set(const std::vector<std::string> &experiment_names,
                                const std::string& set_name,
                                const std::string& description, const bool is_public)
      {
        std::string norm_set_name = utils::normalize_name(set_name);

        if (exists::experiment_set(set_name)) {
          throw deepblue_user_exception(ERR_EXPERIMENT_SET, set_name);
        }

        int es_id = helpers::get_incremente_counter_and_notify(Collections::EXPERIMENT_SETS());
        auto set_id = "es" + utils::integer_to_string(es_id);

        auto experiment_names_array = utils::build_array(experiment_names);

        DocumentBuilder bob;
        bob.append(KVP("_id", set_id));
        bob.append(KVP("name", set_name));
        bob.append(KVP("norm_name", utils::normalize_name(norm_set_name)));
        bob.append(KVP("description", description));
        bob.append(KVP("norm_description", utils::normalize_name(description)));
        bob.append(KVP("public", is_public));
        bob.append(KVP("experiments", experiment_names_array));

        COLLECTION(experiment_sets_collections, Collections::EXPERIMENT_SETS())
        experiment_sets_collections.insert_one(bob.extract());

        return set_id;
      }
    }
  }
}
