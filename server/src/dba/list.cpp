//
//  list.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 07.04.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm>
#include <future>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

#include "../config/config.hpp"

#include "collections.hpp"
#include "column_types.hpp"
#include "dba.hpp"
#include "helpers.hpp"
#include "list.hpp"

#include "../algorithms/levenshtein.hpp"

#include "../dba/genes.hpp"
#include "../dba/users.hpp"

#include "../datatypes/metadata.hpp"
#include "../datatypes/user.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/database.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace dba {
    namespace list {

      const std::vector<utils::IdName> users()
      {
        return helpers::get_all_id_names(Collections::USERS());
      }

      const std::vector<utils::IdName> genomes()
      {
        return helpers::get_all_id_names(Collections::GENOMES());
      }

      const std::vector<utils::IdName> biosources(const datatypes::Metadata &metadata)
      {
        DocumentBuilder query_builder;
        for (const auto& p : metadata) {
          query_builder.append(KVP("extra_metadata." + p.first, p.second));
        }
        return helpers::get_id_names_by_query(Collections::BIOSOURCES(), query_builder.extract());
      }

      const std::vector<utils::IdName> techniques()
      {
        return helpers::get_all_id_names(Collections::TECHNIQUES());
      }

      const std::vector<std::string> samples(const ArrayView &biosources, const datatypes::Metadata &metadata)
      {
        DocumentBuilder query_builder;
        if (!biosources.empty()) {
          query_builder.append(KVP("norm_biosource_name", bson_utils::in_doc(biosources)));
        }

        for (const auto& kv: metadata) {
          query_builder.append(KVP(kv.first, kv.second));
        }

        std::vector<std::string> fields;
        fields.push_back("_id");

        std::vector<std::string> result;
        std::vector<Document> samples = helpers::get_fields_by_query(Collections::SAMPLES(), query_builder.extract(), fields);
        for (const auto & o : samples) {
          result.push_back(bson_utils::get_string(o, "_id"));
        }

        return result;
      }


      const std::vector<utils::IdName> all_projects()
      {
        return helpers::get_all_id_names(Collections::PROJECTS());
      }

      /*
      * List all projects that MUST NOT be available to the user
      */
      const std::vector<utils::IdName> private_projects(const datatypes::User& user)
      {
        if (user.is_admin()) {
          return {};
        }

        DocumentBuilder full_query_builder;
        std::vector<std::string> user_member_projects = user.projects_member();
        if (!user_member_projects.empty()) {
          auto user_projects_bson = MakeDocument(KVP("_id", MakeDocument(
                KVP("$nin", utils::build_normalized_array(user_member_projects)))));

          auto private_projects = MakeDocument(KVP("public", false));
          full_query_builder.append(KVP("$and",
             MakeArray(private_projects, user_projects_bson)));
        } else {
          full_query_builder.append(KVP("public", false));
        }

        return helpers::get_id_names_by_query(Collections::PROJECTS(), full_query_builder.extract());
      }

      /*
      * List all projects that are available to the user
      */
      const std::vector<utils::IdName> public_projects()
      {
        return helpers::get_id_names_by_query(Collections::PROJECTS(), MakeDocument(KVP("public", true)));
      }

      const std::vector<utils::IdName> epigenetic_marks(const datatypes::Metadata &metadata)
      {
        DocumentBuilder query_builder;

        for (const auto& p : metadata) {
          query_builder.append(KVP("extra_metadata." + p.first, p.second));
        }
        return helpers::get_id_names_by_query(Collections::EPIGENETIC_MARKS(), query_builder.extract());
      }

      const std::vector<utils::IdName> experiments(const View& query)
      {
        return helpers::get_id_names_by_query(Collections::EXPERIMENTS(), query);
      }

      const std::vector<utils::IdName> annotations(const std::string &genome)
      {
        return helpers::get_id_names_by_query(Collections::ANNOTATIONS(), bson_utils::one_key_doc("norm_genome", genome));
      }

      const std::vector<utils::IdName> annotations()
      {
        return helpers::get_all_id_names(Collections::ANNOTATIONS());
      }

      const std::vector<utils::IdName> column_types()
      {
        return dba::columns::list_column_types();
      }

      //-----

      bool column_types(std::vector<std::string> &content, std::string  &msg)
      {
        auto cursor = helpers::get_all_by_query(Collections::COLUMN_TYPES(), {});

        processing::StatusPtr status = processing::build_dummy_status();

        for (const auto& o: cursor) {
          columns::ColumnTypePtr column_type = epidb::dba::columns::column_type_document_to_class(o);
          content.push_back(column_type->str());
        }

        return true;
      }

      // --

      std::vector<utils::IdName> gene_models()
      {
        return helpers::get_all_id_names(Collections::GENE_MODELS());
      }

      const std::vector<Document> genes(const std::vector<std::string> &genes_id_or_name, const std::vector<std::string> &go_terms,
                 const std::vector<std::string> &chromosomes,
                 const Position start, const Position end,
                 const std::string &norm_gene_model)
      {
        return dba::genes::get_genes(chromosomes, start, end, "", genes_id_or_name, go_terms, norm_gene_model);
      }

      bool get_genes(const datatypes::User& user,
                     const std::vector<std::string> &chromosomes, const Position start, const Position end,
                     const std::string& strand,
                     const std::vector<std::string>& genes_names_or_id, const std::vector<std::string>& go_terms,
                     const std::string &norm_gene_model,
                     std::vector<Document>& genes, std::string &msg);

      std::vector<utils::IdName> similar_biosources(const std::string &name)
      {
        return similar(Collections::BIOSOURCES(), utils::normalize_name(name));
      }

      std::vector<utils::IdName> similar_techniques(const std::string &name)
      {
        return similar(Collections::TECHNIQUES(), utils::normalize_name(name));
      }

      std::vector<utils::IdName> similar_projects(const std::string &name)
      {
        return similar(Collections::PROJECTS(), utils::normalize_name(name));
      }

      std::vector<utils::IdName> similar_epigenetic_marks(const std::string &name)
      {
        return similar(Collections::EPIGENETIC_MARKS(),  utils::normalize_epigenetic_mark(name));
      }

      std::vector<utils::IdName> similar_genomes(const std::string &name)
      {
        return similar(Collections::GENOMES(), utils::normalize_epigenetic_mark(name));
      }

      const std::vector<utils::IdName> similar_experiments(const std::string &name, const std::string &norm_genome)
      {
        return similar(Collections::EXPERIMENTS(), "name", name, "norm_genome", norm_genome);
      }

      std::vector<utils::IdName> similar(const std::string &where, const std::string &what, const size_t total)
      {
        std::vector<utils::IdName> id_names = helpers::get_all_id_names(where);

        std::vector<std::string> names;
        std::map<std::string, std::string> id_name_map;
        for (const utils::IdName & id_name : id_names) {
          id_name_map[id_name.name] = id_name.id;
          names.push_back(id_name.name);
        }
        std::vector<std::string> ordered = epidb::algorithms::Levenshtein::order_by_score(what, names);

        std::vector<utils::IdName> result;
        size_t count = 0;
        for (const std::string & name : ordered) {
          utils::IdName id_name(id_name_map[name], name);
          result.push_back(id_name);
          count++;
          if (count >= total) {
            break;
          }
        }

        return result;
      }

      std::vector<utils::IdName> similar(const std::string &where, const std::string &field_name, const std::string &what,
                   const std::string &filter_field, const std::string &filter_what,
                   const size_t total)
      {
        auto docs = helpers::get_all_by_query(where, bson_utils::one_key_doc(filter_field, filter_what));

        std::vector<std::string> names;
        std::map<std::string, std::string> id_name_map;
        for (const auto& doc: docs) {
          std::string field_value = bson_utils::get_string(doc, field_name);
          names.push_back(field_value);
          id_name_map[field_value] = bson_utils::get_string(doc, "_id");;
        }

        std::vector<std::string> ordered = epidb::algorithms::Levenshtein::order_by_score(what, names);

        std::vector<utils::IdName> result;
        size_t count = 0;
        for (const std::string & name : ordered) {
          utils::IdName id_name(id_name_map[name], name);
          result.push_back(id_name);
          count++;
          if (count >= total) {
            break;
          }
        }

        return result;
      }

      Document build_list_experiments_bson_query(const datatypes::User& user, const View &args)
      {
        // Get the experiments
        DocumentBuilder experiments_query_builder;

        auto accepted_keys = {"norm_genome",
                              "norm_experiment_name",
                              "norm_epigenetic_mark",
                              "sample_info.norm_biosource_name",
                              "sample_id",
                              // "norm_project", project is dealt bellow
                              "norm_technique",
                              "sample_info",
                              "upload_info"};

        for (const auto& kv: args) {
          auto key = bson_utils::get_key(kv);
          // filter out key-values outside the accepted keys.
          if (std::find(accepted_keys.begin(), accepted_keys.end(), key) == accepted_keys.end()) {
            continue;
          }

          if (key == "norm_experiment_name") {
            if (args["norm_experiment_name"].type() == ArrayType) {
              auto experiment_names = utils::build_vector(args["norm_experiment_name"].get_array().value);

              std::vector<std::string> exp_names_string;
              std::vector<std::string> md5sums;

              for (auto& name: experiment_names) {
                if (name.empty()) {
                  continue;
                }
                if (name[0] == '#') {
                  md5sums.push_back(name.erase(0, 1));
                } else {
                  exp_names_string.push_back(name);
                }
              }

              if (!exp_names_string.empty()) {
                experiments_query_builder.append(KVP("norm_name", bson_utils::in_doc(utils::build_array(exp_names_string))));
              }

              if (!md5sums.empty()) {
                experiments_query_builder.append(KVP("extra_metadata.md5sum", bson_utils::in_doc(utils::build_array(exp_names_string))));
              }
            } else {
              auto exp_name = bson_utils::get_string(args, "norm_experiment_name");
              if (!exp_name.empty()) {
                if (exp_name[0] == '#') {
                  experiments_query_builder.append(KVP("extra_metadata.md5sum", exp_name));
                } else {
                  experiments_query_builder.append(KVP("norm_name", exp_name));
                }
              }
            }
          } else if (kv.type() == ArrayType) {
            experiments_query_builder.append(KVP(key, bson_utils::in_doc(kv)));

          } else if (kv.type() == DocumentType) {
            for (const auto& e: kv.get_document().value) {
              std::string fieldname = bson_utils::get_key(e);
              experiments_query_builder.append(KVP(key + "." + fieldname, e.get_value()));
           }
          } else {
              experiments_query_builder.append(KVP(key, kv.get_value()));
          }
        }

        if (bson_utils::has_key(args, "norm_project")) {
          if (args["norm_project"].type() == ArrayType) {
            experiments_query_builder.append(KVP("norm_project", bson_utils::in_doc(args["norm_project"])));
          } else {
            experiments_query_builder.append(KVP("norm_project", bson_utils::get_string(args, "norm_project")));
          }
        } else {
          std::vector<std::string> user_projects_names = user.projects();
          experiments_query_builder.append(KVP("norm_project", bson_utils::in_doc(utils::build_normalized_array(user_projects_names))));
        }

        experiments_query_builder.append(KVP("upload_info.done", true));

        return experiments_query_builder.extract();
      }

      Document build_list_experiments_query(const datatypes::User& user,
                                        const std::vector<serialize::ParameterPtr> genomes,
                                        const std::vector<serialize::ParameterPtr> types,
                                        const std::vector<serialize::ParameterPtr> epigenetic_marks, const std::vector<serialize::ParameterPtr> biosources,
                                        const std::vector<serialize::ParameterPtr> sample_ids,
                                        const std::vector<serialize::ParameterPtr> techniques,
                                        const std::vector<serialize::ParameterPtr> projects)
      {
        DocumentBuilder args_builder;

        if (!types.empty()) {
          args_builder.append(KVP("upload_info.content_format", utils::build_normalized_array(types)));
        }

        if (!genomes.empty()) {
          args_builder.append(KVP("norm_genome", utils::build_normalized_array(genomes)));
        }


        if (!biosources.empty()) {
          args_builder.append(KVP("sample_info.norm_biosource_name", utils::build_normalized_array(biosources)));
        }

        // epigenetic mark
        if (!epigenetic_marks.empty()) {
          args_builder.append(KVP("norm_epigenetic_mark", utils::build_epigenetic_normalized_array(epigenetic_marks)));
        }
        // sample id
        if (!sample_ids.empty()) {
          args_builder.append(KVP("sample_id", utils::build_array(sample_ids)));
        }

        std::vector<std::string> user_projects_names = user.projects();

        // project.
        if (!projects.empty()) {
          // Filter the projects that are available to the user
          std::vector<serialize::ParameterPtr> filtered_projects;
          for (const auto& project : projects) {
            std::string project_name = project->as_string();
            std::string norm_project = utils::normalize_name(project_name);
            bool found = false;
            for (const auto& user_project : user_projects_names) {
              std::string norm_user_project = utils::normalize_name(user_project);
              if (norm_project == norm_user_project) {
                filtered_projects.push_back(project);
                found = true;
                break;
              }
            }

            if (!found) {
              throw deepblue_user_exception(ERR_INVALID_PROJECT, project_name);
            }
          }
          args_builder.append(KVP("norm_project", utils::build_normalized_array(filtered_projects)));
        } else {
          args_builder.append(KVP("norm_project", utils::build_normalized_array(user_projects_names)));
        }

        // technique
        if (!techniques.empty()) {
          args_builder.append(KVP("norm_technique", utils::build_normalized_array(techniques)));
        }

        return build_list_experiments_bson_query(user, args_builder.extract());
      }

      const std::string get_controlled_vocabulary_key(const std::string& controlled_vocabulary)
      {
        if (controlled_vocabulary == dba::Collections::EPIGENETIC_MARKS()) {
          return "$norm_epigenetic_mark";
        }

        if (controlled_vocabulary == dba::Collections::GENOMES()) {
          return "$norm_genome";
        }

        if (controlled_vocabulary == dba::Collections::BIOSOURCES()) {
          return "$sample_info.norm_biosource_name";
        }

        if (controlled_vocabulary == dba::Collections::SAMPLES()) {
          return "$sample_id";
        }

        if (controlled_vocabulary == dba::Collections::TECHNIQUES()) {
          return "$norm_technique";
        }

        if (controlled_vocabulary == dba::Collections::PROJECTS()) {
          return "$norm_project";
        }

        if (controlled_vocabulary == "types") {
          return "$upload_info.content_format";
        }

        throw deepblue_user_exception(ERR_INVALID_CONTROLLED_VABULARY, controlled_vocabulary,
              dba::Collections::EPIGENETIC_MARKS(), dba::Collections::GENOMES(),
              dba::Collections::BIOSOURCES(),  dba::Collections::SAMPLES(),
              dba::Collections::TECHNIQUES(),  dba::Collections::PROJECTS());
      }


      const std::vector<utils::IdNameCount> in_use(const datatypes::User& user, const std::string &collection, const std::string &key_name)
      {
        std::vector<std::string> project_names = user.projects();
        auto projects_array = utils::build_normalized_array(project_names);

        // Select experiments that are uploaded and from πublic projects or that the user has permission
        auto done = MakeDocument(KVP("upload_info.done", true));
        auto user_projects_bson = MakeDocument(KVP("norm_project", bson_utils::in_doc(projects_array)));
        ArrayBuilder ab;
        ab.append(done.view(), user_projects_bson.view());
        auto query = MakeDocument(KVP("$and", ab.extract()));

        // Group by count
        auto group = MakeDocument(KVP( "_id", key_name),
                                  KVP("total", MakeDocument(KVP("$sum", 1))));

        mongocxx::pipeline p{};
        p.match(query.view());
        p.group(group.view());

        COLLECTION(experiments_collection, Collections::EXPERIMENTS());
        auto cursor = experiments_collection.aggregate(p, mongocxx::options::aggregate{});

        std::vector<utils::IdNameCount> names;
        for (const auto & be : cursor) {
          std::string norm_name = utils::normalize_name(bson_utils::get_string(be, "_id"));
          auto count = be["total"].get_int64().value;

          utils::IdNameCount inc;
          if (collection == Collections::SAMPLES()) {
            inc = utils::IdNameCount(norm_name, "", count);
          } else {
            utils::IdName id_name = helpers::get_id_name(collection, norm_name);
            inc = utils::IdNameCount(id_name.id, id_name.name, count);
          }
          names.push_back(inc);
        }

        return names;
      }

      void __collection_experiments_count(
          const View & experiments_query, const ArrayView & projects_array,
          const std::string collection, const std::string key_name,
          std::unordered_map<std::string, std::vector<utils::IdNameCount> > &faceting_result)
      {
        // Select experiments that are uploaded and from πublic projects or that the user has permission
        auto done = MakeDocument(KVP("upload_info.done", true));
        auto user_projects_bson = MakeDocument(KVP("norm_project", bson_utils::in_doc(projects_array)));
        ArrayBuilder ab;
        ab.append(done, user_projects_bson, experiments_query);
        auto query = MakeDocument(KVP("$and", ab.extract()));

        // Group by count
        auto group = MakeDocument(KVP( "_id", key_name),
                                  KVP("total", MakeDocument(
                                        KVP("$sum", 1 ))));

        mongocxx::pipeline p{};
        p.match(query.view());
        p.group(group.view());

        COLLECTION(experiments_collection, Collections::EXPERIMENTS());
        auto cursor = experiments_collection.aggregate(p, mongocxx::options::aggregate{});

        std::vector<utils::IdNameCount> names;
        for (const auto& be: cursor) {
          std::string norm_name;
          if (!bson_utils::has_key(be, "_id")) {
            norm_name = "null";
          } else {
            norm_name = utils::normalize_name(StringViewToString(be["_id"].get_utf8().value));
          }

          auto count = bson_utils::get_safe_int32(be, "total");

          utils::IdNameCount inc;
          if (collection == Collections::SAMPLES())
            inc = utils::IdNameCount(norm_name, "", count);
          else if (collection == "types") {
            inc = utils::IdNameCount("", norm_name, count);
          } else {
            utils::IdName id_name = helpers::get_id_name(collection, norm_name);
            inc = utils::IdNameCount(id_name.id, id_name.name, count);
          }
          names.push_back(inc);
        }
        std::sort(names.begin(), names.end(), utils::IdNameCountComparer);
        faceting_result[collection] = names;
      }

      const std::vector<utils::IdNameCount> collection_experiments_count(const datatypes::User& user,
                                        const std::string controlled_vocabulary, const View& experiments_query)
      {
        std::vector<utils::IdNameCount> experiments_count;
        std::vector<std::string> project_names = user.projects();
        auto projects_array = utils::build_normalized_array(project_names);

        std::string key_name = get_controlled_vocabulary_key(controlled_vocabulary);

        std::unordered_map<std::string, std::vector<utils::IdNameCount>> faceting_result;
        __collection_experiments_count(experiments_query, projects_array,
                      controlled_vocabulary, key_name, faceting_result);

        return faceting_result[controlled_vocabulary];
      }

      std::unordered_map<std::string, std::vector<utils::IdNameCount>> faceting(const datatypes::User& user, const View& experiments_query)
      {
        auto projects_array = utils::build_normalized_array(user.projects());

        std::vector<std::pair<std::string, std::string> > collums = {
          {"epigenetic_marks", "$norm_epigenetic_mark"},
          {"genomes", "$norm_genome"},
          {"biosources", "$sample_info.norm_biosource_name"},
          {"samples", "$sample_id"},
          {"techniques", "$norm_technique"},
          {"projects", "$norm_project"},
          {"types", "$upload_info.content_format"}
        };

        std::unordered_map<std::string, std::vector<utils::IdNameCount>> faceting_result;

        std::vector<std::future<void> > threads;
        for (const auto& column : collums) {
          std::string collection = column.first;
          std::string key_name = column.second;

          auto t = std::async(std::launch::async,
                              &__collection_experiments_count,
                              std::ref(experiments_query), projects_array.view(),
                              collection, key_name, std::ref(faceting_result));
          threads.push_back(std::move(t));
        }

        size_t thread_count = threads.size();
        for (size_t i = 0; i < thread_count; ++i) {
          threads[i].wait();
        }

        return faceting_result;
      }
    }
  }
}
