//
//  key_mapper.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Fabian Reinartz on 01.07.13.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>

#include <boost/thread/mutex.hpp>

#include "helpers.hpp"
#include "key_mapper.hpp"
#include "collections.hpp"

#include "../log.hpp"

namespace epidb {
  namespace dba {

    const std::string &KeyMapper::BED_DATA()
    {
      static std::string BED_DATA("BED_DATA");
      return BED_DATA;
    }

    const std::string &KeyMapper::BED_DATASIZE()
    {
      static std::string BED_DATA("BED_DATASIZE");
      return BED_DATA;
    }

    const std::string &KeyMapper::DATASET()
    {
      static std::string DATASET("DATASET");
      return DATASET;
    }

    const std::string &KeyMapper::START()
    {
      static std::string START("START");
      return START;
    }

    const std::string &KeyMapper::END()
    {
      static std::string END("END");
      return END;
    }

    const std::string &KeyMapper::FEATURES()
    {
      static std::string FEATURES("FEATURES");
      return FEATURES;
    }

    const std::string &KeyMapper::VALUE()
    {
      static std::string VALUE("VALUE");
      return VALUE;
    }


    const std::string &KeyMapper::WIG_STEP()
    {
      static std::string WIG_STEP("STEP_WIG");
      return WIG_STEP;
    }

    const std::string &KeyMapper::WIG_SPAN()
    {
      static std::string WIG_SPAN("SPAN_WIG");
      return WIG_SPAN;
    }

    const std::string &KeyMapper::WIG_DATA_SIZE()
    {
      static std::string WIG_DATA_SIZE("DATA_SIZE_WIG");
      return WIG_DATA_SIZE;
    }

    const std::string &KeyMapper::WIG_COMPRESSED()
    {
      static std::string WIG_COMPRESSED("COMPRESSED_DATA");
      return WIG_COMPRESSED;
    }

    const std::string &KeyMapper::WIG_TRACK_TYPE()
    {
      static std::string WIG_TRACK_TYPE("TRACK_TYPE_WIG");
      return WIG_TRACK_TYPE;
    }

    const std::string &KeyMapper::WIG_DATA()
    {
      static std::string WIG_DATA("DATA_WIG");
      return WIG_DATA;
    }

    const std::string& KeyMapper::SEQNAME()
    {
      static std::string SEQNAME("SEQNAME");
      return SEQNAME;
    }

    const std::string& KeyMapper::SOURCE()
    {
      static std::string SOURCE("SOURCE");
      return SOURCE;
    }

    const std::string& KeyMapper::CHROMOSOME()
    {
      static std::string CHROMOSOME("CHROMOSOME");
      return CHROMOSOME;
    }

    const std::string& KeyMapper::FEATURE()
    {
      static std::string FEATURE("FEATURE");
      return FEATURE;
    }

    const std::string& KeyMapper::SCORE()
    {
      static std::string SCORE("SCORE");
      return SCORE;
    }

    const std::string& KeyMapper::STRAND()
    {
      static std::string STRAND("STRAND");
      return STRAND;
    }

    const std::string& KeyMapper::FRAME()
    {
      static std::string FRAME("FRAME");
      return FRAME;
    }

    const std::string& KeyMapper::ATTRIBUTES()
    {
      static std::string ATTRIBUTES("ATTRIBUTES");
      return ATTRIBUTES;
    }

    const std::string& KeyMapper::TRACKING_ID()
    {
      static std::string TRACKING_ID("TRACKING_ID");
      return TRACKING_ID;
    }

    const std::string& KeyMapper::GENE_SHORT_NAME()
    {
      static std::string GENE_SHORT_NAME("GENE_SHORT_NAME");
      return GENE_SHORT_NAME;
    }

    const std::string& KeyMapper::GENE_ID()
    {
      static std::string GENE_ID("GENE_ID");
      return GENE_ID;
    }

    const std::string& KeyMapper::FPKM()
    {
      static std::string FPKM("FPKM");
      return FPKM;
    }

    const std::string& KeyMapper::FPKM_LO()
    {
      static std::string FPKM_LO("FPKM_LO");
      return FPKM_LO;
    }

    const std::string& KeyMapper::FPKM_HI()
    {
      static std::string FPKM_HI("FPKM_HI");
      return FPKM_HI;
    }

    const std::string& KeyMapper::FPKM_STATUS()
    {
      static std::string FPKM_STATUS("FPKM_STATUS");
      return FPKM_STATUS;
    }

    const std::string& KeyMapper::NUM_READS()
    {
      static std::string NUM_READS("NUM_READS");
      return NUM_READS;
    }

    const std::string& KeyMapper::TRANSCRIPT_IDS()
    {
      static std::string TRANSCRIPT_IDS("TRANSCRIPT_IDS");
      return TRANSCRIPT_IDS;
    }

    const std::string& KeyMapper::LENGTH()
    {
      static std::string LENGTH("LENGTH");
      return LENGTH;
    }

    const std::string& KeyMapper::EFFECTIVE_LENGTH()
    {
      static std::string EFFECTIVE_LENGTH("EFFECTIVE_LENGTH");
      return EFFECTIVE_LENGTH;
    }

    const std::string& KeyMapper::EXPECTED_COUNT()
    {
      static std::string EXPECTED_COUNT("EXPECTED_COUNT");
      return EXPECTED_COUNT;
    }

    const std::string& KeyMapper::TPM()
    {
      static std::string TPM("TPM");
      return TPM;
    }

    const std::string& KeyMapper::POSTERIOR_MEAN_COUNT()
    {
      static std::string POSTERIOR_MEAN_COUNT("POSTERIOR_MEAN_COUNT");
      return POSTERIOR_MEAN_COUNT;
    }

    const std::string& KeyMapper::POSTERIOR_STANDARD_DEVIATION_OF_COUNT()
    {
      static std::string POSTERIOR_STANDARD_DEVIATION_OF_COUNT("POSTERIOR_STANDARD_DEVIATION_OF_COUNT");
      return POSTERIOR_STANDARD_DEVIATION_OF_COUNT;
    }

    const std::string& KeyMapper::PME_TPM()
    {
      static std::string PME_TPM("PME_TPM");
      return PME_TPM;
    }

    const std::string& KeyMapper::PME_FPKM()
    {
      static std::string PME_FPKM("PME_FPKM");
      return PME_FPKM;
    }

    const std::string& KeyMapper::TPM_CI_LOWER_BOUND()
    {
      static std::string TPM_CI_LOWER_BOUND("TPM_CI_LOWER_BOUND");
      return TPM_CI_LOWER_BOUND;
    }

    const std::string& KeyMapper::TPM_CI_UPPER_BOUND()
    {
      static std::string TPM_CI_UPPER_BOUND("TPM_CI_UPPER_BOUND");
      return TPM_CI_UPPER_BOUND;
    }

    const std::string& KeyMapper::FPKM_CI_LOWER_BOUND()
    {
      static std::string FPKM_CI_LOWER_BOUND("FPKM_CI_LOWER_BOUND");
      return FPKM_CI_LOWER_BOUND;
    }

    const std::string& KeyMapper::FPKM_CI_UPPER_BOUND()
    {
      static std::string FPKM_CI_UPPER_BOUND("FPKM_CI_UPPER_BOUND");
      return FPKM_CI_UPPER_BOUND;
    }

  } // namespace dba
} // namespace epidb