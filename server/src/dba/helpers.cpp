//
//  helpers.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.07.13.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <sstream>
#include <set>
#include <memory>

#include <boost/thread/mutex.hpp>

#include "../config/config.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/serialize.hpp"
#include "../extras/utils.hpp"

#include "collections.hpp"
#include "dba.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"

namespace epidb {
  namespace dba {
    namespace helpers {

      const std::string region_collection_name(const std::string &genome, const std::string &chromosome)
      {
        std::stringstream ss;
        ss << Collections::REGIONS() << "." << utils::normalize_name(genome) << "." << chromosome;
        return ss.str();
      }

      const std::vector<utils::IdName> get_id_names_by_query(const std::string &where, const View& query)
      {
        std::vector<std::string> v;
        v.push_back("_id");
        v.push_back("name");

        auto r = get_fields_by_query(where, query, v);
        return utils::documents_to_id_names(r);
      }

      const std::vector<utils::IdName> get_all_id_names(const std::string &where) {
        DocumentBuilder empty{};
        return get_id_names_by_query(where, empty.extract());
      }

      const std::vector<Document> get_all_by_query(const std::string &where, const View &query)
      {
        COLLECTION(collection, where)
        auto cursor = collection.find(query);

        return std::vector<Document>{cursor.begin(), cursor.end()};
      }

      // Get content where the field content match with the query object and the fields.
      // Return all elements if fields is empty.
      const std::vector<Document> get_fields_by_query(const std::string &where, const View &query, const std::vector<std::string> &fields)
      {
        DocumentBuilder b;
        for (const std::string &f : fields) {
          b.append(KVP(f, 1));
        }

        auto OPTS = mongocxx::options::find{};
        OPTS.projection(b.extract());

        COLLECTION(COLL, where);
        auto data_cursor = COLL.find(query, OPTS);

        return std::vector<Document>{data_cursor.begin(), data_cursor.end()};
      }


      MaybeDocument get_one_by_id(const std::string &where, const std::string &id)
      {
        auto query = bson_utils::id_doc(id);
        return get_one_by_query(where, query);
      }

      MaybeDocument get_one_by_query(const std::string &where, const View &query)
      {
        COLLECTION(collection, where)
        return collection.find_one(query);
      }

      const utils::IdName get_id_name(const std::string &where, const std::string &norm_name)
      {
        std::string field;
        if (where == "users") {
          field = "key";
        } else {
          field = "norm_name";
        }

        auto results = get_one_by_query(where, bson_utils::one_key_doc(field, norm_name));
        if (!results) {
          throw deepblue_user_exception(ERR_NAME_NOT_FOUND, norm_name, where);
        }

        return utils::view_to_id_name(*results);
      }

      const std::string get_id(const std::string &where, const std::string &norm_name)
      {
        auto id_name = get_id_name(where, norm_name);
        return id_name.id;
      }

      const bool check_exist(const std::string &where, const std::string &field, const std::string &content)
      {
        COLLECTION(coll, where);
        unsigned long long count = coll.count_documents(bson_utils::one_key_doc(field, content));

        return count > 0;
      }

      const bool check_exist(const std::string &where, const View& query)
      {
        COLLECTION(coll, where);
        int64_t count = coll.count_documents(query);

        return count > 0;
      }

      const int64_t count(const std::string &where, const View& query)
      {
        COLLECTION(coll, where);
        return coll.count_documents(query);
      }


      const uint32_t remove_one(const std::string &collection, const std::string &id, const std::string &field)
      {
        COLLECTION(COLL, collection)
        auto result = COLL.delete_one(bson_utils::one_key_doc(field, id));
        return result->deleted_count();
      }


      const uint32_t remove_all(const std::string &collection, const View &query)
      {
        COLLECTION(COLL, collection)
        auto result = COLL.delete_many(query);
        return result->deleted_count();
      }


      Array build_dataset_ids_arrays(const std::string &where, const View& query)
      {
        ArrayBuilder datasets_array_builder;

        COLLECTION(collection, where)

        auto cursor = collection.find(query);
        for (const auto& view: cursor) {
          auto value = view[KeyMapper::DATASET()].get_int32().value;
          datasets_array_builder.append(value);
        }

        return datasets_array_builder.extract();
      }


      void remove_collection(const std::string &where)
      {
        COLLECTION(coll, where)
        coll.drop();
      }


      const uint32_t get_incremente_counter_and_notify(const std::string &name) {
        auto id = get_increment_counter(name);
        notify_change_occurred(name);
        return id;
      }

      boost::mutex counter_mutex;
      const uint32_t get_increment_counter(const std::string &name)
      {
        boost::mutex::scoped_lock lock(counter_mutex);
        COLLECTION(counters_collection, Collections::COUNTERS());

        if (!check_exist(Collections::COUNTERS(), "_id", name)) {
          DocumentBuilder b;
          b.append(KVP("_id", name));
          b.append(KVP("seq", static_cast<int32_t>(0)));
          counters_collection.insert_one(b.extract());
        }

        auto query = bson_utils::id_doc(name);
        auto update = MakeDocument(KVP("$inc",MakeDocument(KVP("seq", 1))));
        auto OPTS = mongocxx::options::find_one_and_update();
        OPTS.return_document(mongocxx::options::return_document::k_after);

        auto result = counters_collection.find_one_and_update(query.view(), update.view(), OPTS);

        return result->view()["seq"].get_int32().value;
      }

      const uint32_t get_counter(const std::string &name)
      {
        if (!check_exist(Collections::COUNTERS(), "_id", name)) {
          return get_increment_counter(name);
        }

        COLLECTION(counters_collection, Collections::COUNTERS());
        auto maybe_counter = counters_collection.find_one(bson_utils::id_doc(name));

        if (!maybe_counter) {
          throw deepblue_user_exception(ERR_INVALID_COUNTER, name);
        }

        return maybe_counter->view()["seq"].get_int32().value;
      }

      const uint32_t notify_change_occurred(const std::string &name)
      {
        return get_increment_counter(name + "_operations");
      }
    }
  }
}
