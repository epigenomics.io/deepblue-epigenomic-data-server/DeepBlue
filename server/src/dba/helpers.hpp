//
//  helpers.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 02.07.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2020 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_HELPERS_HPP
#define EPIDB_DBA_HELPERS_HPP

#include <sstream>

#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"

namespace epidb {
  namespace dba {
    namespace helpers {

      typedef std::pair<std::string, std::string> QueryPair;

      const std::string region_collection_name(const std::string &genome, const std::string &chromosome);

      // Maybe get one document by ID
      MaybeDocument get_one_by_id(const std::string &where, const std::string &id);

      const std::vector<Document> get_by_query(const std::string &where, const View &query);

      // Maybe get one document by a query
      MaybeDocument get_one_by_query(const std::string &where, const View &query);

      // Get **all** content where the fields match the query parameter
      const std::vector<Document> get_all_by_query(const std::string &where, const View &query);

      const std::vector<utils::IdName> get_id_names_by_query(const std::string &where, const View& query);

      const std::vector<utils::IdName> get_all_id_names(const std::string &where);

      const std::vector<Document> get_fields_by_query(const std::string &where, const View &query, const std::vector<std::string> &fields);

      const utils::IdName get_id_name(const std::string &where, const std::string &norm_name);

      const std::string get_id(const std::string &where, const std::string &norm_name);

      const bool check_exist(const std::string &where, const std::string &field, const std::string &content);

      const bool check_exist(const std::string &where, const View& query);

      const int64_t count(const std::string &where, const View& query);

      const uint32_t remove_one(const std::string &collection, const std::string &id, const std::string &field = "_id");

      const uint32_t remove_all(const std::string &collection, const View &query);

      void remove_collection(const std::string &collection);

      Array build_dataset_ids_arrays(const std::string &where, const View& query);

      template<typename T>
      Document build_condition_array(const std::vector<T> &params, const std::string &condition)
      {
        ArrayBuilder ab;
        for (const auto& param :  params) {
          ab.append(param);
        }
        DocumentBuilder in_condition;
        in_condition.append(KVP(condition, ab.extract()));
        return in_condition.extract();
      }


      const uint32_t get_incremente_counter_and_notify(const std::string &name);

      /**
      * \brief  Increment counter variable from Database by one
      * \param  name  Name of counter-variable
      *         count Return: Updated counter value
      */
      const uint32_t get_increment_counter(const std::string &name);

      /**
      * \brief  Get counter variable from Database
      * \param  name  Name of counter-variable
      *         count Return: Counter value
      */
      const uint32_t get_counter(const std::string &name);

      /**
      * \brief  To be called if a change occurred in the stored data
      * \param  name  Type of data
      */
      const uint32_t notify_change_occurred(const std::string &name);
    }
  }
}

#endif