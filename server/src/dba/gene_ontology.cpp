//
//  gene_ontogy.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 21.02.2017
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>
#include <list>

#include "../datatypes/user.hpp"

#include "../dba/genes.hpp"

#include "../extras/bson_utils.hpp"

#include "../cache/connected_cache.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "collections.hpp"
#include "full_text.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"
#include "users.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"

namespace epidb {
  namespace dba {
    namespace gene_ontology {

      const std::string GO_NAMESPACE_CELLULAR_COMPONENT = "cellular_component";
      const std::string GO_NAMESPACE_BIOLOGICAL_PROCESS = "biological_process";
      const std::string GO_NAMESPACE_MOLECULAR_FUNCTION = "molecular_function";

      bool exists_gene_ontology_term(const std::string &go_id)
      {
        return helpers::check_exist(Collections::GENE_ONTOLOGY(), "go_id", go_id);
      }

      void validate_gene_ontology(const std::string &go_id,  const std::string &go_label,
                                  const std::string &go_namespace)
      {
        if (exists_gene_ontology_term(go_id)) {
          throw deepblue_user_exception(ERR_DUPLICATED_GENE_ONTOLOGY_TERM_ID, go_id);
        }

        if (helpers::check_exist(Collections::GENE_ONTOLOGY(), "go_label", go_label)) {
          throw deepblue_user_exception(ERR_DUPLICATED_GENE_ONTOLOGY_TERM_LABEL, go_label);
        }

        if ((go_namespace != GO_NAMESPACE_CELLULAR_COMPONENT) &&
            (go_namespace != GO_NAMESPACE_BIOLOGICAL_PROCESS) &&
            (go_namespace != GO_NAMESPACE_MOLECULAR_FUNCTION)) {

          throw deepblue_user_exception(ERR_INVALID_GENE_ONTOLOGY_NAMESPACE, go_namespace);
        }
      }


      std::string add_gene_ontology_term(const datatypes::User& user,
                                  const std::string &go_id,
                                  const std::string &go_label,
                                  const std::string &description, const std::string &norm_description,
                                  const std::string &go_namespace)
      {
        auto id = helpers::get_incremente_counter_and_notify(Collections::GENE_ONTOLOGY());
        std::string gene_ontology_term_id = "go" + utils::integer_to_string(id);

        // Building the data necessary for searching in full text
        DocumentBuilder search_data_builder;
        search_data_builder.append(KVP("_id", gene_ontology_term_id));
        search_data_builder.append(KVP("go_id", go_id));
        search_data_builder.append(KVP("go_label", go_label));
        search_data_builder.append(KVP("description", description));
        search_data_builder.append(KVP("norm_description", norm_description));
        search_data_builder.append(KVP("go_namespace", go_namespace));
        auto search_data = search_data_builder.extract();

        // Building the data of the GO Terms in the regular database
        DocumentBuilder create_gene_ontology_term_builder;
        create_gene_ontology_term_builder.append(Concatenate(search_data.view()));

        create_gene_ontology_term_builder.append(KVP("user", user.id()));

        COLLECTION(gene_ontology_collection, Collections::GENE_ONTOLOGY())
        gene_ontology_collection.insert_one(create_gene_ontology_term_builder.extract());

        search::insert_full_text(Collections::GENE_ONTOLOGY(), gene_ontology_term_id, search_data);

        return gene_ontology_term_id;
      }

      const int32_t annotate_gene(const std::string& gene_ensg_id, const std::string& go_id)
      {
        auto maybe_go_term = helpers::get_one_by_query(Collections::GENE_ONTOLOGY(), bson_utils::one_key_doc("go_id", go_id));
        if (!maybe_go_term) {
          throw deepblue_user_exception(ERR_INVALID_GENE_ONTOLOGY_TERM_ID, go_id);
        }

        auto go_term = maybe_go_term->view();
        DocumentBuilder gene_ontology_term_bob;
        gene_ontology_term_bob.append(KVP(go_term["go_id"].key(), go_term["go_id"].get_value()));
        gene_ontology_term_bob.append(KVP(go_term["go_label"].key(), go_term["go_label"].get_value()));
        gene_ontology_term_bob.append(KVP(go_term["go_namespace"].key(), go_term["go_namespace"].get_value()));
        auto change_value = MakeDocument(KVP("$addToSet", MakeDocument(KVP("go_annotation",  gene_ontology_term_bob.extract()))));

        DocumentBuilder gene_query_bob;
        gene_query_bob.append(KVP(KeyMapper::ATTRIBUTES() + ".gene_id", Regex{"^"+gene_ensg_id}));

        COLLECTION(genes_collection, Collections::GENES())
        auto result = genes_collection.update_many(gene_query_bob.extract(), change_value.view());

        return result->modified_count();
      }


      std::vector<Document> get_annotated_gene(const std::string& go_id)
      {
        auto query = bson_utils::one_key_doc("go_annotation.go_id", go_id);
        return helpers::get_all_by_query(Collections::GENES(), query);
      }

      ConnectedCache go_cache;

      bool __is_connected(const std::string &term_1, const std::string &term_2, const bool recursive)
      {
        if (go_cache.is_connected(term_1, term_2)) {
          return true;
        }

        auto query_obj = bson_utils::one_key_doc("go_id", term_1);
        COLLECTION(gene_ontology_collection, Collections::GENE_ONTOLOGY())
        auto term_bson = gene_ontology_collection.find_one(query_obj.view());

        if (!term_bson) {
          return false;
        }

        auto term_bson_view = term_bson->view();
        if (term_bson_view.find("subs") == term_bson_view.end()) {
          return false;
        }

        auto e = term_bson_view["subs"].get_array().value;
        std::list<std::string> subs;

        for (const auto& be : e) {
          std::string sub = StringViewToString(be.get_utf8().value);
          if (sub == term_2) {
            go_cache.set_connection(term_1, term_2);
            go_cache.set_connection(sub, term_1);
            return true;
          }
          subs.push_back(sub);
        }

        if (recursive) {
          for (const std::string & sub : subs) {
            if (__is_connected(sub, term_2, recursive)) {
              go_cache.set_connection(term_1, term_2);
              go_cache.set_connection(sub, term_2);
              return true;
            } else {
              return false;
            }
          }
        }

        return false;
      }

      void __get_down_connected(const std::string &go_id, std::vector<std::string> &go_terms_id)
      {
        go_terms_id.push_back(go_id);

        COLLECTION(gene_ontology_collection, Collections::GENE_ONTOLOGY())

        MaybeDocument term_bson = gene_ontology_collection.find_one(bson_utils::one_key_doc("go_id", go_id));

        if (!term_bson) {
          throw deepblue_user_exception(ERR_INVALID_GENE_ONTOLOGY_TERM_ID, go_id);
        }

        auto term_bson_view = term_bson->view();

        if (term_bson_view.find("subs") == term_bson_view.end()) {
          return;
        }

        auto e = term_bson_view["subs"].get_array().value;
        for (const auto & be : e) {
          std::string sub = StringViewToString(be.get_utf8().value);
          __get_down_connected(sub, go_terms_id);
        }
      }


      void __get_up_connected(const std::string &go_id, std::vector<std::string> &go_terms_id)
      {
        go_terms_id.push_back(go_id);

        COLLECTION(gene_ontology_collection, Collections::GENE_ONTOLOGY())
        MaybeDocument term_bson = gene_ontology_collection.find_one(bson_utils::one_key_doc("go_id", go_id));

        if (!term_bson) {
          throw deepblue_user_exception(ERR_INVALID_GENE_ONTOLOGY_TERM_ID, go_id);
        }

        auto term_bson_view = term_bson->view();


        if (term_bson_view.find("uppers") == term_bson_view.end()) {
          return;
        }

        auto e = term_bson_view["uppers"].get_array().value;
        for (const auto & be : e) {
          std::string sub = StringViewToString(be.get_utf8().value);
          __get_up_connected(sub, go_terms_id);
        }
      }

      bool __full_text_relation(const std::string &go_term_id, const std::string &smaller_scope)
      {
        // Get the sub terms
        std::vector<std::string> related_terms;

        COLLECTION(gene_ontology_collection, Collections::GENE_ONTOLOGY())
        MaybeDocument term_bson = gene_ontology_collection.find_one(bson_utils::one_key_doc("go_id", go_term_id));

        if (!term_bson) {
          throw deepblue_user_exception(ERR_INVALID_GENE_ONTOLOGY_TERM_ID, go_term_id);
        }

        auto go_term_view = term_bson->view();

        // Load the related terms
        related_terms.push_back(bson_utils::get_string(go_term_view, "go_label"));
        related_terms.push_back(bson_utils::get_string(go_term_view, "go_namespace"));

        auto search_terms = search::get_related_terms(go_term_id, go_term_id,
                                       "go_id", "gene_ontology");

        related_terms.insert(end(related_terms), begin(search_terms), end(search_terms));
        // Get the sub terms
        std::vector<std::string> go_terms_id;
        __get_down_connected(go_term_id, go_terms_id);

        // Include the related terms in sub terms
        for (auto &go_term_id : go_terms_id) {
          search::insert_related_gene_ontology_term(go_term_id, related_terms);
        }

        return true;
      }

      // A term will store ONLY the direct sub elements but
      // a term will store ALL the upper elements
      void set_go_parent(const std::string& bigger_scope, const std::string& smaller_scope)
      {
        if (__is_connected(bigger_scope, smaller_scope, false)) {
          throw deepblue_user_exception(ERR_ALREADY_CONECTED_GENE_ONTOLOGY_TERM, bigger_scope, smaller_scope);
        }

        if (__is_connected(smaller_scope, bigger_scope, true)) {
          throw deepblue_user_exception(ERR_ALREADY_CONECTED_GENE_ONTOLOGY_TERM, smaller_scope, bigger_scope);
        }

        // Include the sub element in the subs of the upper element
        auto query = bson_utils::one_key_doc("go_id", bigger_scope);
        auto append_value = MakeDocument(KVP("$addToSet", MakeDocument(KVP("subs", smaller_scope))));


        COLLECTION(gene_ontology_collection, Collections::GENE_ONTOLOGY())
        gene_ontology_collection.update_one(query.view(), append_value.view());

        // Get the upper elements from upper and include in the sub
        auto upper_query = bson_utils::one_key_doc("go_id", smaller_scope);
        auto upper_append_value = MakeDocument(KVP("$addToSet", MakeDocument(KVP("uppers", bigger_scope))));
        gene_ontology_collection.update_one(upper_query.view(), upper_append_value.view());

        // Set the relation in the full text data
        __full_text_relation(bigger_scope, smaller_scope);
        go_cache.set_connection(bigger_scope, smaller_scope);

        // Now, update the GENES annotated with the smaller scope.

        std::vector<Document> annotated_genes = get_annotated_gene(smaller_scope);

        if (!annotated_genes.empty()) {
          std::vector<std::string> up_terms_id;
          __get_up_connected(bigger_scope, up_terms_id);

          for (auto const& gene_obj: annotated_genes) {
            auto gene_view = gene_obj.view();
            for (auto const& up_go_id: up_terms_id) {
              auto attrs = gene_view[KeyMapper::ATTRIBUTES()].get_document().view();
              const std::string& gene_ensg_id = bson_utils::get_string(attrs, "gene_id");
              annotate_gene(gene_ensg_id, up_go_id);
            }
          }
        }
      }

      std::vector<utils::IdNameCount> count_go_terms_in_genes(const std::vector<std::string> &chromosomes,
                                  const Position start, const Position end,
                                  const std::string& strand,
                                  const std::vector<std::string>& genes, const std::vector<std::string>& go_terms,
                                  const std::string& gene_model, const std::string& norm_gene_model,
                                  size_t &total_go_terms)
      {
        total_go_terms = 0;

        auto query = genes::build_genes_database_query(chromosomes, start, end, strand,
                                               genes, go_terms,
                                               norm_gene_model, false);

        auto group = MakeDocument(
              KVP("_id", MakeDocument(
                KVP("go_id", "$go_annotation.go_id"),
                KVP("go_label", "$go_annotation.go_label"),
                KVP("total", MakeDocument(
                  KVP("$sum", 1 ) ) ))));
        auto sort = MakeDocument(KVP("$sort",  MakeDocument(KVP("total", 1))));

        mongocxx::pipeline p{};
        p.match(query.view());
        p.unwind("$go_annotation");
        p.group(group.view());
        p.sort(sort.view());

        COLLECTION(genes_collection, Collections::GENES());
        auto cursor = genes_collection.aggregate(p, mongocxx::options::aggregate{});

        std::vector<utils::IdNameCount> counts;
        for(const auto& be: cursor) {
          const auto& _id = be["_id"];

          long count = be["total"].get_int64().value;
          utils::IdNameCount inc;
          inc = utils::IdNameCount(
            StringViewToString(_id["go_id"].get_utf8().value),
            StringViewToString(_id["go_label"].get_utf8().value),
            count);
          counts.push_back(inc);
          total_go_terms += count;
        }

        return counts;
      }
    }
  }
}
