//
//  experiments.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 11.11.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_EXPERIMENTS_HPP
#define EPIDB_DBA_EXPERIMENTS_HPP

#include <string>
#include <experimental/optional>

#include "../datatypes/column_types_def.hpp"
#include "../datatypes/metadata.hpp"

#include "../extras/utils.hpp"

#include "../parser/parser_factory.hpp"

namespace epidb {
  namespace dba {
    namespace experiments {
      MaybeDocument by_name(const std::string &name);

      MaybeDocument by_id(const std::string &id);

      std::experimental::optional<std::string> get_genome(const std::string &norm_name);

      std::pair<std::string, std::string> get_experiment_name(const std::string &name_id);

      std::pair<std::vector<std::string>, std::vector<std::string>> get_experiments_names(const std::vector<std::string> &names_ids);

      Document build_metadata(const std::string &name, const std::string &norm_name,
                          const std::string &genome, const std::string &norm_genome,
                          const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                          const std::string &sample_id, const std::string &technique, const std::string &norm_technique,
                          const std::string &project, const std::string &norm_project,
                          const std::string &description, const std::string &norm_description,
                          const View &extra_metadata_obj,
                          const std::string &ip,
                          const parser::FileFormat &format,
                          int &dataset_id,
                          std::string &experiment_id);

      Document build_metadata_with_dataset(const std::string &name, const std::string &norm_name,
                                       const std::string &genome, const std::string &norm_genome,
                                       const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                                       const std::string &sample_id, const std::string &technique, const std::string &norm_technique,
                                       const std::string &project, const std::string &norm_project,
                                       const std::string &description, const std::string &norm_description,
                                       const View &extra_metadata_obj,
                                       const std::string &ip,
                                       const parser::FileFormat &format,
                                       const int dataset_id,
                                       std::string &experiment_id);

      // TODO: move others functions from dba.hpp to here

      std::string create_experiment_set(const std::vector<std::string> &experiment_names, const std::string& set_name,
                                 const std::string& description, const bool is_public);
    }
  }
}

#endif