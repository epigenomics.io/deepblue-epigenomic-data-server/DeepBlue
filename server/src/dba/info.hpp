//
//  info.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 04.04.14.
//  Copyright (c) 2021 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_INFO_HPP
#define EPIDB_DBA_INFO_HPP

#include "../datatypes/metadata.hpp"
#include "../datatypes/user.hpp"

#include <string>
#include <map>

namespace epidb {
  namespace dba {
    namespace info {

      bool get_genome(const std::string &id, datatypes::Metadata &res, MaybeArray& chromosomes, bool full = false);

      bool get_project(const datatypes::User& user, const std::string &id,
                       datatypes::Metadata &res, bool full = false);

      bool get_biosource(const std::string &id,
                         datatypes::Metadata &metadata,
                         datatypes::Metadata &extra_metadata,
                         bool full = false);

      bool get_technique(const std::string &id, datatypes::Metadata &res,
                         datatypes::Metadata &metadata, bool full = false);

      const datatypes::Metadata get_sample_by_id(const std::string &id, bool full = false);

      bool get_epigenetic_mark(const std::string &id, datatypes::Metadata &metadata,
                               datatypes::Metadata &extra_metadata,
                               bool full = false);

      bool get_annotation(const std::string &id,
                          datatypes::Metadata &metadata,
                          datatypes::Metadata &extra_metadata,
                          std::vector<datatypes::Metadata > &columns,
                          datatypes::Metadata &upload_info,
                          bool full = false);

      bool get_experiment(const std::string &id, const std::vector<std::string>& user_projects,
                          datatypes::Metadata &metadata,
                          datatypes::Metadata &extra_metadata,
                          datatypes::Metadata &sample_info,
                          std::vector<datatypes::Metadata > &columns,
                          datatypes::Metadata &upload_info,
                          bool full = false);

      const Document get_experiment_set_info(const std::string& id);

      const Document get_query(const std::string &id);

      datatypes::Metadata get_tiling_region(const std::string &id, bool full = false);

      const Document get_column_type(const std::string &id);

      /*
      * \brief  Convert a map with an "name"-field containing a user-ID to one
      *         containing a user-name in that field
      * \param  map Map to be modified
      */
      void id_to_name(datatypes::Metadata &map);
    }
  }
}

#endif
