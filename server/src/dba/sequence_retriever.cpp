//
//  sequence_retriever.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.06.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <limits>
#include <string>

#include "collections.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"

#include "../errors.hpp"

#include "../config/config.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/database.hpp"

#include "../log.hpp"
#include "../exceptions.hpp"

#include "sequence_retriever.hpp"


namespace epidb {
  namespace dba {
    namespace retrieve {

      SequenceRetriever& SequenceRetriever::singleton()
      {
        static SequenceRetriever DEFAULT_RETRIEVER;
        return DEFAULT_RETRIEVER;
      }

      bool SequenceRetriever::exists(const std::string &genome, const std::string &chromosome)
      {
        std::string norm_genome = utils::normalize_name(genome);
        std::string filename = norm_genome + "." + chromosome;

        GET_BUCKET(BUCKET, SEQUENCES, 2 << 12)
        auto c = BUCKET.find(bson_utils::one_key_doc("filename", filename));
        if (c.begin() != c.end()) {
          return true;
        }

        return false;
      }

      const OID SequenceRetriever::get_file_id(const std::string &genome, const std::string &chromosome, const std::string &filename)
      {
        if (file_ids_.find(filename) != file_ids_.end()) {
          return file_ids_.find(filename)->second;
        }

        FindOptions opts{};
        opts.projection(MakeDocument(KVP("_id", 1)));
        GET_BUCKET(BUCKET, SEQUENCES, 2 << 12)

        auto c = BUCKET.find(bson_utils::one_key_doc("filename", filename), opts);
        if (c.begin() == c.end()) {
          throw deepblue_user_exception(ERR_CHROMOSOME_SEQUENCE_NOT_FOUND, chromosome, genome);
        }

        auto data = c.begin();
        auto oid = (*data)["_id"].get_oid();
        file_ids_[filename] = oid;
        return oid;
      }

      const std::string SequenceRetriever::get_sequence(const std::string &genome, const std::string &chromosome,
                                                        const size_t start, const size_t end)
      {
        std::string norm_genome = utils::normalize_name(genome);
        std::string filename = norm_genome + "." + chromosome;

        auto c = chromosomes_.find(filename);

        std::string full_seq;
        if (c != chromosomes_.end()) {
          full_seq = c->second;
        } else {
          OID oid = get_file_id(genome, chromosome, filename);

          GET_BUCKET(BUCKET, SEQUENCES, 2 << 12)
          std::ostringstream stream;
          BUCKET.download_to_stream(MakeBSONValue(oid), &stream);
          full_seq = stream.str();
          chromosomes_[filename] = full_seq;
        }

        auto sequence = full_seq.substr(start, end-start);

        return sequence;
      }

      const std::string SequenceRetriever::retrieve(const std::string &genome, const std::string &chromosome,
                                       const size_t start, const size_t end)
      {
        return get_sequence(genome, chromosome, start, end);
      }

      void SequenceRetriever::invalidade_cache()
      {
        file_ids_.clear();
        chromosomes_.clear();
      }

    }
  }
}
