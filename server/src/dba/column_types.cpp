//
//  column_types.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 03.02.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm>
#include <string>
#include <vector>
#include <memory>

#include "../datatypes/column_types_def.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../lua/sandbox.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "collections.hpp"
#include "dba.hpp"
#include "exists.hpp"
#include "full_text.hpp"
#include "helpers.hpp"
#include "users.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"

#include "column_types.hpp"

namespace epidb {
  namespace dba {
    namespace columns {

      template<>
      bool ColumnType<long long>::check(const std::string &verify) const
      {
        size_t ll;
        return utils::string_to_long(verify, ll);
      }

      template<>
      const std::string ColumnType<long long>::str() const
      {
        return AbstractColumnType::str() + " type: 'integer'";
      }

      template<>
      const Document ColumnType<long long>::document() const
      {
        Document super = AbstractColumnType::document();

        DocumentBuilder builder;
        builder.append(Concatenate(super.view()));
        builder.append(KVP("column_type", "integer"));

        return builder.extract();
      }

      template<>
      datatypes::COLUMN_TYPES ColumnType<long long>::type() const
      {
        return datatypes::COLUMN_INTEGER;
      }

      template <>
      bool ColumnType<Score>::check(const std::string &verify) const
      {
        Score d;
        return utils::string_to_score(verify, d);
      }

      template<>
      const std::string ColumnType<Score>::str() const
      {
        return AbstractColumnType::str() + " type: 'double'";
      }

      template<>
      datatypes::COLUMN_TYPES ColumnType<Score>::type() const
      {
        return datatypes::COLUMN_DOUBLE;
      }

      template<>
      const Document ColumnType<Score>::document() const
      {
        Document super = AbstractColumnType::document();

        DocumentBuilder builder;
        builder.append(Concatenate(super.view()));
        builder.append(KVP("column_type", "double"));

        return builder.extract();
      }

      template<>
      bool ColumnType<std::string>::check(const std::string &verify) const
      {
        return true;
      }

      template<>
      const std::string ColumnType<std::string>::str() const
      {
        return AbstractColumnType::str() + " type: 'string'";
      }

      template<>
      datatypes::COLUMN_TYPES ColumnType<std::string>::type() const
      {
        return datatypes::COLUMN_STRING;
      }

      template<>
      const Document ColumnType<std::string>::document() const
      {
        Document super = AbstractColumnType::document();

        DocumentBuilder builder;
        builder.append(Concatenate(super.view()));
        builder.append(KVP("column_type", "string"));

        return builder.extract();
      }

      template<>
      bool ColumnType<Range>::check(const std::string &verify) const
      {
        Score d;
        if (!utils::string_to_score(verify, d)) {
          return false;
        }
        return d >= _content.first && d <= _content.second;
      }

      template<>
      const std::string ColumnType<Range>::str() const
      {
        return AbstractColumnType::str() + " type: 'range' : "  +
               utils::score_to_string(_content.first) + "," +
               utils::score_to_string(_content.second);
      }

      template<>
      datatypes::COLUMN_TYPES ColumnType<Range>::type() const
      {
        return datatypes::COLUMN_RANGE;
      }

      template<>
      const Document ColumnType<Range>::document() const
      {
        Document super = AbstractColumnType::document();

        DocumentBuilder builder;
        builder.append(Concatenate(super.view()));
        builder.append(KVP("column_type", "range"));
        builder.append(KVP("minimum", _content.first));
        builder.append(KVP("maximum", _content.second));

        return builder.extract();
      }

      template<>
      bool ColumnType<Category>::check(const std::string &verify) const
      {
        return find(_content.begin(), _content.end(), verify) != _content.end();
      }

      template<>
      const std::string ColumnType<Category>::str() const
      {
        return AbstractColumnType::str() + " type: 'category' values: "  + utils::vector_to_string(_content) + "";
      }

      template<>
      datatypes::COLUMN_TYPES ColumnType<Category>::type() const
      {
        return datatypes::COLUMN_CATEGORY;
      }

      template<>
      const Document ColumnType<Category>::document() const
      {
        Document super = AbstractColumnType::document();

        DocumentBuilder builder;
        builder.append(Concatenate(super.view()));
        builder.append(KVP("column_type", "category"));
        Array arr = utils::build_array(_content);
        builder.append(KVP("items", arr));

        return builder.extract();
      }

      template<>
      std::string ColumnType<Code>::execute(const std::string &chromosome, const AbstractRegion *region, dba::Metafield &metafield)
      {
        lua::Sandbox::LuaPtr lua = _content.second;
        lua->set_current_context(chromosome, region, metafield);
        return lua->execute_row_code();
      }

      template<>
      bool ColumnType<Code>::check(const std::string &verify) const
      {
        return false; //Calculated column types cannot be used as input
      }

      template<>
      const std::string ColumnType<Code>::str() const
      {
        return AbstractColumnType::str() + " type: 'code' : "  + _content.first;
      }

      template<>
      const Document ColumnType<Code>::document() const
      {
        Document super = AbstractColumnType::document();

        DocumentBuilder builder;
        builder.append(Concatenate(super.view()));
        builder.append(KVP("column_type", "calculated"));
        builder.append(KVP("code", _content.first));

        return builder.extract();
      }

      template<>
      datatypes::COLUMN_TYPES ColumnType<Code>::type() const
      {
        return datatypes::COLUMN_CALCULATED;
      }

      /* -------- */

      bool column_type_simple(const std::string &name, datatypes::COLUMN_TYPES type, ColumnTypePtr &column_type)
      {
        switch (type) {
        case datatypes::COLUMN_STRING:
          column_type = std::make_shared<ColumnType<std::string > >(name, "", -1);
          return true;

        case datatypes::COLUMN_INTEGER:
          column_type = std::make_shared<ColumnType<size_t> >(name, 0, -1);
          return true;

        case datatypes::COLUMN_DOUBLE:
          column_type = std::make_shared<ColumnType<Score> >(name, 0.0, -1);
          return true;

        case datatypes::COLUMN_RANGE:
          throw deepblue_user_exception(ERR_USAGE_COLUMN_RANGE);

        case datatypes::COLUMN_CATEGORY:
          throw deepblue_user_exception(ERR_USAGE_COLUMN_CATEGORY);

        default:
          throw deepblue_user_exception(ERR_COLUMN_TYPE_MISSING, type);
        }

      }

      const ColumnTypePtr column_type_simple(const std::string &name, const std::string &type)
      {
        std::string type_l(utils::lower(type));
        if (type_l == "string") {
          return std::make_shared<ColumnType<std::string > >(name, "", -1);
        } else if (type_l == "integer") {
          return std::make_shared<ColumnType<long long> >(name, 0, -1);
        } else if (type_l == "double") {
          return std::make_shared<ColumnType<Score> >(name, 0.0, -1);
        } else {
          throw deepblue_user_exception(ERR_COLUMN_TYPE_NAME_MISSING, type);
        }
      }

      // TODO: Remove 1st parameter (name)
      void validate_column_type_name_valid(const std::string &name, const std::string &norm_name)
      {
        if (exists::column_type(norm_name)) {
          throw deepblue_user_exception(ERR_DUPLICATED_COLUMN_NAME, name);
        }
      }

      bool __check_value_type(const std::string &type)
      {
        if (type == "string" || type == "integer" || type == "double") {
          return true;
        }
        return false;
      }

      Document __create_column_base_document(const datatypes::User& user,
                                const std::string &name, const std::string &norm_name,
                                const std::string &description, const std::string &norm_description,
                                const std::string &type,
                                std::string &column_type_id)
      {
        int id = dba::helpers::get_incremente_counter_and_notify(Collections::COLUMN_TYPES());

        column_type_id = "ct" + utils::integer_to_string(id);

        DocumentBuilder create_column_type_builder;
        create_column_type_builder.append(KVP("_id", column_type_id));
        create_column_type_builder.append(KVP("name", name));
        create_column_type_builder.append(KVP("norm_name", norm_name));
        create_column_type_builder.append(KVP("description", description));
        create_column_type_builder.append(KVP("norm_description", norm_description));
        create_column_type_builder.append(KVP("column_type", type));

        create_column_type_builder.append(KVP("user", user.id()));

        return create_column_type_builder.extract();
      }

      const std::string create_column_type_simple(const datatypes::User& user,
                                     const std::string &name, const std::string &norm_name,
                                     const std::string &description, const std::string &norm_description,
                                     const std::string &type)
      {
        std::string norm_type = type;
        norm_type = utils::lower(norm_type);

        if (!__check_value_type(type)) {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE_SIMPLE, type);
        }

        std::string column_type_id;
        Document obj = __create_column_base_document(user, name, norm_name, description, norm_description,
                        norm_type, column_type_id);

        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        column_types_collection.insert_one(obj.view());

        search::insert_full_text(Collections::COLUMN_TYPES(), column_type_id, obj);

        return column_type_id;
      }


      const std::string create_column_type_calculated(const datatypes::User& user,
                                         const std::string &name, const std::string &norm_name,
                                         const std::string &description, const std::string &norm_description,
                                         const std::string &code)
      {
        lua::Sandbox::LuaPtr lua = lua::Sandbox::new_instance(nullptr);
        lua->store_row_code(code);

        std::string column_type_id;
        Document obj = __create_column_base_document(user, name, norm_name, description, norm_description,
                        "calculated", column_type_id);


        DocumentBuilder create_column_type_calculated_builder;
        create_column_type_calculated_builder.append(Concatenate(obj.view()));

        create_column_type_calculated_builder.append(KVP("code", code));

        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        column_types_collection.insert_one(create_column_type_calculated_builder.extract());

        search::insert_full_text(Collections::COLUMN_TYPES(), column_type_id, obj);

        return column_type_id;;
      }


      const std::string create_column_type_category(const datatypes::User& user,
                                       const std::string &name, const std::string &norm_name,
                                       const std::string &description, const std::string &norm_description,
                                       const std::vector<std::string> &items)
      {
        std::string column_type_id;
        Document obj = __create_column_base_document(user, name, norm_name, description, norm_description,
                        "category", column_type_id);

        DocumentBuilder create_column_type_category_builder;
        create_column_type_category_builder.append(Concatenate(obj.view()));

        Array arr = utils::build_array(items);
        create_column_type_category_builder.append(KVP("items", arr));

        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        column_types_collection.insert_one(create_column_type_category_builder.extract());

        search::insert_full_text(Collections::COLUMN_TYPES(), column_type_id, obj);

        return column_type_id;;
      }

      const std::string create_column_type_range(const datatypes::User& user,
                                    const std::string &name, const std::string &norm_name,
                                    const std::string &description, const std::string &norm_description,
                                    const Score minimum, const Score maximum)
      {
        std::string column_type_id;
        Document obj = __create_column_base_document(user, name, norm_name, description, norm_description, "range", column_type_id);

        DocumentBuilder create_column_type_range_builder;
        create_column_type_range_builder.append(Concatenate(obj.view()));

        create_column_type_range_builder.append(KVP("minimum", minimum));
        create_column_type_range_builder.append(KVP("maximum", maximum));

        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        column_types_collection.insert_one(create_column_type_range_builder.extract());

        search::insert_full_text(Collections::COLUMN_TYPES(), column_type_id, obj);

        return column_type_id;
      }

      const std::vector<utils::IdName> list_column_types()
      {
        std::vector<utils::IdName> content;

        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        auto data_cursor = column_types_collection.find({});

        for (const auto o: data_cursor) {
          ColumnTypePtr column_type = column_type_document_to_class(o);
          utils::IdName name(bson_utils::get_string(o, "_id"), column_type->str());
          content.push_back(name);
        }

        return content;
      }

      ColumnTypePtr column_type_document_to_class(const View &obj)
      {
        const std::string name = bson_utils::get_string(obj, "name");
        const std::string type = bson_utils::get_string(obj, "column_type");
        int pos = -1;
        const auto pos_elem = obj.find("pos");
        if (pos_elem != obj.end()) {
          pos = obj["pos"].get_int32().value;
        }

        if (type == "string") {
          return std::make_shared<ColumnType<std::string > >(name, "", pos);

        } else if (type == "integer") {
          return std::make_shared<ColumnType<long long > >(name, 0, pos);

        } else if (type == "double") {
          return std::make_shared<ColumnType<Score > >(name, 0.0, pos);

        } else if (type == "category") {
          Category category;
          const auto& e = obj["items"].get_array().value;
          for (const auto &be : e) {
            category.push_back(StringViewToString(be.get_utf8().value));
          }
          return std::make_shared<ColumnType<Category > >(name, category, pos);

        } else if (type == "range") {
          Score minimum = obj["minimum"].get_double().value;
          Score maximum = obj["maximum"].get_double().value;
          Range range(minimum, maximum);
          return std::make_shared<ColumnType<Range > >(name, range, pos);

        } else if (type == "calculated") {
          std::string code = bson_utils::get_string(obj, "code");
          lua::Sandbox::LuaPtr lua = lua::Sandbox::new_instance(nullptr);
          lua->store_row_code(code);
          std::pair<std::string, lua::Sandbox::LuaPtr> p(code, lua);
          return std::make_shared<ColumnType<Code > >(name, p, pos);

        }

        throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE, type);
      }

      bool exists_column_type(const std::string &name)
      {
        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        const std::string norm_name = utils::normalize_name(name);
        auto query = bson_utils::one_key_doc("norm_name", norm_name);
        return column_types_collection.count_documents(query.view()) > 0;
      }

      Document load_column_type_from_database(const std::string &name)
      {
        COLLECTION(column_types_collection, Collections::COLUMN_TYPES())
        const std::string norm_name = utils::normalize_name(name);
        auto object = column_types_collection.find_one(bson_utils::one_key_doc("norm_name", norm_name));
        if (!object) {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_NAME, name);
        }

        return *object;
      }


      ColumnTypePtr load_column_type(const std::string &name)
      {
        auto obj_column_type = load_column_type_from_database(name);
        return column_type_document_to_class(obj_column_type.view());
      }

      Document get_column_type(const std::string &id)
      {
        COLLECTION(coll, Collections::COLUMN_TYPES())

        auto o = coll.find_one(bson_utils::id_doc(id));
        if (!o) {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE_ID, id);
        }

        return *o;
      }

      std::map<std::string, std::string> get_column_type_as_map(const std::string &id)
      {
        COLLECTION(coll, Collections::COLUMN_TYPES())

        auto o = coll.find_one(bson_utils::id_doc(id));
        if (!o) {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE_ID, id);
        }

        auto column_view = o->view();
        auto column_type = column_type_document_to_class(column_view);

        std::map<std::string, std::string> res;

        res["_id"] = id;
        res["name"] = column_type->name();
        res["description"] = StringViewToString(column_view["description"].get_utf8().value);
        switch (column_type->type()) {
        case datatypes::COLUMN_STRING: {
          res["column_type"] = "string";
          break;
        }

        case datatypes::COLUMN_INTEGER: {
          res["column_type"] = "integer";
          break;
        }

        case datatypes::COLUMN_DOUBLE: {
          res["column_type"] = "double";
          break;
        }

        case datatypes::COLUMN_RANGE: {
          res["column_type"] = "range";
          ColumnType<Range> *column = static_cast<ColumnType<Range> *>(column_type.get());
          res["minimum"] = utils::integer_to_string(column->content().first);
          res["maximum"] = utils::integer_to_string(column->content().second);
          break;
        }


        case datatypes::COLUMN_CATEGORY: {
          res["column_type"] = "category";
          ColumnType<Category> *column = static_cast<ColumnType<Category> *>(column_type.get());
          res["items"] = utils::vector_to_string(column->content());
          break;
        }

        case datatypes::COLUMN_CALCULATED: {
          res["column_type"] = "calculated";
          ColumnType<Code> *column = static_cast<ColumnType<Code> *>(column_type.get());
          res["code"] = column->content().first;
          break;
        }

        case datatypes::COLUMN_ERR:
        case datatypes::__COLUMN_TYPES_NR_ITEMS__: {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_ID, id);
        }
        }

        return res;
      }

      datatypes::Metadata dataset_column_to_map(const View &o)
      {
        datatypes::Metadata res;
        for (const auto& e: o) {
          std::string content = utils::element_to_string(e);
          if (!content.empty()) {
            res[bson_utils::get_key(e)] = content;
          }
        }
        return res;
      }
    }
  }
}
