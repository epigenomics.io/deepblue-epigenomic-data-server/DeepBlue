//
//  info.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 04.04.14.
//  Copyright (c) 2021 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <map>
#include <string>
#include <vector>

#include "../datatypes/metadata.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "column_types.hpp"
#include "data.hpp"
#include "users.hpp"

namespace epidb {
  namespace dba {
    namespace info {

      const static std::string NORM_("norm_");

      const datatypes::Metadata get_sample_by_id(const std::string &id, bool full = false)
      {
        auto result = data::sample(id);
        if (!result)  {
          throw deepblue_user_exception(ERR_INVALID_SAMPLE_ID, id);
        }

        datatypes::Metadata res;

        for (const auto e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (full || (field_name.compare(0, 5, "norm_") != 0)) {
            res[bson_utils::get_key(e)] = utils::element_to_string(e);
          }
        }

        return res;
      }

      bool get_genome(const std::string &id, datatypes::Metadata &res, MaybeArray& chromosomes, bool full = false)
      {
        auto result = data::genome(id);
        if (!result) {
          return false;
        }

        for (const auto e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (field_name == std::string("chromosomes")) {
            chromosomes = Array(e.get_array().value);
          } else  if (full || (field_name.compare(0, 5 , "norm_") != 0)) {
            res[field_name] = utils::element_to_string(e);
          }
        }

        res["type"] = "genome";

        return true;
      }

      bool get_project(const datatypes::User& user, const std::string &id,
                       datatypes::Metadata &res, bool full = false)
      {
        auto result = data::project(id, user.projects());
        if (!result) {
          return false;
        }

        for (const auto e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (full || field_name.compare(0, 5, "norm_") != 0) {
            res[field_name] = utils::element_to_string(e);
          }
        }

        res["type"] = "project";

        return true;
      }

      bool get_technique(const std::string &id, datatypes::Metadata &res,
                         datatypes::Metadata &metadata, bool full = false)
      {
        auto result = data::technique(id);
        if (!result) {
          return false;
        }

        for (const auto e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (field_name == "extra_metadata") {
            auto metadata_doc  = e.get_document().value;
            for (const auto e_md: metadata_doc) {
              auto field_name = bson_utils::get_key(e_md);
              metadata[field_name] = utils::element_to_string(e_md);
            }
          } else if (full || (field_name.compare(0, 5, "norm_") != 0)) {
            res[field_name] = utils::element_to_string(e);
          }
        }

        res["type"] = "technique";

        return true;
      }

      bool get_biosource(const std::string &id, datatypes::Metadata &res,
                         datatypes::Metadata &metadata,
                         bool full = false)
      {
        auto result = data::biosource(id);
        if (!result) {
          return false;
        }

        for (const auto& e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (field_name == "extra_metadata") {
            auto e_md = e.get_document().value;
            for (const auto& metadata_element: e_md) {
              auto metadata_element_field = bson_utils::get_key(metadata_element);
              metadata[metadata_element_field] = utils::element_to_string(metadata_element);
            }
          } else if (full || (field_name.compare(0, 5, "norm_") != 0)) {
            res[field_name] = utils::element_to_string(e);
          }
        }

        res["type"] = "biosource";

        return true;
      }

      bool get_epigenetic_mark(const std::string &id, datatypes::Metadata &res,
                               datatypes::Metadata &metadata,
                               bool full = false)
      {
        auto result = data::epigenetic_mark(id);
        if (!result) {
          return false;
        }

        for (const auto& e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (field_name == "extra_metadata") {
            auto e_md = e.get_document().value;
            for (const auto& metadata_element: e_md) {
              auto metadata_element_field = bson_utils::get_key(metadata_element);
              metadata[metadata_element_field] = utils::element_to_string(metadata_element);
            }
          } else if (full || (field_name.compare(0, 5, "norm_") != 0)) {
            res[field_name] = utils::element_to_string(e);
          }
        }

        res["type"] = "epigenetic_mark";

        return true;
      }

      bool get_annotation(const std::string &id, datatypes::Metadata &metadata,
                          datatypes::Metadata &extra_metadata,
                          std::vector<datatypes::Metadata > &columns,
                          datatypes::Metadata &upload_info,
                          bool full = false)
      {
        auto result = data::annotation(id);
        if (!result) {
          return false;
        }

        for (const auto& e: result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (field_name == KeyMapper::DATASET()) {
            continue;
          }

          else if (field_name == "extra_metadata") {
            auto e_md = e.get_document().value;
            for (const auto& metadata_element: e_md) {
              auto extra_metadata_element_field = bson_utils::get_key(metadata_element);
              extra_metadata[extra_metadata_element_field] = utils::element_to_string(metadata_element);
            }
          }

          else if (field_name == "upload_info") {
            auto e_md = e.get_document().value;
            for (const auto& metadata_element: e_md) {
              auto metadata_element_field = bson_utils::get_key(metadata_element);
              if (metadata_element_field.compare(0, 5, NORM_) != 0) {
                upload_info[metadata_element_field] = utils::element_to_string(metadata_element);
              }
            }
          }


          else if (field_name == "columns") {
            auto e_md = e.get_array().value;
              for (const auto& column_doc: e_md) {
                columns.push_back(columns::dataset_column_to_map(column_doc.get_document().value));
            }
          }

          else if (full || (field_name.compare(0, 5, "norm_") != 0)) {
            metadata[field_name] = utils::element_to_string(e);
          }
        }

        metadata["type"] = "annotation";

        return true;
      }

      bool get_experiment(const std::string &id, const std::vector<std::string>& user_projects,
                          datatypes::Metadata &metadata,
                          datatypes::Metadata &extra_metadata,
                          datatypes::Metadata &sample_info,
                          std::vector<datatypes::Metadata > &columns,
                          datatypes::Metadata &upload_info,
                          bool full = false)
      {
        auto result = data::experiment(id, user_projects);
        if (!result) {
          return false;
        }

        for (const auto &e: result->view()) {
          auto key = bson_utils::get_key(e);
          if (key == KeyMapper::DATASET()) {
            continue;
          }
          if (key == "sample_info") {
            auto sample_doc = e.get_document().value;
            for (auto const& ee: sample_doc) {
              std::string field_name = bson_utils::get_key(ee);
              if (field_name.compare(0, 5, "norm_") != 0) {
                sample_info[field_name] = utils::element_to_string(ee);
              }
            }
          } else if (key == "extra_metadata") {
            auto extra_metadata_doc = e.get_document().value;
            for (auto const& ee: extra_metadata_doc) {
              std::string field_name = bson_utils::get_key(ee);

              if (field_name.compare(0, 5, "norm_") != 0 &&
                  field_name.compare(0, 2, "__") != 0) {
                extra_metadata[field_name] = utils::element_to_string(ee);
              }

            }
          } else if (key == "upload_info") {
            auto upload_info_doc = e.get_document().value;
            for (auto const& ee: upload_info_doc) {
              std::string field_name = bson_utils::get_key(ee);

              if (field_name.compare(0, 5, "norm_") != 0) {
                if (field_name == "content_format") {
                  std::string type = utils::element_to_string(ee);
                  metadata["data_type"] = type;
                } else {
                  upload_info[field_name] = utils::element_to_string(ee);
                }
              }
            }
          } else if (key == "columns") {
            auto e_md = e.get_array().value;
              for (const auto& column_doc: e_md) {
                columns.push_back(columns::dataset_column_to_map(column_doc.get_document().value));
            }

          } else {
            auto field_name = bson_utils::get_key(e);
            if (full || (
                  field_name.compare(0, 5, "norm_") != 0 &&
                  field_name.compare(0, 2, "__") != 0)) {
              metadata[field_name] = utils::element_to_string(e);
            }
          }
        }

        metadata["type"] = "experiment";

        return true;
      }

      const Document get_experiment_set_info(const std::string& id)
      {
        auto maybe_data_obj = data::experiment_set(id);
        if (!maybe_data_obj) {
          throw deepblue_user_exception(ERR_EXPERIMENT_SET_ID_NOT_FOUND, id);
        }

        auto data_obj = maybe_data_obj->view();
        DocumentBuilder bob;
        bob.append(KVP(data_obj["_id"].key(), data_obj["_id"].get_value()));
        bob.append(KVP(data_obj["name"].key(), data_obj["name"].get_value()));
        bob.append(KVP(data_obj["description"].key(), data_obj["description"].get_value()));
        bob.append(KVP(data_obj["public"].key(), data_obj["public"].get_value()));
        bob.append(KVP(data_obj["experiments"].key(), data_obj["experiments"].get_value()));

        return bob.extract();
      }

      const Document get_query(const std::string &id)
      {
        auto maybe_result = data::query(id);
        if (!maybe_result) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, id);
        }

        auto result = maybe_result->view();
        DocumentBuilder bob;
        bob.append(KVP(result["_id"].key(), result["_id"].get_value()));
        bob.append(KVP(result["type"].key(), result["type"].get_value()));

        const std::string user_id = bson_utils::get_string(result, "user");

        std::string user_name = dba::users::get_user_name_by_id(user_id);
        bob.append(KVP("user", user_name));

        DocumentBuilder arg_builder;
        auto doc = result["args"].get_document().value;
        for (const auto& e: doc) {
          auto field_name = bson_utils::get_key(e);
          if (field_name.compare(0, 5, "norm_") != 0) {
            arg_builder.append(KVP(e.key(), e.get_value()));
          }
        }

        bob.append(KVP("args", arg_builder.extract()));
        return bob.extract();
      }

      const datatypes::Metadata get_tiling_region(const std::string &id, bool full = false)
      {
        auto maybe_result = data::tiling_region(id);
        if (!maybe_result)  {
          throw deepblue_user_exception(ERR_INVALID_TILING_REGIONS_ID, id);
        }

        datatypes::Metadata metadata;
        for (const auto e: maybe_result->view()) {
          auto field_name = bson_utils::get_key(e);
          if (full || (field_name.compare(0, 5, "norm_") != 0)) {
            metadata[field_name] = utils::element_to_string(e);
          }
        }

        metadata["type"] = "tiling_region";
        return metadata;
      }

      const Document get_column_type(const std::string &id)
      {
        auto ct = columns::get_column_type(id);
        auto v = ct.view();

        DocumentBuilder bob;

        for (const auto& kv: v) {
          auto key = bson_utils::get_key(kv);
          if ((!key.compare(0, 2, "__")) ||
              (!key.compare(0, 5, "user"))) {
            continue;
          }
          bob.append(KVP(kv.key(), kv.get_value()));
        }

        bob.append(KVP("type", "column_type"));

        return bob.extract();
      }

      void id_to_name(datatypes::Metadata &map)
      {
        std::string user = map["user"];
        // XXX : Check because version <= 0.9.35 stores the user name, not the ID.
        if (utils::is_id(user, "u")) {
          auto user_name = dba::users::get_user_name_by_id(user);
          map["user"] = user_name;
        }
      }
    }
  }
}