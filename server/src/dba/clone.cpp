//
//  clone.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 07.10.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <chrono>
#include <iterator>
#include <string>
#include <vector>

#include "../parser/parser_factory.hpp"
#include "../extras/utils.hpp"

#include "../datatypes/column_types_def.hpp"
#include "../datatypes/metadata.hpp"

#include "../dba/column_types.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/database.hpp"

#include "annotations.hpp"
#include "collections.hpp"
#include "experiments.hpp"
#include "full_text.hpp"
#include "info.hpp"
#include "users.hpp"


#include "../exceptions.hpp"
#include "../log.hpp"

namespace epidb {
  namespace dba {

    void validate_imutable_columns(const std::string &original_name, const std::string &clone_name)
    {
      if ((original_name == "CHROMOSOME" || original_name == "START" || original_name == "END") && (original_name != clone_name)) {
        throw deepblue_user_exception(ERR_CLONE_IMUTABLE_COLUMNS, original_name);
      }
    }

    void validate_compatible_type(const columns::ColumnTypePtr &original, const columns::ColumnTypePtr clone)
    {
      if (!datatypes::column_type_is_compatible(original->type(), clone->type())) {
        throw deepblue_user_exception(ERR_CLONE_INCOMPATIBLE_COLUMN_TYPES, clone->name(), datatypes::column_type_to_name(clone->type()), original->name(), datatypes::column_type_to_name(original->type()));
      }
    }


    void validate_columns(const ArrayView &original_columns, const ArrayView &clone_columns)
    {
      size_t original_columns_size = std::distance(std::begin(original_columns), std::end(original_columns));
      size_t clone_columns_size = std::distance(std::begin(clone_columns), std::end(clone_columns));

      for (size_t pos = 0; pos < clone_columns_size; pos++) {
        const auto& clone_column_bson = clone_columns[pos].get_document();

        if (pos < original_columns_size) {
          const auto& original_column_bson = original_columns[pos].get_document();

          columns::ColumnTypePtr original_column = columns::column_type_document_to_class(original_column_bson);
          columns::ColumnTypePtr clone_column = columns::column_type_document_to_class(clone_column_bson);

          validate_imutable_columns(original_column->name(), clone_column->name());
          validate_compatible_type(original_column, clone_column);

        } else {
          columns::ColumnTypePtr extra_column = columns::column_type_document_to_class(clone_column_bson);

          if (extra_column->type() != datatypes::COLUMN_CALCULATED) {
            throw deepblue_user_exception(ERR_CLONE_NEW_COLUMN_CALCULATED, extra_column->name());
          }
        }
      }
    }

    const std::string clone_dataset(const datatypes::User& user,
                       const std::string &dataset_id,
                       const std::string &clone_name, const std::string &norm_clone_name,
                       const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                       const std::string &sample_id,
                       const std::string &technique, const std::string &norm_technique,
                       const std::string &project, const std::string &norm_project,
                       const std::string &description, const std::string &norm_description,
                       const std::string &format, const datatypes::Metadata &extra_metadata,
                       const std::string &ip)
    {
      const auto& query = bson_utils::one_key_doc("_id", dataset_id);

      std::string collection;
      if (dataset_id[0] == 'e') {
        collection = Collections::EXPERIMENTS();
      } else {
        collection = Collections::ANNOTATIONS();
      }

      COLLECTION(COLL, collection)
      auto original = COLL.find_one(query.view());

      if (!original) {
        throw deepblue_user_exception(ERR_EXPERIMENT_SET_ID_NOT_FOUND, dataset_id);
      }

      const auto& original_view = original->view();

      std::string clone_genome = bson_utils::get_string(original_view, "genome");
      std::string clone_norm_genome = bson_utils::get_string(original_view, "norm_genome");

      std::string clone_epigenetic_mark;
      std::string clone_norm_epigenetic_mark;
      if (epigenetic_mark.empty()) {
        clone_epigenetic_mark = bson_utils::get_string_safe(original_view, "epigenetic_mark");
        clone_norm_epigenetic_mark = bson_utils::get_string_safe(original_view, "norm_epigenetic_mark");
      } else {
        clone_epigenetic_mark = epigenetic_mark;
        clone_norm_epigenetic_mark = norm_epigenetic_mark;
      }

      std::string clone_sample_id;
      if (sample_id.empty()) {
        clone_sample_id = bson_utils::get_string_safe(original_view, "sample_id");
      } else {
        clone_sample_id = sample_id;
      }


      std::string clone_technique;
      std::string clone_norm_technique;
      if (technique.empty()) {
        clone_technique = bson_utils::get_string_safe(original_view, "technique");
        clone_norm_technique = bson_utils::get_string_safe(original_view, "norm_technique");
      } else {
        clone_technique = technique;
        clone_norm_technique = norm_technique;
      }


      std::string clone_project;
      std::string clone_norm_project;
      if (project.empty()) {
        clone_project = bson_utils::get_string_safe(original_view, "project");
        clone_norm_project = bson_utils::get_string_safe(original_view, "norm_project");
      } else {
        clone_project = project;
        clone_norm_project = norm_project;
      }

      // Check if the description should be replaced
      std::string clone_description;
      std::string clone_norm_description;
      if (description.empty()) {
        clone_description = bson_utils::get_string_safe(original_view, "description");
        clone_norm_description = bson_utils::get_string_safe(original_view, "norm_description");
      } else {
        clone_description = description;
        clone_norm_description = norm_description;
      }

      // Check if the format and columns should be replaced
      parser::FileFormat clone_format;

      std::string original_format = bson_utils::get_string(original_view, "format");
      parser::FileFormat original_file_format = parser::FileFormatBuilder::build(original_format);

      if (format.empty()) {
        clone_format = original_file_format;
      } else {

        // Check format
        clone_format = parser::FileFormatBuilder::build(format);

        if (clone_format.format() != original_file_format.format()) {

          if (clone_format.size() < original_file_format.size()) {
            throw deepblue_user_exception(ERR_CLONE_INVALID_COLUMNS_COUNT, original_file_format.format(), clone_format.format());
          }

          ArrayView original_columns_vector = original_view["columns"].get_array().value;
          Array clone_format_bson = clone_format.to_array();
          validate_columns(original_columns_vector, clone_format_bson.view());
        }
      }

      // Check if the extra_metadata should be replaced
      MaybeDocument extra_metadata_obj;
      if (!extra_metadata.empty()) {
        extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);
      } else {
        extra_metadata_obj = Document{original_view["extra_metadata"].get_document().value};
      }

      DatasetId internal_dataset_id = original_view[KeyMapper::DATASET()].get_int32().value;
      MaybeDocument cloned_metadata{};

      std::string _id;
      if (dataset_id[0] == 'a') {
        // TODO: Replace by a clone() function.
        cloned_metadata = annotations::build_metadata_with_dataset(clone_name, norm_clone_name, clone_genome, clone_norm_genome,
            clone_description, clone_norm_description, extra_metadata_obj->view(),
            ip, clone_format, internal_dataset_id, _id);

      } else {
        // TODO: Replace by a clone() function.
        cloned_metadata = experiments::build_metadata_with_dataset(clone_name, norm_clone_name,
            clone_genome, clone_norm_genome,
            clone_epigenetic_mark, clone_norm_epigenetic_mark,
            clone_sample_id, clone_technique, clone_norm_technique,
            clone_project, clone_norm_project,
            clone_description, clone_norm_description,
            extra_metadata_obj->view(), ip,
            clone_format, internal_dataset_id, _id);
      }

      search::insert_full_text(collection, _id, cloned_metadata->view());

      DocumentBuilder clone_final_builder;
      clone_final_builder.append(Concatenate(cloned_metadata->view()));


      View original_upload_info = original_view["upload_info"].get_document().value;

      DocumentBuilder upload_info_builder;
      for (const auto& e : original_upload_info) {
        const auto& field_name = bson_utils::get_key(e);
        if (field_name ==  "user") {
          upload_info_builder.append(KVP("user", user.id()));
        } else if (field_name == "upload_end") {
          upload_info_builder.append(KVP("upload_end", Date{std::chrono::system_clock::now()}));
        } else if (field_name == "client_address") {
          upload_info_builder.append(KVP("client_address", ip));
        } else {
          upload_info_builder.append(KVP(e.key(), e.get_value()));
        }
      }
      clone_final_builder.append(KVP("upload_info", upload_info_builder.extract()));
      Document clone = clone_final_builder.extract();

      COLL.insert_one(clone.view());

      return _id;
    }
  }
}
