//
//  annotations.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 16.12.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#include <string>

#include "annotations.hpp"
#include "collections.hpp"
#include "helpers.hpp"

#include "../datatypes/metadata.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"

namespace epidb {
  namespace dba {
    namespace annotations {

      MaybeDocument by_name(const std::string &name, const std::string &genome)
      {
        const std::string norm_name = utils::normalize_name(name);
        const std::string norm_genome = utils::normalize_name(genome);

        return helpers::get_one_by_query(Collections::ANNOTATIONS(),
                          MakeDocument(KVP("norm_name", norm_name), KVP("norm_genome", norm_genome)));
      }


      Document build_metadata(const std::string &name, const std::string &norm_name,
                          const std::string &genome, const std::string &norm_genome,
                          const std::string &description, const std::string &norm_description,
                          const View& extra_metadata_obj,
                          const std::string &ip,
                          const parser::FileFormat &format,
                          int &dataset_id,
                          std::string &annotation_id)
      {
        dataset_id = helpers::get_incremente_counter_and_notify("datasets");
        return build_metadata_with_dataset(name, norm_name, genome, norm_genome,
                                         description, norm_description,
                                         extra_metadata_obj,
                                         ip,
                                         format,
                                         dataset_id,
                                         annotation_id);
      }


      Document build_metadata_with_dataset(const std::string &name, const std::string &norm_name,
                                       const std::string &genome, const std::string &norm_genome,
                                       const std::string &description, const std::string &norm_description,
                                       const View extra_metadata_obj,
                                       const std::string &ip,
                                       const parser::FileFormat &format,
                                       const int dataset_id,
                                       std::string &annotation_id)
      {
        int a_id = helpers::get_incremente_counter_and_notify("annotations");
        annotation_id = "a" + utils::integer_to_string(a_id);

        DocumentBuilder annotation_data_builder{};
        annotation_data_builder.append(KVP("_id", annotation_id));
        annotation_data_builder.append(KVP(KeyMapper::DATASET(), dataset_id));
        annotation_data_builder.append(KVP("name", name));
        annotation_data_builder.append(KVP("norm_name", norm_name));
        annotation_data_builder.append(KVP("genome", genome));
        annotation_data_builder.append(KVP("norm_genome", norm_genome));
        annotation_data_builder.append(KVP("description", description));
        annotation_data_builder.append(KVP("norm_description", norm_description));
        annotation_data_builder.append(KVP("format", format.format()));
        annotation_data_builder.append(KVP("columns", format.to_array()));
        annotation_data_builder.append(KVP("extra_metadata", extra_metadata_obj));

        return annotation_data_builder.extract();
      }
    }
  }
}
