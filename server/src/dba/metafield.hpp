//
//  metafield.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 11.03.2014
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_METAFIELD_HPP
#define EPIDB_DBA_METAFIELD_HPP

#include <map>

#include "../datatypes/regions.hpp"

#include "../dba/retrieve.hpp"

#include "../typedefs/bson.hpp"

namespace epidb {
  namespace dba {
    class Metafield {

    private:
      typedef const std::string (Metafield::*Function)(const std::string &, const std::string &, const View&, const AbstractRegion *, processing::StatusPtr);

      static std::map<std::string, Function> functions;
      static std::map<std::string, std::string> functionsReturns;

      std::map<DatasetId, Document> obj_by_dataset_id;

      static const std::map<std::string, Function> createFunctionsMap();

      static const std::map<std::string, std::string> createFunctionsReturnsMap();

      const std::string length(const std::string &, const std::string &, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string name(const std::string &, const std::string &, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string strand(const std::string &, const std::string &, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string sequence(const std::string &, const std::string &, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string count_motif(const std::string &, const std::string &, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string epigenetic_mark(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string calculated(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string project(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string biosource(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string genome(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string sample_id(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string min(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string max(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string sum(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string median(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string mean(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string var(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string sd(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string count(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string gene_attribute(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string gene_id(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string gene_name(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string gene_expression(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::vector<datatypes::GeneOntologyTermPtr> get_gene_ontology_terms(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string go_ids(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

      const std::string go_labels(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status);

    public:
      static bool is_meta(const std::string &s);
      static std::string command_type(const std::string &command);

      const std::string process(const std::string &, const std::string &, const AbstractRegion *region, processing::StatusPtr status);
    };
  }
}

#endif
