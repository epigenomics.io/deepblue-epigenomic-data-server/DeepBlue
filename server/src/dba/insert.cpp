//
//  insert.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 30.04.14.
//  Copyright (c) 2021 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <limits>
#include <map>
#include <string>
#include <sstream>
#include <vector>

#include <math.h>

#include "../datatypes/column_types_def.hpp"
#include "../datatypes/regions.hpp"

#include "../extras/utils.hpp"
#include "../extras/bson_utils.hpp"

#include "../parser/genome_data.hpp"
#include "../parser/parser_factory.hpp"
#include "../parser/wig.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "annotations.hpp"
#include "dba.hpp"
#include "collections.hpp"
#include "experiments.hpp"
#include "full_text.hpp"
#include "genomes.hpp"
#include "info.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"
#include "remove.hpp"
#include "users.hpp"

#include "../errors.hpp"
#include "../log.hpp"


namespace epidb {
  namespace dba {

    const size_t BLOCK_SIZE = 100;
    const size_t BULK_SIZE = 10000;
    const size_t MAXIMUM_SIZE = 10000000; // from mongodb maximum message size: 48000000

    static const void fill_region_builder(DocumentBuilder &builder,
                                          const parser::BedLine &bed_line, const parser::FileFormat &file_format)
    {
      if (bed_line.size() != file_format.size()) {
        throw deepblue_user_exception(ERR_TOKENS_COUNT, bed_line.size(), file_format.size());
      }

      builder.append(KVP(KeyMapper::START(), bed_line.start));
      builder.append(KVP(KeyMapper::END(), bed_line.end));

      size_t i(0);
      for(const auto & column_type: file_format) {
        std::string field_name = column_type->name();

        if ((field_name == "CHROMOSOME") || (field_name == "START") || (field_name == "END")) {
          continue;
        }

        std::string token = bed_line.tokens[i++];

        if (!column_type->check(token)) {
          throw deepblue_user_exception(ERR_INVALID_VALUE_FOR_COLUMN, token, field_name);
        }

        if (column_type->type() == datatypes::COLUMN_STRING) {
          builder.append(KVP(field_name, token));

        } else if (column_type->type() == datatypes::COLUMN_INTEGER) {
          size_t l;
          if (!utils::string_to_long(token, l)) {
            throw deepblue_user_exception(ERR_FIELD_INTEGER_VALUE_INVALID, field_name, token);
          }
          builder.append(KVP(field_name, static_cast<int64_t>(l)));
        } else if (column_type->type() == datatypes::COLUMN_DOUBLE) {
          Score s;
          if (!utils::string_to_score(token, s)) {
            throw deepblue_user_exception(ERR_FIELD_DOUBLE_VALUE_INVALID, field_name, token);
          }
          builder.append(KVP(field_name, s));
        } else if (column_type->type() == datatypes::COLUMN_CATEGORY) {
          builder.append(KVP(field_name, token));
        } else if (column_type->type() == datatypes::COLUMN_RANGE) {
          Score s;
          if (!utils::string_to_score(token, s)) {
            throw deepblue_user_exception(ERR_FIELD_DOUBLE_VALUE_INVALID, field_name, token);
          }
          builder.append(KVP(field_name, s));
        } else {
          std::string err = Error::m(ERR_COLUMN_TYPE_NAME_MISSING, column_type->str());
          throw_runtime_error(err);
        }
      }
    }


    // compress a block (vector of regions) and insert into blocks_bulk
    inline void compress_block(const int dataset_id, size_t &count,
                        const std::vector<Document> &block,
                        std::vector<Document> &blocks_bulk, size_t &bulk_size, size_t &total_size)
    {
      ArrayBuilder ab;
      size_t features = 0;
      size_t uncompress_size = 0;
      int min = std::numeric_limits<int32_t>::max();
      int max = std::numeric_limits<int32_t>::min();

      for (const auto& obj: block) {
        auto obj_view = obj.view();
        uncompress_size += obj_view.length();
        auto start = obj_view[KeyMapper::START()].get_int32().value;
        auto end = obj_view[KeyMapper::END()].get_int32().value;

        if (start < min) {
          min = start;
        }
        if (end > max) {
          max = end;
        }
        features++;
        ab.append(obj);
      }

      int64_t id = static_cast<int64_t>(dataset_id) << 32 | static_cast<int64_t>(count++) ;

      DocumentBuilder block_builder;
      block_builder.append(KVP("_id", static_cast<int64_t>(id)));
      block_builder.append(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));
      block_builder.append(KVP(KeyMapper::START(), static_cast<int32_t>(min)));
      block_builder.append(KVP(KeyMapper::END(), static_cast<int32_t>(max)));
      block_builder.append(KVP(KeyMapper::FEATURES(), (static_cast<int32_t>(features))));
      block_builder.append(KVP(KeyMapper::BED_DATA(), ab.extract()));

      auto block_obj = block_builder.extract();
      blocks_bulk.push_back(block_obj);
      int block_size = block_obj.view().length();
      total_size += block_size;
      bulk_size += block_size;
    }

    inline void compress_and_insert_region_block(const int dataset_id, const std::string &collection,
        size_t &count,
        std::vector<Document> &block,
        std::vector<Document> &blocks_bulk,
        size_t &bulk_size, size_t &total_size,
        size_t min_block_size = 1, size_t min_bulk_size = 1)
    {
      if (collection.empty()) {
        return;
      }
      if (block.size() >= min_block_size) {
        compress_block(dataset_id, count, block, blocks_bulk, bulk_size, total_size);
        block.clear();
      }

      if (blocks_bulk.size() >= min_bulk_size || bulk_size >= MAXIMUM_SIZE) {
        COLLECTION(COLL, collection)
        COLL.insert_many(blocks_bulk);
        bulk_size = 0;
        blocks_bulk.clear();
      }
    }


    Document build_upload_info(const datatypes::User& user,
                           const std::string &client_address, const std::string &content_format)
    {
      DocumentBuilder upload_info_builder;

      upload_info_builder.append(KVP("user", user.id()));
      upload_info_builder.append(KVP("content_format", content_format));
      upload_info_builder.append(KVP("done", false));
      upload_info_builder.append(KVP("client_address", client_address));
      upload_info_builder.append(KVP("upload_start", Date{std::chrono::system_clock::now()}));

      return upload_info_builder.extract();
    }

    void update_upload_info(const std::string &collection, const std::string &annotation_id, const size_t total_size, const ClientSession& session)
    {
      COLLECTION_FROM_SESSION(COLL, collection, session)

      auto query = bson_utils::id_doc(annotation_id);
      auto update = MakeDocument(KVP("$set",
          MakeDocument(
              KVP("upload_info.total_size", static_cast<int64_t>(total_size)),
              KVP("upload_info.done", true),
              KVP("upload_info.upload_end", Date{std::chrono::system_clock::now()}))));

      COLL.update_one(session, query.view(), update.view());
    }


    const std::string insert_experiment(const datatypes::User& user,
                           const std::string &name, const std::string &norm_name,
                           const std::string &genome, const std::string &norm_genome,
                           const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                           const std::string &sample_id, const std::string &technique, const std::string &norm_technique,
                           const std::string &project, const std::string &norm_project,
                           const std::string &description, const std::string &norm_description,
                           const datatypes::Metadata &extra_metadata,
                           const std::string &ip,
                           const parser::WigPtr &wig)
    {
      Document extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);

      bool ignore_unknow_chromosomes = extra_metadata.find("__ignore_unknow_chromosomes__") != extra_metadata.end();
      std::string experiment_id;
      int dataset_id;
      auto experiment_metadata = experiments::build_metadata(name, norm_name, genome, norm_genome,
            epigenetic_mark, norm_epigenetic_mark, sample_id, technique, norm_technique,
            project, norm_project, description, norm_description, extra_metadata_obj,
            ip, parser::FileFormat::wig_format(), dataset_id,  experiment_id);

      auto upload_info = build_upload_info(user, ip, "signal");

      DocumentBuilder experiment_builder;
      experiment_builder.append(Concatenate(experiment_metadata.view()));
      experiment_builder.append(KVP("upload_info", upload_info));

      SESSION(session)
      session.start_transaction();

      COLLECTION_FROM_SESSION(experiments_collection, Collections::EXPERIMENTS(), session)
      experiments_collection.insert_one(session, experiment_builder.extract());

      search::insert_full_text(Collections::EXPERIMENTS(), experiment_id, experiment_metadata, session);

      genomes::GenomeInfoPtr genome_info = genomes::get_genome_info(genome);

      size_t count = 0;
      std::vector<Document> bulk;
      std::string prev_collection;
      size_t actual_size = 0;
      size_t total_size = 0;
      size_t total_regions = 0;

      for (const auto& track: *wig) {
        std::string internal_chromosome = genome_info->internal_chromosome(track->chromosome());
        if (internal_chromosome.empty()) {
          if (ignore_unknow_chromosomes) {
            continue;
          }
          remove::dataset(dataset_id, norm_genome);
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME_GENOME, track->chromosome(), genome);
        }

        DocumentBuilder region_builder;
        region_builder.append(KVP("_id", static_cast<int64_t>(dataset_id) << 32 | static_cast<int64_t>(count++)));
        region_builder.append(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));
        region_builder.append(KVP(KeyMapper::WIG_TRACK_TYPE(), static_cast<int32_t>(track->type())));
        region_builder.append(KVP(KeyMapper::START(), static_cast<int32_t>(track->start())));
        region_builder.append(KVP(KeyMapper::END(), static_cast<int32_t>(track->end())));
        if (track->step()) {
          region_builder.append(KVP(KeyMapper::WIG_STEP(), static_cast<int32_t>(track->step())));
        }
        if (track->span()) {
          region_builder.append(KVP(KeyMapper::WIG_SPAN(), static_cast<int32_t>(track->span())));
        }
        region_builder.append(KVP(KeyMapper::FEATURES(), static_cast<int32_t>(track->features())));
        region_builder.append(KVP(KeyMapper::WIG_DATA_SIZE(), static_cast<int32_t>(track->data_size())));

        auto data_size = track->data_size();

        region_builder.append(KVP(KeyMapper::WIG_DATA(),
          Binary{bsoncxx::binary_sub_type::k_binary, data_size, track->data().get()}));

        actual_size += data_size;
        total_regions +=  track->features();

        // TODO: check regions and positions regards the chromosome size!
        auto size = genome_info->chromosome_size(internal_chromosome);

        auto r = region_builder.extract();
        std::string collection = helpers::region_collection_name(genome, internal_chromosome);

        if (prev_collection != collection) {
          if (!prev_collection.empty() && bulk.size() > 0) {
            COLLECTION(PREV, prev_collection)
            PREV.insert_many(bulk);
            bulk.clear();
          }
          prev_collection = collection;
          actual_size = 0;
        }

        total_size += r.view().length();
        bulk.push_back(r);

        if (bulk.size() % BULK_SIZE == 0 || actual_size > MAXIMUM_SIZE) {
          COLLECTION(COLL, collection)
          COLL.insert_many(bulk);
          bulk.clear();
          actual_size = 0;
        }
      }

      if (bulk.size() > 0) {
        COLLECTION(PREV, prev_collection)
        PREV.insert_many(bulk);
        bulk.clear();
      }

      update_upload_info(Collections::EXPERIMENTS(), experiment_id, total_size, session);

      session.commit_transaction();

      return experiment_id;
    }

    const std::string insert_experiment(const datatypes::User& user,
                           const std::string &name, const std::string &norm_name,
                           const std::string &genome, const std::string &norm_genome,
                           const std::string &epigenetic_mark, const std::string &norm_epigenetic_mark,
                           const std::string &sample_id, const std::string &technique, const std::string &norm_technique,
                           const std::string &project, const std::string &norm_project,
                           const std::string &description, const std::string &norm_description,
                           const datatypes::Metadata &extra_metadata,
                           const std::string &ip,
                           const parser::ChromosomeRegionsMap &map_regions,
                           const parser::FileFormat &format)
    {
      auto extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);
      std::string experiment_id;
      int dataset_id;
      auto experiment_metadata = experiments::build_metadata(name, norm_name, genome, norm_genome,
                                       epigenetic_mark, norm_epigenetic_mark,
                                       sample_id, technique, norm_technique, project, norm_project,
                                       description, norm_description, extra_metadata_obj,
                                       ip, format,
                                       dataset_id,  experiment_id);

      bool ignore_unknow_chromosomes = extra_metadata.find("__ignore_unknow_chromosomes__") != extra_metadata.end();
      bool trim_to_chromosome_size = extra_metadata.find("__trim_to_chromosome_size__") != extra_metadata.end();

      auto upload_info = build_upload_info(user, ip, "peaks");

      DocumentBuilder experiment_builder;
      experiment_builder.append(Concatenate(experiment_metadata.view()));
      experiment_builder.append(KVP("upload_info", upload_info));

      SESSION(session)
      session.start_transaction();

      COLLECTION_FROM_SESSION(experiments_collection, Collections::EXPERIMENTS(), session)
      experiments_collection.insert_one(session, experiment_builder.extract());

      search::insert_full_text(Collections::EXPERIMENTS(), experiment_id, experiment_metadata, session);

      genomes::GenomeInfoPtr genome_info = genomes::get_genome_info(genome);

      size_t count = 0;
      size_t total_size = 0;

      for(parser::ChromosomeBedLines chrom_lines: map_regions) {
        std::string internal_chromosome = genome_info->internal_chromosome(chrom_lines.first);
        if (internal_chromosome.empty()) {
          if (ignore_unknow_chromosomes) {
            continue;
          }
          remove::dataset(dataset_id, norm_genome);
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME_GENOME, chrom_lines.first, genome);
        }

        size_t size = genome_info->chromosome_size(internal_chromosome);
        std::string collection = helpers::region_collection_name(genome, internal_chromosome);

        size_t bulk_size = 0;
        std::vector<Document> block;
        std::vector<Document> blocks_bulk;

        for(parser::BedLine & bed_line: chrom_lines.second) {
          if (trim_to_chromosome_size && bed_line.end > size) {
            bed_line.end = size;
          }

          if (bed_line.start > size || bed_line.end > size) {
            remove::dataset(dataset_id, norm_genome);
            throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_POSITION, bed_line.start, bed_line.end, bed_line.chromosome);
          }

          DocumentBuilder region_builder;
          try {
            fill_region_builder(region_builder, bed_line, format);
          } catch (const deepblue_user_exception& dex) {
            remove::dataset(dataset_id, norm_genome);
            throw dex;
          }

          block.emplace_back(region_builder.extract());
          std::string collection = helpers::region_collection_name(genome, internal_chromosome);
          compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size, BLOCK_SIZE, BULK_SIZE);
        }

        compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size);
      }

      update_upload_info(Collections::EXPERIMENTS(), experiment_id, total_size, session);

      session.commit_transaction();
      return experiment_id;
    }

    const std::string insert_annotation(const datatypes::User& user,
                           const std::string &name, const std::string &norm_name,
                           const std::string &genome, const std::string &norm_genome,
                           const std::string &description, const std::string &norm_description,
                           const datatypes::Metadata &extra_metadata,
                           const std::string &ip,
                           const parser::ChromosomeRegionsMap &map_regions,
                           const parser::FileFormat &format)
    {
      int dataset_id;
      std::string annotation_id;
      auto extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);
      auto annotation_metadata = annotations::build_metadata(name, norm_name, genome, norm_genome,
                                       description, norm_description, extra_metadata_obj,
                                       ip, format, dataset_id, annotation_id);

      auto upload_info = build_upload_info(user, ip, "peaks");

      DocumentBuilder annotation_data_builder;
      annotation_data_builder.append(Concatenate(annotation_metadata.view()));
      annotation_data_builder.append(KVP("upload_info", upload_info));

      SESSION(session)
      session.start_transaction();

      COLLECTION_FROM_SESSION(annotations_collection, Collections::ANNOTATIONS(), session)
      annotations_collection.insert_one(session, annotation_data_builder.extract());

      search::insert_full_text(Collections::ANNOTATIONS(), annotation_id, annotation_metadata.view(), session);

      auto genome_info = genomes::get_genome_info(genome);

      size_t count = 0;
      size_t total_size = 0;
      size_t bulk_size = 0;
      std::vector<Document> block;
      std::vector<Document> blocks_bulk;

      for(parser::ChromosomeBedLines chrom_lines: map_regions) {
        std::string internal_chromosome = genome_info->internal_chromosome(chrom_lines.first);
        if (internal_chromosome.empty()) {
          remove::dataset(dataset_id, norm_genome);
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME_GENOME, chrom_lines.first, genome);
        }

        size_t size = genome_info->chromosome_size(internal_chromosome);

        std::string collection = helpers::region_collection_name(genome, internal_chromosome);

        for(const parser::BedLine & bed_line: chrom_lines.second) {

          if (bed_line.start > size || bed_line.end > size) {
            remove::dataset(dataset_id, norm_genome);
            throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_POSITION, bed_line.start, bed_line.end, bed_line.chromosome);
          }

          DocumentBuilder region_builder;
          fill_region_builder(region_builder, bed_line, format);

          block.emplace_back(region_builder.extract());
          compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size, BLOCK_SIZE, BULK_SIZE);
        }

        compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size);
      }

      update_upload_info(Collections::ANNOTATIONS(), annotation_id, total_size, session);

      session.commit_transaction();
      return annotation_id;
    }

    const std::string insert_annotation(const datatypes::User& user,
                           const std::string &name, const std::string &norm_name,
                           const std::string &genome, const std::string &norm_genome,
                           const std::string &description, const std::string &norm_description,
                           const datatypes::Metadata &extra_metadata,
                           const std::string &ip,
                           const ChromosomeRegionsList &regions,
                           const parser::FileFormat &format)
    {
      std::string annotation_id;
      int dataset_id;
      Document extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);
      auto annotation_metadata = annotations::build_metadata(name, norm_name, genome, norm_genome,
                                       description, norm_description, extra_metadata_obj,
                                       ip, format, dataset_id, annotation_id);

      auto upload_info = build_upload_info(user, ip, "regions");

      DocumentBuilder annotation_data_builder;
      annotation_data_builder.append(Concatenate(annotation_metadata.view()));
      annotation_data_builder.append(KVP("upload_info", upload_info));

      SESSION(session)
      session.start_transaction();

      COLLECTION_FROM_SESSION(annotations_collection, Collections::ANNOTATIONS(), session)
      annotations_collection.insert_one(session, annotation_data_builder.extract());

      search::insert_full_text(Collections::ANNOTATIONS(), annotation_id, annotation_metadata, session);

      genomes::GenomeInfoPtr genome_info = genomes::get_genome_info(genome);

      size_t count = 0;
      size_t total_size = 0;
      size_t bulk_size = 0;
      std::vector<Document> block;
      std::vector<Document> blocks_bulk;

      for(const ChromosomeRegions & chromosome_regions: regions) {
        std::string chromosome = chromosome_regions.first;
        std::string internal_chromosome = genome_info->internal_chromosome(chromosome);
        if (internal_chromosome.empty()) {
          remove::dataset(dataset_id, norm_genome);
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME_GENOME, chromosome, genome);
        }
        size_t chromosome_size = genome_info->chromosome_size(internal_chromosome);

        std::string collection = helpers::region_collection_name(genome, internal_chromosome);

        for (auto &region : chromosome_regions.second) {
          DocumentBuilder region_builder;

          if (region->start() > chromosome_size || region->end() > chromosome_size) {
            remove::dataset(dataset_id, norm_genome);
            throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_POSITION, region->start(), region->end(), chromosome);
          }

          region_builder.append(KVP(KeyMapper::START(), static_cast<int32_t>(region->start())));
          region_builder.append(KVP(KeyMapper::END(),   static_cast<int32_t>(region->end())));

          block.emplace_back(region_builder.extract());

          compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size, BLOCK_SIZE, BULK_SIZE);
        }

        compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size);
      }
      update_upload_info(Collections::ANNOTATIONS(), annotation_id, total_size, session);

      session.commit_transaction();
      return annotation_id;
    }

    const std::string insert_annotation(const datatypes::User& user,
                           const std::string &name, const std::string &norm_name,
                           const std::string &genome, const std::string &norm_genome,
                           const std::string &description, const std::string &norm_description,
                           const datatypes::Metadata &extra_metadata,
                           const std::string &ip,
                           const parser::WigPtr &wig)
    {
      auto extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);
      std::string annotation_id;
      int dataset_id;
      auto annotation_metadata = annotations::build_metadata(name, norm_name, genome, norm_genome,
                                       description, norm_description, extra_metadata_obj,
                                       ip, parser::FileFormat::wig_format(),
                                       dataset_id, annotation_id);

      auto upload_info = build_upload_info(user, ip, "signal");
      DocumentBuilder annotation_builder;
      annotation_builder.append(Concatenate(annotation_metadata.view()));
      annotation_builder.append(KVP("upload_info", upload_info));

      SESSION(session)
      session.start_transaction();

      COLLECTION_FROM_SESSION(annotations_collection, Collections::ANNOTATIONS(), session)
      annotations_collection.insert_one(session, annotation_builder.extract());

      search::insert_full_text(Collections::ANNOTATIONS(), annotation_id, annotation_metadata, session);

      genomes::GenomeInfoPtr genome_info = genomes::get_genome_info(genome);

      size_t count = 0;
      std::vector<Document> bulk;
      std::string prev_collection;
      size_t actual_size = 0;
      size_t total_size = 0;
      size_t total_regions = 0;

      for (const auto& track: *wig) {
        std::string internal_chromosome  = genome_info->internal_chromosome(track->chromosome());
        if (internal_chromosome.empty()) {
          remove::dataset(dataset_id, norm_genome);
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME_GENOME, track->chromosome(), genome);
        }

        DocumentBuilder region_builder;
        region_builder.append(KVP("_id", static_cast<int64_t>(dataset_id) << 32 | static_cast<int64_t>(count++)));
        region_builder.append(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));
        region_builder.append(KVP(KeyMapper::WIG_TRACK_TYPE(), static_cast<int32_t>(track->type())));
        region_builder.append(KVP(KeyMapper::START(), static_cast<int32_t>(track->start())));
        region_builder.append(KVP(KeyMapper::END(), static_cast<int32_t>(track->end())));
        if (track->step()) {
          region_builder.append(KVP(KeyMapper::WIG_STEP(), static_cast<int32_t>(track->step())));
        }
        if (track->span()) {
          region_builder.append(KVP(KeyMapper::WIG_SPAN(), static_cast<int32_t>(track->span())));
        }
        region_builder.append(KVP(KeyMapper::FEATURES(), static_cast<int32_t>(track->features())));
        region_builder.append(KVP(KeyMapper::WIG_DATA_SIZE(), static_cast<int32_t>(track->data_size())));

        auto data = track->data();
        auto data_size = track->data_size();

        region_builder.append(KVP(KeyMapper::WIG_DATA(),
          Binary{bsoncxx::binary_sub_type::k_binary, data_size, track->data().get()}));

        actual_size += data_size;
        total_regions +=  track->features();

        // TODO: check regions and positions regards the chromosome size!
        auto size = genome_info->chromosome_size(internal_chromosome);

        std::string collection = helpers::region_collection_name(genome, internal_chromosome);

        if (prev_collection != collection) {
          if (!prev_collection.empty() && bulk.size() > 0) {
            COLLECTION(PREV, prev_collection)
            PREV.insert_many(bulk);
            bulk.clear();
          }
          prev_collection = collection;
          actual_size = 0;
        }

        auto r = region_builder.extract();
        total_size += r.view().length();
        bulk.emplace_back(r);

        if (bulk.size() % BULK_SIZE == 0 || actual_size > MAXIMUM_SIZE) {
          COLLECTION(COLL, collection)
          COLL.insert_many(bulk);
          bulk.clear();
          actual_size = 0;
        }
      }

      if (bulk.size() > 0) {
        COLLECTION(PREV, prev_collection)
        PREV.insert_many(bulk);
        bulk.clear();
      }

      update_upload_info(Collections::ANNOTATIONS(), annotation_id, total_size, session);

      session.commit_transaction();
      return annotation_id;
    }

    const uint32_t insert_query_region_set(const datatypes::User& user,
                                 const std::string &genome, const std::string &norm_genome,
                                 const std::string &ip,
                                 const parser::ChromosomeRegionsMap &map_regions,
                                 const parser::FileFormat &format)
    {
      uint32_t dataset_id = helpers::get_increment_counter("datasets");

      genomes::GenomeInfoPtr genome_info = genomes::get_genome_info(norm_genome);

      size_t count = 0;
      size_t total_size = 0;
      size_t bulk_size = 0;
      std::vector<Document> block;
      std::vector<Document> blocks_bulk;

      SESSION(session)
      session.start_transaction();

      for(parser::ChromosomeBedLines chrom_lines: map_regions) {
        std::string internal_chromosome = genome_info->internal_chromosome(chrom_lines.first);
        if (internal_chromosome.empty()) {
          remove::dataset(dataset_id, norm_genome);
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME_GENOME, chrom_lines.first, genome);
        }
        size_t size = genome_info->chromosome_size(internal_chromosome);

        std::string collection = helpers::region_collection_name(genome, internal_chromosome);

        for(const parser::BedLine & bed_line: chrom_lines.second) {
          if (bed_line.start > size || bed_line.end > size) {
            remove::dataset(dataset_id, norm_genome);
            throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_POSITION, bed_line.start, bed_line.end, bed_line.chromosome);
          }

          DocumentBuilder region_builder;
          fill_region_builder(region_builder, bed_line, format);

          block.push_back(region_builder.extract());
          compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size, BLOCK_SIZE, BULK_SIZE);
        }

        compress_and_insert_region_block(dataset_id, collection, count, block, blocks_bulk, bulk_size, total_size);
      }

      session.commit_transaction();
      return dataset_id;
    }

  }
}
