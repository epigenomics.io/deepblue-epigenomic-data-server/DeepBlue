//
//  metafield.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 11.03.2014
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <map>
#include <sstream>

#include <boost/ref.hpp>

#include "collections.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"
#include "metafield.hpp"
#include "queries.hpp"
#include "retrieve.hpp"

#include "../cache/column_dataset_cache.hpp"

#include "../dba/genes.hpp"

#include "../extras/bson_utils.hpp"

#include "../processing/running_cache.hpp"

#include "../lua/sandbox.hpp"

#include "../errors.hpp"
#include "../log.hpp"
#include "../extras/utils.hpp"

namespace epidb {
  namespace dba {

    const std::map<std::string, Metafield::Function> Metafield::createFunctionsMap()
    {
      std::map<std::string, Metafield::Function> m;
      m["@LENGTH"] = &Metafield::length;
      m["@NAME"] = &Metafield::name;
      m["@SEQUENCE"] = &Metafield::sequence;
      m["@EPIGENETIC_MARK"] = &Metafield::epigenetic_mark;
      m["@PROJECT"] = &Metafield::project;
      m["@BIOSOURCE"] = &Metafield::biosource;
      m["@GENOME"] = &Metafield::genome;
      m["@SAMPLE_ID"] = &Metafield::sample_id;
      m["@STRAND"] = &Metafield::strand;
      m["@AGG.MIN"] = &Metafield::min;
      m["@AGG.MAX"] = &Metafield::max;
      m["@AGG.SUM"] = &Metafield::sum;
      m["@AGG.MEDIAN"] = &Metafield::median;
      m["@AGG.MEAN"] = &Metafield::mean;
      m["@AGG.VAR"] = &Metafield::var;
      m["@AGG.SD"] = &Metafield::sd;
      m["@AGG.COUNT"] = &Metafield::count;
      m["@COUNT.MOTIF"] = &Metafield::count_motif;
      m["@CALCULATED"] = &Metafield::calculated;
      m["@GENE_ATTRIBUTE"] = &Metafield::gene_attribute;
      m["@GENE_ID"] = &Metafield::gene_id;
      m["@GENE_NAME"] = &Metafield::gene_name;
      m["@GENE_EXPRESSION"] = &Metafield::gene_expression;
      m["@GO_IDS"] = &Metafield::go_ids;
      m["@GO_LABELS"] = &Metafield::go_labels;

      return m;
    }

    const std::map<std::string, std::string> Metafield::createFunctionsReturnsMap()
    {
      std::map<std::string, std::string> m;
      m["@LENGTH"] = "integer";
      m["@NAME"] = "string";
      m["@SEQUENCE"] = "string";
      m["@EPIGENETIC_MARK"] = "string";
      m["@PROJECT"] = "string";
      m["@BIOSOURCE"] = "string";
      m["@GENOME"] = "string";
      m["@SAMPLE_ID"] = "string";
      m["@STRAND"] = "string";
      m["@AGG.MIN"] = "double";
      m["@AGG.MAX"] = "double";
      m["@AGG.SUM"] = "double";
      m["@AGG.MEDIAN"] = "double";
      m["@AGG.MEAN"] = "double";
      m["@AGG.VAR"] = "double";
      m["@AGG.SD"] = "double";
      m["@AGG.COUNT"] = "integer";
      m["@COUNT.MOTIF"] = "integer";
      m["@CALCULATED"] = "string";
      m["@GENE_ATTRIBUTE"] = "string";
      m["@GENE_ID"] = "string";
      m["@GENE_NAME"] = "string";
      m["@GENE_EXPRESSION"] = "double";
      m["@GO_IDS"] = "string";
      m["@GO_LABELS"] = "string";

      return m;
    }

    static const std::string EMPTY_STRING("");

    std::string Metafield::command_type(const std::string &command)
    {
      std::map<std::string, std::string>::iterator it = functionsReturns.find(command);
      if (it == functionsReturns.end()) {
        return std::string();
      }
      return it->second;
    }

    bool Metafield::is_meta(const std::string &s)
    {
      return (s[0] == '@' || s[0] == '$');
    }

    inline std::string metafield_attribute(const std::string& op)
    {
      unsigned int s = op.find("(") + 1;
      unsigned int e = op.find_last_of(")");
      unsigned int length = e - s;
      return op.substr(s, length);
    }

    const std::string Metafield::process(const std::string &op, const std::string &chrom, const AbstractRegion *region_ref,
                            processing::StatusPtr status)
    {
      MaybeDocument obj;
      View v = {};

      // TODO: Workaround - because aggregates does not have a region_set_id
      if (region_ref->dataset_id() != DATASET_EMPTY_ID) {
        obj = cache::get_document_by_dataset_id(region_ref->dataset_id());
        v = obj->view();
      }

      static const std::string open_parenthesis("(");
      std::string command = op.substr(0, op.find(open_parenthesis));
      std::map<std::string, Function>::iterator it;
      it = functions.find(command);
      if (it != functions.end()) {
        Function f = it->second;
        return (*this.*f)(op, chrom, v, region_ref, status);
      }

      throw deepblue_user_exception(ERR_INVALID_META_COLUMN_NAME, op);
    }

    const std::string Metafield::length(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                           processing::StatusPtr status)
    {
      return utils::integer_to_string(region_ref->end() - region_ref->start());
    }

    const std::string Metafield::strand(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                           processing::StatusPtr status)
    {
      if (region_ref->has_strand()) {
        return region_ref->strand();
      } else {
        auto pos = cache::get_column_position_from_dataset(region_ref->dataset_id(), "STRAND");

        if (pos == -1) {
          return "";
        } else {
          return region_ref->get_string(pos);
        }
      }
    }

    const std::string Metafield::name(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                         processing::StatusPtr status)
    {
      return utils::get_by_region_set(obj, "name");
    }

    const std::string Metafield::epigenetic_mark(const std::string &op, const std::string &chrom, const View &obj,
                          const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      return utils::get_by_region_set(obj, "epigenetic_mark");
    }

    const std::string Metafield::project(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      return utils::get_by_region_set(obj, "project");
    }

    const std::string Metafield::biosource(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      if (bson_utils::has_key(obj, "sample_info")) {
        return  StringViewToString(obj["sample_info"]["biosource_name"].get_utf8().value);
      } else {
        return "";
      }
    }

    const std::string Metafield::genome(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                           processing::StatusPtr status)
    {
      return utils::get_by_region_set(obj, "genome");
    }

    const std::string Metafield::sample_id(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      return utils::get_by_region_set(obj, "sample_id");
    }

    const std::string Metafield::sequence(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::string genome = utils::get_by_region_set(obj, "norm_genome");
      return status->running_cache()->get_sequence(genome, chrom, region_ref->start(), region_ref->end(), status);
    }

    const std::string Metafield::count_motif(const std::string &op, const std::string &chrom, const View &obj,
            const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::string pattern = metafield_attribute(op);

      std::string genome = utils::get_by_region_set(obj, "genome");

      auto count = status->running_cache()->count_regions(genome, chrom, pattern, region_ref->start(), region_ref->end(), status);

      return utils::integer_to_string(count);
    }


    const std::string Metafield::min(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                        processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->min());
      } else {
        return "";
      }
    }

    const std::string Metafield::max(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                        processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->max());
      } else {
        return "";
      }
    }

    const std::string Metafield::sum(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                        processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->sum());
      } else {
        return "";
      }
    }

    const std::string Metafield::median(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                           processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->median());
      } else {
        return "";
      }
    }

    const std::string Metafield::mean(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                         processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->mean());
      } else {
        return "";
      }
    }


    const std::string Metafield::var(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                        processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->var());
      } else {
        return "";
      }
    }

    const std::string Metafield::sd(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                       processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::score_to_string(aggregate_region->sd());
      } else {
        return "";
      }
    }

    const std::string Metafield::count(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                          processing::StatusPtr status)
    {
      if (region_ref->has_stats()) {
        const AggregateRegion *aggregate_region = static_cast<const AggregateRegion *>(region_ref);
        return utils::integer_to_string(aggregate_region->count());
      } else {
        return "";
      }
    }

    const std::string Metafield::calculated(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::string code = metafield_attribute(op);

      lua::Sandbox::LuaPtr lua = lua::Sandbox::new_instance(status);
      lua->store_row_code(code);
      lua->set_current_context(chrom, region_ref, *this);
      return lua->execute_row_code();
    }

    const std::string Metafield::gene_attribute(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::string attribute_name = metafield_attribute(op);

      auto it = region_ref->attributes().find(attribute_name);
      if (it == region_ref->attributes().end()) {
        return EMPTY_STRING;
      }
      return it->second;
    }

    const std::string Metafield::gene_id(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                            processing::StatusPtr status)
    {
      std::string strand;

      if (region_ref->has_strand()) {
        strand = region_ref->strand();
      } else {
        strand = Metafield::strand(op, chrom, obj, region_ref, status);
      }

      std::string gene_model = metafield_attribute(op);
      return dba::genes::get_gene_attribute(chrom, region_ref->start(), region_ref->end(), strand, "gene_id", gene_model);
    }

    const std::string Metafield::gene_name(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::string strand;

      if (region_ref->has_strand()) {
        strand = region_ref->strand();
      } else {
        strand = Metafield::strand(op, chrom, obj, region_ref, status);
      }

      std::string gene_model = metafield_attribute(op);
      return dba::genes::get_gene_attribute(chrom, region_ref->start(), region_ref->end(), strand, "gene_name",  gene_model);
    }

    const std::vector<datatypes::GeneOntologyTermPtr> Metafield::get_gene_ontology_terms(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::string strand;

      if (region_ref->has_strand()) {
        strand = region_ref->strand();
      } else {
        strand = Metafield::strand(op, chrom, obj, region_ref, status);
      }

      std::string gene_model = metafield_attribute(op);
      return dba::genes::get_gene_gene_ontology_annotations(chrom, region_ref->start(), region_ref->end(), strand, gene_model);
    }


    const std::string Metafield::go_ids(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref,
                           processing::StatusPtr status)
    {
      std::vector<datatypes::GeneOntologyTermPtr> go_terms = get_gene_ontology_terms(op, chrom, obj, region_ref, status);

      std::stringstream ss;
      bool first = true;
      for (const auto& go_term: go_terms) {
        if (!first) {
          ss << ",";
        }
        ss << go_term->go_id();
        first = false;
      }

      return ss.str();
    }

    const std::string Metafield::go_labels(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      std::vector<datatypes::GeneOntologyTermPtr> go_terms = get_gene_ontology_terms(op, chrom, obj, region_ref, status);

      std::stringstream ss;
      bool first = true;
      for (const auto& go_term: go_terms) {
        if (!first) {
          ss << ",";
        }
        ss << go_term->go_label();
        first = false;
      }

      return ss.str();
    }

    const std::string Metafield::gene_expression(const std::string &op, const std::string &chrom, const View &obj, const AbstractRegion *region_ref, processing::StatusPtr status)
    {
      return "";
    }
  }
}

std::map<std::string, epidb::dba::Metafield::Function> epidb::dba::Metafield::functions =  epidb::dba::Metafield::createFunctionsMap();

std::map<std::string, std::string> epidb::dba::Metafield::functionsReturns =  epidb::dba::Metafield::createFunctionsReturnsMap();
