//
//  full_text.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 30.04.13.
//  Copyright (c) 2021 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <map>
#include <string>
#include <sstream>
#include <vector>

#include <time.h>
#include <math.h>

#include "../config/config.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "collections.hpp"
#include "helpers.hpp"
#include "full_text.hpp"
#include "key_mapper.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"
#include "../log.hpp"

namespace epidb {
  namespace dba {
    namespace search {

      /**
       * Full Text Search
       */

      void insert_full_text(const std::string &type, const std::string &id, const View &data)
      {
        SESSION(session)
        session.start_transaction();

        insert_full_text(type, id, data, session);

        session.commit_transaction();
      }

      void insert_full_text(const std::string &type, const std::string &id, const View &data, const ClientSession& session)
      {
        DocumentBuilder create_text_search_builder;
        for (const auto& e: data) {
          std::string field_name = bson_utils::get_key(e);
          if (field_name == "_id") {
            continue;
          }

          if (field_name == KeyMapper::DATASET()) {
            continue;
          }

          if (e.type() == UTF8Type) {
            create_text_search_builder.append(KVP(field_name, StringViewToString(e.get_utf8().value)));
            continue;
          }

          if (field_name == "extra_metadata" || field_name == "sample_info") {
            auto doc = e.get_document().value;
            for (const auto& em: doc) {
              if (em.type() == UTF8Type) {
                std::string em_field_name = bson_utils::get_key(em);
                create_text_search_builder.append(KVP(field_name + "_" + em_field_name, StringViewToString(em.get_utf8().value)));
              }
            }
          }
        }

        if (type == "experiments" || type == "samples") {
          std::string norm_biosource_name;
          if (type == "experiments") {
            norm_biosource_name = StringViewToString(data["sample_info"]["norm_biosource_name"].get_utf8().value);
          } else {
            norm_biosource_name = StringViewToString(data["norm_biosource_name"].get_utf8().value);
          }

          auto query = MakeDocument(KVP("norm_name", norm_biosource_name),
                                    KVP("type", "biosources"));

          auto maybe_biosource = helpers::get_one_by_query(Collections::TEXT_SEARCH(), query.view());

          if (!maybe_biosource) {
            throw deepblue_user_exception(ERR_DATABASE_INVALID_BIOSOURCE, norm_biosource_name);
          }

          auto biosource_view = maybe_biosource->view();
          if (bson_utils::has_key(biosource_view, "related_terms")) {
            auto arr_related_terms = biosource_view["related_terms"].get_array().value;
            create_text_search_builder.append(KVP("related_terms", arr_related_terms));
          }
        }

        create_text_search_builder.append(KVP("epidb_id", id));
        create_text_search_builder.append(KVP("type", type));

        COLLECTION_FROM_SESSION(text_search_collection, Collections::TEXT_SEARCH(), session)
        text_search_collection.insert_one(session, create_text_search_builder.extract());
      }

      const std::vector<std::string> get_related_terms(const std::string& name, const std::string& norm_name,
                                const std::string& key_name, const std::string& type)
      {
        std::vector<std::string> related_terms;

        auto maybe_term = helpers::get_one_by_query(Collections::TEXT_SEARCH(),
          MakeDocument(KVP(key_name, norm_name),
                        KVP("type", type)));

        if (!maybe_term) {
          throw_runtime_error("Internal error: '" + type + "' '" + name + "' not found.");
        }

        auto term = maybe_term->view();
        if (!bson_utils::has_key(term, "related_terms")) {
          return {};
        }

        auto e = term["related_terms"].get_array().value;
        for (const auto & be : e) {
          related_terms.push_back(StringViewToString(be.get_utf8().value));
        }

        return related_terms;
      }

      void insert_related_term(const utils::IdName &id_name, const std::vector<std::string> &related_terms)
      {
        auto query = bson_utils::one_key_doc("epidb_id", id_name.id);
        auto related_terms_arr = utils::build_array(related_terms);
        auto append_value = MakeDocument(KVP("$addToSet",
          MakeDocument(KVP("related_terms",
            MakeDocument(KVP("$each", related_terms_arr))))));

        // Update the biosource term (that were informed in the id_names.name)
        COLLECTION(text_search_collection, Collections::TEXT_SEARCH())
        text_search_collection.update_many(query.view(), append_value.view());

        // Update the experiments (that were informed in the id_names.id)
        DocumentBuilder update_related_query_builder;
        update_related_query_builder.append(KVP("norm_biosource_name",  id_name.name));
        text_search_collection.update_many(update_related_query_builder.extract(), append_value.view());

        // Update the samples
        DocumentBuilder update_related_query_builder_for_experiments;
        update_related_query_builder_for_experiments.append(KVP("sample_info_norm_biosource_name", id_name.name));
        text_search_collection.update_many(update_related_query_builder_for_experiments.extract(), append_value.view());
      }

      void insert_related_gene_ontology_term(const std::string& go_term_id, const std::vector<std::string>& terms_to_include)
      {
        auto terms_to_include_arr = utils::build_array(terms_to_include);
        auto append_value = MakeDocument(KVP("$addToSet",
          MakeDocument(KVP("related_terms",
            MakeDocument(KVP("$each", terms_to_include_arr))))));

        auto query = MakeDocument(
          KVP("type", "gene_ontology"),
          KVP("go_id",  go_term_id));

        COLLECTION(text_search_collection, Collections::TEXT_SEARCH())
        text_search_collection.update_one(query.view(), append_value.view());
      }

      void change_extra_metadata_full_text(const std::string &id, const std::string &key, const std::string &value,
                                           const std::string &norm_value, const bool is_sample)
      {
        auto query = bson_utils::one_key_doc("epidb_id", id);
        MaybeDocument change_value;

        std::string db_key;
        std::string norm_db_key;

        if (is_sample) {
          db_key = key;
          norm_db_key = "norm_" + key;
        } else {
          db_key = "extra_metadata_" + key;
          norm_db_key = "extra_metadata_norm_" + key;
        }

        if (value.empty()) {
          if (is_sample) {
            change_value = MakeDocument(KVP("$unset",
                            MakeDocument(
                              KVP(db_key, ""),
                              KVP(norm_db_key, ""))));
          } else {
            change_value = MakeDocument(KVP("$unset", MakeDocument(KVP(db_key, ""))));
          }

        } else {
          if (is_sample) {
            change_value = MakeDocument(KVP("$set",
                            MakeDocument(
                              KVP(db_key, value),
                              KVP(norm_db_key, norm_value))));
          } else {
            change_value = MakeDocument(KVP("$set", MakeDocument(KVP(db_key, value))));
          }
        }

        COLLECTION(text_search_collection, Collections::TEXT_SEARCH())
        text_search_collection.update_one(query.view(), change_value->view());
      }

      const std::vector<TextSearchResult> search_full_text(const std::string &text,
                            const std::vector<std::string> &types,
                            const std::vector<std::string>& private_projects)
      {
        std::vector<TextSearchResult> results;

        DocumentBuilder cmd_builder;
        if (!types.empty()) {
          cmd_builder.append(KVP("type", helpers::build_condition_array<std::string>(types, "$in")));
        }

        if (!private_projects.empty()) {
          cmd_builder.append(KVP("norm_project", helpers::build_condition_array<std::string>(private_projects, "$nin")));
          cmd_builder.append(KVP("norm_name", helpers::build_condition_array<std::string>(private_projects, "$nin")));
        }

        DocumentBuilder text_builder;
        text_builder.append(KVP("$search", text));
        cmd_builder.append(KVP("$text", text_builder.extract()));

        DocumentBuilder view_builder;
        view_builder.append(KVP("epidb_id", 1));
        view_builder.append(KVP("name", 1));
        view_builder.append(KVP("type", 1));
        view_builder.append(KVP("score", bson_utils::one_key_doc("$meta", "textScore")));

        auto SORT = MakeDocument(KVP("score",
                      MakeDocument(KVP("$meta", "textScore"))));

        const size_t MAX_RESULTS = 50;

        auto OPTS = mongocxx::options::find{};
        OPTS.sort(SORT.view());
        OPTS.limit(MAX_RESULTS);
        OPTS.projection(view_builder.extract());

        COLLECTION(text_search_collection, Collections::TEXT_SEARCH())
        auto cursor = text_search_collection.find(cmd_builder.extract(), OPTS);

        for (const auto& o: cursor) {
          TextSearchResult res;
          res.id = bson_utils::get_string(o, "epidb_id");
          if (bson_utils::has_key(o, "name")) {
            res.name = bson_utils::get_string(o, "name");
          }
          res.type = bson_utils::get_string(o, "type");
          res.score = o["score"].get_double().value;
          results.push_back(res);
        }

        return results;
      }

      bool remove(const std::string &id)
      {
        return helpers::remove_all(Collections::TEXT_SEARCH(), bson_utils::one_key_doc("epidb_id", id));
      }
    }
  }
}
