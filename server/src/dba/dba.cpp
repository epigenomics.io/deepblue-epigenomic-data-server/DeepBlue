//
//  dba.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.06.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <map>
#include <string>
#include <sstream>
#include <vector>

#include <time.h>
#include <math.h>

#include "../algorithms/patterns.hpp"

#include "../basic_populator/basic_populator.hpp"

#include "../dba/genomes.hpp"

#include "../cache/column_dataset_cache.hpp"
#include "../cache/queries_cache.hpp"

#include "../config/config.hpp"

#include "../datatypes/metadata.hpp"
#include "../datatypes/regions.hpp"
#include "../datatypes/user.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"
#include "../parser/genome_data.hpp"
#include "../parser/parser_factory.hpp"
#include "../parser/wig_parser.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../version.hpp"

#include "annotations.hpp"
#include "dba.hpp"
#include "users.hpp"
#include "collections.hpp"
#include "controlled_vocabulary.hpp"
#include "exists.hpp"
#include "full_text.hpp"
#include "genes.hpp"
#include "gene_ontology.hpp"
#include "genomes.hpp"
#include "helpers.hpp"
#include "insert.hpp"
#include "key_mapper.hpp"
#include "queries.hpp"
#include "retrieve.hpp"
#include "sequence_retriever.hpp"
#include "info.hpp"

#include "../errors.hpp"
#include "../log.hpp"

namespace epidb {
  namespace dba {

    bool is_initialized()
    {
      return helpers::check_exist(Collections::SETTINGS(), MakeDocument(KVP("initialized", true)));
    }

    bool check_mongodb(std::string &msg)
    {
      EPIDB_LOG_DBG("Verifying if database " << epidb::config::DATABASE_NAME() << " is ready.");
      bool init = dba::is_initialized();
      if (init) {
        EPIDB_LOG_DBG("Database " << epidb::config::DATABASE_NAME() << " is ready.");
      } else {
        EPIDB_LOG_DBG("Database " << epidb::config::DATABASE_NAME() << " is not initialized.");
      }
      return true;
    }

    bool init_system(const std::string &name, const std::string &email, const std::string &institution,
                     datatypes::User& admin_user, std::string &msg)
    {
      admin_user = datatypes::User(name, email, institution);
      admin_user.generate_key();
      admin_user.permission_level(datatypes::PermissionLevel::ADMIN);
      dba::users::add_user(admin_user);

      {
        datatypes::User anon_user = datatypes::User("anonymous", "anonymous.deepblue@mpi-inf.mpg.de", "DeepBlue Epigenomic Data Server");
        anon_user.key("anonymous_key");
        anon_user.password("anonymous");
        anon_user.permission_level(datatypes::PermissionLevel::GET_DATA);
        dba::users::add_user(anon_user);
      }

      DocumentBuilder create_settings_builder;

      create_settings_builder.append(KVP("date", Date(std::chrono::system_clock::now())));
      create_settings_builder.append(KVP("initialized", true));
      create_settings_builder.append(KVP("version", Version::version()));

      COLLECTION(settings_collection, Collections::SETTINGS())
      settings_collection.insert_one(create_settings_builder.extract());

      std::string column_id;

      basic_populator::columns(admin_user);
      basic_populator::indexes(admin_user);

      // Clear caches
      cv::biosources_cache.invalidate();
      gene_ontology::go_cache.invalidate();
      query::invalidate_cache();
      users::invalidate_cache();
      genes::invalidate_cache();
      cache::column_dataset_cache_invalidate();
      cache::queries_cache_invalidate();
      config::set_old_request_age_in_sec(config::get_default_old_request_age_in_sec());
      config::set_janitor_periodicity(config::get_default_janitor_periodicity());
      retrieve::SequenceRetriever::singleton().invalidade_cache();

      return true;
    }

    void create_chromosome_collection(const std::string &genome_norm_name, const std::string &chromosome)
    {
      std::string collection_name = helpers::region_collection_name(genome_norm_name, chromosome);

      DB(db)
      auto collection = db.create_collection(collection_name);

      // Set indexes
      DocumentBuilder dataset_start_end_index;
      dataset_start_end_index.append(KVP(KeyMapper::DATASET(), 1));
      dataset_start_end_index.append(KVP(KeyMapper::START(), 1));
      dataset_start_end_index.append(KVP(KeyMapper::END(), 1));
      collection.create_index(dataset_start_end_index.extract());

      DocumentBuilder start_end_index;
      start_end_index.append(KVP(KeyMapper::START(), 1));
      start_end_index.append(KVP(KeyMapper::END(), 1));
      collection.create_index(start_end_index.extract());
    }


    bool add_genome(const datatypes::User& user,
                    const std::string &name, const std::string &norm_name,
                    const std::string &description, const std::string &norm_description,
                    const parser::ChromosomesInfo &genome_info,
                    const std::string &ip,
                    std::string &genome_id, std::string &msg)
    {
      int collection_counter = helpers::get_incremente_counter_and_notify(Collections::GENOMES());
      genome_id = "g" + utils::integer_to_string(collection_counter);

      DocumentBuilder search_data_builder;
      search_data_builder.append(KVP("_id", genome_id));
      search_data_builder.append(KVP("name", name));
      search_data_builder.append(KVP("norm_name", norm_name));
      search_data_builder.append(KVP("description", description));
      search_data_builder.append(KVP("norm_description", norm_description));

      auto search_data = search_data_builder.extract();
      DocumentBuilder create_genome_builder;
      create_genome_builder.append(Concatenate(search_data.view()));

      ArrayBuilder ab;
      for (const auto &chr : genome_info) {
        DocumentBuilder chromosome_builder;
        chromosome_builder.append(KVP("name", chr.first));
        chromosome_builder.append(KVP("size", static_cast<std::int32_t>(chr.second)));
        ab.append(chromosome_builder.extract());

        create_chromosome_collection(name, chr.first);
      }
      create_genome_builder.append(KVP("chromosomes", ab.extract()));
      create_genome_builder.append(KVP("user", user.id()));

      COLLECTION(genomes_collections, Collections::GENOMES())
      genomes_collections.insert_one(create_genome_builder.extract());

      search::insert_full_text(Collections::GENOMES(), genome_id, search_data);

      DatasetId id = DATASET_EMPTY_ID;

      ChromosomeRegionsList chromosome_regions_list;
      for (const auto &chr : genome_info) {
        Regions regions = Regions(1);
        regions.emplace_back(build_simple_region(0, chr.second, id));
        ChromosomeRegions chromosome_regions(chr.first, std::move(regions));
        chromosome_regions_list.push_back(std::move(chromosome_regions));
      }

      std::string ann_name = "Chromosomes size for " + name;
      std::string ann_norm_name = utils::normalize_annotation_name(ann_name);
      std::string ann_description = "Chromosomes and sizes of the genome " + name + " (" + description + ")";
      std::string ann_norm_description = utils::normalize_name(ann_description);
      datatypes::Metadata extra_metadata;
      std::string annotation_id = insert_annotation(user, ann_name, ann_norm_name, name, norm_name, ann_description, ann_norm_description,
                    extra_metadata, ip, chromosome_regions_list, parser::FileFormat::default_format());

      return true;
    }

    bool add_epigenetic_mark(const datatypes::User& user,
                             const std::string &name, const std::string &norm_name,
                             const std::string &description, const std::string &norm_description,
                             const datatypes::Metadata &extra_metadata,
                             std::string &epigenetic_mark_id, std::string &msg)
    {
      int collection_counter = helpers::get_incremente_counter_and_notify(Collections::EPIGENETIC_MARKS());
      epigenetic_mark_id = "em" + utils::integer_to_string(collection_counter);

      DocumentBuilder search_data_builder{};
      search_data_builder.append(KVP("_id", epigenetic_mark_id));
      search_data_builder.append(KVP("name", name));
      search_data_builder.append(KVP("norm_name", norm_name));
      search_data_builder.append(KVP("description", description));
      search_data_builder.append(KVP("norm_description", norm_description));

      DocumentBuilder metadata_builder;
      for (auto cit = extra_metadata.begin(); cit != extra_metadata.end(); ++cit) {
        metadata_builder.append(KVP(cit->first, cit->second));
      }
      search_data_builder.append(KVP("extra_metadata", metadata_builder.extract()));

      auto search_data = search_data_builder.extract();
      DocumentBuilder create_epi_mark_builder;
      create_epi_mark_builder.append(Concatenate(search_data.view()));

      create_epi_mark_builder.append(KVP("user", user.id()));
      auto cem = create_epi_mark_builder.extract();

      COLLECTION(epigenetic_marks_collections, Collections::EPIGENETIC_MARKS())
      epigenetic_marks_collections.insert_one(cem.view());

      search::insert_full_text(Collections::EPIGENETIC_MARKS(), epigenetic_mark_id, search_data);

      return true;
    }

    bool add_biosource(const datatypes::User& user,
                       const std::string &name, const std::string &norm_name,
                       const std::string &description, const std::string &norm_description,
                       const datatypes::Metadata &extra_metadata,
                       std::string &biosource_id, std::string &msg)
    {
      int collection_counter = helpers::get_incremente_counter_and_notify(Collections::BIOSOURCES());
      biosource_id = "bs" + utils::integer_to_string(collection_counter);

      DocumentBuilder search_data_builder;
      search_data_builder.append(KVP("_id", biosource_id));
      search_data_builder.append(KVP("name", name));
      search_data_builder.append(KVP("norm_name", norm_name));
      search_data_builder.append(KVP("description", description));
      search_data_builder.append(KVP("norm_description", norm_description));

      DocumentBuilder metadata_builder;
      for (const auto& cit : extra_metadata) {
        metadata_builder.append(KVP(cit.first, cit.second));
      }
      search_data_builder.append(KVP("extra_metadata", metadata_builder.extract()));

      auto search_data = search_data_builder.extract();
      DocumentBuilder create_biosource_builder;
      create_biosource_builder.append(Concatenate(search_data.view()));

      create_biosource_builder.append(KVP("user", user.id()));

      COLLECTION(biosources_collection, Collections::BIOSOURCES())
      biosources_collection.insert_one(create_biosource_builder.extract());

      search::insert_full_text(Collections::BIOSOURCES(), biosource_id, search_data);

      return true;
    }

    bool add_technique(const datatypes::User& user,
                       const std::string &name, const std::string &norm_name,
                       const std::string &description, const std::string &norm_description,
                       const datatypes::Metadata &extra_metadata,
                       std::string &technique_id, std::string &msg)
    {
      int collection_counter = helpers::get_incremente_counter_and_notify(Collections::TECHNIQUES());
      technique_id = "t" + utils::integer_to_string(collection_counter);

      DocumentBuilder search_data_builder;
      search_data_builder.append(KVP("_id", technique_id));
      search_data_builder.append(KVP("name", name));
      search_data_builder.append(KVP("norm_name", norm_name));
      search_data_builder.append(KVP("description", description));
      search_data_builder.append(KVP("norm_description", norm_description));

      DocumentBuilder metadata_builder;
      for (auto cit: extra_metadata) {
        metadata_builder.append(KVP(cit.first, cit.second));
      }
      search_data_builder.append(KVP("extra_metadata", metadata_builder.extract()));


      auto search_data = search_data_builder.extract();
      DocumentBuilder create_technique_builder;
      create_technique_builder.append(Concatenate(search_data.view()));

      create_technique_builder.append(KVP("user", user.id()));

      COLLECTION(techniques_collection, Collections::TECHNIQUES())
      techniques_collection.insert_one(create_technique_builder.extract());

      search::insert_full_text(Collections::TECHNIQUES(), technique_id, search_data);

      return true;
    }

    bool add_sample(const datatypes::User& user,
                    const std::string &biosource_name, const std::string &norm_biosource_name,
                    const datatypes::Metadata &metadata,
                    std::string &sample_id, std::string &msg)
    {
      DocumentBuilder data_builder;
      data_builder.append(KVP("biosource_name", biosource_name));
      data_builder.append(KVP("norm_biosource_name", norm_biosource_name));

      std::map<std::string, std::string> names_values;

      for (const datatypes::Metadata::value_type &kv : metadata) {
        if (names_values.find(kv.first) != names_values.end()) {
          throw deepblue_user_exception(ERR_DUPLICATED_FIELD, kv.first);
        }

        names_values[kv.first] = kv.second;
      }

      for (const auto &name_value : names_values) {
        std::string norm_title = "norm_" + name_value.first;
        std::string norm_value = utils::normalize_name(name_value.second);
        data_builder.append(KVP(name_value.first, name_value.second));
        data_builder.append(KVP(norm_title, norm_value));
      }

      auto data = data_builder.extract();

      COLLECTION(samples_collection, Collections::SAMPLES())


      // If we already have a sample with exactly the same information
      auto maybe_sample = samples_collection.find_one(data.view());
      if (maybe_sample) {
        auto o = maybe_sample->view();
        sample_id = bson_utils::get_string(o, "_id");
        return true;
      }

      int collection_counter = helpers::get_incremente_counter_and_notify(Collections::SAMPLES());
      sample_id = "s" + utils::integer_to_string(collection_counter);

      DocumentBuilder create_sample_builder;
      create_sample_builder.append(KVP("_id", sample_id));
      create_sample_builder.append(Concatenate(data.view()));
      create_sample_builder.append(KVP("user", user.id()));
      auto cem = create_sample_builder.extract();

      samples_collection.insert_one(cem.view());

      search::insert_full_text(Collections::SAMPLES(), sample_id, data.view());

      return true;
    }

    void add_chromosome_sequence(const datatypes::User& user,
                                 const std::string &genome, const std::string &norm_genome,
                                 const std::string &chromosome,
                                 const std::string &sequence)
    {
      std::string filename = norm_genome + "." + chromosome;

      // 2 << 12 - 8Kb for each piece in the bucket
      GET_BUCKET(BUCKET, SEQUENCES, 2 << 12)

      // check for possible duplicate
      auto c = BUCKET.find(bson_utils::one_key_doc("filename", filename));

      if (c.begin() != c.end()) {
        throw deepblue_user_exception(ERR_DUPLICATED_SEQUENCE, chromosome, genome);
      }

      auto uploader = BUCKET.open_upload_stream(filename);

      std::vector<uint8_t> data(sequence.begin(), sequence.end());

      uploader.write(data.data(), data.size());
      auto result = uploader.close();

      auto query = MakeDocument(
        KVP("norm_name", norm_genome),
        KVP("chromosomes.name", chromosome)
      );

      auto set = MakeDocument(
        KVP("$set", MakeDocument(
          KVP("chromosomes.$.sequence_file", filename)
        ))
      );

      COLLECTION(genomes_collection, Collections::GENOMES())
      genomes_collection.update_one(query.view(), set.view());
    }

    void validate_biosource_name(const std::string &name, const std::string &norm_name)
    {
      if (exists::biosource(norm_name)) {
        throw deepblue_user_exception(ERR_DUPLICATED_BIOSOURCE_NAME, name);
      }

      if (exists::biosource_synonym(norm_name)) {
        throw deepblue_user_exception(ERR_DUPLICATED_BIOSOURCE_NAME, name);
      }
    }

    void validate_technique_name(const std::string &name, const std::string &norm_name)
    {
      if (exists::technique(norm_name)) {
        throw deepblue_user_exception(ERR_DUPLICATED_TECHNIQUE_NAME, name);
      }
    }

    void validate_epigenetic_mark(const std::string &name, const std::string &norm_name)
    {
      if (exists::epigenetic_mark(norm_name)) {
        throw deepblue_user_exception(ERR_DUPLICATED_EPIGENETIC_MARK_NAME, name);
      }
    }

    void validate_project(const std::string &name, const std::string &norm_name)
    {
      if (exists::project(norm_name)) {
        throw deepblue_user_exception(ERR_DUPLICATED_PROJECT_NAME, name);
      }
    }

    void validate_genome(const std::string &genome, const std::string &norm_genome)
    {
      if (exists::genome(norm_genome)) {
        throw deepblue_user_exception(ERR_DUPLICATED_GENOME_NAME, genome);
      }
    }

    bool set_biosource_parent(const datatypes::User user,
                              const std::string &biosource_more_embracing, const std::string &norm_biosource_more_embracing,
                              const std::string &biosource_less_embracing, const std::string &norm_biosource_less_embracing,
                              bool more_embracing_is_syn, const bool less_embracing_is_syn,
                              std::string &msg)
    {
      return cv::set_biosource_parent(user, biosource_more_embracing, norm_biosource_more_embracing,
                                      biosource_less_embracing, norm_biosource_less_embracing,
                                      more_embracing_is_syn, less_embracing_is_syn, msg);
    }

    bool get_biosource_children(const std::string &biosource_name, const std::string &norm_biosource_name,
                                bool is_biosource,
                                std::vector<utils::IdName> &related_biosources, std::string &msg)
    {
      std::vector<std::string> norm_subs;

      if (!cv::get_biosource_children(biosource_name, norm_biosource_name, is_biosource, norm_subs, msg)) {
        return false;
      }

      for (const std::string &norm_sub : norm_subs) {
        utils::IdName sub_biosource_name = helpers::get_id_name(Collections::BIOSOURCES(), norm_sub);
        related_biosources.push_back(sub_biosource_name);
      }
      return true;
    }

    bool get_biosource_parents(const std::string &biosource_name, const std::string &norm_biosource_name,
                               bool is_biosource,
                               std::vector<utils::IdName> &related_biosources, std::string &msg)
    {
      std::vector<std::string> norm_subs;

      if (!cv::get_biosource_parents(biosource_name, norm_biosource_name, is_biosource, norm_subs, msg)) {
        return false;
      }

      for (const std::string &norm_sub : norm_subs) {
        utils::IdName sub_biosource_name = helpers::get_id_name(Collections::BIOSOURCES(), norm_sub);
        related_biosources.push_back(sub_biosource_name);
      }
      return true;
    }

    // TODO: change the type of start and end to remove the warnings bellow
    const ChromosomeRegionsList process_pattern(const std::string &genome, const std::string &motif, const bool overlap,
                              std::vector<std::string> &chromosomes, const int32_t start, const int32_t end)
    {
      std::string norm_genome = utils::normalize_name(genome);

      retrieve::SequenceRetriever &retriever = retrieve::SequenceRetriever::singleton();
      std::vector<std::string> missing;
      for (const std::string &chromosome_name : chromosomes) {
        if (!retriever.exists(genome, chromosome_name)) {
          missing.push_back(chromosome_name);
          break;
        }
      }
      if (!missing.empty()) {
        throw deepblue_user_exception(ERR_CHROMOSOME_SEQUENCE_NOT_FOUND, utils::vector_to_string(missing), genome);
      }

      ChromosomeRegionsList pattern_regions;
      for (const std::string &chromosome_name : chromosomes) {
        size_t chromosome_size = genomes::chromosome_size(genome, chromosome_name);

        size_t real_start;
        if (start < 0) {
          real_start = 0;
        } else if (start >= static_cast<int64_t>(chromosome_size)) {
          real_start = chromosome_size - 1;
        } else {
          real_start = start;
        }

        size_t real_end;
        if (end < 0) {
          real_end = chromosome_size - 1;
        } else if (end >= static_cast<int64_t>(chromosome_size)) {
          real_end = chromosome_size - 1;
        } else {
          real_end = end;
        }

        if (real_start > real_end) {
          real_end = real_start;
        }

        std::string sequence = retriever.retrieve(norm_genome, chromosome_name, real_start, real_end);

        algorithms::PatternFinder pf(sequence, motif);
        Regions regions;
        if (overlap) {
          regions = pf.overlap_regions();
        } else {
          regions = pf.non_overlap_regions();
        }
        ChromosomeRegions chromosome_regions(chromosome_name, std::move(regions));
        pattern_regions.push_back(std::move(chromosome_regions));
      }

      return pattern_regions;
    }
  }
}
