//
//  regions.h
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 09.07.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the ters of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_QUERIES_HPP
#define EPIDB_DBA_QUERIES_HPP

#include <iostream>
#include <map>

#include "dba.hpp"
#include "column_types.hpp"

#include "../datatypes/regions.hpp"
#include "../datatypes/user.hpp"

#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"


namespace epidb {
  namespace processing {
    class Status;
    typedef std::shared_ptr<Status> StatusPtr;
  }

  namespace dba {
    namespace query {

      void invalidate_cache();

      const std::string store_query(const datatypes::User& user, const std::string &type, const View &args, const std::string& derived_from = "");

      const std::string modify_query(const datatypes::User& user,
                        const std::string &query_id, const std::string &key, const std::string &value);

      const ChromosomeRegionsList retrieve_query(const datatypes::User& user,
                                    const std::string &query_id, processing::StatusPtr status, bool reduced_mode = false);

      const std::vector<utils::IdName> get_experiments_by_query(const datatypes::User& user,
                                          const std::string &query_id, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_experiment_select_query(const datatypes::User& user,
                                            const View &query, processing::StatusPtr status, bool reduced_mode = false);

      const size_t count_regions(const datatypes::User& user, const std::string &query_id, processing::StatusPtr status);

      const std::vector<std::string> build_annotation_query(const datatypes::User& user, const View &query, View &regions_query);

      Document build_experiment_query(const datatypes::User& user, const View &args);

      Document build_experiment_query(const int start, const int end, const std::string &experiment_name);

      Document build_experiment_query(const int start, const int end, const ArrayView& datasets_array);

      const ChromosomeRegionsList retrieve_annotation_select_query(const datatypes::User& user,
                                            const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_genes_select_query(const datatypes::User& user,
                                       const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_find_motif_query(const datatypes::User& user,
                                     const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_expression_select_query(const datatypes::User& user,
                                            const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_intersection_query(const datatypes::User& user,
                                       const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_overlap_query(const datatypes::User& user,
                                  const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_flank_query(const datatypes::User& user,
                                const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_extend_query(const datatypes::User& user,
                                 const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_merge_query(const datatypes::User& user,
                                const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_query_region_set(const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_annotation_select_query(const datatypes::User& user,
                                            const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_filter_query(const datatypes::User& user,
                                 const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList retrieve_filter_by_motif_query(const datatypes::User& user,
                                          const View &query, processing::StatusPtr status);

      const DatasetId add_tiling(const std::string &genome, const size_t &tiling_size);

      const ChromosomeRegionsList retrieve_tiling_query(const View &query, processing::StatusPtr status);

      const ChromosomeRegionsList process_aggregate(const datatypes::User& user,const View &query, processing::StatusPtr status);

      bool is_canceled(processing::StatusPtr status);

      const std::vector<std::string> get_main_experiment_data(const datatypes::User &user,
                const std::string &query_id, const std::string field_key, processing::StatusPtr status);

      /* These two functions and struct must not be accessed directly. Use the cache:: methods */
      /* TODO: move to another file */
      const std::vector<Document> __get_columns_from_dataset(const DatasetId &dataset_id);

      Document __get_document_by_dataset_id(DatasetId dataset_id);
    }
  }
}

#endif
