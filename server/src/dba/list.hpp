//
//  list.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 07.04.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_LIST_HPP
#define EPIDB_DBA_LIST_HPP

#include <string>
#include <unordered_map>
#include <vector>

#include "../datatypes/metadata.hpp"
#include "../datatypes/user.hpp"

#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"

namespace epidb {
  namespace dba {
    namespace list {

      /**
       * Lists
       */
      const std::vector<utils::IdName> genomes();

      const std::vector<utils::IdName> biosources(const datatypes::Metadata &metadata);

      const std::vector<std::string> samples(const ArrayView &biosources,
                   const datatypes::Metadata &metadata);

      const std::vector<utils::IdName> private_projects(const datatypes::User& user);

      const std::vector<utils::IdName> public_projects();

      const std::vector<utils::IdName> all_projects();

      const std::vector<utils::IdName> epigenetic_marks(const datatypes::Metadata &metadata);

      const std::vector<utils::IdName> experiments(const View& query);

      const std::vector<utils::IdName> annotations(const std::string &genome);

      const std::vector<utils::IdName> annotations();

      const std::vector<utils::IdName> users();

      const std::vector<utils::IdName> techniques();

      const std::vector<utils::IdName> column_types();

      std::vector<utils::IdName> gene_models();

      const std::vector<Document> genes(const std::vector<std::string> &genes_id_or_name, const std::vector<std::string> &go_terms,
                 const std::vector<std::string> &chromosomes,
                 const Position start, const Position end, const std::string &norm_gene_model);

      /**
       * List similars
       */

      std::vector<utils::IdName> similar_biosources(const std::string &name);

      std::vector<utils::IdName> similar_techniques(const std::string &nameg);

      std::vector<utils::IdName> similar_projects(const std::string &name);

      std::vector<utils::IdName> similar_epigenetic_marks(const std::string &name);

      std::vector<utils::IdName> similar_genomes(const std::string &name);

      const std::vector<utils::IdName> similar_experiments(const std::string &name, const std::string &norm_genome);

      std::vector<utils::IdName> similar(const std::string &where, const std::string &what,
                   const size_t total = 20);

      std::vector<utils::IdName> similar(const std::string &where, const std::string &field, const std::string &what,
                   const std::string &filter_field, const std::string &filter_what,
                   const size_t total = 20);


      Document build_list_experiments_query(const datatypes::User& user,
                                        const std::vector<serialize::ParameterPtr> genomes, const std::vector<serialize::ParameterPtr> types,
                                        const std::vector<serialize::ParameterPtr> epigenetic_marks, const std::vector<serialize::ParameterPtr> biosources,
                                        const std::vector<serialize::ParameterPtr> sample_ids, const std::vector<serialize::ParameterPtr> techniques,
                                        const std::vector<serialize::ParameterPtr> projects);

      /**
       * List in use
       */
      const std::string get_controlled_vocabulary_key(const std::string& controlled_vocabulary);

      const std::vector<utils::IdNameCount> in_use(const datatypes::User& user,
          const std::string &collection, const std::string &field_name);

      const std::vector<utils::IdNameCount> collection_experiments_count(const datatypes::User& user,
          const std::string controlled_vocabulary, const View& experiments_query);

      std::unordered_map<std::string, std::vector<utils::IdNameCount>> faceting(const datatypes::User& user, const View& experiments_query);

      Document build_list_experiments_bson_query(const datatypes::User& user, const View& args);
    }
  }
}

#endif