//
//  data.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 06.11.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <string>

#include "collections.hpp"
#include "column_types.hpp"
#include "helpers.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace dba {
    namespace data {

      MaybeDocument sample(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::SAMPLES(), id);
      }

      MaybeDocument genome(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::GENOMES(), id);
      }

      MaybeDocument project(const std::string &id, const std::vector<std::string>& user_projects)
      {
        auto array = ArrayBuilder{};
        array.append(MakeDocument(KVP("_id", id)));
        array.append(MakeDocument(KVP("norm_name",
          MakeDocument(KVP("$in", utils::build_normalized_array(user_projects))))));


        auto query_builder = DocumentBuilder{};
        query_builder.append(KVP("$and", array.extract()));
        auto query = query_builder.extract();

        return helpers::get_one_by_query(Collections::PROJECTS(), query.view());
      }

      MaybeDocument technique(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::TECHNIQUES(), id);
      }

      MaybeDocument biosource(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::BIOSOURCES(), id);
      }

      MaybeDocument epigenetic_mark(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::EPIGENETIC_MARKS(), id);
      }

      MaybeDocument annotation(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::ANNOTATIONS(), id);
      }

      MaybeDocument gene_model(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::GENE_MODELS(), id);
      }

      MaybeDocument gene(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::GENES(), id);
      }

      MaybeDocument experiment(const std::string &id, const std::vector<std::string>& user_projects)
      {
        if (id.empty()) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, id);
        }

        DocumentBuilder bob;

        if (id[0] == '#') {
          auto _new_id = id;
          _new_id.erase(0, 1);
          bob.append(KVP("extra_metadata.md5sum", _new_id));
        } else  {
          bob.append(KVP("_id", id));
        }

        auto array = ArrayBuilder{};
        array.append(bob.extract());
        array.append(MakeDocument(KVP("norm_project",
            MakeDocument(KVP("$in", utils::build_normalized_array(user_projects))))));

        auto query_builder = DocumentBuilder{};
        query_builder.append(KVP("$and", array.extract()));
        auto query = query_builder.extract();

        return helpers::get_one_by_query(Collections::EXPERIMENTS(), query.view());
      }

      MaybeDocument experiment_set(const std::string& id)
      {
        return helpers::get_one_by_id(Collections::EXPERIMENT_SETS(), id);
      }

      MaybeDocument query(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::QUERIES(), id);
      }

      MaybeDocument tiling_region(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::TILINGS(), id);
      }

      MaybeDocument column_type(const std::string &id)
      {
        return helpers::get_one_by_id(Collections::COLUMN_TYPES(), id);
      }

    }
  }
}