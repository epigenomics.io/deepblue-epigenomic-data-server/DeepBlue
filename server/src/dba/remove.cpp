//
//  delete.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 06.11.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>

#include "../datatypes/expressions_manager.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "collections.hpp"
#include "controlled_vocabulary.hpp"
#include "data.hpp"
#include "full_text.hpp"
#include "genomes.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"
#include "remove.hpp"
#include "users.hpp"
#include "list.hpp"

#include "../exceptions.hpp"

namespace epidb {
  namespace dba {
    namespace remove {

      void validate_permission(const datatypes::User& user, const View& entity, bool is_exp_ann)
      {
        if (user.is_admin()) {
          return;
        }

        std::string owner;
        if (is_exp_ann) {
          auto upload_info = StringViewToString(entity["upload_info"]["user"].get_utf8().value);
        } else {
          owner = bson_utils::get_string(entity, "user");
        }

        if (owner != user.id()) {
          auto id = bson_utils::get_string(entity, "_id");
          throw deepblue_user_exception(ERR_NO_PERMISSION_TO_REMOVE, id);
        }
      }

      void dataset(const int dataset_id, const std::string &norm_genome)
      {
        std::set<std::string> chromosomes = genomes::get_chromosomes(norm_genome);

        // Check if the genome regions collections are empty.
        for (const std::string &internal_chromosome : chromosomes ) {
          std::string collection = helpers::region_collection_name(norm_genome, internal_chromosome);
          helpers::remove_all(collection, MakeDocument(KVP(KeyMapper::DATASET(), dataset_id)));
        }

        helpers::notify_change_occurred("dataset_operations");
      }

      void annotation(const datatypes::User& user, const std::string &id)
      {
        auto maybe_annotation = data::annotation(id);
        if (!maybe_annotation) {
          throw deepblue_user_exception(ERR_INVALID_ANNOTATION_ID, id);
        }

        auto annotation_view = maybe_annotation->view();
        validate_permission(user, annotation_view, true);

        DatasetId dataset_id = annotation_view[KeyMapper::DATASET()].get_int32().value;
        std::string genome_name = bson_utils::get_string(annotation_view, "norm_genome");

        auto q = MakeDocument(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));

        // keep the dataset if it has more than one experiment pointing to this dataset
        // if have one, remove dataset
        if (helpers::count(Collections::ANNOTATIONS(), q) == 1) {
          remove::dataset(dataset_id, genome_name);
        }

        // delete from full text search
        search::remove(id);

        // Delete from collection
        helpers::remove_one(Collections::ANNOTATIONS(), id);

        helpers::notify_change_occurred(Collections::ANNOTATIONS());
      }

      void gene_model(const datatypes::User& user, const std::string &id)
      {
        auto maybe_gene_model = data::gene_model(id);
        if (!maybe_gene_model) {
          throw deepblue_user_exception(ERR_INVALID_GENE_MODEL_ID, id);
        }

        auto gene_model = maybe_gene_model->view();
        validate_permission(user, gene_model, true);

        auto dataset_id = gene_model[KeyMapper::DATASET()].get_int32().value;

        helpers::remove_all(Collections::GENES(), MakeDocument(KVP(KeyMapper::DATASET(), dataset_id)));

        // delete from full text search
        search::remove(id);

        // Delete from collection
        helpers::remove_one(Collections::GENE_MODELS(), id);

        helpers::notify_change_occurred(Collections::GENE_MODELS());
        helpers::notify_change_occurred(Collections::GENES());
      }

      // TODO: move to GeneExpression class
      void gene_expression(const datatypes::User& user, const std::string &id)
      {
        MaybeDocument maybe_gene_expression =datatypes::ExpressionManager::INSTANCE()->GENE_EXPRESSION()->data(id);
        if (!maybe_gene_expression) {
          throw deepblue_user_exception(ERR_INVALID_EXPRESSION_ID, id);
        }

        auto gene_expression = maybe_gene_expression->view();

        validate_permission(user, gene_expression, true);

        auto dataset_id = gene_expression[KeyMapper::DATASET()].get_int32().value;

        auto q = MakeDocument(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));
        helpers::remove_all(Collections::GENE_SINGLE_EXPRESSIONS(), q);

        // delete from full text search
        search::remove(id);

        // Delete from collection
        helpers::remove_one(Collections::GENE_EXPRESSIONS(), id);

        helpers::notify_change_occurred(Collections::GENE_EXPRESSIONS());
        helpers::notify_change_occurred(Collections::GENE_SINGLE_EXPRESSIONS());
      }


      void experiment(const datatypes::User& user, const std::string &id)
      {
        std::vector<std::string> user_projects;
        for (const auto& project : user.projects()) {
          user_projects.push_back(utils::normalize_name(project));
        }

        auto maybe_experiment = data::experiment(id, user_projects);
        if (!maybe_experiment) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, id);
        }

        auto experiment = maybe_experiment->view();

        validate_permission(user, experiment, true);

        auto dataset_id = experiment[KeyMapper::DATASET()].get_int32().value;
        std::string genome_name = bson_utils::get_string(experiment, "norm_genome");


        // TODO: implement
        auto q = MakeDocument(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));
        auto count = helpers::count(Collections::EXPERIMENTS(), q);

        // keep the internal dataset if it has more than one experiment pointing to it
        // if have only one experiment, remove dataset
        if (count == 1) {
          remove::dataset(dataset_id, genome_name);
        }

        // delete from full text search
        search::remove(id);

        // Delete from collection
        helpers::remove_one(Collections::EXPERIMENTS(), id);

        helpers::notify_change_occurred(Collections::EXPERIMENTS());
      }

      void genome(const datatypes::User& user, const std::string &id)
      {
        auto maybe_genome = data::genome(id);
        if (!maybe_genome) {
          throw deepblue_user_exception(ERR_INVALID_GENOME_ID, id);
        }

        auto genome = maybe_genome->view();

        validate_permission(user, genome, false);

        const std::string genome_name = bson_utils::get_string(genome, "norm_name");

        // Check if some experiment is still using this genome
        auto maybe_experiment = helpers::get_one_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("norm_genome", genome_name));
        if (maybe_experiment) {
          auto name = bson_utils::get_string(*maybe_experiment, "name");
          throw deepblue_user_exception(ERR_GENOME_BEING_USED_BY_EXPERIMENT, name);
        }

        // Check if some annotations is still using this genome
        auto annotations = helpers::get_all_by_query(Collections::ANNOTATIONS(),
          bson_utils::one_key_doc("norm_genome", genome_name));
        for (const auto &ann: annotations) {
          auto own_annotation_name = bson_utils::get_string(ann, "norm_name");
          if (own_annotation_name != utils::normalize_name("Chromosomes size for " + genome_name)) {
            auto nice_name = bson_utils::get_string(ann, "name");
            throw deepblue_user_exception(ERR_GENOME_BEING_USED_BY_ANNOTATION, nice_name);
          }
          auto own_annotation_id = bson_utils::get_string(ann, "_id");
          remove::annotation(user, own_annotation_id);
        }

        std::set<std::string> chromosomes = genomes::get_chromosomes(genome_name);

        // Check if the genome regions collections are empty.
        for (const std::string &internal_chromosome : chromosomes) {
          std::string collection = helpers::region_collection_name(genome_name, internal_chromosome);
          auto size = helpers::count(collection, {});
          if (size > 0) {
            throw_runtime_error("Fatal error while removing genome '" + id + "'. Collection '" + collection + "' is not empty. Please, contact the developers");
          }
        }

        // Start deleting the data
        // delete from full text search
        search::remove(id);

        // For chromosome, remove the collection
        // Check if the genome regions collections are empty.
        for (const std::string &internal_chromosome : chromosomes ) {
          std::string collection = helpers::region_collection_name(genome_name, internal_chromosome);
          helpers::remove_collection(collection);
        }

        // Delete genome from genomes collection
        helpers::remove_one(Collections::GENOMES(), id);
        helpers::notify_change_occurred(Collections::GENOMES());
      }


      void project(const datatypes::User& user, const std::string &id)
      {
        std::vector<std::string> user_projects = user.projects();

        auto maybe_project = data::project(id, user_projects);
        if (!maybe_project) {
          throw deepblue_user_exception(ERR_INVALID_PROJECT_ID, id);
        }

        auto project = maybe_project->view();
        validate_permission(user, project, false);

        const std::string project_name = bson_utils::get_string(project, "norm_name");

        // Check if some experiment is still using this project
        auto maybe_experiments = helpers::get_one_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("norm_project", project_name));

        if (maybe_experiments) {
          //  (TODO: show experiments)
          throw deepblue_user_exception(ERR_PROJECT_BEING_USED_BY_EXPERIMENT);
        }

        // Start deleting the data
        // delete from full text search
        search::remove(id);

        // Delete project from projects collection
        helpers::remove_one(Collections::PROJECTS(), id);

        helpers::notify_change_occurred(Collections::PROJECTS());
      }

      void biosource(const datatypes::User& user, const std::string &id)
      {
        auto maybe_biosource = data::biosource(id);
        if (!maybe_biosource) {
          throw deepblue_user_exception(ERR_INVALID_BIOSOURCE_ID);
        }

        auto biosource = maybe_biosource->view();

        validate_permission(user, biosource, false);

        const std::string biosource_name = bson_utils::get_string(biosource, "name");
        const std::string norm_biosource_name = bson_utils::get_string(biosource, "norm_name");

        // Check if some experiment is still using this project
        auto maybe_samples = helpers::get_one_by_query(Collections::SAMPLES(),
          bson_utils::one_key_doc("norm_biosource_name", norm_biosource_name));
        if (maybe_samples) {
          //  (TODO: show experiments)
          throw deepblue_user_exception(ERR_BIOSOURCE_BEING_USED_BY_SAMPLES);
        }

        // TODO: change here
        std::string msg;
        std::vector<std::string> norm_subs;
        if (!cv::get_biosource_children(biosource_name, norm_biosource_name, true, norm_subs, msg)) {
          return;
        }

        // get_biosource_children return at least 1 element , that is the given biosource
        if (norm_subs.size() > 1) {
          throw deepblue_user_exception(ERR_BIOSOURCE_SCOPE_CONTENT);
        }

        cv::remove_biosouce(id, biosource_name, norm_biosource_name, msg);

        helpers::notify_change_occurred(Collections::BIOSOURCES());
      }


      void sample(const datatypes::User& user, const std::string &id)
      {
        auto maybe_sample = data::sample(id);
        if (!maybe_sample) {
          throw deepblue_user_exception(ERR_INVALID_SAMPLE_ID, id);
        }

        auto sample = maybe_sample->view();
        validate_permission(user, sample, false);

        const std::string sample_id = bson_utils::get_string(sample, "_id");;

        // Check if some experiment is still using this project
        std::vector<Document> experiments = helpers::get_all_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("sample_id", sample_id));
        if (!experiments.empty()) {
          //  (TODO: show experiments)
          throw deepblue_user_exception(ERR_SAMPLE_BEING_USED_BY_EXPERIMENT);
        }

        // Start deleting the data
        // delete from full text search
        search::remove(id);

        // Delete sample from samples collection
        helpers::remove_one(Collections::SAMPLES(), id);

        helpers::notify_change_occurred(Collections::SAMPLES());
      }


      void epigenetic_mark(const datatypes::User& user, const std::string &id)
      {
        auto maybe_epigenetic_mark = data::epigenetic_mark(id);
        if (!maybe_epigenetic_mark) {
          throw deepblue_user_exception(ERR_INVALID_EPIGENETIC_MARK_ID, id);
        }

        auto epigenetic_mark = maybe_epigenetic_mark->view();

        validate_permission(user, epigenetic_mark, false);

        const std::string epigenetic_mark_name = bson_utils::get_string(epigenetic_mark, "norm_name");

        // Check if some experiment is still using this project
        auto maybe_experiment = helpers::get_one_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("norm_epigenetic_mark", epigenetic_mark_name));

        if (maybe_experiment) {
          //  (TODO: show experiments)
          throw deepblue_user_exception(ERR_EPIGENETIC_MARK_BEING_USED_BY_EXPERIMENT);
        }

        // Start deleting the data
        // delete from full text search
        search::remove(id);

        // Delete epigenetic mark from epigenetic marks collection
        helpers::remove_one(Collections::EPIGENETIC_MARKS(), id);

        helpers::notify_change_occurred(Collections::EPIGENETIC_MARKS());
      }

      void technique(const datatypes::User& user, const std::string &id)
      {
        auto maybe_technique = data::technique(id);
        if (!maybe_technique) {
          throw deepblue_user_exception(ERR_INVALID_TECHNIQUE_ID, id);
        }

        auto technique = maybe_technique->view();
        validate_permission(user, technique, false);

        const std::string technique_name = bson_utils::get_string(technique, "norm_name");

        // Check if some experiment is still using this project
        auto maybe_experiments = helpers::get_one_by_query(Collections::EXPERIMENTS(),
          bson_utils::one_key_doc("norm_technique", technique_name));
        if (maybe_experiments) {
          //  (TODO: show experiments)
          throw deepblue_user_exception(ERR_TECHNIQUE_BEING_USED_BY_EXPERIMENT);
        }

        // Start deleting the data
        // delete from full text search
        search::remove(id);

        // Delete epigenetic mark from epigenetic marks collection
        helpers::remove_one(Collections::TECHNIQUES(), id);

        helpers::notify_change_occurred(Collections::TECHNIQUES());
      }


      void column_type(const datatypes::User& user, const std::string &id)
      {
        auto maybe_column_type = data::column_type(id);
        if (!maybe_column_type) {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE_ID, id);
        }

        auto column_type = maybe_column_type->view();

        validate_permission(user, column_type, false);

        // delete from full text search
        search::remove(id);

        // Delete from collection
        helpers::remove_one(Collections::COLUMN_TYPES(), id);

        helpers::notify_change_occurred(Collections::COLUMN_TYPES());
      }
    }
  }
}
