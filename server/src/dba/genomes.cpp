//
//  genomes.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 04.04.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm>
#include <ctype.h>
#include <functional>
#include <iostream>
#include <string>
#include <utility>

#include "collections.hpp"
#include "genomes.hpp"
#include "helpers.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"


namespace epidb {
  namespace dba {
    namespace genomes {
      const size_t GenomeInfo::chromosome_size(const std::string &intern_chromosome) const
      {
        auto it = data_.find(intern_chromosome);
        if (it != data_.end()) {
          return it->second;
        } else {
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME, intern_chromosome);
        }
      }

      const std::string GenomeInfo::internal_chromosome(const std::string &chromosome) const
      {
        if (chromosome.empty()) {
          throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_LIST);
        }

        std::string norm_chromosome = utils::normalize_name(chromosome);

        NamesPairs::const_iterator p;

        // Return if the name is correct
        p = names_pair_.find(norm_chromosome);
        if (p != names_pair_.end()) {
          return p->second;
        }

        // Check if the name contains extra "chr". Remove it and check.
        if ((norm_chromosome.size() > 3) && (norm_chromosome.compare(0, 3, "chr") == 0)) {
          std::string tmp = norm_chromosome.substr(3);
          p = names_pair_.find(tmp);
          if (p != names_pair_.end()) {
            return p->second;
          }
        }

        // Final try: Include "chr" in front of the chromosome name.
        std::string tmp = std::string("chr") + norm_chromosome;
        p = names_pair_.find(tmp);
        if (p != names_pair_.end()) {
          return p->second;
        }

        return "";
      }

      const ChromosomeInfo GenomeInfo::get_chromosome(const std::string &name) const
      {
        std::string norm_chromosome = utils::normalize_name(name);
        auto it = data_.find(name);
        if (it != data_.end()) {
          ChromosomeInfo chromosome_info;
          chromosome_info.name = it->first;
          chromosome_info.size = it->second;
          return chromosome_info;
        }

        throw deepblue_user_exception(ERR_INVALID_CHROMOSOME_NAME, norm_chromosome);
      }

      const std::vector<std::string> GenomeInfo::chromosomes() const
      {
        std::vector<std::string> r;

        for (const auto &c : data_) {
          r.push_back(c.first);
        }
        return r;
      }

      NamesPairs GenomeInfo::names_pairs() const
      {
        return this->names_pair_;
      }

      template <class T>
      const std::set<std::string> get_chromosomes(const T &genomes)
      {
        std::set<std::string> chromosomes;
        for (const auto &genome : genomes) {
          auto nc = get_chromosomes(genome);
          chromosomes.insert(nc.begin(), nc.end());
        }
        return chromosomes;
      }

      const std::set<std::string> get_chromosomes(const std::set<std::string> &genomes)
      {
        return get_chromosomes<std::set<std::string>>(genomes);
      }

      const std::set<std::string> get_chromosomes(const std::vector<std::string> &genomes)
      {
        return get_chromosomes<std::vector<std::string>>(genomes);
      }

      const std::set<std::string> get_chromosomes(const std::string &genome)
      {
        GenomeInfoPtr genome_info = get_genome_info(genome);

        std::set<std::string> chromosomes;
        std::vector<std::string> chroms = genome_info->chromosomes();
        for (const auto &chrom : chroms) {
          chromosomes.insert(chrom);
        }
        return chromosomes;
      }

      const std::vector<ChromosomeInfo> get_chromosomes_info(const std::string &genome)
      {
        GenomeInfoPtr genome_info = get_genome_info(genome);

        std::vector<ChromosomeInfo> chromosomes;
        std::vector<std::string> chroms = genome_info->chromosomes();
        for (const auto &chrom : chroms) {
          chromosomes.emplace_back(genome_info->get_chromosome(chrom));
        }
        std::sort(chromosomes.begin(), chromosomes.end());

        return chromosomes;
      }

      const GenomeInfoPtr get_genome_info(const std::string &id_name)
      {
        MaybeDocument maybe_result;
        if (utils::is_id(id_name, "g")) {
           maybe_result = helpers::get_one_by_id(Collections::GENOMES(), id_name);
          if (!maybe_result) {
            throw deepblue_user_exception(ERR_INVALID_GENOME_ID, id_name);
          }
        } else {
          std::string norm_genome = utils::normalize_name(id_name);
          maybe_result =
            helpers::get_one_by_query(Collections::GENOMES(), bson_utils::one_key_doc("norm_name", norm_genome));
          if (!maybe_result) {
            throw deepblue_user_exception(ERR_INVALID_GENOME_NAME, id_name);
          }
        }

        auto result_view = maybe_result->view();
        auto c = result_view["chromosomes"].get_array().value;
        std::string genome_name = bson_utils::get_string(result_view, "name");

        GenomeData data;
        NamesPairs names_pairs;
        for (auto&  e : c) {
          std::string name = StringViewToString(e["name"].get_utf8().value);
          std::string norm_name = utils::normalize_name(name);
          int size = e["size"].get_int32().value;
          data[name] = size;
          names_pairs[norm_name] = name;
        }

        return std::make_shared<GenomeInfo>(genome_name, data, names_pairs);
      }

      const size_t chromosome_size(const std::string &genome, const std::string &chromosome)
      {
        GenomeInfoPtr gi = get_genome_info(genome);
        ChromosomeInfo chromosome_info = gi->get_chromosome(chromosome);
        return  chromosome_info.size;
      }
    }
  }
}