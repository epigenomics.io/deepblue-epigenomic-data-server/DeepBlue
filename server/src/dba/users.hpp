//
//  users.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 03.11.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_DBA_USERS_HPP
#define EPIDB_DBA_USERS_HPP

#include <string>

#include "../datatypes/user.hpp"
#include "../extras/utils.hpp"

namespace epidb {
  namespace dba {
    namespace users {

      const std::string add_user(datatypes::User& user);

      void modify_user(datatypes::User& user);

      void remove_user(const datatypes::User& user);

      const datatypes::User get_user_by_key(const std::string& key);

      const datatypes::User get_user_by_email(const std::string& email, const std::string& password);

      const datatypes::User get_user_by_id(const std::string& id);

      bool is_valid_email(const std::string &email, std::string &msg);

      /*
       * \brief give the user name or Id and receive the ID. Useful for reading some commands inputs
       */
      const std::string get_id(const std::string &user);

      /*
      * \brief  Get user-name for given user-ID
      * \param  user_id     The user-ID
      *        user_name   Return: The user-key
      */
      const std::string get_user_name_by_id(const std::string &user_id);


      const std::string bind_user(const std::string &email, const std::string &password, const std::string &user_key, const std::string &admin_key);

      /*
      * \brief Cleans cache
      */
      void invalidate_cache();

      const datatypes::User get_owner(const std::string& id);
    }
  }
}

#endif
