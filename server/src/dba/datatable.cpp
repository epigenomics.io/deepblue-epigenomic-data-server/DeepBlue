//
//  datatable.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 22.03.2016.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>
#include <vector>

#include "../processing/processing.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/stringbuilder.hpp"

#include "../typedefs/database.hpp"

#include "column_types.hpp"
#include "collections.hpp"
#include "genes.hpp"
#include "helpers.hpp"
#include "list.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace dba {
    namespace datatable {


      std::vector<std::string> format_row(const std::string collection, const std::vector<std::string>& fields, const View& obj)
      {
        std::vector<std::string> row;
        for (const auto& field : fields) {
          std::string s;
          if (field.compare("data_type") == 0) {
            s = StringViewToString(obj["upload_info"]["content_format"].get_utf8().value);
          } else if (field.compare("biosource") == 0) {
            s = StringViewToString(obj["sample_info"]["biosource_name"].get_utf8().value);
          } else if (field.compare("extra_metadata") == 0) {
            StringBuilder sb;

            if (collection == "experiments") {
              sb.append("<div class='exp-metadata'>");
            }

            std::string extra_metadata = utils::format_extra_metadata(obj["extra_metadata"].get_document().view());
            if (!extra_metadata.empty()) {
              sb.append(extra_metadata);
              sb.append("<br/>");
            }

            if (obj.find("upload_info") != obj.end()) {
              sb.append("<b>Upload information</b>:<br/><br/>");

              if (collection == "gene_models") {
                sb.append("<b>number of genes</b>: ");
                sb.append(utils::element_to_string(obj["upload_info"]["total_genes"]));
                sb.append("<br/>");
              } else {
                sb.append("<b>data size</b>: ");
                sb.append(utils::element_to_string(obj["upload_info"]["total_size"]));
                sb.append("kbytes<br/>");
              }

              sb.append("<b>data inserted</b>: ");
              sb.append(utils::element_to_string(obj["upload_info"]["upload_end"]));
              sb.append("<br/>");
            }

            if (obj.find("sample_info") != obj.end()) {
              sb.append("<br/>");
              sb.append("<b>Sample metadata</b><br/><br />");
              sb.append(utils::format_extra_metadata(obj["sample_info"].get_document().view()));
            }
            if (collection == "experiments") {
              sb.append("</div><div class='exp-metadata-more-view'>-- View metadata --</div>");
            }

            s = sb.to_string();

          } else {
            s = utils::element_to_string(obj[field]);
          }
          row.emplace_back(std::move(s));
        }

        return row;
      }



      std::vector<std::string> format_column_type(const View& obj)
      {
        std::vector<std::string> row;

        auto res = columns::dataset_column_to_map(obj);

        row.emplace_back(res["_id"]);
        row.emplace_back(res["name"]);
        row.emplace_back(res["description"]);

        std::string column_type = res["column_type"];

        row.emplace_back(column_type);

        std::string information;
        if (column_type == "category") {
          information = "Acceptable items: " + res["items"];
        } else if (column_type == "range") {
          information = res["minimum"] + " " + res["maximum"];
        } else if (column_type == "calculated") {
          information = res["code"];
        }
        row.emplace_back(information);

        return row;
      }

      std::string element_value(const View& o, const std::string& key)
      {
        if (o.find(key) != o.end()) {
          return bson_utils::get_string(o, key);
        }

        return std::string("");
      }

      std::vector<std::string> format_gene(const View& obj)
      {
        std::vector<std::string> row;
        View attributes = obj[KeyMapper::ATTRIBUTES()].get_document().view();

        row.emplace_back(bson_utils::get_string(obj, "_id"));

        std::string gene_model = genes::get_gene_model_by_dataset_id(obj[KeyMapper::DATASET()].get_int32().value);

        row.emplace_back(gene_model);
        row.emplace_back(element_value(obj, KeyMapper::SOURCE()));
        row.emplace_back(element_value(obj, KeyMapper::CHROMOSOME()));
        row.emplace_back(utils::integer_to_string( obj[KeyMapper::START()].get_int32().value));
        row.emplace_back(utils::integer_to_string( obj[KeyMapper::END()].get_int32().value));
        row.emplace_back(element_value(obj, KeyMapper::STRAND()));
        row.emplace_back(element_value(attributes, "gene_id"));
        row.emplace_back(element_value(attributes, "gene_name"));
        row.emplace_back(element_value(attributes, "gene_type"));
        row.emplace_back(element_value(attributes, "gene_status"));
        row.emplace_back(element_value(attributes, "level"));

        return row;
      }

      std::vector<std::string> format_sample(const View& obj)
      {
        std::vector<std::string> row;

        row.emplace_back(bson_utils::get_string(obj, "_id"));
        row.emplace_back(bson_utils::get_string(obj, "biosource_name"));

        StringBuilder sb;
        for (const auto& e: obj) {
          auto elem_name = bson_utils::get_key(e);
          if (elem_name.compare("type") && elem_name.compare("_id") && elem_name.compare("biosource_name") && elem_name.compare("user") &&
              elem_name.compare(0, 2, "__")  && elem_name.compare(0, 5, "norm_")) {

            std::string content = std::string(utils::element_to_string(e));
            sb.append("<b>");
            sb.append(elem_name);
            sb.append("</b>: ");
            sb.append(content);
            sb.append("<br/>");
          }
        }
        row.emplace_back(sb.to_string());

        return row;
      }

      std::string get_gene_sort_column(std::string name)
      {
        if (name == "_id") {
          return "_id";
        }

        if (name == "gene_model") {
          return KeyMapper::DATASET();
        }

        if (name == "level" || name == "gene_name" || name == "gene_id" || name == "gene_status" || name == "gene_type") {
          return KeyMapper::ATTRIBUTES() + "." + name;
        }

        std::transform(name.begin(), name.end(), name.begin(), ::toupper);

        return name;
      }

      void build_gene_query(const datatypes::Metadata& columns_filters, DocumentBuilder& query_builder)
      {
        for (const auto& column : columns_filters) {
          std::string name = column.first;
          std::string value = column.second;

          if (name == "_id") {
            query_builder.append(KVP("_id", Regex{value, "i"}));

          } else if (name == "level" || name == "gene_name" || name == "gene_id" || name == "gene_status" || name == "gene_type") {
            std::string key = KeyMapper::ATTRIBUTES() + "." + name;
            query_builder.append(KVP(key, Regex{value, "i"})); // case insensitivity and ignore spaces and special characters

          } else if (name == "start" || name == "end") {
            int int_value;
            if (!utils::string_to_int(value, int_value)) {
              continue;
            }
            std::transform(name.begin(), name.end(), name.begin(), ::toupper);

            if (name == "START") {
              query_builder.append(KVP(name, MakeDocument(KVP("$gte", int_value))));
            } else {
              query_builder.append(KVP(name, MakeDocument(KVP("$lte", int_value))));
            }

          } else if (name == "strand") {
            query_builder.append(KVP(KeyMapper::STRAND(), value));

          } else if (name == "gene_model") {
            DocumentBuilder gene_datasets_query_builder;
            gene_datasets_query_builder.append(KVP("norm_name", Regex{value, "i"}));

            Array IDs = helpers::build_dataset_ids_arrays(Collections::GENE_MODELS(), gene_datasets_query_builder.extract());
            query_builder.append(KVP(KeyMapper::DATASET(), MakeDocument(KVP("$in", IDs))));

          } else {
            std::transform(name.begin(), name.end(), name.begin(), ::toupper);
            query_builder.append(KVP(name, Regex{value, "i"})); // case insensitivity and ignore spaces and special characters
          }
        }

      }

      std::vector<std::vector<std::string>> datatable(const datatypes::User& user,
                     const std::string collection, const std::vector<std::string> columns,
                     const long long start, const long long length,
                     const std::string& global_search, const std::string& sort_column, const std::string& sort_direction,
                     const bool has_filter, const datatypes::Metadata& columns_filters,
                     size_t& total_elements)
      {
        std::vector<std::vector<std::string>> results;

        if (!dba::Collections::is_valid_search_collection(collection)) {
          throw deepblue_user_exception(ERR_INVALID_COLLECTION_NAME, collection,
              utils::vector_to_string(dba::Collections::valid_search_Collections()));
        }

        if (start < 0) {
          throw deepblue_user_exception(ERR_INVALID_START, start);
        }

        if (length <= 0) {
          throw deepblue_user_exception(ERR_INVALID_LENGTH, length);
        }


        DocumentBuilder b{};

        if ( (collection != Collections::SAMPLES()) &&
             (collection != Collections::COLUMN_TYPES()) &&
             (collection != Collections::GENES())) {

          for (const std::string & c : columns) {
            if (c == "data_type") {
              b.append(KVP("upload_info.content_format", 1));
            } else if (c == "biosource") {
              b.append(KVP("sample_info.biosource_name", 1));
            } else if (c == "extra_metadata") {
              b.append(KVP("extra_metadata", 1));
              b.append(KVP("sample_info", 1));
              b.append(KVP("upload_info", 1));
            } else {
              b.append(KVP(c, 1));
            }
          }
        }

        DocumentBuilder query_builder{};
        if (!global_search.empty()) {
          DocumentBuilder text_builder;
          text_builder.append(KVP("$search", global_search));
          query_builder.append(KVP("$text", text_builder.extract()));
        }

        if (collection == Collections::GENES()) {
          build_gene_query(columns_filters, query_builder);

        } else {
          for (const auto& filter : columns_filters) {
            if (filter.first == "extra_metadata" && global_search.empty()) {
              DocumentBuilder text_builder;
              text_builder.append(KVP("$search", filter.second));
              query_builder.append(KVP("$text", text_builder.extract()));
            } else if (filter.first == "data_type") {
              query_builder.append(KVP("upload_info.content_format", Regex{filter.second, "ix"}));
            } else if (filter.first == "biosource") {
              query_builder.append(KVP("sample_info.norm_biosource_name", Regex{utils::normalize_name(filter.second), "ix"}));
            } else {
              query_builder.append(KVP(filter.first, Regex{filter.second, "i"}));
            }
          }
        }

        if (collection == Collections::EXPERIMENTS() ||
            collection == Collections::PROJECTS() ||
            collection == Collections::GENE_EXPRESSIONS()) {

          std::vector<std::string> user_projects_names = user.projects();

          if (collection == Collections::PROJECTS()) {
            query_builder.append(KVP("norm_name",
                        MakeDocument(KVP("$in", utils::build_normalized_array(user_projects_names)))));
          } else {
            query_builder.append(KVP("norm_project",
                        MakeDocument(KVP("$in", utils::build_normalized_array(user_projects_names)))));
          }
        }

        DocumentBuilder order_builder;
        int sort = 1;
        if (sort_direction == "desc") {
          sort = -1;
        }

        if (!sort_column.empty()) {
          if (collection == Collections::GENES()) {
            order_builder.append(KVP(get_gene_sort_column(sort_column), sort));
          } else if (sort_column == "data_type") {
            order_builder.append(KVP("upload_info.content_format", sort));
          } else if (sort_column == "biosource") {
            order_builder.append(KVP("sample_info.biosource_name", sort));
          } else {
            order_builder.append(KVP(sort_column, sort));
          }
        }

        auto opts = mongocxx::options::find{};
        opts.sort(order_builder.extract());
        opts.limit(length);
        opts.skip(start);
        opts.projection(b.extract());

        COLLECTION(COLL, collection)
        auto query_obj = query_builder.extract();
        auto cursor = COLL.find(query_obj.view(), opts);

        std::vector<std::vector<std::string>> rows;

        for (const auto& obj: cursor) {
          std::vector<std::string> row;

          if (collection == Collections::SAMPLES()) {
            row = format_sample(obj);

          } else if (collection == Collections::COLUMN_TYPES()) {
            row = format_column_type(obj);

          } else if (collection == Collections::GENES()) {
            row = format_gene(obj);

          } else {
            row = format_row(collection, columns, obj);
          }
          results.emplace_back(std::move(row));
        }

        total_elements = helpers::count(collection, query_obj.view());

        return results;
      }
    }
  }
}
