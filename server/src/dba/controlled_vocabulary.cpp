//
//  controlled_vocabulary.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.08.13.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <list>

#include "../cache/connected_cache.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "controlled_vocabulary.hpp"
#include "collections.hpp"
#include "exists.hpp"
#include "helpers.hpp"
#include "full_text.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"
#include "../log.hpp"

namespace epidb {
  namespace dba {
    namespace cv {

      ConnectedCache biosources_cache;

      bool __get_synonyms_from_biosource(const std::string &id,
                                         const std::string &biosource_name, const std::string &norm_biosource_name,
                                         std::vector<utils::IdName> &syns, std::string &msg)
      {
        utils::IdName id_name_biosource;
        if (id.empty()) {
          id_name_biosource = helpers::get_id_name(Collections::BIOSOURCES(), norm_biosource_name);
        } else {
          id_name_biosource = utils::IdName(id, biosource_name);
        }

        syns.push_back(id_name_biosource);

        COLLECTION(biosource_synonyms_collection, Collections::BIOSOURCE_SYNONYMS())
        auto maybe_syns = biosource_synonyms_collection.find_one(
                                    bson_utils::one_key_doc("norm_name", norm_biosource_name)
                                   );

        if (!maybe_syns) {
          return true;
        }

        auto syn_view = maybe_syns->view();
        auto e = syn_view["synonyms"].get_array().value;
        COLLECTION(biosource_synonym_names_collection, Collections::BIOSOURCE_SYNONYM_NAMES())

        for (const auto & be : e) {
          std::string norm_synonym = StringViewToString(be.get_utf8().value);
          const auto query = bson_utils::one_key_doc("norm_synonym", norm_synonym);
          auto maybe_syns_names = biosource_synonym_names_collection.find_one(query.view());

          if (!maybe_syns_names) {
            throw deepblue_user_exception(ERR_SYNONYM_NAME_NOT_FOUND, norm_synonym);
          }
          auto e_syn = maybe_syns_names->view();
          std::string syn_name = bson_utils::get_string(e_syn, "synonym");
          utils::IdName id_syn_name(id_name_biosource.id, syn_name);
          syns.push_back(id_syn_name);
        }

        return true;
      }

      bool __get_synonyms_from_synonym(const std::string &synonym, const std::string &norm_synonym,
                                       std::vector<utils::IdName> &syns, std::string &msg)
      {
        auto query = bson_utils::one_key_doc("norm_synonym", norm_synonym);

        GET_ORDER(opts, "synonym", 1)
        COLLECTION(biosource_synonym_names_collection, Collections::BIOSOURCE_SYNONYM_NAMES())

        auto maybe_syns = biosource_synonym_names_collection.find_one(query.view(), opts);

        if (!maybe_syns) {
          throw deepblue_user_exception(ERR_SYNONYM_NAME_NOT_FOUND, norm_synonym);
        }

        auto syn_bson = maybe_syns->view();
        std::string biosource_name = bson_utils::get_string(syn_bson, "biosource_name");
        std::string norm_biosource_name = bson_utils::get_string(syn_bson, "norm_biosource_name");

        if (!__get_synonyms_from_biosource("", biosource_name, norm_biosource_name, syns, msg)) {
          return false;
        }

        return true;
      }

      bool __full_text_relation(const std::string &term, const std::string &norm_term,
                                const std::string &biosource, const std::string &norm_biosource,
                                std::string &msg)
      {
        std::vector<std::string> related_terms;

        // Get the synonyms from this term
        std::vector<utils::IdName> syns;
        if (!__get_synonyms_from_biosource("", term, norm_term, syns, msg)) {
          return false;
        }

        for (const utils::IdName & syn : syns) {
          related_terms.push_back(syn.name);
        }

        auto search_terms = search::get_related_terms(term, norm_term,
                                       "norm_name", "biosources");

        related_terms.insert(end(related_terms), begin(search_terms), end(search_terms));
        // Get the sub terms
        std::vector<std::string> norm_subs;
        if (!get_biosource_children(biosource, norm_biosource, true, norm_subs, msg)) {
          return false;
        }

        std::vector<utils::IdName> id_names;
        for (const std::string & norm_sub : norm_subs) {
          std::string biosource_id = helpers::get_id(Collections::BIOSOURCES(), norm_sub);
          id_names.push_back(utils::IdName(biosource_id, norm_sub));
        }

        // Update
        for (auto &id_name : id_names) {
          search::insert_related_term(id_name, related_terms);
        }

        return true;
      }


      // TODO: Maybe merge/reuse __get_synonyms_from_synonym ?
      bool get_synonym_root(const std::string &synonym, const std::string &norm_synonym,
                            std::string &biosource_name, std::string &norm_biosource_name, std::string &msg)
      {
        auto query = bson_utils::one_key_doc("norm_synonym", norm_synonym);

        GET_ORDER(opts, "synonym", 1)
        COLLECTION(biosource_synonym_names_collection, Collections::BIOSOURCE_SYNONYM_NAMES())

        auto maybe_syns = biosource_synonym_names_collection.find_one(query.view(), opts);

        if (!maybe_syns) {
          throw deepblue_user_exception(ERR_BIOSOURCE_SYNONYM_ROOT_NOT_FOUND, synonym);
        }

        auto syn_view = maybe_syns->view();
        biosource_name = bson_utils::get_string(syn_view, "biosource_name");
        norm_biosource_name = bson_utils::get_string(syn_view, "norm_biosource_name");

        return true;
      }


      bool __set_biosource_synonym(const datatypes::User& user,
                                   const std::string &input_biosource_name, const std::string &synonym,
                                   bool is_biosource, const bool is_syn, std::string &msg)
      {
        std::string biosource_name;
        std::string norm_biosource_name;

        if (is_syn) {
          std::string norm_input_biosource_name = utils::normalize_name(input_biosource_name);
          if (!get_synonym_root(input_biosource_name, norm_input_biosource_name,
                                biosource_name, norm_biosource_name, msg)) {
            return false;
          }
        } else {
          biosource_name = input_biosource_name;
          norm_biosource_name = utils::normalize_name(input_biosource_name);
        }

        std::string norm_synonym = utils::normalize_name(synonym);

        auto query = bson_utils::one_key_doc("norm_name", norm_biosource_name);
        auto value = bson_utils::one_key_doc("synonyms", norm_synonym);

        DocumentBuilder append_value;
        if (is_biosource) {
          DocumentBuilder syn_insert_builder;
          syn_insert_builder.append(KVP("name", input_biosource_name));
          syn_insert_builder.append(KVP("norm_name", norm_biosource_name));

          append_value.append(KVP("$set", syn_insert_builder.extract()));
          append_value.append(KVP("$addToSet", value));
        } else {
          append_value.append(KVP("$addToSet", value));
        }
        auto to_append = append_value.extract();

        COLLECTION(biosource_synonyms_collection, Collections::BIOSOURCE_SYNONYMS())
        EXECUTE_UPSERT(biosource_synonyms_collection, query.view(), to_append.view(), upsert_result)

        DocumentBuilder syn_builder;
        syn_builder.append(KVP("synonym", synonym));
        syn_builder.append(KVP("norm_synonym", norm_synonym));
        syn_builder.append(KVP("biosource_name", biosource_name));
        syn_builder.append(KVP("norm_biosource_name", norm_biosource_name));
        syn_builder.append(KVP("user", user.id()));
        syn_builder.append(KVP("public", true));

        COLLECTION(biosource_synonym_names_collection, Collections::BIOSOURCE_SYNONYM_NAMES())
        biosource_synonym_names_collection.insert_one(syn_builder.extract());

        if (!__full_text_relation(biosource_name, norm_biosource_name,
                                  biosource_name, norm_biosource_name,
                                  msg)) {
          return false;
        }

        return true;
      }

      bool set_biosource_synonym_complete(const datatypes::User& user,
                                          const std::string &biosource_name, const std::string &synonym_name,
                                          std::string& msg)
      {

        const std::string norm_biosource_name = utils::normalize_name(biosource_name);

        // TODO Move to a helper function: get_biosource_root
        // Check if the actual biosource exists
        bool is_biosource = exists::biosource(norm_biosource_name);
        bool is_syn = exists::biosource_synonym(norm_biosource_name);

        if (!(is_biosource || is_syn)) {
          msg = Error::m(ERR_INVALID_BIOSOURCE_NAME, biosource_name);
          return false;
        }

        // TODO Move to a helper function: get_biosource_root
        // Check if synonym name is already being user
        std::string norm_synoynm_name = utils::normalize_name(synonym_name);
        bool syn_is_biosource = exists::biosource(norm_synoynm_name);
        bool syn_is_syn = exists::biosource_synonym(norm_synoynm_name);

        if (syn_is_biosource || syn_is_syn) {
          msg = Error::m(ERR_INVALID_BIOSOURCE_SYNONYM, synonym_name);
          return false;
        }

        return __set_biosource_synonym(user, biosource_name, synonym_name, is_biosource, is_syn, msg);
      }

      bool get_biosource_synonyms(const std::string &id, const std::string &biosource_name,
                                  const std::string &norm_biosource_name,
                                  bool is_biosource,
                                  std::vector<utils::IdName> &syns, std::string &msg)
      {
        if (is_biosource) {
          return __get_synonyms_from_biosource(id, biosource_name, norm_biosource_name, syns, msg);
        } else {
          return __get_synonyms_from_synonym(biosource_name, norm_biosource_name, syns, msg);
        }
      }

      bool __is_connected(const std::string &norm_s1, const std::string &norm_s2,
                          const bool recursive,
                          bool &r, std::string &msg)
      {
        if (biosources_cache.is_connected(norm_s1, norm_s2)) {
          r = true;
          return true;
        }

        DocumentBuilder syn_query_builder;
        syn_query_builder.append(KVP("norm_biosource_name", norm_s1));

        auto query_obj = syn_query_builder.extract();
        COLLECTION(biosource_embracing_collection, Collections::BIOSOURCE_EMBRACING())
        auto maybe_syns = biosource_embracing_collection.find_one(query_obj.view());

        if (!maybe_syns) {
          r = false;
          return true;
        }

        auto syn_bson = maybe_syns->view();
        auto e = syn_bson["subs"].get_array().value;

        std::list<std::string> subs;

        for (const auto & be : e) {
          std::string sub = StringViewToString(be.get_utf8().value);
          if (sub == norm_s2) {
            r = true;
            biosources_cache.set_connection(norm_s1, norm_s2);
            biosources_cache.set_connection(sub, norm_s2);
            return true;
          }
          subs.push_back(sub);
        }

        if (recursive) {
          for (const std::string & norm_sub : subs) {
            bool rr;
            if (!__is_connected(norm_sub, norm_s2, recursive, rr, msg)) {
              return false;
            }
            if (rr) {
              r = true;
              biosources_cache.set_connection(norm_s1, norm_s2);
              biosources_cache.set_connection(norm_sub, norm_s2);
              return true;
            }
          }
        }

        r = false;
        return true;
      }

      bool set_biosource_parent(const datatypes::User& user,
                                const std::string &biosource_more_embracing, const std::string &norm_biosource_more_embracing,
                                const std::string &biosource_less_embracing, const std::string &norm_biosource_less_embracing,
                                bool more_embracing_is_syn, const bool less_embracing_is_syn,
                                std::string &msg)
      {
        std::string more_embracing_root;
        std::string norm_more_embracing_root;
        std::string less_embracing_root;
        std::string norm_less_embracing_root;

        if (more_embracing_is_syn) {
          if (!get_synonym_root(biosource_more_embracing, norm_biosource_more_embracing,
                                more_embracing_root, norm_more_embracing_root, msg)) {
            return false;
          }
        } else {
          more_embracing_root = biosource_more_embracing;
          norm_more_embracing_root = norm_biosource_more_embracing;
        }

        if (less_embracing_is_syn) {
          if (!get_synonym_root(biosource_less_embracing, norm_biosource_less_embracing,
                                less_embracing_root, norm_less_embracing_root, msg)) {
            return false;
          }
        } else {
          less_embracing_root = biosource_less_embracing;
          norm_less_embracing_root = norm_biosource_less_embracing;
        }

        if (norm_more_embracing_root == norm_less_embracing_root) {
          throw deepblue_user_exception(ERR_BIOSOURCE_SAME, biosource_more_embracing, biosource_less_embracing);
        }

        bool is_connected(false);
        if (!__is_connected(norm_more_embracing_root, norm_less_embracing_root, false, is_connected, msg)) {
          return false;
        }

        if (is_connected) {
          throw deepblue_user_exception(ERR_ALREADY_CONECTED_BIOSOURCE_NAME,
                                   biosource_more_embracing, biosource_less_embracing);
        }

        if (!__is_connected(norm_less_embracing_root, norm_more_embracing_root, true, is_connected, msg)) {
          return false;
        }

        if (is_connected) {
          throw deepblue_user_exception(ERR_ALREADY_CONECTED_BIOSOURCE_NAME,
                                    biosource_more_embracing, biosource_less_embracing);
        }

        auto query = bson_utils::one_key_doc("norm_biosource_name", norm_more_embracing_root);
        auto append_value = DocumentStreamBuilder{} <<
          "$addToSet" << Open <<
            "subs" << norm_less_embracing_root
          << Close << StreamFinalize;

        COLLECTION(biosource_embracing_collection, Collections::BIOSOURCE_EMBRACING())

        EXECUTE_UPSERT(biosource_embracing_collection, query.view(), append_value.view(), result)

        if (!result) {
            throw_runtime_error("Problem setting BioSource parent");
        }


        if (!__full_text_relation(more_embracing_root, norm_more_embracing_root,
                                  less_embracing_root, norm_less_embracing_root, msg)) {
          return false;
        }

        biosources_cache.set_connection(norm_biosource_more_embracing, norm_biosource_less_embracing);

        return true;
      }

      bool __get_down_connected(const std::string &norm_s1,
                                std::vector<std::string> &norm_names, std::string &msg)
      {
        norm_names.push_back(norm_s1);

        COLLECTION(biosource_embracing_collection, Collections::BIOSOURCE_EMBRACING());
        auto maybe_syn = biosource_embracing_collection.find_one(bson_utils::one_key_doc("norm_biosource_name", norm_s1));

        if (!maybe_syn) {
          return true;
        }

        auto syn_view = maybe_syn->view();
        auto e = syn_view["subs"].get_array().value;

        for (const auto & be : e) {
          std::string sub = StringViewToString(be.get_utf8().value);
          if (!__get_down_connected(sub, norm_names, msg)) {
            return false;
          }
        }

        return true;
      }

      bool get_biosource_children(const std::string &biosource_name, const std::string &norm_biosource_name,
                                  bool is_biosource,
                                  std::vector<std::string> &norm_subs, std::string &msg)
      {
        std::string more_embracing_root;
        std::string norm_more_embracing_root;

        if (!is_biosource) {
          if (!get_synonym_root(biosource_name, norm_biosource_name,
                                more_embracing_root, norm_more_embracing_root, msg)) {
            return false;
          }
        } else {
          norm_more_embracing_root = norm_biosource_name;
        }

        if (!__get_down_connected(norm_more_embracing_root, norm_subs, msg)) {
          return false;
        }
        return true;
      }

      bool __get_upper_connected(const std::string &norm_s1,
                                 std::vector<std::string> &norm_uppers, std::string &msg)
      {
        COLLECTION(biosource_embracing_collection, Collections::BIOSOURCE_EMBRACING())
        auto upper_cursor = biosource_embracing_collection.find(bson_utils::one_key_doc("subs", norm_s1));

        for (const auto& upper_bson: upper_cursor) {
          std::string e = bson_utils::get_string(upper_bson, "norm_biosource_name");
          norm_uppers.push_back(e);
        }

        return true;
      }

      bool get_biosource_parents(const std::string &biosource_name, const std::string &norm_biosource_name,
                                 bool is_biosource,
                                 std::vector<std::string> &norm_uppers, std::string &msg)
      {
        std::string more_embracing_root;
        std::string norm_more_embracing_root;

        if (!is_biosource) {
          if (!get_synonym_root(biosource_name, norm_biosource_name,
                                more_embracing_root, norm_more_embracing_root, msg)) {
            return false;
          }
        } else {
          norm_more_embracing_root = norm_biosource_name;
        }

        if (!__get_upper_connected(norm_more_embracing_root, norm_uppers, msg)) {
          return false;
        }
        return true;
      }

      bool remove_biosouce(const std::string &id, const std::string &biosource_name, const std::string &norm_biosource_name, std::string &msg)
      {
        // Start deleting the data
        // delete from full text search
        search::remove(id);

        COLLECTION(text_search_collection, Collections::TEXT_SEARCH())

        text_search_collection.update_many(
          DocumentStreamBuilder{} << "related_terms" << norm_biosource_name << StreamFinalize,
          DocumentStreamBuilder{} << "$pull" << Open << "related_terms" << norm_biosource_name << Close  << StreamFinalize
        );

        // Remove from embracing collection // useless. just for sake
        helpers::remove_all(
            Collections::BIOSOURCE_EMBRACING(),
            bson_utils::one_key_doc("norm_biosource_name", norm_biosource_name)
        );

        // Remove from the others biosources scope
        COLLECTION(biosources_embracing_collection, Collections::BIOSOURCE_EMBRACING())
        biosources_embracing_collection.update_many(
          DocumentStreamBuilder{} << "subs" << norm_biosource_name << StreamFinalize,
          DocumentStreamBuilder{} << "$pull" << Open << "subs" << norm_biosource_name << Close  << StreamFinalize
        );

        // remove from synonyms collection
        helpers::remove_all(
          Collections::BIOSOURCE_SYNONYMS(),
          bson_utils::one_key_doc("norm_name", norm_biosource_name)
        );

        // remove from synonyms names collection
        helpers::remove_all(
          Collections::BIOSOURCE_SYNONYM_NAMES(),
          bson_utils::one_key_doc("norm_biosource_name", norm_biosource_name)
        );

        // remove itself
        helpers::remove_one(
          Collections::BIOSOURCES(),
          id
        );

        return true;
      }
    }
  }
}
