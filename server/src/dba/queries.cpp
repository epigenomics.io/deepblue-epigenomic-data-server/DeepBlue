//
//  queries.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 09.07.13.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <ctime>
#include <cstring>
#include <set>
#include <iterator>
#include <unordered_map>
#include <utility>

#include <strtk.hpp>

#include <boost/thread.hpp>

#include "../algorithms/algorithms.hpp"
#include "../algorithms/filter.hpp"

#include "../cache/column_dataset_cache.hpp"
#include "../cache/queries_cache.hpp"

#include "../datatypes/column_types_def.hpp"
#include "../datatypes/expressions_manager.hpp"
#include "../datatypes/regions.hpp"

#include "../dba/experiments.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../parser/parser_factory.hpp"

#include "../processing/running_cache.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "annotations.hpp"
#include "collections.hpp"
#include "dba.hpp"
#include "genes.hpp"
#include "genomes.hpp"
#include "helpers.hpp"
#include "list.hpp"
#include "key_mapper.hpp"
#include "metafield.hpp"
#include "retrieve.hpp"
#include "users.hpp"

#include "queries.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"
#include "../log.hpp"
#include "../macros.hpp"

namespace epidb {
  namespace dba {
    namespace query {

      // TODO: Merge wtth users cache and use templates
      class NameDatasetIdCache {
      private:
        boost::shared_mutex _access;
        std::unordered_map<std::string, DatasetId> name_dataset_id;
        std::unordered_map<DatasetId, std::string> dataset_id_name;

      public:
        DatasetId get_dataset_id(const std::string &name)
        {
          boost::shared_lock<boost::shared_mutex> lock(_access);
          return name_dataset_id[name];
        }

        const std::string &get_name(const DatasetId id)
        {
          boost::shared_lock<boost::shared_mutex> lock(_access);
          return dataset_id_name[id];
        }

        void set(const std::string &name, const DatasetId id)
        {
          boost::unique_lock<boost::shared_mutex> lock(_access);
          name_dataset_id[name] = id;
          dataset_id_name[id] = name;
        }

        bool exists_dataset_id(const std::string &name)
        {
          boost::shared_lock<boost::shared_mutex> lock(_access);
          return name_dataset_id.find(name) != name_dataset_id.end();
        }

        bool exists_name(const DatasetId id)
        {
          boost::shared_lock<boost::shared_mutex> lock(_access);
          return dataset_id_name.find(id) != dataset_id_name.end();
        }

        void invalidate()
        {
          boost::unique_lock<boost::shared_mutex> lock(_access);
          name_dataset_id.clear();
        }
      };

      NameDatasetIdCache experiment_name_dataset_id_cache;
      NameDatasetIdCache annotation_pattern_cache;

      void invalidate_cache()
      {
        experiment_name_dataset_id_cache.invalidate();
        annotation_pattern_cache.invalidate();
      }

      Document reduce_args(const View &args)
      {
        // Keys to be removed
        std::set<std::string> keys = {"annotation",
                                      "genome", "genomes",
                                      "experiment_name", "epigenetic_mark",
                                      "project", "technique"};
        DocumentBuilder bob;
        for (const auto &e : args) {
          std::string field_name = bson_utils::get_key(e);
          if (keys.find(field_name) == keys.end()) {
            bob.append(KVP(e.key(), e.get_value()));
          }
        }

        return bob.extract();
      }

      const std::string store_query(const datatypes::User &user,
                                    const std::string &type, const View &args, const std::string& derived_from)
      {
        DocumentBuilder search_query_builder;

        auto query_args = reduce_args(args);

        search_query_builder.append(KVP("type", type));
        search_query_builder.append(KVP("user", user.id()));
        search_query_builder.append(KVP("query_args", query_args));

        auto query_args_view = query_args.view();
        if (bson_utils::has_key(query_args, "norm_experiment_name")) {
          auto experiments = query_args_view["norm_experiment_name"].get_array().value;
          search_query_builder.append(KVP("query_args.norm_experiment_name", experiments));
        }

        auto search_query = search_query_builder.extract();
        auto maybe_result = helpers::get_one_by_query(Collections::QUERIES(), search_query);
        if (maybe_result) {
          return bson_utils::get_string(maybe_result->view(), "_id");
        }

        auto query_counter = helpers::get_increment_counter(Collections::QUERIES());
        auto query_id = "q" + utils::integer_to_string(query_counter);

        DocumentBuilder stored_query_builder;

        stored_query_builder.append(KVP("_id", query_id));
        stored_query_builder.append(KVP("type", type));
        stored_query_builder.append(KVP("user", user.id()));
        stored_query_builder.append(KVP("time", Date(std::chrono::system_clock::now())));
        if (!derived_from.empty()) {
          stored_query_builder.append(KVP("derived_from", derived_from));
        }
        stored_query_builder.append(KVP("args", args));
        stored_query_builder.append(KVP("query_args", query_args));

        COLLECTION(queries_collection, Collections::QUERIES())

        queries_collection.insert_one(stored_query_builder.extract());

        return query_id;
      }

      const std::string modify_query(const datatypes::User &user,
                                     const std::string &query_id, const std::string &key, const std::string &value)
      {
        auto maybe_old_query = helpers::get_one_by_id(Collections::QUERIES(), query_id);
        if (!maybe_old_query) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, query_id);
        }

        auto old_query_view = maybe_old_query->view();
        if (bson_utils::get_string(old_query_view, "user") != user.id()) {
          throw deepblue_user_exception(ERR_PERMISSION_QUERY, query_id);
        }

        auto old_args = old_query_view["args"].get_document().value;
        DocumentBuilder new_args_builder;


        // Code for dealing with keys in the format xxx.zzz
        // It supports only 2 level of inner documents.
        std::string main;
        std::string sub;
        bool has_dot = strtk::parse(key, ".", main, sub);
        if (has_dot) {
          DocumentBuilder main_arg_builder{};
          main_arg_builder.append(KVP(sub, value));

          if (bson_utils::has_key(old_args, main)) {
            auto main_old_args = old_args[main].get_document().value;
            for (const auto &main_old_e : main_old_args) {
              auto old_args_key = bson_utils::get_key(main_old_e);
              if (old_args_key != sub) {
                main_arg_builder.append(KVP(main_old_e.key(), main_old_e.get_value()));
              }
            }
          }

          new_args_builder.append(KVP(main, main_arg_builder.extract()));
        } else {
          new_args_builder.append(KVP(key, value));
        }

        for (const auto &old_e : old_args) {
          auto old_args_key = bson_utils::get_key(old_e);
          if ((old_args_key != key) && (old_args_key != main)) {
            new_args_builder.append(KVP(old_e.key(), old_e.get_value()));
          }
        }

        return store_query(user,
          bson_utils::get_string(old_query_view, "type"),
          new_args_builder.extract(), query_id);
      }

      const ChromosomeRegionsList retrieve_query(const datatypes::User &user, const std::string &query_id,
                                                 processing::StatusPtr status, bool reduced_mode)
      {
        START_OPERATION(status, processing::PROCESS_QUERY, View());

        auto maybe_query = helpers::get_one_by_id(Collections::QUERIES(), query_id);
        if (!maybe_query) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, query_id);
        }

        auto query = maybe_query->view();
        const auto args = query["args"].get_document().value;
        if (bson_utils::has_key(args, "cache") &&
            bson_utils::get_string(args, "cache") == "yes") {

          return cache::get_query_cache(user, bson_utils::get_string(query, "derived_from"), status);
        }

        const std::string type = bson_utils::get_string(query, "type");

        if (type == "experiment_select") {
          return retrieve_experiment_select_query(user, query, status, reduced_mode);
        } else if (type == "intersect") {
          return retrieve_intersection_query(user, query, status);
        } else if (type == "overlap") {
          return retrieve_overlap_query(user, query, status);
        } else if (type == "flank") {
          return retrieve_flank_query(user, query, status);
        } else if (type == "extend") {
          return retrieve_extend_query(user, query, status);
        } else if (type == "merge") {
          return retrieve_merge_query(user, query, status);
        } else if (type == "annotation_select") {
          return retrieve_annotation_select_query(user, query, status);
        } else if (type == "genes_select") {
          return retrieve_genes_select_query(user, query, status);
        } else if (type == "find_motif") {
          return retrieve_find_motif_query(user, query, status);
        } else if (type == "expressions_select") {
          return retrieve_expression_select_query(user, query, status);
        } else if (type == "filter") {
          return retrieve_filter_query(user, query, status);
        } else if (type == "filter_by_motif") {
          return retrieve_filter_by_motif_query(user, query, status);
        } else if (type == "tiling") {
          return retrieve_tiling_query(query, status);
        } else if (type == "aggregate") {
          return process_aggregate(user, query, status);
        } else if (type == "input_regions") {
          return retrieve_query_region_set(query, status);
        } else {
          throw deepblue_user_exception(ERR_UNKNOW_QUERY_TYPE, type);
        }
      }

      const std::vector<utils::IdName> get_experiments_by_query(const datatypes::User &user, const std::string &query_id, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::GET_EXPERIMENT_BY_QUERY, View());

        ChromosomeRegionsList chromossome_regions = retrieve_query(user, query_id, status);

        std::set<DatasetId> datasets_it_set;
        for (auto &regions : chromossome_regions) {
          for (auto &region : regions.second) {
            datasets_it_set.insert(region->dataset_id());
          }
        }

        std::vector<utils::IdName> datasets;
        for (const DatasetId &dataset_id : datasets_it_set) {
          auto obj = cache::get_document_by_dataset_id(dataset_id);
          if (!obj) {
            throw deepblue_user_exception(ERR_DATASET_NOT_FOUND, dataset_id);
          }

          auto obj_view = obj->view();

          std::string _id = bson_utils::get_string(obj_view, "_id");
          std::string name = bson_utils::get_string(obj_view, "name");
          utils::IdName p(_id, name);
          datasets.push_back(p);
        }

        return datasets;
      }

      const size_t count_regions(const datatypes::User &user,
                                 const std::string &query_id, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::COUNT_REGIONS, View());

        auto results = helpers::get_one_by_query(Collections::QUERIES(), bson_utils::id_doc(query_id));
        if (!results) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, query_id);
        }

        ChromosomeRegionsList regions = retrieve_query(user, query_id, status, true);

        return count_regions(regions);
      }

      Document build_experiment_query(const int start, const int end, const std::string &norm_experiment_name)
      {
        DatasetId dataset_id;

        if (experiment_name_dataset_id_cache.exists_dataset_id(norm_experiment_name)) {
          dataset_id = experiment_name_dataset_id_cache.get_dataset_id(norm_experiment_name);
        } else {
          auto maybe_experiment = dba::experiments::by_name(norm_experiment_name);
          if (!maybe_experiment) {
            throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, norm_experiment_name);
          }

          auto experiment_view = maybe_experiment->view();

          dataset_id = experiment_view[KeyMapper::DATASET()].get_int32().value;
          experiment_name_dataset_id_cache.set(norm_experiment_name, dataset_id);
        }

        DocumentBuilder regions_query_builder;
        regions_query_builder.append(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));

        if (end > -1) {
          regions_query_builder.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", static_cast<int32_t>(end)))));
        }

        if (start > -1) {
          regions_query_builder.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", static_cast<int32_t>(start)))));
        }

        return regions_query_builder.extract();
      }

      Document build_experiment_query(const int start, const int end, const ArrayView &datasets_array)
      {
        DocumentBuilder regions_query_builder;

        regions_query_builder.append(KVP(KeyMapper::DATASET(), MakeDocument(KVP("$in", datasets_array))));
        regions_query_builder.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", static_cast<int32_t>(end)))));
        regions_query_builder.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", static_cast<int32_t>(start)))));
        return regions_query_builder.extract();
      }

      Document build_experiment_query(const datatypes::User &user, const View &args)
      {
        auto args_query = list::build_list_experiments_bson_query(user, args);

        auto datasets_array = helpers::build_dataset_ids_arrays(Collections::EXPERIMENTS(), args_query);

        DocumentBuilder regions_query_builder;
        regions_query_builder.append(KVP(KeyMapper::DATASET(), MakeDocument(KVP("$in", datasets_array))));

        int32_t start;
        int32_t end;
        bool has_start = bson_utils::has_key(args, "start");
        bool has_end = bson_utils::has_key(args, "end");

        if (has_start) {
          start = args["start"].get_int32().value;
        }
        if (has_end) {
          end = args["end"].get_int32().value;
        }

        if (has_start && has_end) {
          regions_query_builder.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", end))));
          regions_query_builder.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", start))));
        } else if (has_start) {
          regions_query_builder.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", start))));
        } else if (has_end) {
          regions_query_builder.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", end))));
        }

        return regions_query_builder.extract();
      }

      Document build_annotation_query(const View &query)
      {
        auto args = query["args"].get_document().value;

        DocumentBuilder annotations_query_builder;

        // TODO: do the same as build_experiment_query, not verifying the type()
        if (args["norm_annotation"].type() == ArrayType) {
          annotations_query_builder.append(KVP("norm_name", MakeDocument(KVP("$in", args["norm_annotation"].get_array()))));
        } else {
          annotations_query_builder.append(KVP("norm_name", bson_utils::get_string(args, "norm_annotation")));
        }
        annotations_query_builder.append(KVP("upload_info.done", true));

        Document annotation_query = annotations_query_builder.extract();
        Array datasets_array = helpers::build_dataset_ids_arrays(Collections::ANNOTATIONS(), annotation_query);

        DocumentBuilder regions_query_builder;
        regions_query_builder.append(KVP(KeyMapper::DATASET(), MakeDocument(KVP("$in", datasets_array.view()))));

        bool has_start = bson_utils::has_key(args, "start");
        bool has_end = bson_utils::has_key(args, "end");

        int32_t start;
        int32_t end;

        if (has_start) {
          start = args["start"].get_int32().value;
        }
        if (has_end) {
          end = args["end"].get_int32().value;
        }

        if (has_start && has_end) {
          regions_query_builder.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", end))));
          regions_query_builder.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", start))));
        } else if (has_start) {
          regions_query_builder.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", start))));
        } else if (has_end) {
          regions_query_builder.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", end))));
        }

        return regions_query_builder.extract();
      }

      const ChromosomeRegionsList retrieve_experiment_select_query(const datatypes::User &user,
                                                                   const View &query, processing::StatusPtr status, bool reduced_mode)
      {
        START_OPERATION(status, processing::RETRIEVE_EXPERIMENT_SELECT_QUERY, query);

        auto args = query["args"].get_document().value;
        auto regions_query = build_experiment_query(user, args);

        std::set<std::string> genomes;
        if (bson_utils::has_key(args, "norm_genome")) {
          genomes = utils::build_set(args["norm_genome"].get_array().value);
        } else {
          std::vector<std::string> norm_names = utils::build_vector(args["norm_experiment_name"].get_array().value);
          for (auto experiment_norm_name : norm_names) {
            auto maybe_genome = dba::experiments::get_genome(experiment_norm_name);
            if (!maybe_genome) {
              throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, experiment_norm_name);
            }
            genomes.insert(*maybe_genome);
          }
        }

        std::vector<std::string> chromosomes;
        if (bson_utils::has_key(args, "chromosomes")) {
          chromosomes = utils::build_vector(args["chromosomes"].get_array().value);
        } else {
          std::set<std::string> chrom = dba::genomes::get_chromosomes(genomes);
          chromosomes = std::vector<std::string>(chrom.begin(), chrom.end());
        }

        std::vector<ChromosomeRegionsList> genome_regions;
        for (const auto &genome : genomes) {
          ChromosomeRegionsList reg = retrieve::get_regions(genome, chromosomes, regions_query, false, status);
          genome_regions.push_back(std::move(reg));
        }

        ChromosomeRegionsList regions;
        // merge region data of all genomes
        auto rit = genome_regions.begin();
        if (rit == genome_regions.end()) {
          return regions;
        }

        ChromosomeRegionsList &last = *rit;
        rit++;
        for (; rit != genome_regions.end(); ++rit) {
          last = algorithms::merge_chromosome_regions(last, *rit);
        }
        return last;
      }

      const ChromosomeRegionsList retrieve_query_region_set(const View &query, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_QUERY_REGION_SET, query);
        auto args = query["args"].get_document().value;

        auto regions_query = MakeDocument(KVP(KeyMapper::DATASET(), args["dataset_id"].get_int32().value));

        std::vector<std::string> chromosomes = utils::build_vector(args["chromosomes"].get_array().value);
        std::string genome = bson_utils::get_string(args, "norm_genome");

        return retrieve::get_regions(genome, chromosomes, regions_query, false, status);
      }

      const ChromosomeRegionsList retrieve_annotation_select_query(const datatypes::User &user, const View &query,
                                                                   processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_ANNOTATION_SELECT_QUERY, query);

        auto regions_query = build_annotation_query(query);

        auto args = query["args"].get_document().value;

        ArrayBuilder genome_arr_builder;
        ArrayView genome_arr;

        if (bson_utils::has_key(args, "norm_genome")) {
          auto e = args["norm_genome"];
          if (e.type() == ArrayType) {
            genome_arr = args["norm_genome"].get_array().value;
          } else {
            genome_arr_builder.append(args["norm_genome"].get_value());
            genome_arr = genome_arr_builder.extract();
          }
        }

        std::vector<ChromosomeRegionsList> genome_regions;
        ArrayView::iterator git;

        for (const auto& git: genome_arr) {
          std::string genome = StringViewToString(git.get_utf8().value);

          std::vector<std::string> chromosomes;
          if (bson_utils::has_key(args, "chromosomes")) {
            chromosomes = utils::build_vector(args["chromosomes"].get_array().value);
          } else {
            std::set<std::string> chrom = dba::genomes::get_chromosomes(genome);
            chromosomes = std::vector<std::string>(chrom.begin(), chrom.end());
          }

          ChromosomeRegionsList reg = retrieve::get_regions(genome, chromosomes, regions_query, false, status);
          genome_regions.push_back(std::move(reg));
        }

        // merge region data of all genomes
        std::vector<ChromosomeRegionsList>::iterator rit = genome_regions.begin();
        ChromosomeRegionsList &last = *rit;
        rit++;
        for (; rit != genome_regions.end(); ++rit) {
          last = algorithms::merge_chromosome_regions(last, *rit);
        }
        return last;
      }

      const ChromosomeRegionsList retrieve_genes_select_query(const datatypes::User &user, const View &query,
                                                              processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_GENES_DATA, query);

        auto args_view = query["args"].get_document().value;

        std::vector<std::string> genes;
        if (bson_utils::has_key(args_view, "genes")) {
          genes = utils::build_vector(args_view["genes"].get_array().value);
        }

        std::vector<std::string> go_terms;
        if (bson_utils::has_key(args_view, "go_terms")) {
          go_terms = utils::build_vector(args_view["go_terms"].get_array().value);
        }

        std::string gene_model;
        if (bson_utils::has_key(args_view, "gene_model")) {
          gene_model = bson_utils::get_string(args_view, "gene_model");
        }

        std::vector<std::string> chromosomes;
        if (bson_utils::has_key(args_view, "chromosomes")) {
          chromosomes = utils::build_vector(args_view["chromosomes"].get_array().value);
        }

        int start;
        int end = std::numeric_limits<Position>::max();

        if (bson_utils::has_key(args_view, "start")) {
          start = args_view["start"].get_int32().value;
        } else {
          start = std::numeric_limits<Position>::min();
        }

        if (bson_utils::has_key(args_view, "end")) {
          end = args_view["end"].get_int32().value;
        } else {
          end = std::numeric_limits<Position>::max();
        }

        return genes::get_genes_from_database(chromosomes, start, end, "", genes, go_terms, gene_model);
      }

      const ChromosomeRegionsList retrieve_expression_select_query(const datatypes::User &user, const View &query,
                                            processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_EXPRESSIONS_DATA, query);
        auto args = query["args"].get_document().value;

        const std::string &expression_type_name = bson_utils::get_string(args, "expression_type");

        GET_EXPRESSION_TYPE(expression_type_name, expression_type)

        std::vector<std::string> sample_ids;
        if (bson_utils::has_key(args, "sample_ids")) {
          sample_ids = utils::build_vector(args["sample_ids"].get_array().value);
        }

        std::vector<long> replicas;
        if (bson_utils::has_key(args, "replicas")) {
          replicas = utils::build_vector_long(args["replicas"].get_array().value);
        }

        std::vector<std::string> genes;
        if (bson_utils::has_key(args, "genes")) {
          genes = utils::build_vector(args["genes"].get_array().value);
        }

        std::vector<std::string> project;
        if (bson_utils::has_key(args, "norm_project")) {
          project = utils::build_vector(args["norm_project"].get_array().value);
        }

        std::string gene_model;
        if (bson_utils::has_key(args, "gene_model")) {
          gene_model = bson_utils::get_string(args, "gene_model");
        }

        return expression_type->load_data(sample_ids, replicas, genes, project, gene_model);
      }

      const ChromosomeRegionsList retrieve_find_motif_query(const datatypes::User &user,
                                       const View &query, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_FIND_MOTIF_QUERY, query);

        auto args = query["args"].get_document().value;

        std::string motif = bson_utils::get_string(args, "motif");
        std::string genome = bson_utils::get_string(args, "norm_genome");
        bool overlap = args["overlap"].get_bool().value;
        auto start = args["start"].get_int32().value;
        auto end = args["end"].get_int32().value;

        std::vector<std::string> chromosomes;
        if (bson_utils::has_key(args, "chromosomes")) {
          chromosomes = utils::build_vector(args["chromosomes"].get_array().value);
        } else {
          std::set<std::string> chrom = dba::genomes::get_chromosomes(genome);
          chromosomes = std::vector<std::string>(chrom.begin(), chrom.end());
        }

        return process_pattern(genome, motif, overlap, chromosomes, start, end);
      }

      const ChromosomeRegionsList retrieve_intersection_query(const datatypes::User &user,
                                       const View &query, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_INTERSECTION_QUERY, query);

        auto args = query["args"].get_document().value;

        const std::string query_a_id = bson_utils::get_string(args, "qid_1");
        auto maybe_query = helpers::get_one_by_id(Collections::QUERIES(), query_a_id);
        if (!maybe_query) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, query_a_id);
        }

        // Load all data from query 1, and intersect.
        // load both region sets.
        auto qid_1_str = bson_utils::get_string(args, "qid_1");
        ChromosomeRegionsList regions_a = retrieve_query(user, qid_1_str, status);

        // Load regions that will be used for the overlap
        const std::string query_b_id = bson_utils::get_string(args, "qid_2");
        ChromosomeRegionsList range_regions = retrieve_query(user, query_b_id, status);

        return algorithms::intersect(regions_a, range_regions, status);
      }

      const ChromosomeRegionsList retrieve_overlap_query(const datatypes::User &user, const View &query,
                                  processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_OVERLAP_QUERY, query);

        auto args = query["args"].get_document().value;

        const std::string query_a_id = bson_utils::get_string(args, "qid_1");
        auto maybe_query = helpers::get_one_by_id(Collections::QUERIES(), query_a_id);
        if (!maybe_query) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, query_a_id);
        }

        // Load all data from query 1, and intersect.
        // load both region sets.
        auto qid_1_str = bson_utils::get_string(args, "qid_1");
        ChromosomeRegionsList regions_a = retrieve_query(user, qid_1_str, status);

        // Load regions that will be used for the overlap
        const std::string query_b_id = bson_utils::get_string(args, "qid_2");
        ChromosomeRegionsList range_regions = retrieve_query(user, query_b_id, status);

        const bool overlap = args["overlap"].get_bool().value;
        const double amount = args["amount"].get_int32().value;
        const std::string amount_type = bson_utils::get_string(args, "amount_type");

        return algorithms::overlap(regions_a, range_regions, overlap, amount, amount_type, status);
      }

      const ChromosomeRegionsList retrieve_extend_query(const datatypes::User &user, const View &query,
                                 processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_EXTEND_QUERY, query);

        auto args = query["args"].get_document().value;
        const std::string query_id = bson_utils::get_string(args, "query_id");
        const Length length = args["length"].get_int32().value;
        const std::string direction = bson_utils::get_string(args, "direction");
        const bool use_strand = args["use_strand"].get_bool().value;

        ChromosomeRegionsList query_regions = retrieve_query(user, query_id, status);

        return algorithms::extend(query_regions, length, direction, use_strand);
      }

      const ChromosomeRegionsList retrieve_flank_query(const datatypes::User &user, const View &query, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_FLANK_QUERY, query);

        auto args = query["args"].get_document().value;
        const std::string query_id = bson_utils::get_string(args, "query_id");
        const Offset start = args["start"].get_int32().value;
        const Length length = args["length"].get_int32().value;
        const bool use_strand = args["use_strand"].get_bool().value;

        ChromosomeRegionsList query_regions = retrieve_query(user, query_id, status);
        return algorithms::flank(query_regions, start, length, use_strand);
      }

      const ChromosomeRegionsList retrieve_merge_query(const datatypes::User &user, const View &query,
                                processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_MERGE_QUERY, query);

        auto args = query["args"].get_document().value;

        // load both region sets.
        auto qid_1_str = bson_utils::get_string(args, "qid_1");
        ChromosomeRegionsList regions_a = retrieve_query(user, qid_1_str, status);

        auto qid_2_str = bson_utils::get_string(args, "qid_2");
        ChromosomeRegionsList regions_b = retrieve_query(user, qid_2_str, status);

        return algorithms::merge_chromosome_regions(regions_a, regions_b);
      }

      bool filter_region(const AbstractRegion *region_ref, const std::string &field, const dba::columns::ColumnTypePtr column, Metafield &metafield, const std::string &chrom, algorithms::FilterBuilder::FilterPtr filter, processing::StatusPtr status)
      {
        if (field == "START") {
          return filter->is(region_ref->start());
        }

        if (field == "END") {
          return filter->is(region_ref->end());
        }

        if (field == "CHROMOSOME") {
          return filter->is(chrom);
        }

        // TODO: optimize for "@AGG." values
        if (field[0] == '@') {
          std::string value = metafield.process(field, chrom, region_ref, status);
          return filter->is(value);
        } else {
          if (datatypes::column_type_is_compatible(column->type(), datatypes::COLUMN_STRING)) {
            return filter->is(region_ref->get_string(column->pos()));
          } else {
            return filter->is(region_ref->value(column->pos()));
          }
        }
      }

      const ChromosomeRegionsList retrieve_filter_query(const datatypes::User &user, const View &query,
                processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_FILTER_QUERY, query);

        auto args = query["args"].get_document().view();

        // load original query
        auto query_str = bson_utils::get_string(args, "query");
        ChromosomeRegionsList regions = retrieve_query(user, query_str, status);

        std::string type = bson_utils::get_string(args, "type");
        std::string operation = bson_utils::get_string(args, "operation");
        std::string value = bson_utils::get_string(args, "value");
        std::string field = bson_utils::get_string(args, "field");

        bool error;
        algorithms::FilterBuilder::FilterPtr filter;

        if (type.compare("string") == 0) {
          filter = algorithms::FilterBuilder::getInstance().build(field, operation, value);
        } else if (type.compare("number") == 0 || type.compare("integer") == 0 || type.compare("double") == 0) {
          filter = algorithms::FilterBuilder::getInstance().build(field, operation, atof(value.c_str()));
        } else {
          throw deepblue_user_exception(ERR_FILTER_INVALID_TYPE);
        }

        DatasetId dataset_id = -1;
        dba::columns::ColumnTypePtr column;

        size_t total = 0;
        size_t removed = 0;
        size_t total_removed_size = 0;
        size_t keep = 0;

        ChromosomeRegionsList filtered_regions;
        Metafield metafield;
        for (auto &chromosome_regions_list : regions) {
          const std::string &chromosome = chromosome_regions_list.first;
          Regions saved = Regions();
          for (auto &region : chromosome_regions_list.second) {
            if (!dba::Metafield::is_meta(field)) {
              if (region->dataset_id() != dataset_id) {
                dataset_id = region->dataset_id();
                column = cache::get_column_type_from_dataset(dataset_id, field);
              }
            }
            total++;

            // TODO: use column_type for better filtering. i.e. type conversion
            if (filter_region(region.get(), field, column, metafield, chromosome, filter, status)) {
              saved.emplace_back(std::move(region));
              keep++;
            } else {
              total_removed_size += region->size();
              removed++;
            }
          }

          if (!saved.empty()) {
            ChromosomeRegions chr_region(chromosome, std::move(saved));
            filtered_regions.push_back(std::move(chr_region));
          }
        }

        status->subtract_regions(removed);
        status->subtract_size(total_removed_size);

        return filtered_regions;
      }

      const ChromosomeRegionsList retrieve_filter_by_motif_query(const datatypes::User &user, const View &query,
            processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_FILTER_MOTIF_QUERY, query);

        auto args = query["args"].get_document().value;

        // load original query
        auto query_str = bson_utils::get_string(args, "query");
        ChromosomeRegionsList regions = retrieve_query(user, query_str, status);

        std::string motif = bson_utils::get_string(args, "motif");
        if (motif.empty()) {
          throw deepblue_user_exception(ERR_USER_MOTIF_MISSING);
        }

        std::string genome;
        DatasetId dataset_id = -1;

        long total = 0;
        Metafield metafield;

        ChromosomeRegionsList filtered_regions;
        for (auto &chromosome_regions_list : regions) {
          const std::string &chromosome = chromosome_regions_list.first;
          Regions saved = Regions();

          for (auto &region : chromosome_regions_list.second) {
            if (region->dataset_id() != dataset_id) {

              // TODO: Workaround - because aggregates does not have a region_set_id
              if (region->dataset_id() == DATASET_EMPTY_ID) {
                throw deepblue_user_exception(ERR_FILTER_BY_MOTIF_AGGREGATED);
              }

              auto dataset_obj = cache::get_document_by_dataset_id(region->dataset_id());
              if (!dataset_obj) {
                throw deepblue_user_exception(ERR_DATASET_NOT_FOUND, region->dataset_id());
              }
              genome = utils::get_by_region_set(dataset_obj->view(), "genome");
            }

            size_t count = status->running_cache()->count_regions(genome, chromosome, motif, region->start(), region->end(), status);

            if (count > 0) {
              saved.emplace_back(std::move(region));
            } else {
              status->subtract_size(region->size());
              status->subtract_regions(1);
            }
          }

          if (!saved.empty())
          {
            ChromosomeRegions chr_region(chromosome, std::move(saved));
            filtered_regions.push_back(std::move(chr_region));
          }
        }
        return filtered_regions;
      }

      const DatasetId add_tiling(const std::string &genome, const size_t &tiling_size)
      {
        std::string norm_genome = utils::normalize_name(genome);

        auto maybe_tiling = helpers::get_one_by_query(Collections::TILINGS(),
                     MakeDocument(
                       KVP("norm_genome", norm_genome),
                       KVP("tiling_size", static_cast<int32_t>(tiling_size))
                     ));

        if (maybe_tiling)
        {
          auto tiling = maybe_tiling->view();
          return tiling[KeyMapper::DATASET()].get_int32().value;
        }

        auto t_id = helpers::get_increment_counter("tiling");
        std::string tiling_id = "tr" + utils::integer_to_string(t_id);

        DatasetId dataset_id = helpers::get_increment_counter("datasets");

        DocumentBuilder tiling_data_builder;

        std::stringstream name;
        name << "Tiling regions of " << tiling_size << " (Genome " << genome << ")";
        tiling_data_builder.append(KVP("_id", tiling_id));
        tiling_data_builder.append(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));
        tiling_data_builder.append(KVP("name", name.str()));
        tiling_data_builder.append(KVP("norm_genome", norm_genome));
        tiling_data_builder.append(KVP("tiling_size", static_cast<int32_t>(tiling_size)));
        tiling_data_builder.append(KVP("columns", ArrayView()));

        COLLECTION(tilings_collection, Collections::TILINGS())
        tilings_collection.insert_one(tiling_data_builder.extract());

        return dataset_id;
      }

      const ChromosomeRegionsList retrieve_tiling_query(const View &query, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::RETRIEVE_TILING_QUERY, query);

        auto args = query["args"].get_document().value;

        const std::string norm_genome = bson_utils::get_string(args, "norm_genome");
        const size_t tiling_size = args["size"].get_int64().value;

        std::vector<std::string> chromosomes;

        if (bson_utils::has_key(args, "chromosomes")) {
          chromosomes = utils::build_vector(args["chromosomes"].get_array().value);
        } else {
          std::set<std::string> chrom = dba::genomes::get_chromosomes(norm_genome);
          chromosomes = std::vector<std::string>(chrom.begin(), chrom.end());
        }

        genomes::GenomeInfoPtr genome_info = genomes::get_genome_info(norm_genome);

        std::vector<std::string>::iterator cit;

        DatasetId tiling_id = add_tiling(norm_genome, tiling_size);

        ChromosomeRegionsList regions;
        for (cit = chromosomes.begin(); cit != chromosomes.end(); ++cit) {
          dba::genomes::ChromosomeInfo chromosome_info = genome_info->get_chromosome(*cit);

          Regions regs = Regions();
          for (size_t i = 0; i + tiling_size < chromosome_info.size; i += tiling_size) {
            auto r = build_simple_region(i, i + tiling_size, tiling_id);
            if (!status->sum_and_check_size(r->size())) {
              throw deepblue_user_exception(ERR_MEMORY_EXAUSTED, status->total_size(), status->maximum_size());
            }
            regs.emplace_back(std::move(r));
          }
          regions.push_back(ChromosomeRegions(*cit, std::move(regs)));
        }

        return regions;
      }

      const ChromosomeRegionsList process_aggregate(const datatypes::User &user, const View &query, processing::StatusPtr status)
      {
        START_OPERATION(status, processing::PROCESS_AGGREGATE, query);

        auto args = query["args"].get_document().value;
        const std::string query_id = bson_utils::get_string(args, "data_id");
        const std::string regions_id = bson_utils::get_string(args, "ranges_id");
        const std::string field = bson_utils::get_string(args, "field");

        ChromosomeRegionsList data = retrieve_query(user, query_id, status);
        ChromosomeRegionsList ranges = retrieve_query(user, regions_id, status);

        return algorithms::aggregate(data, ranges, field, status);
      }

      //
      const std::vector<std::string> get_main_experiment_data(const datatypes::User &user, const std::string &query_id,
                  const std::string field_key, processing::StatusPtr status)
      {
        auto maybe_query = helpers::get_one_by_id(Collections::QUERIES(), query_id);

        if (!maybe_query) {
          throw deepblue_user_exception(ERR_INVALID_QUERY_ID, query_id);
        }

        auto query = maybe_query->view();

        std::vector<std::string> values;

        const std::string type = bson_utils::get_string(query, "type");
        const auto args = query["args"].get_document().value;

        if (((field_key == "norm_genome") || (field_key == "genome")) && bson_utils::has_key(args, "norm_genome"))
        {
          values.emplace_back(bson_utils::get_string(args, "norm_genome"));
          return values;
        }

        if (((field_key == "norm_genome") || (field_key == "genome")) && bson_utils::has_key(args, "gene_model"))
        {
          auto model_name = bson_utils::get_string(args, "gene_model");
          auto gene_model_obj = genes::get_gene_model_obj(model_name);
          values.emplace_back(bson_utils::get_string(gene_model_obj, "norm_genome"));
          return values;
        }

        if (type == "experiment_select") {
          std::vector<std::string> norm_names = utils::build_vector(args["norm_experiment_name"].get_array().value);
          if (!norm_names.empty()) {
            for (const auto &exp_name : norm_names) {
              auto maybe_experiment = dba::experiments::by_name(exp_name);
              if (!maybe_experiment) {
                throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, exp_name);
              }

              auto exp_view = maybe_experiment->view();
              auto value = bson_utils::get_string(exp_view, field_key);
              values.push_back(value);
            }
            return values;
          }

          if (bson_utils::has_key(args, field_key)) {
            if (args[field_key].type() == ArrayType) {
              values = utils::build_vector(args[field_key].get_array().value);
            } else {
              values.push_back(bson_utils::get_string(args, field_key));
            }
          }
          return values;

        } else if (type == "annotation_select") {
          auto ann_names = utils::build_vector(args["annotation"].get_array().value);
          const std::string genome = bson_utils::get_string(args, "genome");

          for (const auto &ann_name : ann_names) {
            MaybeDocument ann_obj = dba::annotations::by_name(ann_name, genome);
            if (!ann_obj) {
              throw deepblue_user_exception(ERR_INVALID_ANNOTATION_NAME, ann_name, genome);
            }

            auto ann_view = ann_obj->view();
            if (bson_utils::has_key(ann_view, field_key)) {
              // TODO: maybe the value here is dotted (bla.blu), check improve get_string
              auto value = bson_utils::get_string(ann_view, field_key);
              values.push_back(value);
            }
          }
          return values;

        } else if (type == "intersect") {
          const std::string query_id = bson_utils::get_string(args, "qid_1");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "overlap") {
          const std::string query_id = bson_utils::get_string(args, "qid_1");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "flank") {
          const std::string query_id = bson_utils::get_string(args, "qid_1");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "extend") {
          const std::string query_id = bson_utils::get_string(args, "qid_1");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "merge") {
          const std::string query_id = bson_utils::get_string(args, "qid_1");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "genes_select") {
          if (bson_utils::has_key(args, "genes")) {
            auto genes = utils::build_vector(args["genes"].get_array().value);
            values.insert(values.end(), genes.begin(), genes.end());
          }

          std::vector<std::string> go_terms;
          if (bson_utils::has_key(args, "go_terms")) {
            go_terms = utils::build_vector(args["go_terms"].get_array().value);
            values.insert(values.end(), go_terms.begin(), go_terms.end());
          }

          // Honestly I dont like it, but since we changed these parameters and we already have a database with queries...
          // TODO: manually change the database.
          std::string gene_model;
          if (bson_utils::has_key(args, "gene_model")) {
            gene_model = bson_utils::get_string(args, "gene_model");
          }
          values.push_back(gene_model);
          return values;

        } else if (type == "find_motif") {
          std::string name = bson_utils::get_string(args, "motif") + " (motif)";
          values.push_back(name);
          return values;

        } else if (type == "expressions_select") {
          if (bson_utils::has_key(args, "genes")) {
            auto genes = utils::build_vector(args["genes"].get_array().value);
            values.insert(values.end(), genes.begin(), genes.end());
          }
          return values;

        } else if (type == "filter") {
          const std::string query_id = bson_utils::get_string(args, "query");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "filter_by_motif") {
          const std::string query_id = bson_utils::get_string(args, "query");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "tiling") {
          std::string name = utils::integer_to_string(args["size"].get_int32().value) + " (tiling regions)";
          values.push_back(name);
          return values;

        } else if (type == "aggregate") {
          const std::string query_id = bson_utils::get_string(args, "query");
          return get_main_experiment_data(user, query_id, field_key, status);

        } else if (type == "input_regions") {
          values.push_back("input_regions");
          return values;

        } else {
          throw deepblue_user_exception(ERR_UNKNOW_QUERY_TYPE, type);
        }
      }

      // TODO: move to another file
      const std::vector<Document> __get_columns_from_dataset(const DatasetId &dataset_id)
      {
        std::vector<Document> columns;
        if (dataset_id == DATASET_EMPTY_ID) {
          return columns;
        }


        Document o = MakeDocument(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));

        auto collections = {
          Collections::EXPERIMENTS(),
          Collections::ANNOTATIONS()
        };

        for (const auto& collection: collections) {
          auto cursor = helpers::get_all_by_query(collection, o.view());

          bool found = false;
          for (const auto& experiment : cursor) {
            int s_count = 0;
            int n_count = 0;

            auto tmp_columns = experiment.view()["columns"].get_array().value;
            for (const auto &e : tmp_columns) {
              auto column = e.get_document().value;
              const std::string &column_type = bson_utils::get_string(column, "column_type");
              const std::string &column_name = bson_utils::get_string(column, "name");

              DocumentBuilder bob;
              bob.append(Concatenate(column));
              if (column_name != "CHROMOSOME" && column_name != "START" && column_name != "END") {
                int pos = -1;
                if (column_type == "string") {
                  pos = s_count++;

                } else if (column_type == "integer") {
                  pos = n_count++;

                } else if (column_type == "double") {
                  pos = n_count++;

                } else if (column_type == "range") {
                  pos = n_count++;

                } else if (column_type == "category") {
                  pos = s_count++;

                } else if (column_type == "calculated") {
                  throw deepblue_user_exception(ERR_CALCULATED_FIELD_DATA);

                } else {
                  throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE, column_type);
                }

                bob.append(KVP("pos", pos));
              }

              columns.emplace_back(bob.extract());
            }
            found = true;
          }
          if (found) {
            return columns;
          }
        }

        auto maybe_tiling = helpers::get_one_by_query(Collections::TILINGS(), o.view());
        if (maybe_tiling) {
          auto tiling_view = maybe_tiling->view();
          auto tmp_columns = tiling_view["columns"].get_array().value;
          for (const auto &e : tmp_columns) {
            DocumentBuilder clone;
            columns.emplace_back(Document(e.get_document().value));
          }
          return columns;
        }

        auto maybe_query = helpers::get_one_by_query(Collections::QUERIES(), MakeDocument(
            KVP("type", "input_regions"),
            KVP("args.dataset_id", static_cast<int32_t>(dataset_id))
        ));

        if (maybe_query) {
          parser::FileFormat file_format;
          auto query_view = maybe_query->view();
          auto input_regions = query_view["args"].get_document().value;

          if (bson_utils::has_key(input_regions, "format")) {
            file_format = parser::FileFormatBuilder::build(bson_utils::get_string(input_regions, "format"));
          } else {
            file_format = parser::FileFormat::default_format();
          }

          int s_count = 0;
          int n_count = 0;

          for (const auto &column : file_format) {
            auto column_obj = column->document();
            auto column_obj_view = column_obj.view();
            const std::string &column_type = bson_utils::get_string(column_obj_view, "column_type");
            const std::string &column_name = bson_utils::get_string(column_obj_view, "name");

            DocumentBuilder bob;
            bob.append(Concatenate(column_obj_view));;

            if (column_name != "CHROMOSOME" && column_name != "START" && column_name != "END") {
              int pos = -1;
              if (column_type == "string") {
                pos = s_count++;
              } else if (column_type == "integer") {
                pos = n_count++;
              } else if (column_type == "double") {
                pos = n_count++;
              } else if (column_type == "range") {
                pos = n_count++;
              } else if (column_type == "category") {
                pos = s_count++;
              } else if (column_type == "calculated") {
                throw deepblue_user_exception(ERR_CALCULATED_FIELD_DATA);
              } else {
                throw deepblue_user_exception(ERR_INVALID_COLUMN_TYPE, column_type);
              }

              bob.append(KVP("pos", pos));
              columns.push_back(bob.extract());
            }
          }
          return columns;
        }

        bool found = false;

        auto gene_models_cursor = helpers::get_all_by_query(Collections::GENE_MODELS(), o);

        for (const auto& doc: gene_models_cursor) {
          int pos = 0;
          for (const auto &column : parser::FileFormat::gtf_format()) {
            DocumentBuilder bob;
            const std::string &column_name = column->name();
            if (column_name != "CHROMOSOME" && column_name != "START" && column_name != "END")
            {
              bob.append(Concatenate(column->document().view()));
              bob.append(KVP("pos", pos++));
              columns.emplace_back(bob.extract());
            }
          }
          found = true;
        }
        if (found) {
          return columns;
        }

        auto gene_expressions_cursor = helpers::get_all_by_query(Collections::GENE_EXPRESSIONS(), o);
        for (const auto& gene_expression: gene_expressions_cursor) {
          int s_count = 0;
          int n_count = 0;
          auto tmp_columns = gene_expression.view()["columns"].get_array().value;
          for (const auto &e : tmp_columns) {
            auto column = e.get_document().value;
            const std::string &column_type = bson_utils::get_string(column, "column_type");
            const std::string &column_name = bson_utils::get_string(column, "name");
            DocumentBuilder bob;
            if (column_name != "CHROMOSOME" && column_name != "START" && column_name != "END")
            {
              int pos = -1;
              if (column_type == "string") {
                pos = s_count++;

              } else if (column_type == "integer") {
                pos = n_count++;

              } else if (column_type == "double") {
                pos = n_count++;
              }
              bob.append(Concatenate(column));
              bob.append(KVP("pos", pos));
              columns.emplace_back(bob.extract());
            }
          }
          found = true;
        }

        if (found) {
          return columns;
        }

        throw deepblue_user_exception(ERR_DATASET_NOT_FOUND, dataset_id);
      }

      Document __get_document_by_dataset_id(DatasetId dataset_id)
      {
        auto collections = { Collections::EXPERIMENTS(),
                             Collections::ANNOTATIONS(),
                             Collections::GENE_MODELS(),
                             Collections::GENE_EXPRESSIONS(),
                             Collections::TILINGS() };

        for (const auto &coll : collections)
        {
          auto maybe_doc = helpers::get_one_by_query(coll,
            MakeDocument(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id))));

          if (maybe_doc) {
            return *maybe_doc;
          }
        }

        COLLECTION(queries_collection, Collections::QUERIES())
        auto maybe_obj = queries_collection.find_one(MakeDocument(
            KVP("type", "input_regions"),
            KVP("args.dataset_id", static_cast<int32_t>(dataset_id))));

        if (maybe_obj) {
          auto obj_view = maybe_obj->view();
          auto query_id = bson_utils::get_string(obj_view, "_id");
          auto norm_genome = StringViewToString(obj_view["args"]["norm_genome"].get_utf8().value);
          return MakeDocument(
              KVP("name", "Query " + query_id + " regions set"),
              KVP("norm_genome", norm_genome));
        }

        throw deepblue_user_exception(ERR_DATASET_NOT_FOUND, dataset_id);
      }
    }
  }
}
