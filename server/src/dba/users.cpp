//
//  users.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 03.11.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>

#include "../datatypes/user.hpp"

#include "../dba/collections.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../dba/helpers.hpp"

#include "users.hpp"
#include "collections.hpp"
#include "exists.hpp"
#include "helpers.hpp"
#include "list.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"

namespace epidb {
  namespace dba {
    namespace users {

      class NameCache {
      private:

        std::map<std::string, std::string> id_name;

      public:

        std::string get_user_name(const std::string &id)
        {
          return id_name[id];
        }

        void set_user_name(const std::string &id, const std::string &name)
        {
          id_name[id] = name;
        }

        bool exists_user_id(const std::string &id)
        {
          if (id_name.find(id) != id_name.end()) {
            return true;
          }
          return false;
        }

        void invalidate()
        {
          id_name.clear();
        }
      };

      NameCache name_cache;

      const std::string add_user(datatypes::User& user)
      {
        Document bson = user.toDoc();

        auto result = dba::helpers::get_increment_counter(Collections::USERS());
        user.id(datatypes::User::PREFIX + std::to_string(result));

        DocumentStreamBuilder builder{};
        builder << Concatenate(bson.view());
        builder << datatypes::User::FIELD_ID <<  user.id();
        Document userBson = builder.extract();

        COLLECTION(collection, dba::Collections::USERS());
        collection.insert_one(userBson.view());

        return user.id();
      }

      void modify_user(datatypes::User& user)
      {
        auto user_doc = user.toDoc();

        COLLECTION(users_collection, dba::Collections::USERS());

        users_collection.update_one(
          bson_utils::id_doc(user.id()),
          MakeDocument(KVP("$set", user_doc.view())));
      }

      const datatypes::User __load_user(const View& user_object)
      {
        std::vector<utils::IdName> public_projects;

        if (user_object[datatypes::User::FIELD_PERMISSION_LEVEL].get_int32() == 0) {
          public_projects = list::all_projects();
        } else {
          public_projects = list::public_projects();
        }
        std::vector<std::string> public_projects_names;
        for (const auto& pp: public_projects) {
          public_projects_names.push_back(pp.name);
        }

        std::vector<utils::IdName> private_projects;
        if (bson_utils::has_key(user_object, datatypes::User::FIELD_PROJECTS)) {
          auto user_projects_bson = MakeDocument(KVP("_id",
              MakeDocument(KVP("$in", user_object[datatypes::User::FIELD_PROJECTS].get_value()))));
          private_projects = helpers::get_id_names_by_query(Collections::PROJECTS(), user_projects_bson);
        }

        std::vector<std::string> private_projects_names;
        for (const auto& pp: private_projects) {
          private_projects_names.push_back(pp.name);
        }

        return datatypes::User(user_object, public_projects_names, private_projects_names);
      }

      bool remove_user(const datatypes::User& user, std::string& msg)
      {
        return dba::helpers::remove_one(dba::Collections::USERS(), user.id());
      }

      const datatypes::User get_user_by_key(const std::string& key)
      {
        auto maybe_user = dba::helpers::get_one_by_query(dba::Collections::USERS(),
          bson_utils::one_key_doc(datatypes::User::FIELD_KEY, key));

        if (!maybe_user) {
          throw deepblue_user_exception(ERR_INVALID_USER_KEY);
        }
        return __load_user(maybe_user->view());
      }

      const datatypes::User get_user_by_email(const std::string& email, const std::string& password)
      {
        auto maybe_user = dba::helpers::get_one_by_query(dba::Collections::USERS(),
          MakeDocument(
            KVP(datatypes::User::FIELD_EMAIL, email),
            KVP(datatypes::User::FIELD_PASSWORD, password)
          ));

        if (!maybe_user) {
          throw deepblue_user_exception(ERR_INVALID_USER_EMAIL_PASSWORD);
        }
        return __load_user(maybe_user->view());
      }

      const datatypes::User get_user_by_id(const std::string& id)
      {
        const auto maybe_user = dba::helpers::get_one_by_id(dba::Collections::USERS(), id);

        if (!maybe_user) {
          throw deepblue_user_exception(ERR_INVALID_USER_ID, id);
        }
        return __load_user(maybe_user->view());
      }

      bool is_valid_email(const std::string &email, std::string &msg)
      {
        if (helpers::check_exist(Collections::USERS(), "email", email)) {
          std::stringstream ss;
          ss << "Email '" << email << "' is already in use.";
          msg = ss.str();
          return false;
        }
        return true;
      }

      const std::string get_id(const std::string &user)
      {
        if (utils::is_id(user, "u")) {
          return user;
        }

        auto maybe_user = dba::helpers::get_one_by_query(dba::Collections::USERS(),
          bson_utils::one_key_doc("name", user));

        if (!maybe_user) {
          throw deepblue_user_exception(ERR_INVALID_USER_NAME, user);
        }

        return bson_utils::get_string(*maybe_user, "_id");
      }

      const std::string get_user_name_by_id(const std::string &user_id)
      {
        if (name_cache.exists_user_id(user_id)) {
          return name_cache.get_user_name(user_id);
        } else {
          auto maybe_user = dba::helpers::get_one_by_query(dba::Collections::USERS(),
            bson_utils::id_doc(user_id));

          auto user_name = bson_utils::get_string(*maybe_user, "name");
          name_cache.set_user_name(user_id, user_name);
          return user_name;
        }
      }

      void invalidate_cache()
      {
        name_cache.invalidate();
      }

      const datatypes::User get_owner(const std::string& id)
      {
        std::string collection = dba::Collections::get_collection_for_id(id);

        auto maybe_object = helpers::get_one_by_id(collection, id);

        if (!maybe_object) {
          throw deepblue_user_exception(ERR_INVALID_USER_ID, id);
        }

        auto object_view = maybe_object->view();
        std::string user_id;
        if (object_view.find("user") != object_view.end()) {
          user_id = bson_utils::get_string(object_view, "user");
        } else if (object_view.find("upload_info") != object_view.end()) {
          auto user_obj = object_view["upload_info"]["user"].get_document().view();
          user_id = bson_utils::get_string(user_obj, "user");
        } else {
          throw deepblue_user_exception(ERR_INVALID_OBJECT_OWNER, id);
        }
        return get_user_by_id(user_id);
      }
    }
  }
}
