//
//  genes.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 09.09.2015
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#include <string>

#include <boost/algorithm/string.hpp>

#include "../datatypes/metadata.hpp"
#include "../datatypes/gene_ontology_terms.hpp"
#include "../datatypes/user.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../parser/gtf.hpp"

#include "../interfaces/serializable.hpp"

#include "../typedefs/database.hpp"
#include "../typedefs/bson.hpp"

#include "annotations.hpp"
#include "collections.hpp"
#include "data.hpp"
#include "full_text.hpp"
#include "genes.hpp"
#include "helpers.hpp"
#include "info.hpp"
#include "remove.hpp"
#include "users.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"

namespace epidb {
  namespace dba {
    namespace genes {

      Document build_gene_info(const View& gene_db_obj)
      {
        DocumentBuilder gene_builder{};

        gene_builder.append(KVP("_id",        gene_db_obj["_id"].get_value()));
        gene_builder.append(KVP("chromosome", bson_utils::get_string(gene_db_obj, KeyMapper::CHROMOSOME())));
        gene_builder.append(KVP("start",      gene_db_obj[KeyMapper::START()].get_int32().value));
        gene_builder.append(KVP("end",        gene_db_obj[KeyMapper::END()].get_int32().value));
        gene_builder.append(KVP("source",     bson_utils::get_string(gene_db_obj, KeyMapper::SOURCE())));
        gene_builder.append(KVP("score",      gene_db_obj[KeyMapper::SCORE()].get_double().value));
        gene_builder.append(KVP("strand",     bson_utils::get_string(gene_db_obj, KeyMapper::STRAND())));
        gene_builder.append(KVP("frame",      bson_utils::get_string(gene_db_obj, KeyMapper::FRAME())));

        gene_builder.append(Concatenate(gene_db_obj[KeyMapper::ATTRIBUTES()].get_document().value));

        return gene_builder.extract();
      }

      Document gene_model_info(const std::string& id)
      {
        auto data_obj = data::gene_model(id);
        if (!data_obj) {
          throw deepblue_user_exception(ERR_INVALID_GENE_MODEL_ID, id);
        }

        auto view = data_obj->view();
        DocumentBuilder bob;

        bob.append(KVP("_id",         view["_id"].get_value()));
        bob.append(KVP("name",        view["name"].get_value()));
        bob.append(KVP("description", view["description"].get_value()));
        bob.append(KVP("genome",      view["genome"].get_value()));
        bob.append(KVP("format",      view["format"].get_value()));
        bob.append(KVP("total_genes", view["upload_info"]["total_genes"].get_value()));
        bob.append(Concatenate(view["extra_metadata"].get_document().value));

        return bob.extract();
      }

      Document gene_info(const std::string& id)
      {
        auto data_obj = data::gene(id);
        if (!data_obj) {
          throw deepblue_user_exception(ERR_INVALID_GENE_ID, id);
        }

        return build_gene_info(data_obj->view());
      }

      Document build_upload_info(const datatypes::User& user, const std::string &client_address,
                                  const std::string &content_format)
      {
        DocumentBuilder upload_info_builder;

        upload_info_builder.append(KVP("user", user.id()));
        upload_info_builder.append(KVP("content_format", content_format));
        upload_info_builder.append(KVP("done", false));
        upload_info_builder.append(KVP("client_address", client_address));
        upload_info_builder.append(KVP("upload_start", Date(std::chrono::system_clock::now())));

        return upload_info_builder.extract();
      }

      Document build_metadata(const std::string &name, const std::string &norm_name,
                          const std::string &genome, const std::string &norm_genome,
                          const std::string &description, const std::string &norm_description,
                          const std::string &format,
                          const View& extra_metadata_obj,
                          const std::string &ip,
                          int &dataset_id,
                          std::string &gene_model_id)
      {
        dataset_id = helpers::get_incremente_counter_and_notify("datasets");

        int _id = helpers::get_incremente_counter_and_notify(Collections::GENE_MODELS());
        gene_model_id = "gs" + utils::integer_to_string(_id);

        DocumentBuilder gene_model_metadata_builder{};
        gene_model_metadata_builder.append(KVP("_id", gene_model_id));
        gene_model_metadata_builder.append(KVP(KeyMapper::DATASET(), dataset_id));
        gene_model_metadata_builder.append(KVP("name", name));
        gene_model_metadata_builder.append(KVP("norm_name", norm_name));
        gene_model_metadata_builder.append(KVP("genome", genome));
        gene_model_metadata_builder.append(KVP("norm_genome", norm_genome));
        gene_model_metadata_builder.append(KVP("format", format));
        gene_model_metadata_builder.append(KVP("description", description));
        gene_model_metadata_builder.append(KVP("norm_description", norm_description));
        gene_model_metadata_builder.append(KVP("extra_metadata", extra_metadata_obj));

        return gene_model_metadata_builder.extract();
      }

      void update_upload_info(const std::string &collection, const std::string &annotation_id, const size_t total_genes)
      {
        COLLECTION(COLL, collection)

        auto query = bson_utils::id_doc(annotation_id);
        auto update = MakeDocument(KVP("$set", MakeDocument(
          KVP("upload_info.total_genes", static_cast<int64_t>(total_genes)),
          KVP("upload_info.done",  true),
          KVP("upload_info.upload_end", Date{std::chrono::system_clock::now()}))
        ));

        COLL.update_one(query.view(), update.view());
      }

      Document to_document(const int dataset_id, const std::string& gene_id, const parser::GTFRow& row)
      {
        DocumentBuilder bob;

        bob.append(KVP("_id", gene_id));
        bob.append(KVP(KeyMapper::DATASET(), dataset_id));
        bob.append(KVP(KeyMapper::CHROMOSOME(), row.seqname()));
        bob.append(KVP(KeyMapper::SOURCE(), row.source()));
        bob.append(KVP(KeyMapper::FEATURE(), row.feature()));
        bob.append(KVP(KeyMapper::START(), static_cast<int32_t>(row.start())));
        bob.append(KVP(KeyMapper::END(), static_cast<int32_t>(row.end())));
        bob.append(KVP(KeyMapper::SCORE(), row.score()));
        bob.append(KVP(KeyMapper::STRAND(), (std::string) row.strand()));
        bob.append(KVP(KeyMapper::FRAME(), (std::string) row.frame()));

        const parser::GTFRow::Attributes& attributes = row.attributes();
        DocumentBuilder attributes_bob;
        for (const auto& kv : attributes) {
          attributes_bob.append(KVP(kv.first, kv.second));
        }
        bob.append(KVP(KeyMapper::ATTRIBUTES(), attributes_bob.extract()));

        // TODO: include in full text
        return bob.extract();
      }


      const std::string insert(const datatypes::User& user,
                  const std::string &name, const std::string &norm_name,
                  const std::string &genome, const std::string &norm_genome,
                  const std::string &description, const std::string &norm_description,
                  datatypes::Metadata extra_metadata,
                  const parser::GTFPtr &gtf,
                  const std::string &ip)
      {
        Document extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);

        int dataset_id;
        std::string gene_model_id;
        auto gene_model_metadata = build_metadata(name, norm_name, genome, norm_genome,
                            description, norm_description, "GTF", extra_metadata_obj,
                            ip, dataset_id, gene_model_id);

        auto upload_info = build_upload_info(user, ip, "GTF");

        DocumentBuilder gene_model_builder;
        gene_model_builder.append(Concatenate(gene_model_metadata.view()));
        gene_model_builder.append(KVP("upload_info", upload_info));

        COLLECTION(gene_models_collection, Collections::GENE_MODELS())
        gene_models_collection.insert_one(gene_model_builder.extract());

        search::insert_full_text(Collections::GENE_MODELS(), gene_model_id, gene_model_metadata);

        size_t total_genes = 0;

        COLLECTION(genes_collection, Collections::GENES())
        auto bulk = genes_collection.create_bulk_write();

        for (const auto& row :  gtf->rows()) {
          auto _id =  helpers::get_incremente_counter_and_notify(Collections::GENES());
          std::string gene_id = "gn" + utils::integer_to_string(_id);
          bulk.append(InsertOneOp{to_document(dataset_id, gene_id, row)});
          total_genes++;
        }

        bulk.execute();

        update_upload_info(Collections::GENE_MODELS(), gene_model_id, total_genes);

        return gene_model_id;
      }

      Document get_gene_model_obj(const std::string& norm_gene_model)
      {
        auto maybe_gene_model = helpers::get_one_by_query(Collections::GENE_MODELS(),
                          MakeDocument(KVP("norm_name", norm_gene_model)));

        if (!maybe_gene_model) {
          throw deepblue_user_exception(ERR_INVALID_GENE_MODEL_NAME, norm_gene_model);
        }
        return *maybe_gene_model;
      }

      Document build_genes_database_query(const std::vector<std::string> &chromosomes, const int start, const int end,
                                      const std::string& strand,
                                      const std::vector<std::string>& genes, const std::vector<std::string>& go_terms,
                                      const std::string& norm_gene_model,
                                      const bool exactly)
      {
        Document gene_model_obj = get_gene_model_obj(norm_gene_model);
        auto view = gene_model_obj.view();

        DocumentBuilder bob;
        DatasetId dataset_id = view[KeyMapper::DATASET()].get_int32().value;
        bob.append(KVP(KeyMapper::DATASET(), static_cast<int32_t>(dataset_id)));

        ArrayBuilder data;
        bool has_data = false;

        if (!genes.empty()) {
          Array genes_array = utils::build_array(genes);

          auto b_in_gene_name = MakeDocument(KVP(KeyMapper::ATTRIBUTES() + ".gene_name", MakeDocument(KVP("$in", genes_array.view()))));
          auto b_in_gene_id = MakeDocument(KVP(KeyMapper::ATTRIBUTES() + ".gene_id", MakeDocument(KVP("$in", genes_array.view()))));

          data.append(MakeDocument(KVP("$or", MakeArray(b_in_gene_name, b_in_gene_id))));
          has_data = true;
        }

        if (!go_terms.empty()) {
          Array go_array = utils::build_array(go_terms);

          auto b_in_go_name = MakeDocument(KVP(KeyMapper::ATTRIBUTES() + ".go_annotation.go_id", MakeDocument(KVP("$in", go_array.view()))));
          auto b_in_go_id = MakeDocument(KVP(KeyMapper::ATTRIBUTES() + ".go_annotation.go_label", MakeDocument(KVP("$in", go_array.view()))));

          data.append(MakeDocument(KVP("$or", MakeArray(b_in_go_name, b_in_go_id))));
          has_data = true;
        }

        if (has_data) {
          bob.append(KVP("$or", data.extract()));
        }

        if (!chromosomes.empty()) {
          bob.append(KVP(KeyMapper::CHROMOSOME(), MakeDocument(KVP("$in", utils::build_array(chromosomes)))));
        }

        if (exactly) {
          if (start > -1) {
            bob.append(KVP(KeyMapper::START(), start));
          }
          if (end > -1) {
            bob.append(KVP(KeyMapper::END(), end));
          }
        } else {
          if (start > -1) {
            bob.append(KVP(KeyMapper::END(), MakeDocument(KVP("$gte", start))));
          }
          if (end > -1) {
            bob.append(KVP(KeyMapper::START(), MakeDocument(KVP("$lte", end))));
          }
        }

        if (!strand.empty()) {
          bob.append(KVP(KeyMapper::STRAND(), strand));
        }

        return bob.extract();
      }

      const std::string get_gene_attribute(const std::string& chromosome, const Position start, const Position end, const std::string& strand,
                              const std::string& attribute_name, const std::string& gene_model)
      {
        std::string norm_gene_model = utils::normalize_name(gene_model);
        // TODO: Change to GeneRegion*
        RegionPtr gene = get_gene_by_location(chromosome, start, end, strand, norm_gene_model);

        auto it = gene->attributes().find(attribute_name);
        if (it == gene->attributes().end()) {
          throw deepblue_user_exception(ERR_INVALID_GENE_ATTRIBUTE, attribute_name);
        }

        return it->second;
      }

      const std::vector<datatypes::GeneOntologyTermPtr> get_gene_gene_ontology_annotations(const std::string& chromosome,
                                              const Position start, const Position end,
                                              const std::string& strand, const std::string& gene_model)
      {
        std::string norm_gene_model = utils::normalize_name(gene_model);
        // TODO: Change to GeneRegion*
        RegionPtr gene = get_gene_by_location(chromosome, start, end, strand, norm_gene_model);

        const GeneRegion* gene_region = static_cast<const GeneRegion*>(gene.get());
        return gene_region->get_gene_ontology_terms();
      }

      std::unordered_map<int, std::string> id_to_gene_model;
      const std::string get_gene_model_by_dataset_id(const int dataset_id)
      {
        auto it = id_to_gene_model.find(dataset_id);
        if (it == id_to_gene_model.end()) {
          auto maybe_obj = helpers::get_one_by_query(Collections::GENE_MODELS(),
            MakeDocument(KVP(KeyMapper::DATASET(), dataset_id)));

          if (!maybe_obj) {
            throw deepblue_user_exception(ERR_DATASET_NOT_FOUND, dataset_id);
          }

          auto view = maybe_obj->view();
          std::string name = bson_utils::get_string(view, "name");
          id_to_gene_model[dataset_id] = name;
          return name;
        } else {
          return it->second;
        }
      }


      const std::vector<Document> get_genes(const std::vector<std::string> &chromosomes, const Position start, const Position end,
                     const std::string& strand,
                     const std::vector<std::string>& genes_names_or_id, const std::vector<std::string>& go_terms,
                     const std::string &norm_gene_model)
      {
        auto query = build_genes_database_query(chromosomes, start, end, strand, genes_names_or_id, go_terms, norm_gene_model, false);
        auto opts = mongocxx::options::find{};
        opts.sort(MakeDocument(
          KVP(KeyMapper::CHROMOSOME(), 1),
          KVP(KeyMapper::START(), 1)
        ));

        COLLECTION(genes_collection, Collections::GENES());
        auto c = genes_collection.find(query.view(), opts);

        std::vector<Document> genes;
        for (const auto& gene_db_obj : c) {
          genes.emplace_back(build_gene_info(gene_db_obj));
        }

        return genes;
      }

      const ChromosomeRegionsList get_genes_from_database(const std::vector<std::string> &chromosomes,
                              const Position start, const Position end, const std::string& strand,
                              const std::vector<std::string>& genes, const std::vector<std::string>& go_terms,
                              const std::string& norm_gene_model)
      {
        auto query = build_genes_database_query(chromosomes, start, end, strand, genes, go_terms, norm_gene_model, false);
        COLLECTION(genes_collection, dba::Collections::GENES())

        auto opts = mongocxx::options::find{};
        opts.sort(MakeDocument(
          KVP(KeyMapper::CHROMOSOME(), 1),
          KVP(KeyMapper::START(), 1)
        ));

        auto data_cursor = genes_collection.find(query.view(), opts);

        std::string actual_chromosome("");
        Regions actual_regions;

        ChromosomeRegionsList chromosomeRegionsList;
        for (const auto& gene: data_cursor) {
          auto e_it = gene.begin();

          std::string gene_id    = StringViewToString(e_it->get_utf8().value); e_it++;
          DatasetId dataset_id   = e_it->get_int32().value;                    e_it++;
          std::string chromosome = StringViewToString(e_it->get_utf8().value); e_it++;
          std::string source     = StringViewToString(e_it->get_utf8().value); e_it++;
          std::string feature    = StringViewToString(e_it->get_utf8().value); e_it++;
          Position start         = e_it->get_int32().value;                    e_it++;
          Position end           = e_it->get_int32().value;                    e_it++;
          Score score            = e_it->get_double().value;                   e_it++;
          std::string strand     = StringViewToString(e_it->get_utf8().value); e_it++;
          std::string frame      = StringViewToString(e_it->get_utf8().value);
          datatypes::Metadata attributes = datatypes::document_to_metadata(gene[KeyMapper::ATTRIBUTES()].get_document().value);

          std::vector<datatypes::GeneOntologyTermPtr> go_terms;
          if (bson_utils::has_key(gene, "go_annotation")) {
            auto go_annotations = gene["go_annotation"].get_array().value;

            for (auto go_term : go_annotations) {
              std::string go_id = StringViewToString(go_term["go_id"].get_utf8().value);
              std::string go_label = StringViewToString(go_term["go_label"].get_utf8().value);
              std::string go_namespace = StringViewToString(go_term["go_namespace"].get_utf8().value);
              datatypes::GeneOntologyTermPtr go_term_ptr = datatypes::GeneOntolyTermsPool::get_go_term(go_id, go_label, go_namespace);
              go_terms.emplace_back(go_term_ptr);
            }
          }

          if (chromosome != actual_chromosome) {
            if (!actual_chromosome.empty() && !actual_regions.empty()) {
              chromosomeRegionsList.emplace_back(std::move(actual_chromosome), std::move(actual_regions));
            }
            actual_chromosome = chromosome;
            actual_regions = Regions();
          }

          actual_regions.emplace_back(build_gene_region(start, end, dataset_id, source, score, feature,  strand, frame, attributes, go_terms));
        }

        if (!actual_chromosome.empty()) {
          chromosomeRegionsList.emplace_back(std::move(actual_chromosome), std::move(actual_regions));
        }

        return chromosomeRegionsList;
      }

      struct GeneLocation {
        std::string chromosome;
        Position start;
        Position end;
        std::string strand;

        GeneLocation(std::string c, Position s, Position e, std::string d) :
          chromosome(c),
          start(s),
          end(e),
          strand(d) {}

        bool operator==(const GeneLocation &other) const
        {
          if (other.strand == ".") {
            return (chromosome == other.chromosome
                    && start == other.start
                    && end == other.end);
          }
          else {
            return (chromosome == other.chromosome
              && start == other.start
              && end == other.end
              && strand == other.strand);
          }
        }
      };

      struct GeneLocationHasher {
        std::size_t operator()(const GeneLocation& k) const
        {
          using std::hash;

          size_t res = 17;
          res = res * 31 + hash<std::string>()( k.chromosome );
          res = res * 31 + hash<Position>()( k.start );
          res = res * 31 + hash<Position>()( k.end );
          return res;
        }
      };

      typedef std::unordered_map<std::string, GeneLocation> GeneModelCache;
      typedef std::unordered_map<std::string, GeneModelCache> GeneModelsCache;

      typedef std::unordered_map<GeneLocation, RegionPtr, GeneLocationHasher> GeneLocationCache;
      typedef std::unordered_map<std::string, GeneLocationCache> GeneLocationsCache;

      void load_gene_model(const std::string & norm_gene_model,
                           GeneModelCache & cache_tracking_id, GeneModelCache & cache_gene_name,
                           GeneLocationCache & cache_location_gene)
      {
        std::vector<std::string> chromosomes;
        std::vector<std::string> genes;
        std::vector<std::string> go_terms;

        ChromosomeRegionsList chromosomeRegionsList = get_genes_from_database(chromosomes, -1, -1, "", genes, go_terms, norm_gene_model);

        for (const auto &chromosomes_regions : chromosomeRegionsList) {
          const std::string &chromosome = chromosomes_regions.first;
          const Regions &regions = chromosomes_regions.second;
          for (const auto& r : regions) {
            // We have to clone because the original gene_region is an unique_ptr and it will be destroyed.
            auto region = r->clone();

            const GeneRegion* gene_region = static_cast<const GeneRegion *>(region.get());
            const std::string& gene_id = gene_region->attributes().at("gene_id");
            const std::string& gene_name = gene_region->attributes().at("gene_name");

            std::vector<std::string> gene_id_parts;
            boost::split(gene_id_parts, gene_id, boost::is_any_of("."));
            /* Not supported by GCC 4.7 (MPI's compiler)
            cache.emplace(std::piecewise_construct,
                          std::forward_as_tuple(gene_id),
                          std::forward_as_tuple(chromosome, gene_region->start(), gene_region->end()));
            */
            // See the get_string in gene_regions
            auto gl = GeneLocation(chromosome, gene_region->start(), gene_region->end(), gene_region->get_string(3));
            cache_tracking_id.insert( std::make_pair(gene_id_parts[0], gl));
            cache_gene_name.insert( std::make_pair(gene_name, gl));

            cache_location_gene.insert( std::make_pair(gl,  std::move(region)));
          }
        }
      }

      GeneModelsCache gene_models_cache_tracking_id;
      GeneModelsCache gene_models_cache_gene_name;
      GeneLocationsCache gene_locations_cache;

      void invalidate_cache()
      {
        gene_models_cache_tracking_id.clear();
        gene_models_cache_gene_name.clear();
        gene_locations_cache.clear();
      }

      void load_gene_location_caches(const std::string & gene_model)
      {
        // Put a lock here
        if (gene_models_cache_tracking_id.find(gene_model) != gene_models_cache_tracking_id.end()) {
          return;
        }

        GeneModelCache cache_tracking_id;
        GeneModelCache cache_gene_name;
        GeneLocationCache gene_location_cache;
        load_gene_model(gene_model, cache_tracking_id, cache_gene_name, gene_location_cache);

        gene_models_cache_tracking_id.insert(std::make_pair(gene_model, cache_tracking_id));
        gene_models_cache_gene_name.insert(std::make_pair(gene_model, cache_gene_name));
        gene_locations_cache.insert(std::make_pair(gene_model, std::move(gene_location_cache)));
      }

      RegionPtr get_gene_by_location(const std::string& chromosome, const Position start, const Position end, const std::string& strand,
                                const std::string& gene_model)
      {
        load_gene_location_caches(gene_model);

        const auto &cache = gene_locations_cache[gene_model];
        auto it = cache.find({chromosome, start, end, strand});

        if (it == cache.end()) {
          throw deepblue_user_exception(ERR_INVALID_GENE_LOCATION, chromosome, start, end, gene_model);
        }

        return it->second->clone();
      }


      void map_gene_location(const std::string & gene_tracking_id,
                             const std::string & gene_name, const std::string & gene_model,
                             std::string & chromosome, Position & start, Position & end, std::string& strand)
      {
        load_gene_location_caches(gene_model);

        std::vector<std::string> gene_tracking_id_parts;
        boost::split(gene_tracking_id_parts, gene_tracking_id, boost::is_any_of("."));
        const auto &cache_tracking_id = gene_models_cache_tracking_id[gene_model];
        auto it = cache_tracking_id.find(gene_tracking_id_parts[0]);

        if (it == cache_tracking_id.end()) {
          const auto &cache_gene_name = gene_models_cache_gene_name[gene_model];
          it = cache_gene_name.find(gene_name);
          if (it == cache_gene_name.end()) {
            if (gene_tracking_id.empty()) {
              throw deepblue_user_exception(ERR_INVALID_GENE_NAME, gene_name, gene_model);
            } else {
              throw deepblue_user_exception(ERR_INVALID_GENE_TRACKING_ID, gene_tracking_id, gene_name, gene_model);
            }
          }
        }

        const auto &region = it->second;
        chromosome = region.chromosome;
        start = region.start;
        end = region.end;
        strand = region.strand;
      }

      const bool exists_gene_ensg(const std::string& gene_ensg_id)
      {
        DocumentBuilder bob;
        bob.append(KVP(KeyMapper::ATTRIBUTES() + ".gene_id", Regex{"^"+gene_ensg_id}));

        return helpers::check_exist(Collections::GENES(), bob.extract());
      }
    }
  }
}
