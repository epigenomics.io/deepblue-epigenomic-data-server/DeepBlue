//
//  retrieve.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.06.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm>
#include <cstdlib>
#include <future>
#include <limits>
#include <numeric>
#include <memory>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

#include <boost/ref.hpp>
#include <boost/thread.hpp>
#include <boost/function.hpp>

#include "collections.hpp"
#include "helpers.hpp"
#include "key_mapper.hpp"
#include "queries.hpp"

#include "../errors.hpp"

#include "../config/config.hpp"

#include "../datatypes/regions.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../parser/wig.hpp"
#include "../processing/processing.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../exceptions.hpp"
#include "../log.hpp"

#include "retrieve.hpp"

namespace epidb {
  namespace dba {
    namespace retrieve {

      void insert_bed_regions(const ArrayView& arrobj, Regions &_regions, size_t& _it_count, size_t& _it_size,
                              const Position _query_start, const Position _query_end, DatasetId dataset_id,
                              const bool full_overlap, const bool reduced_mode);

      const size_t BULK_SIZE = 20000;

      struct RegionProcess {
        bool _full_overlap;
        bool _reduced_mode;
        size_t _it_count;
        size_t _it_size;
        Regions &_regions;
        Position _query_start;
        Position _query_end;

        RegionProcess(Regions &regions, Position query_start, Position query_end, bool full_overlap, bool reduced_mode) :
          _full_overlap(full_overlap),
          _reduced_mode(reduced_mode),
          _it_count(0),
          _it_size(0),
          _regions(regions),
          _query_start(query_start),
          _query_end(query_end)
        { }

        void read_region(const View &region_bson)
        {
          if (bson_utils::has_key(region_bson, KeyMapper::WIG_TRACK_TYPE())) {
            DatasetId dataset_id = region_bson[KeyMapper::DATASET()].get_int32().value;

            int track_type = region_bson[KeyMapper::WIG_TRACK_TYPE()].get_int32().value;

            // The following 4 Elements are optional, because it I use "get_safe_int32()"
            Position start = bson_utils::get_safe_int32(region_bson, KeyMapper::START());
            Length step = bson_utils::get_safe_int32(region_bson, KeyMapper::WIG_STEP());
            Length span = bson_utils::get_safe_int32(region_bson, KeyMapper::WIG_SPAN());
            Length size = bson_utils::get_safe_int32(region_bson, KeyMapper::FEATURES());

            auto binary = region_bson[KeyMapper::WIG_DATA()].get_binary();
            auto data = binary.bytes;
            auto db_data_size = binary.size;

            const Position *starts;
            const Position *ends;
            const Score *scores;
            if (track_type  == parser::FIXED_STEP) {
              scores = reinterpret_cast<const Score *>(data);
              for (Length i = 0;
                   (i < size) &&
                   (start + (i * step) < _query_end);
                   i++) {

                if ((start + (i * step) < _query_end) &&  // Region START < Range END
                    (start + (i * step) + span > _query_start)) { // Region END > Range START
                  RegionPtr region = build_wig_region(start + (i * step), start + (i * step) + span, dataset_id, scores[i]);
                  _it_size += region->size();
                  _regions.emplace_back(std::move(region));
                  _it_count++;
                }
              }

            } else if (track_type == parser::VARIABLE_STEP) {
              starts = reinterpret_cast<const Position *>(data);
              scores = reinterpret_cast<const Score *>(data + (size * sizeof(Position)));
              for (Length i = 0;
                   (i < size) &&
                   (starts[i] < _query_end);
                   i++) {

                if ((starts[i] < _query_end) &&  // Region START < Range END
                    ((starts[i] + span) > _query_start)) { // Region END > Range START

                  RegionPtr region = build_wig_region(starts[i], starts[i] + span, dataset_id, scores[i]);
                  _it_size += region->size();
                  _regions.emplace_back(std::move(region));
                  _it_count++;
                }
              }

            } else if ((track_type == parser::ENCODE_BEDGRAPH) || (track_type == parser::MISC_BEDGRAPH)) {
              starts = reinterpret_cast<const Position *>(data);
              ends = reinterpret_cast<const Position *>(data + (size * sizeof(Position)));
              scores = reinterpret_cast<const Score *>(data + (size * sizeof(Position) + (size * sizeof(Position))));

              for (Length i = 0;
                   (i < size) &&
                   (starts[i] < _query_end);
                   i++) {

                if ((starts[i] < _query_end) &&
                    (ends[i] > _query_start)) {

                  RegionPtr region = build_wig_region(starts[i], ends[i], dataset_id, scores[i]);
                  _it_size += region->size();
                  _regions.emplace_back(std::move(region));
                  _it_count++;
                }
              }
            }
          }

          else if (bson_utils::has_key(region_bson, KeyMapper::BED_DATA())) {
            DatasetId dataset_id = region_bson[KeyMapper::DATASET()].get_int32().value;
            auto arrobj = region_bson[KeyMapper::BED_DATA()].get_array().value;
            insert_bed_regions(arrobj, _regions, _it_count, _it_size, _query_start, _query_end, dataset_id, _full_overlap, _reduced_mode);
          }
        }
      };

      inline void insert_bed_regions(const ArrayView& arrobj, Regions &_regions, size_t& _it_count, size_t& _it_size,
                                     const Position _query_start, const Position _query_end, DatasetId dataset_id,
                                     bool full_overlap, bool reduced_mode)
      {
        for (const auto& e: arrobj) {
          const auto &region_bson = e.get_document().value;

          auto i = region_bson.begin();
          Position start = i->get_int32().value;
          i++;
          Position end = i->get_int32().value;
          i++;

          if (full_overlap) {
            if ((start < _query_start) || (end > _query_end)) {
              continue;
            }
          } else {
            if ((start >= _query_end) || (end <= _query_start)) {
              continue;
            }
          }

          RegionPtr region;
          if (reduced_mode) {
            region = build_simple_region(start, end, dataset_id);
          } else {
            region = build_bed_region(start, end, dataset_id);
            while ( i != region_bson.end()) {
              switch (i->type()) {
              case UTF8Type:
                region->insert(StringViewToString(i->get_utf8().value));
                break;
              case DoubleType:
                region->insert((float) i->get_double().value);
                break;
              case Int32Type:
                region->insert(static_cast<int32_t>(i->get_int32().value));
                break;
              case Int64Type:
                region->insert(static_cast<int32_t>(i->get_int64().value));
                break;
              default:
                region->insert(utils::element_to_string(*i));
              }
              i++;
            }
          }
          _it_size += region->size();
          _regions.emplace_back(std::move(region));
          _it_count++;
        }
      }

      FindOptions get_start_end_and_opts(const View &regions_query, Position& start, Position& end)
      {
        if (bson_utils::has_key(regions_query, KeyMapper::START())) {
          auto o = regions_query[KeyMapper::START()].get_document().value;
          end = o["$lte"].get_int32().value;
        } else {
          end = std::numeric_limits<Position>::max();
        }

        if (bson_utils::has_key(regions_query, KeyMapper::END())) {
          auto o = regions_query[KeyMapper::END()].get_document().value;
          start = o["$gte"].get_int32().value;
        } else {
          start = std::numeric_limits<Position>::min();
        }

        auto hint_and_sort_doc = MakeDocument(
          KVP(KeyMapper::DATASET() , 1),
          KVP(KeyMapper::START(), 1),
          KVP(KeyMapper::END(), 1)
        );

        FindOptions opts{};
        opts.hint(mongocxx::hint{hint_and_sort_doc.view()});
        opts.sort(hint_and_sort_doc.view());
        opts.no_cursor_timeout(true);
        opts.batch_size(BULK_SIZE);

        return opts;
      }

      const Regions get_regions_from_collection(const std::string &collection, const View &regions_query, const bool full_overlap,
                                       processing::StatusPtr status, bool reduced_mode)
      {
        Regions regions;

        COLLECTION(COLL, collection)
        auto count = helpers::count(collection, regions_query);

        if (count == 0) {
          return regions;
        }

        regions.reserve(count);

        Position start;
        Position end;
        auto opts = get_start_end_and_opts(regions_query, start, end);
        auto cursor = COLL.find(regions_query, opts);

        RegionProcess rp(regions, start, end, full_overlap, reduced_mode);

        for (auto&& o: cursor ) {
            // Check if processing was canceled
            IS_PROCESSING_CANCELLED(status);

            rp.read_region(o);
            status->sum_regions(rp._it_count);

            // Check memory consumption
            if (!status->sum_and_check_size(rp._it_size)) {
              throw deepblue_user_exception(ERR_MEMORY_EXAUSTED, status->total_size(), status->maximum_size());
            }

            // Reset iteration stats
            rp._it_count = 0;
            rp._it_size = 0;
        }

        std::sort(regions.begin(), regions.end(), RegionPtrComparer);

        return regions;
      }

      std::tuple<bool, std::string> get_regions_job(const std::string &genome, const std::shared_ptr<std::vector<std::string> > chromosomes,
          const View &regions_query, const bool full_overlap, const bool reduced_mode,
          processing::StatusPtr status, std::shared_ptr<ChromosomeRegionsList> result)
      {

        for (std::vector<std::string>::const_iterator chrom_it = chromosomes->begin(); chrom_it != chromosomes->end(); chrom_it++) {
          std::string collection = helpers::region_collection_name(genome, *chrom_it);
          Regions regions = get_regions_from_collection(collection, regions_query, full_overlap, status, reduced_mode);

          if (regions.size() > 0) {
            ChromosomeRegions chromosomeRegions(*chrom_it, std::move(regions));
            result->push_back(std::move(chromosomeRegions));
          }
        }

        return std::make_tuple(true, std::string(""));
      }

      const Regions get_regions_preview(const std::string &genome, const std::string &chromosome, const View &regions_query)
      {
        Regions regions;
        std::string collection = helpers::region_collection_name(genome, chromosome);

        COLLECTION(COLL, collection)
        auto count = helpers::count(collection, regions_query);
        if (count == 0) {
          return regions;
        }

        regions.reserve(count);

        Position start;
        Position end;
        auto opts = get_start_end_and_opts(regions_query, start, end);
        auto cursor = COLL.find(regions_query, opts);

        for (const auto &c: cursor) {
          if (regions.size() > 5) {
            break;
          }
          RegionProcess rp(regions, start, end, true, false);
          rp.read_region(c);
        }

        return regions;
      }

      const Regions get_regions(const std::string &genome, const std::string &chromosome,
                       const View &regions_query, const bool full_overlap,
                       processing::StatusPtr status, bool reduced_mode)
      {
        std::string collection = helpers::region_collection_name(genome, chromosome);
        return get_regions_from_collection(collection, regions_query, full_overlap, status, reduced_mode);
      }

      const ChromosomeRegionsList get_regions(const std::string &genome, const std::vector<std::string> &chromosomes,
                                              const View &regions_query, const bool full_overlap,
                                              processing::StatusPtr status, bool reduced_mode)
      {
        const size_t max_threads = 8;

        std::vector<std::future<std::tuple<bool, std::string> > > threads;
        std::vector<std::shared_ptr<ChromosomeRegionsList> > result_parts;

        size_t chunk_size = ceil(double(chromosomes.size()) / double(max_threads));

        for (size_t i = 0; i < max_threads; ++i) {
          std::vector<std::string>::const_iterator start = chromosomes.begin() + i * chunk_size;
          if (start >= chromosomes.end()) {
            break;
          }
          std::vector<std::string>::const_iterator end = start + chunk_size;
          if (end > chromosomes.end()) {
            end = chromosomes.end();
          }

          std::shared_ptr<std::vector<std::string> > chrs(new std::vector<std::string>(start, end));
          std::shared_ptr<ChromosomeRegionsList> result_part(new ChromosomeRegionsList);

          auto t = std::async(std::launch::async,
                              &get_regions_job,
                              std::ref(genome), chrs, std::ref(regions_query),
                              full_overlap, reduced_mode, status, result_part);

          threads.push_back(std::move(t));
          result_parts.push_back(result_part);
        }

        // kill threads
        size_t thread_count = threads.size();
        for (size_t i = 0; i < thread_count; ++i) {
          threads[i].wait();
          auto result = threads[i].get();
          if (!std::get<0>(result)) {
            auto msg = std::get<1>(result);
            throw deepblue_user_exception(msg);
          }
        }

        // unite results
        ChromosomeRegionsList results;
        for (auto &chromosome_regions_pre_result : result_parts) {
          for (auto &chromosome_regions_list : *chromosome_regions_pre_result) {
            results.push_back(std::move(chromosome_regions_list));
          }
        }

        return results;
      }

      bool count_regions(const std::string &genome, const std::string &chromosome, const View &regions_query, const bool full_overlap,
                         processing::StatusPtr status, size_t &count)
      {
        std::string collection_name = helpers::region_collection_name(genome, chromosome);
        std::string msg;
        Regions regions = get_regions_from_collection(collection_name, regions_query, full_overlap, status, true);
        count = regions.size();
        return true;
      }

      void count_regions_job(const std::string &genome, const std::shared_ptr<std::vector<std::string> > chromosomes,
                             const View &regions_query, const bool full_overlap,
                             processing::StatusPtr status, std::shared_ptr<std::vector<size_t> > results)
      {
        size_t size = 0;
        for (std::vector<std::string>::const_iterator it = chromosomes->begin(); it != chromosomes->end(); ++it) {
          std::string collection_name = helpers::region_collection_name(genome, *it);
          Regions regions = get_regions_from_collection(collection_name, regions_query, full_overlap, status, true);
          size += regions.size();;
        }
        results->push_back(size);
      }

      bool count_regions(const std::string &genome, const std::vector<std::string> &chromosomes,
                         const View &regions_query, const bool full_overlap,
                         processing::StatusPtr status, size_t &size, std::string &msg)
      {
        const size_t max_threads = 8;
        std::vector<boost::thread *> threads;
        std::vector<std::shared_ptr<std::vector<size_t> > > result_parts;

        size_t chunk_size = ceil(double(chromosomes.size()) / double(max_threads));

        for (size_t i = 0; i < max_threads; ++i) {
          std::vector<std::string>::const_iterator start = chromosomes.begin() + i * chunk_size;
          if (start >= chromosomes.end()) {
            break;
          }
          std::vector<std::string>::const_iterator end = start + chunk_size;
          if (end > chromosomes.end()) {
            end = chromosomes.end();
          }
          std::shared_ptr<std::vector<std::string> > chrs(new std::vector<std::string>(start, end));
          std::shared_ptr<std::vector<size_t> > result_part(new std::vector<size_t>);
          boost::thread *t = new boost::thread(&count_regions_job, boost::cref(genome), chrs,
                                               boost::cref(regions_query), full_overlap, status, result_part);
          threads.push_back(t);
          result_parts.push_back(result_part);
        }

        // kill threads
        size_t thread_count = threads.size();
        for (size_t i = 0; i < thread_count; ++i) {
          threads[i]->join();
          delete threads[i];
        }

        size = 0;
        std::vector<std::shared_ptr<std::vector<size_t> > >::iterator it;
        for (it = result_parts.begin(); it != result_parts.end(); ++it) {
          std::vector<size_t> v = **it;
          size += std::accumulate(v.begin(), v.end(), 0ll);
        }
        return true;
      }
    }
  }
}
