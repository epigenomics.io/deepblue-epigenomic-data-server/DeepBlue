//
//  engine.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.04.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <regex>
#include <string>
#include <sstream>
#include <vector>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/stream.hpp>

#include "commands.hpp"
#include "engine.hpp"

#include "../config/config.hpp"

#include "../dba/queries.hpp"
#include "../dba/users.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/stringbuilder.hpp"
#include "../extras/utils.hpp"

#include "../mdbq/common.hpp"
#include "../mdbq/cleaner.hpp"

#include "../processing/processing.hpp"

#include "../storage/storage.hpp"

#include "../log.hpp"
#include "../version.hpp"

namespace epidb {

  Engine::Engine()
    : _hub(config::get_mongodb_server(), config::DATABASE_NAME())
  {
    EPIDB_LOG("Creating Engine");
  }


  bool Engine::execute(const std::string &name, const std::string &ip, unsigned long long id,
                 serialize::Parameters &parameters, serialize::Parameters& results) const
  {
    const Command *command = Command::get_command(name);
    if (command == 0) {
      std::stringstream ss;
      ss << "Request (" << id << ") from " + ip << ": " << "Command " << name << " does not exists.";
      throw_runtime_error(ss.str());
    }

    std::string msg;
    if (!command->check_parameters(parameters, msg)) {
      std::stringstream ss;
      ss << "Request (" << id << ") from " << ip << ": " << name << " with bad typed parameters (" <<
                parameters.string(true) << "). " << msg;
      throw_runtime_error(ss.str());
    }

    EPIDB_LOG("Request (" << id << ") from " << ip << ": " << name << "(" <<  parameters.string(true) << ")");
    return command->run(ip, parameters, results);
  }

  const std::string Engine::queue(const View &job, unsigned int timeout)
  {
    std::string existing_id;
    if (_hub.exists_job(utils::unroll_object("misc.", job), existing_id, true)) {
      return existing_id;
    }

    return _hub.insert_job(job, timeout, Version::version_value());
  }

  const std::string Engine::queue_count_regions(const datatypes::User& user, const std::string &query_id)
  {
    return queue(MakeDocument(
      KVP("command", "count_regions"),
      KVP("query_id", query_id),
      KVP("user_id", user.id())
    ),60 * 60);
  }

  const std::string Engine::queue_binning(const datatypes::User& user, const std::string &query_id, const std::string &column_name, const int bars)
  {
    return queue(MakeDocument(
        KVP("command", "binning"),
        KVP("query_id", query_id),
        KVP("column_name", column_name),
        KVP("bars", bars),
        KVP("user_id", user.id())), 60 * 60);
  }

  const std::string Engine::queue_distinct(const datatypes::User& user, const std::string &query_id, const std::string &column_name)
  {
    return queue(MakeDocument(
      KVP("command", "distinct"),
      KVP("query_id", query_id),
      KVP("column_name", column_name),
      KVP("user_id", user.id())), 60 * 60);
  }

  const std::string Engine::queue_calculate_enrichment(const datatypes::User& user, const std::string &query_id, const std::string &gene_model)
  {
    return queue(MakeDocument(
      KVP("command", "calculate_enrichment"),
      KVP("query_id", query_id),
      KVP("gene_model", gene_model),
      KVP("user_id", user.id())), 60 * 60);
  }

  const std::string Engine::queue_coverage(const datatypes::User& user, const std::string &query_id, const std::string &genome)
  {
    return queue(MakeDocument(
      KVP("command", "coverage"),
      KVP("query_id", query_id),
      KVP("genome", genome),
      KVP("user_id", user.id())), 60 * 60);
  }

  const std::string Engine::queue_get_regions(const datatypes::User& user, const std::string &query_id, const std::string &output_format)
  {
    return queue(MakeDocument(
      KVP("command", "get_regions"),
      KVP("query_id", query_id),
      KVP("format", output_format),
      KVP("user_id", user.id())), 60 * 60);
  }

  const std::string Engine::queue_score_matrix(const datatypes::User& user,
                                  const std::vector<std::pair<std::string, std::string>> &experiments_formats,
                                  const std::string &aggregation_function, const std::string &regions_query_id)
  {
    ArrayBuilder bob_formats;

    for (auto &exp_format : experiments_formats) {
      bob_formats.append(bson_utils::vector_to_array({exp_format.first, exp_format.second}));
    }

    return queue(MakeDocument(
      KVP("command", "score_matrix"),
      KVP("experiments_formats", bob_formats.extract()),
      KVP("aggregation_function", aggregation_function),
      KVP("query_id", regions_query_id),
      KVP("user_id", user.id())), 60 * 60);
  }

  const std::string Engine::queue_lola(const datatypes::User& user,
                          const std::string& query_id, const std::string& universe_query_id,
                          const std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> &databases,
                          const std::string& genome)
  {
    DocumentBuilder databases_builder;

    for (const auto &database : databases) {
      ArrayBuilder bab;
      const std::string &name = database.first;
      for (const auto& experiment: database.second) {
        bab.append(MakeDocument(
          KVP("name", experiment.first),
          KVP("description", experiment.second)));
      }
      databases_builder.append(KVP(name, bab.extract()));
    }

    return queue(MakeDocument(
          KVP("command", "lola"),
          KVP("query_id", query_id),
          KVP("universe_query_id", universe_query_id),
          KVP("databases", databases_builder.extract()),
          KVP("genome", genome),
          KVP("user_id", user.id())), 60 * 60);
  }


  const std::string Engine::queue_region_enrich_fast(const datatypes::User& user,
                                        const std::string& query_id, const View &query)
  {
    return queue(MakeDocument(
          KVP("command", "enrich_regions_fast"),
          KVP("query_id", query_id),
          KVP("experiments_query", query),
          KVP("user_id", user.id())), 60 * 60);
  }


  const std::string Engine::queue_get_experiments_by_query(const datatypes::User& user, const std::string &query_id)
  {
    return queue(MakeDocument(
      KVP("command", "get_experiments_by_query"),
      KVP("query_id", query_id),
      KVP("user_id", user.id())), 60 * 60);
  }


  const request::Status Engine::request_status(const datatypes::User& user, const std::string &request_id)
  {
    auto maybe_o = _hub.get_job(request_id); // TODO still check
    if (!maybe_o) {
      throw deepblue_user_exception(ERR_REQUEST_ID_INVALID, request_id);
    }

    auto o = maybe_o->view();

    request::Status request_status;
    request_status.state = mdbq::Hub::state_name(o);
    request_status.message = mdbq::Hub::state_message(o);
    return request_status;
  }

  request::Job Engine::get_job_info(const View& o)
  {
    request::Job job;
    request::Status status;

    status.state = mdbq::Hub::state_name(o);
    status.message = mdbq::Hub::state_message(o);
    job.status = status;

    job.create_time = mdbq::Hub::get_create_time(o);
    if (status.state == mdbq::Hub::state_name(mdbq::TS_DONE)) {
      job.finish_time = mdbq::Hub::get_finish_time(o);
    }

    const auto misc_obj = mdbq::Hub::get_misc(o);
    auto misc = misc_obj.view();
    job.command = bson_utils::get_string(misc, "command");
    job.user_id = bson_utils::get_string(misc, "user_id");
    job.query_id = bson_utils::get_string(misc, "query_id");

    if (bson_utils::has_key(misc, "format")) {
      job.misc["format"] = bson_utils::get_string(misc, "format");
    }

    if (bson_utils::has_key(misc, "aggregation_function")) {
      job.misc["aggregation_function"] = bson_utils::get_string(misc, "aggregation_function");
    }

    if (bson_utils::has_key(misc, "experiments_formats")) {
      const auto& experiments_formats = misc["experiments_formats"].get_array().value;
      for ( const auto& e: experiments_formats) {
        job.misc[std::string("experiment:") + StringViewToString(e[0].get_utf8().value)] = StringViewToString(e[1].get_utf8().value);
      }
    }

    job._id = mdbq::Hub::get_id(o);

    return job;
  }

  const request::Job Engine::request_job(const std::string & request_id)
  {
    auto maybe_o = _hub.get_job(request_id); // TODO still check
    if (!maybe_o) {
      throw deepblue_user_exception(ERR_REQUEST_ID_INVALID, request_id);
    }
    auto o = maybe_o->view();
    return get_job_info(o);
  }

  const std::vector<request::Job> Engine::request_jobs(const datatypes::User& user, const std::string & status_find)
  {
    mdbq::TaskState task_state = mdbq::Hub::state_number(status_find);
    auto jobs_bson = _hub.get_jobs(task_state, user.id());
    std::vector<request::Job> ret;
    for (auto &job_bson : jobs_bson) {
      ret.push_back(get_job_info(job_bson));
    }
    return ret;
  }

  void Engine::validate_request(const datatypes::User& user, const std::string & request_id)
  {
    auto maybe_o = _hub.get_job(request_id, true); //TODO still check
    if (!maybe_o) {
      throw deepblue_user_exception(ERR_REQUEST_ID_INVALID, request_id);
    }

    auto o = maybe_o->view();
    if (mdbq::Hub::is_failed(o)) {
      auto err_msg = StringViewToString(o["error"].get_utf8().value);
      throw deepblue_user_exception(ERR_REQUEST_ERROR, request_id, err_msg);
    }

    if (mdbq::Hub::is_cleared(o)) {
      _hub.reprocess_job(o);
      throw deepblue_user_exception(ERR_REQUEST_CLEARED, request_id);
    }

    if (!mdbq::Hub::is_done(o)) {
      throw deepblue_user_exception(ERR_REQUEST_NOT_FINISHED, request_id);
    }
  }

  const std::string Engine::request_download_data(const datatypes::User& user, const std::string & request_id)
  {
    validate_request(user, request_id);

    auto maybe_o = _hub.get_job(request_id); //TODO still check
    auto o = maybe_o->view();

    auto result = o["result"].get_document().value;

    if (bson_utils::has_key(result, "__file__")) {
      std::string file_name = bson_utils::get_string(result, "__file__");
      // Get compressed file from mongo filesystem
      return storage::load(file_name);
    }

    throw_runtime_error("Request ID '" + request_id + "' does not contain a __file__ with result.");
  }


  const serialize::Parameters Engine::request_data(const datatypes::User& user, const std::string & request_id)
  {
    std::string msg;
    validate_request(user, request_id);

    auto maybe_o = _hub.get_job(request_id, true);
    auto o = maybe_o->view();
    auto result = o["result"].get_document().value;

    serialize::Parameters request_data;

    if (bson_utils::has_key(result, "__id_names__")) {
      auto id_names = result["__id_names__"].get_document().value;
      request_data.add_param(utils::bson_to_parameters(id_names));

    } else if (bson_utils::has_key(result, "__file__")) {
      std::string file_name = bson_utils::get_string(result, "__file__");

      // Get compressed file from mongo filesystem
      auto file_content = storage::load(file_name);

      // uncompress
      std::istringstream inStream(file_content, std::ios::binary);
      std::stringstream outStream;
      boost::iostreams::filtering_streambuf< boost::iostreams::input> in;
      in.push( boost::iostreams::bzip2_decompressor());
      in.push( inStream );
      // copy output
      boost::iostreams::copy(in, outStream);

      request_data.add_string_content(outStream.str());
      return request_data;

    } else {
      request_data.add_param(utils::bson_to_parameters(result));
    }

    return request_data;
  }


  const bool Engine::user_owns_request(const std::string & request_id, const std::string & user_id)
  {
    return _hub.job_has_user_id(request_id, user_id);
  }

  const bool Engine::reprocess_request(const datatypes::User& user, const std::string & request_id)
  {
    validate_request(user, request_id);
    auto maybe_o = _hub.get_job(request_id);
    return _hub.reprocess_job(maybe_o->view());
  }

  const bool Engine::cancel_request(const datatypes::User& user, const std::string & request_id)
  {
    return mdbq::cancel_request(user, request_id);
  }

  const bool Engine::remove_request_data(const std::string & request_id, const mdbq::TaskState state)
  {
    return mdbq::remove_request_data(request_id, state);
  }
}