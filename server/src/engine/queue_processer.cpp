//
//  queue_processer.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 22.01.15.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>
#include <regex>
#include <string>
#include <sstream>

#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/stream.hpp>

#include <mongocxx/exception/exception.hpp>

#include "../config/config.hpp"

#include "../datatypes/user.hpp"

#include "../dba/users.hpp"
#include "../dba/list.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/stringbuilder.hpp"
#include "../extras/utils.hpp"

#include "../mdbq/client.hpp"
#include "../mdbq/janitor.hpp"

#include "../processing/processing.hpp"

#include "../errors.hpp"
#include "../exceptions.hpp"
#include "../log.hpp"

#include "queue_processer.hpp"

namespace epidb {
  namespace engine {

    QueueHandler::QueueHandler(size_t id, std::string &url, std::string &prefix) :
      mdbq::Client(url, prefix),
      _id(id) {}

    void QueueHandler::run()
    {
      EPIDB_LOG_TRACE("Starting QueueHandler - " << utils::integer_to_string(_id));
      this->reg(ios, 1);
      ios.run();
    }

    void QueueHandler::handle_task(const std::string& r_id, const View &o)
    {
      auto user_id = bson_utils::get_string(o, "user_id");
      auto command = bson_utils::get_string(o, "command");

      bool success = false;
      MaybeDocument result;

      try {
        auto user = dba::users::get_user_by_id(user_id);
        processing::StatusPtr status = processing::build_status(r_id, user.memory_limit());
        result = process(user, o, status);
        success = true;

      // TODO: put the others catch here!
      } catch (const mongocxx::exception& e) {
        result = MakeDocument(KVP("__error__", e.what()), KVP("where", command), KVP("r_id", r_id));
        success = false;

      } catch (const deepblue_user_exception& e) {
        result = MakeDocument(KVP("__error__", e.msg()), KVP("where", command), KVP("r_id", r_id));
        success = false;

      } catch (const std::exception& e) {
        result = MakeDocument(KVP("__error__", e.what()), KVP("where", command), KVP("r_id", r_id));
        success = false;

      } catch (const std::string& ex) {
        result = MakeDocument(KVP("__error__", ex), KVP("where", command), KVP("r_id", r_id));
        success = false;
      }

      finish(result->view(), success);
    }

    const Document QueueHandler::process(const datatypes::User &user, const View &job, processing::StatusPtr status)
    {
      std::string command = bson_utils::get_string(job, "command");

      if (command == "count_regions") {
        return process_count(user, bson_utils::get_string(job, "query_id"), status);
      }
      if (command == "coverage") {
        return process_coverage(user, bson_utils::get_string(job, "query_id"),
                                      bson_utils::get_string(job, "genome"),
                                      status);
      }
      if (command == "get_regions") {
        return process_get_regions(user, bson_utils::get_string(job, "query_id"),
                                         bson_utils::get_string(job, "format"),
                                         status);
      }
      if (command == "score_matrix") {
        return process_score_matrix(user, job["experiments_formats"].get_array().value,
                                bson_utils::get_string(job,"aggregation_function"),
                                bson_utils::get_string(job, "query_id"),
                                status);
      }
      if (command == "get_experiments_by_query") {
        return process_get_experiments_by_query(user, bson_utils::get_string(job, "query_id"), status);
      }
      if (command == "binning") {
        return process_binning(user, bson_utils::get_string(job, "query_id"),
                                     bson_utils::get_string(job, "column_name"),
                                     job["bars"].get_int32().value,
                                     status);
      }
      if (command == "distinct") {
        return process_distinct(user, bson_utils::get_string(job, "query_id"),
                                      bson_utils::get_string(job, "column_name"),
                                      status);
      }
      if (command == "calculate_enrichment") {
        return process_calculate_enrichment(user, bson_utils::get_string(job, "query_id"),
                                                  bson_utils::get_string(job, "gene_model"),
                                                  status);
      }
      if (command == "lola") {
        return process_lola(user, bson_utils::get_string(job, "query_id"),
                                  bson_utils::get_string(job, "universe_query_id"),
                                  job["databases"].get_document().value,
                                  bson_utils::get_string(job, "genome"),
                                  status);
      }
      if (command == "enrich_regions_fast") {
        return process_enrich_regions_fast(user, bson_utils::get_string(job, "query_id"),
                                                 job["experiments_query"].get_document().value,
                                                 status);

      } else {
        DocumentBuilder bob;
        datatypes::User user;

        bob.append(KVP("__error__", "Invalid command " + command));
        return bson_utils::one_key_doc("__error__", "Invalid command " + command);
      }
    }

    const Document QueueHandler::process_count(const datatypes::User &user, const std::string &query_id,
                                     processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto count = processing::count_regions(user, query_id, status);

      status->set_total_stored_data(sizeof(long long));
      status->set_total_stored_data_compressed(sizeof(long long));

      DocumentBuilder bob;
      bob.append(KVP("count", static_cast<int64_t>(count)));
      return bob.extract();
    }

    const Document QueueHandler::process_binning(const datatypes::User &user,
                                       const std::string &query_id, const std::string& column_name, const int bars,
                                       processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto binning = processing::binning(user, query_id, column_name, bars, status);

      auto size = binning.view().length();

      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("binning", binning));
      return bob.extract();
    }

    const Document QueueHandler::process_distinct(const datatypes::User &user,
                                        const std::string &query_id, const std::string& column_name,
                                        processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto distinct = processing::distinct(user, query_id, column_name, status);

      auto size = distinct.view().length();

      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("distinct", distinct));
      return bob.extract();
    }

    const Document QueueHandler::process_calculate_enrichment(const datatypes::User &user, const std::string &query_id,
                                  const std::string& gene_model, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto enrichment = processing::calculate_enrichment(user, query_id, gene_model, status);

      int size = enrichment.view().length();
      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("enrichment", enrichment));
      return bob.extract();
    }

    const Document QueueHandler::process_lola(const datatypes::User &user,
                                    const std::string &query_id, const std::string &universe_query_id,
                                    const View& datasets, const std::string& genome,
                                    processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto enrichment = processing::lola(user, query_id, universe_query_id, datasets, genome, status);

      int size = enrichment.view().length();
      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("enrichment", enrichment));
      return bob.extract();
    }


    const Document QueueHandler::process_enrich_regions_fast(const datatypes::User &user,
              const std::string &query_id, const View &experiments_query, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      std::vector<utils::IdName> names  = dba::list::experiments(experiments_query);
      auto enrichment = processing::enrich_regions_fast(user, query_id, names, status);

      int size = enrichment.view().length();
      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("enrichment", enrichment));
      return bob.extract();
    }


    const Document QueueHandler::process_coverage(const datatypes::User &user, const std::string &query_id, const std::string &genome,
                                        processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto coverage_infos = processing::coverage(user, query_id, genome, status);

      DocumentBuilder coverages_bob;
      for (const auto &coverage_info : coverage_infos) {
        coverages_bob.append(KVP(coverage_info.chromosome_name,
                              MakeDocument(
                                KVP("size", static_cast<int64_t>(coverage_info.chromosome_size)),
                                KVP("total", static_cast<int64_t>(coverage_info.total)),
                                KVP("coverage", static_cast<double>(coverage_info.total * 100.0) / coverage_info.chromosome_size)
                              )
                            ));
      }

      auto o = coverages_bob.extract();
      int size = o.view().length();

      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("coverages", o));
      return bob.extract();
    }

    const Document QueueHandler::process_get_regions(const datatypes::User &user,
                                    const std::string &query_id, const std::string &format,
                                    processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)

      const auto&& sb = processing::get_regions(user, query_id, format, status);

      status->start_operation(processing::BUILDING_OUTPUT,
                              MakeDocument(KVP("string_builder_size", static_cast<int64_t>(sb.size()))));

      std::string result_string = sb.to_string();

      status->start_operation(processing::COMPRESSING_OUTPUT,
                              MakeDocument(KVP("string_size", static_cast<int64_t>(result_string.size()))));

      std::stringbuf inStream(std::move(result_string));
      std::stringbuf outStream;
      boost::iostreams::filtering_streambuf< boost::iostreams::input> in;
      in.push( boost::iostreams::bzip2_compressor());
      in.push( inStream );
      boost::iostreams::copy(in, outStream);

      std::string compressed_s = outStream.str();
      auto compressed = compressed_s.data();

      std::string filename = store_result(reinterpret_cast<const uint8_t*>(compressed), compressed_s.size());

      DocumentBuilder bob;
      bob.append(KVP("__file__", filename));
      bob.append(KVP("__original_size__", static_cast<int64_t>(result_string.size())));
      bob.append(KVP("__compressed_size__", static_cast<int64_t>(compressed_s.size())));

      status->set_total_stored_data(result_string.size());
      status->set_total_stored_data_compressed(compressed_s.size());

      return bob.extract();
    }

    const Document QueueHandler::process_score_matrix(const datatypes::User &user,
                                            const ArrayView &experiments_formats_bson, const std::string &aggregation_function, const std::string &regions_query_id, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)

      std::vector<std::pair<std::string, std::string>> experiments_formats;
      for (const auto& e: experiments_formats_bson) {
        std::string experiment_name = StringViewToString(e[0].get_utf8().value);
        std::string columns_name = StringViewToString(e[1].get_utf8().value);
        experiments_formats.emplace_back(experiment_name, columns_name);
      }

      const auto& sb = processing::score_matrix(user, experiments_formats, aggregation_function, regions_query_id, status);

      std::string matrix = sb.to_string();
      size_t matrix_size = matrix.size();
      std::stringbuf inStream(std::move(matrix));
      std::stringbuf outStream;
      boost::iostreams::filtering_streambuf< boost::iostreams::input> in;
      in.push( boost::iostreams::bzip2_compressor());
      in.push( inStream );
      boost::iostreams::copy(in, outStream);

      std::string compressed_s = outStream.str();
      const char* compressed = compressed_s.data();

      std::string filename = store_result(reinterpret_cast<const uint8_t *>(compressed), compressed_s.size());

      DocumentBuilder bob;
      bob.append(KVP("__file__", filename));
      bob.append(KVP("__original_size__", static_cast<int64_t>(matrix_size)));
      bob.append(KVP("__compressed_size__", static_cast<int64_t>(compressed_s.size())));

      status->set_total_stored_data(matrix_size);
      status->set_total_stored_data_compressed(compressed_s.size());

      return bob.extract();
    }

    const Document QueueHandler::process_get_experiments_by_query(const datatypes::User &user,
        const std::string &query_id, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status)
      auto experiments = processing::get_experiments_by_query(user, query_id, status);

      DocumentBuilder experiments_ids_bob;
      for (auto &exp_format : experiments) {
        experiments_ids_bob.append(KVP(exp_format.id, exp_format.name));
      }
      auto o = experiments_ids_bob.extract();

      int size = o.view().length();
      status->set_total_stored_data(size);
      status->set_total_stored_data_compressed(size);

      DocumentBuilder bob;
      bob.append(KVP("__id_names__", o));
      return bob.extract();
    }

    const bool QueueHandler::is_canceled(processing::StatusPtr status)
    {
      if (status->is_canceled()) {
        return true;
      }
      return false;
    }

    void queue_processer_run(size_t num)
    {
      boost::asio::io_service io;

      std::string server = config::get_mongodb_server();
      std::string collection = config::DATABASE_NAME();

      QueueHandler *clients[num];
      boost::thread *threads[num];

      for (size_t i = 0; i < num; ++i) {
        clients[i] = new QueueHandler(i, server, collection);
        threads[i] = new boost::thread(boost::bind(&QueueHandler::run, clients[i]));
      }

      mdbq::Janitor* janitor = new mdbq::Janitor();

      config::get_config_subject()->attachObserver(janitor);
      boost::thread *t = new boost::thread(boost::bind(&mdbq::Janitor::run, janitor));
    }
  }
}