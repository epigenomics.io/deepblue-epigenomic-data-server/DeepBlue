//
//  parser_factory.h
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 05.06.13.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_PARSER_PARSER_FACTORY_HPP
#define EPIDB_PARSER_PARSER_FACTORY_HPP


#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

#include <experimental/optional>

#include "../dba/column_types.hpp"

#include "../typedefs/bson.hpp"

namespace epidb {
  namespace parser {

    class BedLine {
    public:
      std::string chromosome;
      int32_t start;
      int32_t end;
      std::vector<std::string> tokens;
      size_t size() const;
    };

    bool operator<(const BedLine &a, const BedLine &b);

    typedef std::vector<parser::BedLine> BedLines;
    typedef std::pair<std::string, BedLines> ChromosomeBedLines;

    typedef struct ChromosomeRegionsMap {
      typedef std::map<std::string, BedLines>::iterator iterator;
      typedef std::map<std::string, BedLines>::const_iterator const_iterator;
      std::map<std::string, BedLines > data_;

      iterator begin()
      {
        return data_.begin();
      }

      const_iterator begin() const
      {
        return data_.begin();
      }

      iterator end()
      {
        return data_.end();
      }

      const_iterator end() const
      {
        return data_.end();
      }

      void insert(BedLine  &&region)
      {
        const std::string &chromosome = region.chromosome;
        if (data_.find(chromosome) == data_.end()) {
          data_[chromosome] = std::vector<parser::BedLine>();
        }
        data_[chromosome].push_back(std::move(region));
      }

      void finish()
      {
        for (auto &chromosome : data_) {
          std::vector<parser::BedLine> &regions = chromosome.second;
          std::sort(regions.begin(), regions.end());
        }
      }

      std::vector<std::string> chromosomes()
      {
        std::vector<std::string> chromosomes;
        for (auto kv : data_) {
          chromosomes.push_back(kv.first);
        }
        return chromosomes;
      }

    } ChromosomeRegionsMap;

    typedef std::map<std::string, std::string> ParsedLine;
    //typedef std::vector<bed_line> Tokens;

    // TODO: move to bed_format.hpp
    class FileFormat {
    private:
      std::string format_;
      std::vector<dba::columns::ColumnTypePtr> fields_;

      static const FileFormat default_format_builder();
      static const FileFormat wig_format_builder();
      static const FileFormat gtf_format_builder();
      static const FileFormat cufflinks_format_builder();
      static const FileFormat grape2_format_builder();
      static const FileFormat salmon_format_builder();

    public:
      typedef std::vector<dba::columns::ColumnTypePtr>::iterator iterator;
      typedef std::vector<dba::columns::ColumnTypePtr>::const_iterator const_iterator;

      Array to_array() const;

      static const FileFormat default_format();
      static const FileFormat wig_format();
      static const FileFormat gtf_format();
      static const FileFormat cufflinks_format();
      static const FileFormat grape2_format();
      static const FileFormat salmon_format();

      bool operator==(const FileFormat &other) const
      {
        if (size() != other.size()) {
          return false;
        }

        for (auto& this_field : fields_) {
          bool found = false;
          for (auto& other_field : other) {
            if (this_field->name() == other_field->name()) {
              found = true;
              break;
            }
          }
          if (!found) {
            return false;
          }
        }

        return true;
      }

      bool operator !=(const FileFormat &other) const
      {
        return !(*this == other);
      }

      void set_format(const std::string& format)
      {
        format_ = format;
      }

      const std::string format() const
      {
        return format_;
      }

      size_t size() const
      {
        return fields_.size();
      }

      iterator begin()
      {
        return fields_.begin();
      }

      const_iterator begin() const
      {
        return fields_.begin();
      }

      iterator end()
      {
        return fields_.end();
      }

      const_iterator end() const
      {
        return fields_.end();
      }

      void add(dba::columns::ColumnTypePtr column)
      {
        fields_.push_back(column);
      }
    };

    class FileFormatBuilder {
    public:
      static const FileFormat build(const std::string &format);
      static const FileFormat build_for_outout(const std::string &format, const std::vector<Document> &experiment_columns);
      static const FileFormat deduce_format(const std::string &conten);
      static void validate_output(const std::string &format);

    private:
      static epidb::dba::columns::ColumnTypePtr build_metafield_column(const std::string &name);
    };

    class Parser {
    private:
      Parser();
      std::string actual_line_content_;
      size_t actual_line_;
      bool first_column_useless;
      std::unique_ptr<std::istream> input_;
      bool first_;
      FileFormat format_;
      size_t chromosome_pos;
      size_t start_pos;
      size_t end_pos;

    public:
      Parser(std::unique_ptr<std::istream> &&input, FileFormat &format);
      void validate_format();
      std::experimental::optional<BedLine> parse_line();
      size_t actual_line();
      const std::string actual_line_content();
      size_t count_fields();
      bool eof();
    };
  }
}

#endif
