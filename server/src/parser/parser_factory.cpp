//
//  parser_factory.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 05.06.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <sstream>
#include <string>
#include <limits>

#include <boost/algorithm/string.hpp>

#include "../dba/metafield.hpp"
#include "../extras/utils.hpp"


#include "../extras/bson_utils.hpp"
#include "../typedefs/bson.hpp"

#include "parser_factory.hpp"

#include "../exceptions.hpp"
#include "../log.hpp"


namespace epidb {
  namespace parser {

    size_t BedLine::size() const
    {
      return tokens.size() + 3;
    }

    bool operator<(const BedLine &a, const BedLine &b)
    {
      return a.start < b.start;
    }

    Parser::Parser(std::unique_ptr<std::istream> &&input, FileFormat &format) :
      actual_line_content_(""),
      actual_line_(0),
      first_column_useless(false),
      input_(std::move(input)),
      first_(true),
      format_(format),
      chromosome_pos(std::numeric_limits<size_t>::max()),
      start_pos(std::numeric_limits<size_t>::max()),
      end_pos(std::numeric_limits<size_t>::max())
    {
      // TODO: check duplicated fields
      // TODO: check missing fields
      size_t count = 0;
      for (auto &column : format_) {
        if (column->name() == "CHROMOSOME") {
          chromosome_pos = count;
        }

        if (column->name() == "START") {
          start_pos = count;
        }

        if (column->name() == "END") {
          end_pos = count;
        }
        count++;
      }
    }

    void Parser::validate_format()
    {
      if (chromosome_pos == std::numeric_limits<size_t>::max()) {
        throw deepblue_user_exception(ERR_FORMAT_CHROMOSOME_MISSING);
      }
      if (start_pos == std::numeric_limits<size_t>::max()) {
        throw deepblue_user_exception(ERR_FORMAT_START_MISSING);
      }
      if (end_pos == std::numeric_limits<size_t>::max()) {
        throw deepblue_user_exception(ERR_FORMAT_END_MISSING);
      }
    }

    std::experimental::optional<BedLine> Parser::parse_line()
    {
      std::string line;
      do {
        if (input_->eof()) {
          return std::experimental::nullopt;
        }
        std::getline(*input_, line);
        actual_line_content_ = line;
        boost::trim(line);

        if (line.empty()) {
          return std::experimental::nullopt;
        }
      } while ((line[0] == '#') ||
               (line.substr(0, 5) == "track") ||
               (line.substr(0, 7) == "browser") ||
               (line.compare(0, 9, "chr\tstart") == 0));

      if (first_) {
        std::stringstream ss(line);
        std::string first;
        ss >> first;
        std::string second;
        ss >> second;

        std::vector<std::string> strs;
        boost::split(strs, line, boost::is_any_of("\t"));

        if (strs.size() == format_.size()) {
          first_column_useless = false;
        } else if (strs.size() - 1 == format_.size()) {
          first_column_useless = true;
        } else {
          throw deepblue_user_exception(ERR_PARSING_TOKENS_COUNT, line, strs.size(), format_.size(), format_.format());
        }

        first_ = false;
      }

      std::vector<std::string> strs;
      boost::split(strs, line, boost::is_any_of("\t"));

      std::vector<std::string>::iterator it = strs.begin();
      if (first_column_useless) {
        it++;
      }

      BedLine bed_line;
      for (size_t pos = 0; it < strs.end(); pos++, it++) {
        std::string s = *it;
        boost::trim(s);

        if (pos == chromosome_pos) {
          bed_line.chromosome = s;
        } else if (pos == start_pos) {
          Position start;
          if (!utils::string_to_position(s, start)) {
            throw deepblue_user_exception(ERR_PARSING_START_POSITION, s);
          }
          bed_line.start = start;
        } else if (pos == end_pos) {
          Position end;
          if (!utils::string_to_position(s, end)) {
            throw deepblue_user_exception(ERR_PARSING_END_POSITION, s);
          }
          bed_line.end = end;
        } else {
          bed_line.tokens.push_back(s);
        }
      }

      if (bed_line.size() != format_.size() ) {
        throw deepblue_user_exception(ERR_PARSING_TOKENS_COUNT, line, strs.size(), format_.size(), format_.format());
      }

      actual_line_++;
      return bed_line;
    }

    size_t Parser::count_fields()
    {
      return format_.size();
    }

    bool Parser::eof()
    {
      return input_->eof();
    }

    size_t Parser::actual_line()
    {
      return actual_line_;
    }

    const std::string Parser::actual_line_content()
    {
      return actual_line_content_;
    }

    const FileFormat FileFormat::default_format()
    {
      static FileFormat DEFAULT_FORMAT = FileFormat::default_format_builder();
      return DEFAULT_FORMAT;
    }

    const FileFormat FileFormat::wig_format()
    {
      static FileFormat WIG_FORMAT = FileFormat::wig_format_builder();
      return WIG_FORMAT;
    }

    const FileFormat FileFormat::gtf_format()
    {
      static FileFormat GTF_FORMAT = FileFormat::gtf_format_builder();
      return GTF_FORMAT;
    }

    const FileFormat FileFormat::cufflinks_format()
    {
      static FileFormat CUFFLINKS_FORMAT = FileFormat::cufflinks_format_builder();
      return CUFFLINKS_FORMAT;
    }

    const FileFormat FileFormat::grape2_format()
    {
      static FileFormat GRAPE2_FORMAT = FileFormat::grape2_format_builder();
      return GRAPE2_FORMAT;
    }

    const FileFormat FileFormat::salmon_format()
    {
      static FileFormat SALMON_FORMAT = FileFormat::salmon_format_builder();
      return SALMON_FORMAT;
    }

    const FileFormat FileFormat::default_format_builder()
    {
      dba::columns::ColumnTypePtr chromosome = dba::columns::load_column_type("CHROMOSOME");
      dba::columns::ColumnTypePtr start = dba::columns::load_column_type("START");
      dba::columns::ColumnTypePtr end = dba::columns::load_column_type("END");

      FileFormat format;
      format.set_format("CHROMOSOME,START,END");

      format.add(chromosome);
      format.add(start);
      format.add(end);

      return format;
    }

    const FileFormat FileFormat::wig_format_builder()
    {
      dba::columns::ColumnTypePtr chromosome = dba::columns::load_column_type("CHROMOSOME");
      dba::columns::ColumnTypePtr start = dba::columns::load_column_type("START");
      dba::columns::ColumnTypePtr end = dba::columns::load_column_type("END");
      dba::columns::ColumnTypePtr value = dba::columns::load_column_type("VALUE");

      FileFormat format;
      format.set_format("CHROMOSOME,START,END,VALUE");

      format.add(chromosome);
      format.add(start);
      format.add(end);
      format.add(value);

      return format;
    }

    const FileFormat FileFormat::gtf_format_builder()
    {
      dba::columns::ColumnTypePtr chromosome = dba::columns::load_column_type("CHROMOSOME");
      dba::columns::ColumnTypePtr source = dba::columns::load_column_type("SOURCE");
      dba::columns::ColumnTypePtr feature = dba::columns::load_column_type("FEATURE");
      dba::columns::ColumnTypePtr start = dba::columns::load_column_type("START");
      dba::columns::ColumnTypePtr end = dba::columns::load_column_type("END");
      dba::columns::ColumnTypePtr score = dba::columns::load_column_type("GTF_SCORE");
      dba::columns::ColumnTypePtr strand = dba::columns::load_column_type("STRAND");
      dba::columns::ColumnTypePtr frame = dba::columns::load_column_type("FRAME");
      dba::columns::ColumnTypePtr attributes = dba::columns::load_column_type("GTF_ATTRIBUTES");

      FileFormat format;
      format.set_format("CHROMOSOME,SOURCE,FEATURE,START,END,GTF_SCORE,STRAND,FRAME,GTF_ATTRIBUTES");

      format.add(chromosome);
      format.add(source);
      format.add(feature);
      format.add(start);
      format.add(end);
      format.add(score);
      format.add(strand);
      format.add(frame);
      format.add(attributes);
      return format;
    }


    const FileFormat FileFormat::cufflinks_format_builder()
    {
      dba::columns::ColumnTypePtr tracking_id = dba::columns::load_column_type("TRACKING_ID");
      dba::columns::ColumnTypePtr gene_id = dba::columns::load_column_type("GENE_ID");
      dba::columns::ColumnTypePtr gene_short_name = dba::columns::load_column_type("GENE_SHORT_NAME");
      dba::columns::ColumnTypePtr fpkm = dba::columns::load_column_type("FPKM");
      dba::columns::ColumnTypePtr fpkm_lo = dba::columns::load_column_type("FPKM_CONF_LO");
      dba::columns::ColumnTypePtr fpkm_hi = dba::columns::load_column_type("FPKM_CONF_HI");
      dba::columns::ColumnTypePtr fpkm_status = dba::columns::load_column_type("FPKM_STATUS");

      FileFormat format;
      format.set_format("TRACKING_ID,GENE_ID,GENE_SHORT_NAME,FPKM,FPKM_CONF_LO,FPKM_CONF_HI,FPKM_STATUS");

      format.add(tracking_id);
      format.add(gene_id);
      format.add(gene_short_name);
      format.add(fpkm);
      format.add(fpkm_lo);
      format.add(fpkm_hi);
      format.add(fpkm_status);
      return format;
    }

    const FileFormat FileFormat::salmon_format_builder()
    {
      dba::columns::ColumnTypePtr gene_id = dba::columns::load_column_type("GENE_ID");
      dba::columns::ColumnTypePtr length = dba::columns::load_column_type("LENGTH");
      dba::columns::ColumnTypePtr effective_length = dba::columns::load_column_type("EFFECTIVE_LENGTH");
      dba::columns::ColumnTypePtr tpm = dba::columns::load_column_type("TPM");
      dba::columns::ColumnTypePtr num_reads = dba::columns::load_column_type("NUM_READS");

      FileFormat format;
      format.set_format("GENE_ID,LENGTH,EFFECTIVE_LENGTH,TPM,NUM_READS");

      format.add(gene_id);
      format.add(length);
      format.add(effective_length);
      format.add(tpm);
      format.add(num_reads);

      return format;
    }

    const FileFormat FileFormat::grape2_format_builder()
    {
      dba::columns::ColumnTypePtr tracking_id = dba::columns::load_column_type("TRACKING_ID");
      dba::columns::ColumnTypePtr gene_id = dba::columns::load_column_type("GENE_ID");
      dba::columns::ColumnTypePtr transcript_ids = dba::columns::load_column_type("TRANSCRIPT_IDS");
      dba::columns::ColumnTypePtr length = dba::columns::load_column_type("LENGTH");
      dba::columns::ColumnTypePtr effective_length = dba::columns::load_column_type("EFFECTIVE_LENGTH");
      dba::columns::ColumnTypePtr expected_count = dba::columns::load_column_type("EXPECTED_COUNT");
      dba::columns::ColumnTypePtr TPM = dba::columns::load_column_type("TPM");
      dba::columns::ColumnTypePtr FPKM = dba::columns::load_column_type("FPKM");
      dba::columns::ColumnTypePtr posterior_mean_count = dba::columns::load_column_type("POSTERIOR_MEAN_COUNT");
      dba::columns::ColumnTypePtr posterior_standard_deviation_of_count = dba::columns::load_column_type("POSTERIOR_STANDARD_DEVIATION_OF_COUNT");
      dba::columns::ColumnTypePtr pme_TPM = dba::columns::load_column_type("PME_TPM");
      dba::columns::ColumnTypePtr pme_FPKM = dba::columns::load_column_type("PME_FPKM");
      dba::columns::ColumnTypePtr TPM_ci_lower_bound = dba::columns::load_column_type("TPM_CI_LOWER_BOUND");
      dba::columns::ColumnTypePtr TPM_ci_upper_bound = dba::columns::load_column_type("TPM_CI_UPPER_BOUND");
      dba::columns::ColumnTypePtr FPKM_ci_lower_bound = dba::columns::load_column_type("FPKM_CI_LOWER_BOUND");
      dba::columns::ColumnTypePtr FPKM_ci_upper_bound = dba::columns::load_column_type("FPKM_CI_UPPER_BOUND");

      FileFormat format;
      format.set_format("GENE_ID,TRANSCRIPT_IDS,LENGTH,EFFECTIVE_LENGTH,EXPECTED_COUNT,TPM,FPKM,POSTERIOR_MEAN_COUNT,POSTERIOR_STANDARD_DEVIATION_OF_COUNT,PME_TPM,PME_FPKM,TPM_CI_LOWER_BOUND,TPM_CI_UPPER_BOUND,FPKM_CI_LOWER_BOUND,FPKM_CI_UPPER_BOUND");

      format.add(gene_id);
      format.add(transcript_ids);
      format.add(length);
      format.add(effective_length);
      format.add(expected_count);
      format.add(TPM);
      format.add(FPKM);
      format.add(posterior_mean_count);
      format.add(posterior_standard_deviation_of_count);
      format.add(pme_TPM);
      format.add(pme_FPKM);
      format.add(TPM_ci_lower_bound);
      format.add(TPM_ci_upper_bound);
      format.add(FPKM_ci_lower_bound);
      format.add(FPKM_ci_upper_bound);

      return format;
    }

    Array FileFormat::to_array() const
    {
      ArrayStreamBuilder builder;
      for (const auto& field: fields_) {
        builder << field->document();
      }
      return builder << StreamFinalize;
    }

    void FileFormatBuilder::validate_output(const std::string &format)
    {
      if (format.empty()) {
        return;
      }

      std::vector<std::string> fields_string;
      boost::split(fields_string, format, boost::is_any_of(","));

      for (std::string & field_string : fields_string) {
        boost::trim(field_string);

        if (field_string.empty()) {
          throw deepblue_user_exception(ERR_FORMAT_COLUMN_NAME_MISSING);
        }

        dba::columns::ColumnTypePtr column_type;
        bool found = false;

        // Check if it is metafield
        if (dba::Metafield::is_meta(field_string)) {
          // It throws an exception if field is invalid
          build_metafield_column(field_string);
          found = true;
        }

        // Load from database
        if (!found && dba::columns::exists_column_type(field_string)) {
          found = true;
        }

        if (!found) {
          throw deepblue_user_exception(ERR_INVALID_COLUMN_NAME, field_string);
        }
      }
    }


    const FileFormat FileFormatBuilder::build_for_outout(const std::string &format, const std::vector<Document> &experiment_columns)
    {
      FileFormat file_format;

      if (format.empty()) {
        file_format = FileFormat::default_format();
        file_format.set_format("CHROMOSOME,START,END");
        return file_format;
      }

      file_format.set_format(format);
      std::vector<std::string> fields_string;
      boost::split(fields_string, format, boost::is_any_of(","));

      for (std::string & field_string : fields_string) {
        boost::trim(field_string);

        if (field_string.empty()) {
          throw deepblue_user_exception(ERR_FORMAT_COLUMN_NAME_MISSING);
        }

        dba::columns::ColumnTypePtr column_type;
        bool found = false;

        // Look into experiment columns
        for (const auto& column : experiment_columns) {
          if (bson_utils::get_string(column, "name") == field_string) {
            column_type = dba::columns::column_type_document_to_class(column);
            file_format.add(column_type);
            found = true;
            break;
          }
        }

        // Check if it is metafield
        if (dba::Metafield::is_meta(field_string)) {
          // It throws an exception if field is invalid
          column_type = build_metafield_column(field_string);
          found = true;
          file_format.add(column_type);
        }
        // Load from database
        if (!found) {
          column_type = dba::columns::load_column_type(field_string);
          found = true;
          file_format.add(column_type);
        }
      }

      return file_format;
    }


    const FileFormat FileFormatBuilder::build(const std::string &format)
    {
      FileFormat file_format;
      if (format.empty()) {
        file_format = FileFormat::default_format();
        file_format.set_format("CHROMOSOME,START,END");
        return file_format;
      }

      file_format.set_format(format);
      std::vector<std::string> fields_string;
      boost::split(fields_string, format, boost::is_any_of(","));

      for (const std::string & field_string : fields_string) {
        dba::columns::ColumnTypePtr column_type = dba::columns::load_column_type(field_string);
        file_format.add(column_type);
      }
      return file_format;
    }


    dba::columns::ColumnTypePtr FileFormatBuilder::build_metafield_column(const std::string &op)
    {
      static const std::string open_parenthesis("(");
      std::string command = op.substr(0, op.find(open_parenthesis));
      std::string type = dba::Metafield::command_type(command);
      if (type.empty()) {
        throw deepblue_user_exception(ERR_INVALID_META_COLUMN_NAME, command);
      }
      return dba::columns::column_type_simple(op, type);
    }

    const FileFormat FileFormatBuilder::deduce_format(const std::string &content)
    {
      std::istringstream f(content);
      std::string line;

      if (!std::getline(f, line)) {
        throw deepblue_user_exception(ERR_PARSING_EMPTY_FILE);
      }

      boost::trim(line);

      std::vector<std::string> strs;
      boost::split(strs, line, boost::is_any_of("\t"));

      size_t tokens = strs.size();

      if (tokens == 3) {
        return FileFormat::default_format();
      }

      std::string format;
      if (tokens == 4) {
        double d;
        if (utils::string_to_double(strs[3], d)) {
          format = "CHROMOSOME,START,END,SCORE";
        } else {
          format = "CHROMOSOME,START,END,NAME";
        }
      }

      if (tokens == 5) {
        format = "CHROMOSOME,START,END,SCORE,NAME";
      }

      if (tokens == 10) {
        format = "CHROMOSOME,START,END,NAME,SCORE,STRAND,SIGNAL_VALUE,P_VALUE,Q_VALUE,PEAK";
      }

      if (tokens == 12) {
        format = "CHROMOSOME,START,END,NAME,SCORE,STRAND,THICK_START,THICK_END,ITEM_RGB,BLOCK_COUNT,BLOCK_SIZES,BLOCK_STARTS";
      }

      return build(format);
    }
  }
}
