//
//  macros.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 15.11.2020.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2020 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __MACROS_HPP__
#define __MACROS_HPP__

#include "dba/helpers.hpp"

#include "exceptions.hpp"


#define GET_EXPRESSION_MANAGER_INSTANCE(__EM_INSTANCE)                         \
  using ::epidb::datatypes::ExpressionManager;                                 \
  const auto *__EM_INSTANCE = ExpressionManager::INSTANCE();

#define GET_EXPRESSION_MANAGER(__EXPRESSION_TYPE_NAME, __EXPRESSION_TYPE)      \
  const datatypes::ExpressionTypePtr& __EXPRESSION_TYPE =                      \
       em->get_manager(__EXPRESSION_TYPE_NAME);

#define GET_EXPRESSION_TYPE(__EXPRESSION_TYPE_NAME, __EXPRESSION_TYPE)                  \
  GET_EXPRESSION_MANAGER_INSTANCE(em)                                                   \
  if (!em->is_expression_type(__EXPRESSION_TYPE_NAME)) {                                \
    throw deepblue_user_exception(ERR_INVALID_EXPRESSION_TYPE, __EXPRESSION_TYPE_NAME); \
  }                                                                                     \
  GET_EXPRESSION_MANAGER(__EXPRESSION_TYPE_NAME, __EXPRESSION_TYPE)                     \


#define STR1(x)  #x
#define STR(x)  STR1(x)


#endif /* __MACROS_HPP__ */