//
//  column_dataset_cache.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 30.12.2016.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>

#include <boost/bimap/set_of.hpp>

#include "column_dataset_cache.hpp"

#include "../algorithms/lru.hpp"

#include "../dba/column_types.hpp"
#include "../dba/queries.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace cache {

    dataset_id_bson::QUERY_RESULT fn_load_bson_by_dataset_id(const dataset_id_bson::QUERY_KEY& qk)
    {
      dataset_id_bson::QUERY_RESULT qr;
      qr.obj = dba::query::__get_document_by_dataset_id(qk.dataset_id);

      return qr;
    }

    lru_cache_using_boost<
      dataset_id_bson::QUERY_KEY,
      dataset_id_bson::QUERY_RESULT,
      boost::bimaps::set_of> DATASET_ID_BSON_CACHE(fn_load_bson_by_dataset_id, 4096);


    MaybeDocument get_document_by_dataset_id(DatasetId dataset_id)
    {
      dataset_id_bson::QUERY_KEY qk;
      qk.dataset_id = dataset_id;

      dataset_id_bson::QUERY_RESULT qr = DATASET_ID_BSON_CACHE(qk);

      return qr.obj;
    }

    // --

    dataset_columnn_cache::QUERY_RESULT fn_load_dataset_columns(const dataset_columnn_cache::QUERY_KEY& qk)
    {
      dataset_columnn_cache::QUERY_RESULT result;
      result.columns = dba::query::__get_columns_from_dataset(qk.dataset_id);

      return result;
    }

    lru_cache_using_boost<
      dataset_columnn_cache::QUERY_KEY,
      dataset_columnn_cache::QUERY_RESULT,
      boost::bimaps::set_of> DATASET_QUERY_CACHE(fn_load_dataset_columns, 4096);


    const std::vector<Document> get_columns_from_dataset(const DatasetId & dataset_id)
    {
      dataset_columnn_cache::QUERY_KEY qk;
      qk.dataset_id = dataset_id;

      dataset_columnn_cache::QUERY_RESULT qr = DATASET_QUERY_CACHE(qk);

      return std::move(qr.columns);
    }

    // --

    column_position_dataset::QUERY_RESULT fn_load_column_position_from_dataset(const column_position_dataset::QUERY_KEY& qk)
    {
      column_position_dataset::QUERY_RESULT result;

      std::cerr << " QUERY ID" << qk.dataset_id << std::endl;
      std::cerr << " QUERYname" << qk.column_name << std::endl;

      auto columns = cache::get_columns_from_dataset(qk.dataset_id);

      for (const auto& c : columns) {
        auto c_view = c.view();
        auto column_name = bson_utils::get_string(c_view, "name");
        if (bson_utils::get_string(c_view, "name") == qk.column_name) {
          result.pos = c_view["pos"].get_int32().value;
          return result;
        }
      }

      //  result.msg = Error::m(ERR_INVALID_EXPERIMENT_COLUMN, obj->view()["name"].get_utf8().value, qk.column_name);
      result.pos = -1;

      return result;
    }

    lru_cache_using_boost<
      column_position_dataset::QUERY_KEY,
      column_position_dataset::QUERY_RESULT,
      boost::bimaps::set_of> COLUMN_POSITION_DATASET_CACHE(fn_load_column_position_from_dataset, 4096);


    int32_t get_column_position_from_dataset(const DatasetId &dataset_id, const std::string &column_name)
    {
      column_position_dataset::QUERY_KEY qk;
      qk.dataset_id = dataset_id;
      qk.column_name = column_name;

      column_position_dataset::QUERY_RESULT qr = COLUMN_POSITION_DATASET_CACHE(qk);

      return qr.pos;
    }

    // --

    column_type_dataset::QUERY_RESULT fn_load_column_type_from_dataset(const column_type_dataset::QUERY_KEY& qk)
    {
      const std::string& column_name = qk.column_name;
      DatasetId dataset_id = qk.dataset_id;

      column_type_dataset::QUERY_RESULT result;

      if ((column_name == "CHROMOSOME") || (column_name == "START") || (column_name == "END")) {
        result.column_type = dba::columns::load_column_type(column_name);
        return result;
      }

      std::vector<Document> experiment_columns = cache::get_columns_from_dataset(dataset_id);

      for (auto column : experiment_columns) {
        auto actual_column_name = bson_utils::get_string(column.view(), "name");
        if (column_name == actual_column_name) {
            result.column_type = dba::columns::column_type_document_to_class(column);
            return result;
        }
      }

      result.column_type = nullptr;
      return result;
    }

    lru_cache_using_boost<
      column_type_dataset::QUERY_KEY,
      column_type_dataset::QUERY_RESULT,
      boost::bimaps::set_of> COLUMN_TYPE_DATASET_CACHE(fn_load_column_type_from_dataset, 4096);


    dba::columns::ColumnTypePtr get_column_type_from_dataset(const DatasetId &dataset_id, const std::string &column_name)
    {
      column_type_dataset::QUERY_KEY qk;
      qk.dataset_id = dataset_id;
      qk.column_name = column_name;

      column_type_dataset::QUERY_RESULT qr = COLUMN_TYPE_DATASET_CACHE(qk);

      return qr.column_type;
    }

    void column_dataset_cache_invalidate() {
      DATASET_ID_BSON_CACHE.clear();
      DATASET_QUERY_CACHE.clear();
      COLUMN_POSITION_DATASET_CACHE.clear();
      COLUMN_TYPE_DATASET_CACHE.clear();
    }
  }
}
