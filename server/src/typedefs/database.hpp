//
//  mongo.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.11.20
//  Copyright (c) 2020,2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TYPEDEFS_MONGO_H_
#define TYPEDEFS_MONGO_H_

#include "bson.hpp"

#include <mongocxx/collection.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/pool.hpp>

#include <mongocxx/gridfs/bucket.hpp>

#include "../config/config.hpp"

namespace epidb {
  typedef mongocxx::collection Collection;

  #define DB(_db) \
    auto __conn##_db = config::pool().acquire(); \
    auto _db = (*__conn##_db)[epidb::config::DATABASE_NAME()];

  #define COLLECTION(_collection, _col_name) \
    auto __conn##_collection = config::pool().acquire(); \
    auto __db##_collection = (*__conn##_collection)[epidb::config::DATABASE_NAME()]; \
    mongocxx::collection _collection = __db##_collection[_col_name];

  #define SESSION(_session) \
    auto __conn##_session = config::pool().acquire(); \
    auto _session = __conn##_session->start_session();

  #define COLLECTION_FROM_SESSION(_collection, _col_name, _session) \
    const auto& __conn##_collection = _session.client(); \
    auto __db##_collection = __conn##_collection[epidb::config::DATABASE_NAME()]; \
    mongocxx::collection _collection = __db##_collection[_col_name];

  #define ModelUpdateOne mongocxx::model::update_one

  #define GET_ORDER(OPTS, KEY, ORDER) \
    auto OPTS = mongocxx::options::find{}; \
    auto OPTS##_order = DocumentStreamBuilder{} << KEY << ORDER << StreamFinalize; \
    OPTS.sort(OPTS##_order.view());

  #define EXECUTE_UPSERT(COLLECTION, QUERY, APPEND, RESULT) \
        auto COLLECTION##_bulk = COLLECTION.create_bulk_write(); \
        ModelUpdateOne COLLECTION##_upsert_op{QUERY, APPEND};  \
        COLLECTION##_upsert_op.upsert(true); \
        COLLECTION##_bulk.append(COLLECTION##_upsert_op); \
        auto RESULT = COLLECTION##_bulk.execute();

  #define GET_BUCKET(BUCKET, NAME, SIZE) \
    DB(NAME##_db) \
    mongocxx::options::gridfs::bucket opts_##NAME; \
    opts_##NAME.bucket_name(#NAME); \
    opts_##NAME.chunk_size_bytes(SIZE); \
    auto BUCKET = NAME##_db.gridfs_bucket(opts_##NAME);

}

#endif /* TYPEDEFS_BSON_H_ */