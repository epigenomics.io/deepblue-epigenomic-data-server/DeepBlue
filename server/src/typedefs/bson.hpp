//
//  bson.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.11.20
//  Copyright (c) 2020 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TYPEDEFS_BSON_H_
#define TYPEDEFS_BSON_H_

#include <optional>

#include <bsoncxx/builder/stream/array.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/builder/concatenate.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/builder/basic/document.hpp>

#include <bsoncxx/types.hpp>
#include <bsoncxx/types/bson_value/make_value.hpp>

#include <bsoncxx/json.hpp>

#include <bsoncxx/stdx/string_view.hpp>
#include <bsoncxx/stdx/optional.hpp>

#include <mongocxx/options/find.hpp>
#include <mongocxx/collection.hpp>
#include <mongocxx/client_session.hpp>


namespace epidb {

  #define DocumentBuilder bsoncxx::builder::basic::document
  #define ArrayBuilder bsoncxx::builder::basic::array
  #define SubArray bsoncxx::builder::basic::sub_array
  #define MakeDocument bsoncxx::builder::basic::make_document
  #define MakeArray bsoncxx::builder::basic::make_array
  #define Concatenate bsoncxx::builder::concatenate
  #define KVP bsoncxx::builder::basic::kvp

  #define DocumentStreamBuilder bsoncxx::builder::stream::document
  #define ArrayStreamBuilder bsoncxx::builder::stream::array
  #define Open bsoncxx::builder::stream::open_document
  #define Close bsoncxx::builder::stream::close_document
  #define StreamFinalize bsoncxx::builder::stream::finalize

  #define FindOptions mongocxx::options::find

  #define MakeBSONValue bsoncxx::types::bson_value::make_value

  typedef bsoncxx::document::value Document;
  typedef bsoncxx::document::view View;

  typedef bsoncxx::array::value Array;
  typedef bsoncxx::array::view ArrayView;
  typedef bsoncxx::array::element ArrayElement;

  typedef bsoncxx::oid ObjectID;

  typedef bsoncxx::document::element Element;
  typedef bsoncxx::types::bson_value::view ElementValue;
  typedef bsoncxx::types::b_utf8 UTF8;
  typedef bsoncxx::types::b_oid OID;
  typedef bsoncxx::types::b_binary Binary;
  typedef bsoncxx::types::b_date Date;
  typedef bsoncxx::types::b_regex Regex;
  typedef bsoncxx::types::b_undefined Undefined;
  typedef bsoncxx::types::b_document ElementDocument;

  #define DocumentType bsoncxx::type::k_document
  #define UTF8Type     bsoncxx::type::k_utf8
  #define DoubleType   bsoncxx::type::k_double
  #define Int32Type    bsoncxx::type::k_int32
  #define Int64Type    bsoncxx::type::k_int64
  #define ArrayType    bsoncxx::type::k_array
  #define BoolType     bsoncxx::type::k_bool
  #define DateType     bsoncxx::type::k_date
  #define OIDType      bsoncxx::type::k_oid

  typedef bsoncxx::stdx::optional<Document> MaybeDocument;
  typedef bsoncxx::stdx::optional<Array> MaybeArray;
  #define EMPTY_DOCUMENT bsoncxx::stdx::nullopt

  typedef bsoncxx::string::view_or_value StringView;

  #define InsertOneOp mongocxx::model::insert_one

  #define ClientSession mongocxx::client_session

  #define ToJSON bsoncxx::to_json

}

#endif /* TYPEDEFS_BSON_H_ */
