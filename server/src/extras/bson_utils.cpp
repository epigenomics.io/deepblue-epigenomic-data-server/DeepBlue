//
//  bson_utils.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 11.11.20
//  Copyright (c) 2020 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#ifndef BSON_UTILS_H_
#define BSON_UTILS_H_

#include <iostream>
#include <vector>
#include <string>

#include <bsoncxx/types.hpp>
#include <bsoncxx/string/to_string.hpp>

#include "bson_utils.hpp"

#include "serialize.hpp"
#include "utils.hpp"

#include "../typedefs/bson.hpp"

namespace epidb {
  namespace bson_utils {

    Document one_key_doc(const std::string& key, const std::string& value) {
      return MakeDocument(KVP(key, value));
    }

    Document id_doc(const std::string& id) {
      return one_key_doc("_id", id);
    }

    Document in_doc(const Element& array_elemenent) {
      return in_doc(array_elemenent.get_array().value);
    }

    Document in_doc(const Array& array) {
      return in_doc(array.view());
    }

    Document in_doc(const ArrayView& array) {
      return MakeDocument(KVP("$in", array));
    }


    std::string get_string(const View &view, const std::string &element) {
      return StringViewToString(view[element].get_utf8().value);
    }

    std::string get_string_safe(const View &view, const std::string &element) {
      if (has_key(view, element)) {
        return StringViewToString(view[element].get_utf8().value);
      }
      return "";
    }


    int32_t get_safe_int32(const View& obj_view, const std::string &element) {
      if (has_key(obj_view, element)) {
        auto e = obj_view[element];
        if (e.type() == Int32Type) {
          return e.get_int32().value;
        } else {
          std::cerr << " INFO: " << element << " is not a int32_t " << std::endl;
        }
      }
      return -1;
    }

    int64_t get_safe_int64(const View& obj_view, const std::string &element) {
      if (has_key(obj_view, element)) {
        auto e = obj_view[element];
        if (e.type() == Int64Type) {
          return e.get_int64().value;
        } else if (e.type() == Int32Type) {
          return e.get_int32().value;
        } else {
          std::cerr << " INFO: " << element << " is not a int32_t neither int64_t " << std::endl;
        }
      }
      return -1;
    }

    // TODO: remove it and use the utils:: functions
    // concatenates arbitrary ranges into an array context
    template <typename begin_t, typename end_t>
    class range_array_appender {
      public:
      range_array_appender(begin_t begin, end_t end)
        : _begin(std::move(begin)), _end(std::move(end)) {}

      void operator()(bsoncxx::builder::stream::array_context<> ac) const {
        for (auto iter = _begin; iter != _end; ++iter) {
            ac = ac << *iter;
        }
      }

      private:
      begin_t _begin;
      end_t _end;
    };


    // TODO: remove it and use the utils:: functions
    template <typename begin_t, typename end_t>
    range_array_appender<begin_t, end_t> make_range_array_appender(begin_t&& begin, end_t&& end) {
      return range_array_appender<begin_t, end_t>(std::forward<begin_t>(begin),
                                                std::forward<end_t>(end));
    }


    // TODO: remove it and use the utils:: functions
    Array parameters_vector_to_array(const std::vector<serialize::ParameterPtr> &pptr_vector) {
      auto str_vector = utils::build_vector(pptr_vector);
      return ArrayStreamBuilder() <<
        make_range_array_appender(str_vector.begin(), str_vector.end()) <<
        StreamFinalize;
    }


    // TODO: remove it and use the utils:: functions
    Array parameters_long_vector_to_array(const std::vector<serialize::ParameterPtr> &pptr_vector) {
      auto long_vector = utils::build_vector_long(pptr_vector);
      return ArrayStreamBuilder() <<
        make_range_array_appender(long_vector.begin(), long_vector.end())  <<
        StreamFinalize;
    }


    // TODO: remove it and use the utils:: functions
    template <typename type_t>
    Array vector_to_array(const std::vector<type_t> &vector) {
      return ArrayStreamBuilder() <<
        make_range_array_appender<type_t, type_t>(vector.begin(), vector.end()) <<
        StreamFinalize;
    }


    // TODO: remove it and use the utils:: functions
    Array vector_to_array(const std::vector<std::string> &vector) {
      return ArrayStreamBuilder() <<
        make_range_array_appender(vector.begin(), vector.end()) <<
        StreamFinalize;
    }

    // TODO: remove it and use the utils:: functions
    Array vector_to_array(const std::vector<int32_t> &vector) {
      return ArrayStreamBuilder() <<
        make_range_array_appender(vector.begin(), vector.end()) <<
        StreamFinalize;
    }

    // TODO: remove it and use the utils:: functions
    Array vector_to_array(const std::vector<int64_t> &vector) {
      return ArrayStreamBuilder() <<
        make_range_array_appender(vector.begin(), vector.end()) <<
        StreamFinalize;
    }

    // TODO: remove it and use the utils:: functions
    Array vector_to_array(const std::vector<Score> &vector) {
      return ArrayStreamBuilder() <<
        make_range_array_appender(vector.begin(), vector.end()) <<
        StreamFinalize;
    }


    const std::string get_key(const Element &element_view) {
      return to_string(element_view.key());
    }

    const std::string to_string(const StringView &string_view) {
      auto terminated = string_view.terminated();
      return std::string(terminated.data());
    }


    void copy_elemements_by_name(const View source, const std::vector<std::string>& keys, DocumentBuilder& builder) {
      for (const auto &key: keys) {
        auto elem = source[key];
        builder.append(KVP(key, elem.get_value()));
      }
    }

    Document clone_sample_info(const View source) {
      DocumentBuilder sample_builder{};

      View sample = source["sample_info"].get_document().view();
      for (auto e : sample) {
        const auto key = get_key(e);
        if (strncmp("norm_", key.c_str(), 5) != 0) {
          sample_builder.append(KVP(key, e.get_value()));
        }
      }

      return sample_builder.extract();
    }

    const bool has_key(const View& v, const std::string s) {
      return (v.find(s) != v.end());
    }

  }
}

#endif /* BSON_UTILS_H_ */
