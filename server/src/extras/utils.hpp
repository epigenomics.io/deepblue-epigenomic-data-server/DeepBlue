//
//  utils.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 27.06.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_UTILS_HPP
#define EPIDB_UTILS_HPP

#include <algorithm>
#include <vector>
#include <string>
#include <set>
#include <sstream>
#include <iterator>
#include <functional>

#include "serialize.hpp"

#include "../typedefs/bson.hpp"

#include "../types.hpp"


namespace epidb {
  namespace utils {

    class IdName {
    public:
      std::string id;
      std::string name;

      IdName() :
        id(""), name("") {}

      IdName(std::string i, std::string n) :
        id(i), name(n) {}

      bool operator<(const IdName& other) const
      {
        return name.compare(other.name) < 0;
      }

    };

    std::ostream &operator<<(std::ostream &os, const IdName &o);

    std::vector<IdName> documents_to_id_names(const std::vector<Document> &docs);
    IdName view_to_id_name(const View& bson);


    class IdNameCount {
    public:
      IdNameCount() :
        id(""), name(""), count(0) {}
      IdNameCount(std::string i, std::string n, size_t c) :
        id(i), name(n), count(c) {}
      std::string id;
      std::string name;
      size_t count;
    };

    std::ostream &operator<<(std::ostream &os, const IdName &o);

    std::ostream &operator<<(std::ostream &os, const IdNameCount &o);

    std::vector<std::string> capitalize_vector(std::vector<std::string> vector);

    static struct {
      bool operator()(const IdNameCount &lhs, const IdNameCount &rhs) const
      {
        return lhs.count <= rhs.count;
      }
    } IdNameCountComparer;

    std::string lower_case(std::string string);

    template <typename Type> std::string vector_to_string(std::vector<Type> vector, std::string sep = ",")
    {
      std::ostringstream oss;
      if (!vector.empty()) {
        std::ostream_iterator<Type> oss_it(oss, sep.c_str());
        std::copy(vector.begin(), vector.end() - 1, oss_it);
        oss << vector.back();
      }
      return oss.str();
    }

    class ExperimentInfo {
    public:
      ExperimentInfo(std::string &i, std::string &n, std::string &em, std::string s, std::string &t,
                     std::string &p, std::string &d):
        id(i),
        name(n),
        epigenetic_mark(em),
        sample_id(s),
        technique(t),
        project(p),
        description(d) {}

      std::string id;
      std::string name;
      std::string epigenetic_mark;
      std::string sample_id;
      std::string technique;
      std::string project;
      std::string description;
    };


    bool string_to_int(const std::string &s, int &i);

    bool string_to_long(const std::string &s, size_t &i);

    bool string_to_position(const std::string &s, Position &p);

    bool string_to_score(const std::string &s, Score &c);

    bool string_to_double(const std::string &s_, double &c);

    std::string score_to_string(const Score s);

    std::string integer_to_string(const int d);

    std::string size_t_to_string(const size_t t);

    std::string long_to_string(const long t);

    bool is_number(const std::string &s);

    std::string lower(const std::string &in);

    std::string normalize_name(const std::string &name);

    std::string normalize_annotation_name(const std::string &annotation_name);

    std::string normalize_epigenetic_mark(const std::string &histone_modification);

    std::string format_extra_metadata(const View &key_value);

    std::string element_to_string(const Element &e);

    std::string element_to_string(const ElementValue &e);

    serialize::ParameterPtr bson_to_parameters(const View &o);

    serialize::ParameterPtr element_to_parameter(const Element &e);
    serialize::ParameterPtr element_to_parameter(const ElementValue &e);

    Document unroll_object(const std::string &prefix, const View &o);

    std::string sanitize(const std::string &data);

    Array build_array(const std::vector<std::string> &params);
    Array build_array(const std::vector<long> &params);
    Array build_array(const std::vector<Score> &params);
    Array build_array(const std::vector<serialize::ParameterPtr> &params);
    Array build_array_long(const std::vector<serialize::ParameterPtr> &params);
    Array build_array_long(const std::vector<long> &params);
    Array build_normalized_array(const std::vector<std::string> &params);
    Array build_normalized_array(const std::vector<serialize::ParameterPtr> &params);
    Array build_epigenetic_normalized_array(const std::vector<serialize::ParameterPtr> &params);
    Array build_annotation_normalized_array(const std::vector<serialize::ParameterPtr> &params);

    std::vector<std::string> build_vector(const std::vector<serialize::ParameterPtr> &params);
    std::vector<std::string> build_vector(const ArrayView &params);
    std::vector<long> build_vector_long(const std::vector<serialize::ParameterPtr> &params);
    std::vector<long> build_vector_long(const ArrayView &params);
    std::set<std::string> build_set(const ArrayView &params);

    bool check_parameters(const std::vector<serialize::ParameterPtr> &params, const std::function<std::string(const std::string&)> &normalizer, const std::function<bool(const std::string&)> &checker, std::string &wrong);

    bool check_parameters(const std::vector<std::string> &params, const std::function<std::string(const std::string&)> &normalizer, const std::function<bool(const std::string&)> &checker, std::string &wrong);

    bool is_id(const std::string &id, const std::string &prefix);

    const std::string get_by_region_set(const View &obj, const std::string &field);
  }
}

#endif /* defined(EPIDB_UTILS_HPP) */
