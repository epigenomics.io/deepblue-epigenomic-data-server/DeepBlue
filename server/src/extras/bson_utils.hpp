//
//  bson_utils.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.11.20.
//  Copyright (c) 2020 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_BSON_UTILS_HPP
#define EPIDB_BSON_UTILS_HPP


#include <vector>
#include <string>

#include <bsoncxx/string/to_string.hpp>

#include "serialize.hpp"
#include "../types.hpp"
#include "../typedefs/bson.hpp"

namespace epidb {
  namespace bson_utils {

    #define StringViewToString bsoncxx::string::to_string

    std::string get_string(const View &obj_view, const std::string &element);
    std::string get_string_safe(const View &obj_view, const std::string &element);

    int32_t get_safe_int32(const View& obj_view, const std::string &element);
    int64_t get_safe_int64(const View& obj_view, const std::string &element);

    Document one_key_doc(const std::string& key, const std::string& value);
    Document id_doc(const std::string& id);

    Document in_doc(const Element& array_elemenent);
    Document in_doc(const Array& array);
    Document in_doc(const ArrayView& array);

    void copy_elemements_by_name(const View source, const std::vector<std::string>& keys, DocumentBuilder& builder);
    Document clone_sample_info(const View source);

    Array parameters_vector_to_array(const std::vector<serialize::ParameterPtr> &vector);
    Array parameters_long_vector_to_array(const std::vector<serialize::ParameterPtr> &pptr_vector);

    Array vector_to_array(const std::vector<std::string> &vector);
    Array vector_to_array(const std::vector<int32_t> &vector);
    Array vector_to_array(const std::vector<int64_t> &vector);
    Array vector_to_array(const std::vector<Score> &vector);

    const std::string get_key(const Element &element_view);
    const std::string to_string(const StringView &string_view);

    const bool has_key(const View& v, const std::string s);
  }
}


#endif /* EPIDB_BSON_UTILS_HPP */
