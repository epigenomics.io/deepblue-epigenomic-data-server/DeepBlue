UNAME_S := $(shell uname -s)
UNAME_N := $(shell uname -n)

#Root Directory
ROOT=$(shell pwd)/..

LUA=$(ROOT)/third_party/luajit-2.0/src
STRTK=$(ROOT)/third_party/strtk
DATE=$(ROOT)/third_party/date
JEMALLOC=$(ROOT)/third_party/jemalloc

GIT_VERSION := $(shell git describe --abbrev=8 --dirty --always --tags)
GIT_URL := $(shell git config --get remote.origin.url)


CXX = g++ -std=c++14 -DBOOST_LOG_DYN_LINK -DGIT_VERSION=\"$(GIT_VERSION)\" -DGIT_URL=\"$(GIT_URL)\"
LD  = g++
LDFLAGS   = -pthread
NEW_MONGO=/opt/mongo-cxx-driver
NEW_MONGO_INCLUDE_BSON = $(NEW_MONGO)/include/bsoncxx/v_noabi/
NEW_MONGO_INCLUDE_MONGO = $(NEW_MONGO)/include/mongocxx/v_noabi/
NEW_MONGO_LIBS = $(NEW_MONGO)/lib
BOOST_DIR = /opt/boost/lib
DEFCXXFLAGS = -std=c++14 -Wall -O3 -ffast-math -march=native -Ofast -I$(NEW_MONGO_INCLUDE_BSON) -I$(NEW_MONGO_INCLUDE_MONGO) -I/opt/boost/include/ -I$(STRTK) -I$(LUA) -I${JEMALLOC}/include -I${DATE} -Wno-deprecated-declarations -Wno-unused-variable -Wno-unused-parameter -Wno-ignored-qualifiers -Wno-deprecated-declarations
BOOSTLIBS = -lboost_filesystem -lboost_thread -lboost_system -lboost_regex -lboost_log -lboost_timer -lboost_chrono -lboost_program_options -lboost_iostreams -lboost_serialization -lrt -ljemalloc -L${NEW_MONGO_LIBS} -Wl,-rpath,${NEW_MONGO_LIBS} -lbsoncxx -lmongocxx -lfmt


THIRDLIBS = ../third_party/libexpat_linux.a

DEFLIBS = $(BOOSTLIBS) -L$(LUA) -lluajit

AR	= ar
ARFLAGS	=
RANLIB	= ranlib

RM	= rm
ECHO	= echo

SHELL	= /bin/sh
