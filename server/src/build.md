# DeepBlue Server Install

It is recommended to use Ubuntu 2021.04 .

## Building the Server

### Cloning DeepBlue Server code
* git clone https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/DeepBlue.git
* git submodule update --init --recursive

### Installing MongoDB C++ Drivers (mongocxx)

* Install the boost libraries:

`sudo apt-get install libboost-all-dev --install-suggests`

(Idea to make the docker image smaller: could go through the invidial libraries and install only them)`

* Install bson and mongo C libraries:

`sudo apt-get install libbson-dev libmongoc-dev`
 
* Currently DeepBlue uses mongocxx version 3.6.3:

Download

```
wget https://github.com/mongodb/mongo-cxx-driver/archive/refs/tags/r3.6.3.tar.gz
tar -zxvf r3.6.3.tar.gz
mongo-cxx-driver-r3.6.3/build
```

Build

```
cmake ..                                            \
    -DCMAKE_BUILD_TYPE=Release                      \
    -DBSONCXX_POLY_USE_BOOST=1                      \
    -DCMAKE_PREFIX_PATH=/opt/mongo-c-driver         \
    -DCMAKE_INSTALL_PREFIX=/opt/mongo-cxx-driver
make --build . -j 10                                
cmake --build . --target install
```

### Install Auxiliary libraries

* Install bzip2 libs

It is used for compressing the bitmaps used in the fast enrichment method.

`sudo apt-get install -y libbz2-dev`


* Install jemalloc libs
`sudo apt-get install libjemalloc-dev`

* Install FMT Lib

It is used for formatting strings.

```
wget https://github.com/fmtlib/fmt/releases/download/7.1.3/fmt-7.1.3.zip
unzip fmt-7.1.3.zip
cd dmt-7.1.3
mkdir build          # Create a directory to hold the build output.
cd build
cmake ..  # Generate native build scripts.
cd ..
make
sudo make install
 ```

* Install luajit

Lua language is used for the calculate rows

```
cd server/third_party/luajit-2.0
make
cd src
ln -s libluajit.so libluajit-5.1.so.2
```

### Build the DeepBlue Server
Now we can (hopefully) build DeepBlue Server

If any issue happens, take a look at `server/src/Makefile.inc`

```
cd server/src
make
```

# Executing the DeepBlue

## MongoDB
Before, make sure that you have a MongoDB **cluster** with replica sets running.

For quick start, **only for development**, I recommend [run-rs](https://www.npmjs.com/package/run-rs).

I also recommend downloading mongodb and the mongodb binary mongod in /opt/mongo/mongod. 
Where you can execute run-rs as:
```
run-rs -n 1 -k --mongod /opt/mongo/mongod  --dbpath /opt/data --quiet
```

## DeepBlue Server

```
cd server/src
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../third_party/luajit-2.0/src/
./server
```

Execute `./server --help` for different parameters (e.g. defining MongoDB address)


# Tests

## Executing tests

Optional: execute MongoDB using a ram disk for the data, the tests will be much faster:

```
sudo mkdir /mnt/ramdisk
sudo mount -t tmpfs -o size=2048m tmpfs /mnt/ramdisk
mongod --dbpath /mnt/ramdisk
```

Starting DeepBlue:

```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../third_party/luajit-2.0/src/
- ./server -D tests_suite
```

Executing the tests:
```
cd server/tests
python tests.py
```
