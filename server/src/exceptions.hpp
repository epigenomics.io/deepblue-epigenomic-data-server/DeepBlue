//
//  exceptions.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 31.03.2021.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __EXCEPTIONS_HPP__
#define __EXCEPTIONS_HPP__

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "errors.hpp"

namespace epidb {

class deepblue_runtime_error : public std::runtime_error {
    std::string msg;
public:
    deepblue_runtime_error(const std::string &arg, const char *file, int line) :
    std::runtime_error(arg) {
        std::ostringstream o;
        o << file << ":" << line << ": " << arg;
        msg = o.str();
    }

    ~deepblue_runtime_error() throw() {}

    const char *what() const throw() {
        return msg.c_str();
    }
};

class deepblue_user_exception : public std::exception {
    const Error& _e;
    std::string _msg;
public:
    template<typename... Args>
    deepblue_user_exception(const Error& e, const Args & ... args)
        : _e(e)
    {
      _msg = Error::m(e, args...);
    }

    deepblue_user_exception(const std::string& msg)
        : _e(ERR_WORKAROUND_STRING_ERROR)
    {
      _msg = msg;
    }

    ~deepblue_user_exception() throw() {}

    const std::string msg() const throw() {
        return _msg;
    }

    const char *what() const throw() {
        return _msg.c_str();
    }

    const Error& error() const throw() {
        return _e;
    }
};

#define throw_runtime_error(arg) throw deepblue_runtime_error(arg, __FILE__, __LINE__);

}

#endif
