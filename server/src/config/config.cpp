//
//  config.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Fabian Reinartz on 11.03.2014
//  Copyright (c) 2021 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>
#include <vector>

#include <mongocxx/pool.hpp>
#include <mongocxx/uri.hpp>

#include "../config/config.hpp"


#include "config_observer.hpp"
#include "errors.hpp"
#include "log.hpp"

namespace epidb {
  namespace config {
    std::unique_ptr<mongocxx::pool> pool_ptr;

    std::string mongodb_server;
    std::string database_name;
    mongocxx::uri mongodb_server_connection;
    long long processing_max_memory;

    unsigned long long default_old_request_age_in_sec;
    unsigned long long old_request_age_in_sec;

    unsigned long long default_janitor_periodicity;
    unsigned long long janitor_periodicity;

    std::shared_ptr<ConfigSubject> config_subject = std::make_shared<ConfigSubject>();

    ConfigSubjectPtr get_config_subject()
    {
      return config_subject;
    }

    void set_mongodb_server(const std::string &server)
    {
      static std::string MONGODB_ADDR("mongodb://");
      static size_t SIZE = MONGODB_ADDR.size();

      if ((server.size() < SIZE) ||
          server.compare(0, SIZE, MONGODB_ADDR)) {
        mongodb_server = MONGODB_ADDR + server;
      } else {
        mongodb_server = server;
      }

      mongodb_server_connection = mongocxx::uri{mongodb_server};

      EPIDB_LOG("MongoDB Creating Pool.");
      pool_ptr = std::make_unique<mongocxx::pool>(mongodb_server_connection);
    }

    mongocxx::pool& pool() {
      return *pool_ptr;
    }

    const mongocxx::uri& get_mongodb_uri()
    {
      return mongodb_server_connection;
    }

    const std::string get_mongodb_server()
    {
      return mongodb_server;
    }

    void set_database_name(const std::string &name)
    {
      database_name = name;
    }

    void set_processing_max_memory(const long long memory)
    {
      processing_max_memory = memory;
    }

    long long get_processing_max_memory()
    {
      return processing_max_memory;
    }

    void set_default_old_request_age_in_sec(const unsigned long long default_oo)
    {
      default_old_request_age_in_sec = default_oo;
    }

    unsigned long long get_default_old_request_age_in_sec()
    {
      return default_old_request_age_in_sec;
    }

    unsigned long long get_old_request_age_in_sec()
    {
      return old_request_age_in_sec;
    }

    void set_old_request_age_in_sec(const unsigned long long oo)
    {
      old_request_age_in_sec = oo;
    }

    void set_janitor_periodicity(const unsigned long long jp)
    {
      janitor_periodicity = jp;
      config_subject->notifyObservers();
    }

    unsigned long long get_janitor_periodicity() {
      return janitor_periodicity;
    }

    void set_default_janitor_periodicity(const unsigned long long jp)
    {
      default_janitor_periodicity = jp;
    }

    unsigned long long get_default_janitor_periodicity() {
      return default_janitor_periodicity;
    }

    const std::string DATABASE_NAME()
    {
      return database_name;
    }

  }
}
