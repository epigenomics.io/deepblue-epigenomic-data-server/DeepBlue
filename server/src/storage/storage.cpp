//
//  storage.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 21.10.17.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "../extras/bson_utils.hpp"

#include "../typedefs/database.hpp"

#include "../config/config.hpp"
#include "../exceptions.hpp"
#include "../log.hpp"


namespace epidb {
  namespace storage {


    void store(const std::string filename, const uint8_t *ptr, size_t len)
    {
      GET_BUCKET(BUCKET, STORAGE, 2 << 22) // 8MB
      auto uploader = BUCKET.open_upload_stream(filename);

      uploader.write(ptr, len);
      auto result = uploader.close();

      DocumentBuilder bob;
      bob.append(KVP("_id", result.id()));
      bob.append(KVP("filename", filename));

      COLLECTION(files_collection, "storage_files");
      files_collection.insert_one(bob.extract());
    }

    const ElementValue get_file_id(const std::string &filename)
    {
      COLLECTION(files_collection, "storage_files");
      auto maybe_file = files_collection.find_one(bson_utils::one_key_doc("filename", filename));

      if (maybe_file) {
        auto fileinfo = maybe_file->view();
        return fileinfo["_id"].get_value();
      } else {
        throw_runtime_error("The result data under the request '" + filename + "'' was not found");
      }
    }

    const std::string load(const std::string &filename)
    {
      auto oid = get_file_id(filename);

      GET_BUCKET(BUCKET, STORAGE, 2 << 22) // 8MB
      auto downloader = BUCKET.open_download_stream(oid);

      auto file_length = downloader.file_length();
      auto bytes_counter = 0;

      auto buffer_size = std::min(file_length, static_cast<std::int64_t>(downloader.chunk_size()));
      auto buffer = std::make_unique<std::uint8_t[]>(static_cast<std::size_t>(buffer_size));

      std::stringstream ss;
      while (auto length_read = downloader.read(buffer.get(), static_cast<std::size_t>(buffer_size))) {

        bytes_counter += static_cast<std::int32_t>(length_read);
        ss.write(reinterpret_cast<char *>(buffer.get()), length_read);
      }

      return ss.str();
    }

    void remove(const std::string &filename)
    {
      GET_BUCKET(BUCKET, STORAGE, 2 << 22)
      auto oid = get_file_id(filename);
      BUCKET.delete_file(oid);
      COLLECTION(files_collection, "storage_files");
      files_collection.delete_one(bson_utils::one_key_doc("filename", filename));
    }
  }
}
