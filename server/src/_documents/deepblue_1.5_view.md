# View on DeepBlue 1.5

## Goal:

* Update the code base for using the last version of MongoDB CXX driver.
  * The actual driver does not support the newer versions of MongoDB.
  * The actual driver is sometimes too verbose and overcomplicated to use.

* Organize, simplify and reduce the code base.
  * DeepBlue code base is large and complex, not allowing other people to work on it.
  * For the version 1.5 we will not have a major rewrite, but code will be removed/moved:
    - [x] Remove functionalities that are not used:
      - [x] Remove add_sample_from_gsm - operation to import metadata from gsm
      - [x] Remove list_recent_experiments - operation to list the new data included in DeepBlue
      - [x] Remove the extra support for sharding that is not being used.
      - [x] Connection Pool code - The newer MongoDB Driver contains a Connection Pool
      - [x] Replace all time_t, boost time, posix time to std::chrono
      - [x] Remove compressing (LZO) - The newer MongoDB now compress the data automatically
      - [x] Remove document key names shortener  - Newer  MongoDB does it automatically.

    - [ ] Move the indexes creating to a python script - easier to maintain.
      - [x] Remove createIndex() from the C++ code
      - [ ] Create Python script for creating the indexes
    - [x] Remove internal code dependency, removing #includes  - reducing bugs and compile time.

It is an internal reorganization, the end user will hardly notice any chance.
 - Do no break the API, unless removing operations that are not used and easily replaced by a script.
