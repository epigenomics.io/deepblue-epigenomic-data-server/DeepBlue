//
//  gene_expressions.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 24.10.16.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>
#include <vector>

#include "../datatypes/metadata.hpp"

#include "../dba/collections.hpp"
#include "../dba/data.hpp"
#include "../dba/full_text.hpp"
#include "../dba/genes.hpp"
#include "../dba/helpers.hpp"
#include "../dba/key_mapper.hpp"
#include "../dba/remove.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../extras/bson_utils.hpp"

#include "expressions.hpp"
#include "gene_expressions.hpp"

#include "../exceptions.hpp"

namespace epidb {
  namespace datatypes {

    const Document GeneExpressionType::info(const std::string & id)
    {
      auto data_obj = data(id);

      DocumentBuilder builder{};

      bson_utils::copy_elemements_by_name(
        data_obj->view(),
        {"_id", "format", "replica", "sample_id", "extra_metadata", "columns"},
        builder);

      builder.append(KVP("content_format", data_obj->view()["upload_info"]["content_format"].get_value()));
      builder.append(KVP("total_genes", data_obj->view()["upload_info"]["total_genes"].get_value()));

      const auto sample = data_obj->view()["sample_info"].get_value();

      Document sample_info = bson_utils::clone_sample_info(data_obj->view());

      builder.append(KVP("sample_info", sample_info));

      return builder.extract();
    }

    MaybeDocument GeneExpressionType::data(const std::string& id)
    {
      return dba::helpers::get_one_by_id(dba::Collections::GENE_EXPRESSIONS(), id);
    }

    bool GeneExpressionType::exists(const std::string & sample_id, const int replica)
    {
      return dba::helpers::check_exist(dba::Collections::GENE_EXPRESSIONS(),
                                       MakeDocument(
                                         KVP("sample_id", sample_id),
                                         KVP("replica", replica))
      );
    }

    bool GeneExpressionType::update_upload_info(const std::string & collection, const std::string & annotation_id,
        const size_t total_genes)
    {
      COLLECTION(c, collection)
      c.update_one(
        bson_utils::id_doc(annotation_id),
        MakeDocument(
            KVP("$set", MakeDocument(
              KVP("upload_info.total_genes", static_cast<std::int64_t>(total_genes)),
              KVP("upload_info.done", true),
              KVP("upload_info.upload_end", Date{std::chrono::system_clock::now()}))
            )
        )
      );

      return true;
    }

    Document GeneExpressionType::to_bson(const int dataset_id, const std::string & gene_id, const ISerializablePtr & row)
    {
      DocumentBuilder builder{};

      auto row_bson = row->to_document();

      builder.append(KVP("_id", gene_id));
      builder.append(KVP(dba::KeyMapper::DATASET(), dataset_id));
      builder.append(Concatenate(row_bson.view()));

      return builder.extract();
    }

    const std::string GeneExpressionType::insert(const datatypes::User& user,
                                    const std::string & sample_id, const int replica, datatypes::Metadata extra_metadata,
                                    const ISerializableFilePtr& file, const std::string & format,
                                    const std::string & project, const std::string & norm_project,
                                    const std::string & ip)
    {
      Document extra_metadata_obj = datatypes::metadata_to_document(extra_metadata);
      int dataset_id;

      std::string expression_id;
      Document gene_expression_metadata = build_expression_type_metadata(sample_id, replica, format, project, norm_project,
                                            extra_metadata_obj, ip, dataset_id, expression_id);

      Document upload_info = build_upload_info(user, ip, format);

      DocumentBuilder gene_expression_builder;
      gene_expression_builder.append(Concatenate(gene_expression_metadata.view()));
      gene_expression_builder.append(KVP("upload_info", upload_info.view()));

      COLLECTION(gene_expressions, dba::Collections::GENE_EXPRESSIONS())
      gene_expressions.insert_one(gene_expression_builder.extract());

      try {
        dba::search::insert_full_text(dba::Collections::GENE_EXPRESSIONS(), expression_id, gene_expression_metadata.view());
      } catch (const deepblue_user_exception& ex) {
        dba::remove::gene_expression(user, expression_id);
        throw ex;
      }

      size_t total_genes = 0;
      COLLECTION(gene_single_expressions, dba::Collections::GENE_SINGLE_EXPRESSIONS())
      auto rows_obj_bulk = gene_single_expressions.create_bulk_write();
      for (const auto& row : file->rows()) {
        int _id = dba::helpers::get_incremente_counter_and_notify(dba::Collections::GENE_SINGLE_EXPRESSIONS());

        std::string gene_id = "gsx" + utils::integer_to_string(_id);
        Document row_obj = to_bson(dataset_id, gene_id, row);

        InsertOneOp insert_op{row_obj.view()};
        rows_obj_bulk.append(insert_op);
        total_genes++;
      }
      auto result = rows_obj_bulk.execute();

      if (!update_upload_info(dba::Collections::GENE_EXPRESSIONS(), expression_id, total_genes)) {
        dba::remove::gene_model(user, expression_id);
      }

      return expression_id;
    }


    const ChromosomeRegionsList GeneExpressionType::load_data(const std::vector<std::string> &sample_ids,
                    const std::vector<int64_t>& replicas,
                    const std::vector<std::string> &genes, const std::vector<std::string> &project,
                    const std::string & norm_gene_model)
    {
      COLLECTION(gene_models, dba::Collections::GENE_MODELS())

      auto res = gene_models.find_one(bson_utils::one_key_doc("norm_name", norm_gene_model));
      if (!res) {
        throw deepblue_user_exception(ERR_INVALID_GENE_MODEL_NAME, norm_gene_model);
      }

      DocumentBuilder ges_builder;
      if (!sample_ids.empty()) {
        ges_builder.append(KVP("sample_id",
          MakeDocument(KVP("$in", bson_utils::vector_to_array(sample_ids)))));
      }

      if (!replicas.empty()) {
        ges_builder.append(KVP("replica",
          MakeDocument(KVP("$in", bson_utils::vector_to_array(replicas)))));
      }

      if (!project.empty()) {
        ges_builder.append(KVP("norm_project",
          MakeDocument(KVP("$in", bson_utils::vector_to_array(project)))));
      }


      Document ges_query = ges_builder.extract();

      Array ges_datasets = dba::helpers::build_dataset_ids_arrays(dba::Collections::GENE_EXPRESSIONS(), ges_query);

      DocumentBuilder builder;
      // Look at the tracking ID, gene id, and short name
      builder.append(KVP(dba::KeyMapper::DATASET(), MakeDocument(KVP("$in", ges_datasets))));

      if (!genes.empty()) {
        Document b_in_tracking_id = MakeDocument(
          KVP(dba::KeyMapper::TRACKING_ID(),
            MakeDocument(KVP("$in", bson_utils::vector_to_array(genes)))));

        Document b_in_gene_id = MakeDocument(
          KVP(dba::KeyMapper::GENE_ID(),
            MakeDocument(KVP("$in", bson_utils::vector_to_array(genes)))));

        Document b_in_short_name = MakeDocument(
          KVP(dba::KeyMapper::GENE_SHORT_NAME(),
            MakeDocument(KVP("$in", bson_utils::vector_to_array(genes)))));

        builder.append(KVP("$or", MakeArray(b_in_tracking_id, b_in_gene_id, b_in_short_name )));
      }

      COLLECTION(gene_single_expressions, dba::Collections::GENE_SINGLE_EXPRESSIONS())
      auto cursor = gene_single_expressions.find(builder.extract());
      std::unordered_map<std::string, Regions> gene_expressions;

      for (const auto &gene: cursor) {
        std::string gene_short_name;
        std::string tracking_id;

        DatasetId dataset_id = static_cast<uint32_t>(gene[dba::KeyMapper::DATASET()].get_int32().value);

        if (gene.find(dba::KeyMapper::GENE_SHORT_NAME()) != gene.end()) {
          gene_short_name = bson_utils::get_string(gene, dba::KeyMapper::GENE_SHORT_NAME());
        }

        if (gene.find(dba::KeyMapper::TRACKING_ID()) != gene.end()) {
          tracking_id = bson_utils::get_string(gene, dba::KeyMapper::TRACKING_ID());
        }

        std::string chromosome;
        Position start;
        Position end;
        std::string strand;
        dba::genes::map_gene_location(tracking_id, gene_short_name, norm_gene_model, chromosome, start, end, strand);

        RegionPtr region = build_stranded_region(start, end, dataset_id, strand);

        auto it = gene.begin();
        // Jump the _id field
        it++;
        // Jump the dataset_id field
        it++;

        for ( ; it != gene.end(); it++) {
          if (it->type() == bsoncxx::type::k_utf8) {
            region->insert(StringViewToString(it->get_utf8().value));
          }

          else if (it->type() == bsoncxx::type::k_double) {
            region->insert(static_cast<Score>(it->get_double().value));
          }

          else if (it->type() == bsoncxx::type::k_int32 	) {
            region->insert(static_cast<int>(it->get_int32().value));
          }

          else {
            std::cerr << "Invalid e->type() for " << bson_utils::get_key(*it) << std::endl;
          }
        }
        gene_expressions[chromosome].emplace_back(std::move(region));
      }

      ChromosomeRegionsList  chromosome_regions_list;
      for (auto &chromosome_regions : gene_expressions) {
        std::sort(chromosome_regions.second.begin(), chromosome_regions.second.end(), RegionPtrComparer);
        chromosome_regions_list.emplace_back(chromosome_regions.first, std::move(chromosome_regions.second));
      }

      return chromosome_regions_list;
    }

    const std::vector<utils::IdName> GeneExpressionType::list(const View& query)
    {
      return dba::helpers::get_id_names_by_query(dba::Collections::GENE_EXPRESSIONS(), query);
    }
  }
}
