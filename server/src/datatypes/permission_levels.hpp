//
//  permission_levels.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.11.20
//  Copyright (c) 2020 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#ifndef PERMISSION_LEVELS_H_
#define PERMISSION_LEVELS_H_

#include <string>

namespace epidb {
  namespace datatypes {
    enum PermissionLevel {
      ADMIN = 0,
      INCLUDE_COLLECTION_TERMS = 10,
      INCLUDE_EXPERIMENTS = 20,
      INCLUDE_ANNOTATIONS = 30,
      GET_DATA = 40,
      LIST_COLLECTIONS = 50,
      NONE = 1000,
      NOT_SET = 10000
    };

    std::string permission_level_to_string(PermissionLevel pl);
  }
}


#endif
