//
//  projects.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 27.05.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <string>

#include "projects.hpp"

#include "../config/config.hpp"

#include "../dba/collections.hpp"
#include "../dba/full_text.hpp"
#include "../dba/helpers.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../exceptions.hpp"


namespace epidb {
  namespace datatypes {
    namespace projects {

      const std::string add_project(const std::string &name, const std::string &norm_name,
                       const std::string &description, const std::string &norm_description,
                       const utils::IdName &user)
      {
        int id = dba::helpers::get_incremente_counter_and_notify(dba::Collections::PROJECTS());
        std::string project_id = "p" + utils::integer_to_string(id);

        DocumentStreamBuilder search_data_builder{};
        search_data_builder << "_id" << project_id;
        search_data_builder << "name" << name;
        search_data_builder << "norm_name" << norm_name;
        search_data_builder << "description" << description;
        search_data_builder << "norm_description" << norm_description;

        auto search_data = search_data_builder.extract();

        DocumentBuilder create_project_builder{};
        create_project_builder.append(Concatenate(search_data.view()));
        create_project_builder.append(KVP("user", user.id));
        auto cem = create_project_builder.extract();


        COLLECTION(projects, dba::Collections::PROJECTS())
        projects.insert_one(cem.view());

        dba::search::insert_full_text(dba::Collections::PROJECTS(), project_id, search_data);

        return project_id;
      }

      const std::string get_id(const std::string &project)
      {
        if (utils::is_id(project, "p")) {
          return project;
        }

        const std::string norm_name = utils::normalize_name(project);
        return dba::helpers::get_id(dba::Collections::PROJECTS(), norm_name);
      }

      void set_public(const std::string &project_id, const bool set)
      {
        auto query = bson_utils::id_doc(project_id);
        auto update = DocumentStreamBuilder{} <<
          "$set" << Open << "public" << set << Close <<
          StreamFinalize;


        COLLECTION(PROJECTS, dba::Collections::PROJECTS())
        auto result = PROJECTS.update_one(query.view(), update.view());
        if (!result) {
          throw deepblue_user_exception(ERR_SET_PUBLIC_PROJECT, project_id);
        }

        dba::helpers::notify_change_occurred(dba::Collections::PROJECTS());
      }

      void add_user_to_project(const std::string &user_id, const std::string &project_id, const bool include)
      {
        auto query = bson_utils::id_doc(user_id);

        DocumentBuilder builder{};
        if (include) {
          builder.append(KVP("$addToSet", MakeDocument(KVP("projects", project_id))));
        } else {
          builder.append(KVP("$pull", MakeDocument(KVP("projects", project_id))));
        }

        COLLECTION(USERS, dba::Collections::USERS())
        USERS.update_one(query.view(), builder.extract());

        dba::helpers::notify_change_occurred(dba::Collections::PROJECTS());
      }
    }
  }
}
