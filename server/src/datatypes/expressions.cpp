//
//  expressions.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 24.10.16.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <chrono>
#include <string>
#include <vector>

#include "../dba/collections.hpp"
#include "../dba/info.hpp"
#include "../dba/users.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../parser/parser_factory.hpp"

#include "../errors.hpp"
#include "../macros.hpp"
#include "../log.hpp"

#include "expressions.hpp"

namespace epidb {
  namespace datatypes {

    const std::string& AbstractExpressionType::name()
    {
      return _expression_type_name;
    }

    const Document AbstractExpressionType::build_expression_type_metadata(const std::string &sample_id, const int replica,
        const std::string &format, const std::string &project, const std::string &norm_project,
        const View extra_metadata_obj,
        const std::string &ip,
        int &dataset_id, std::string &gene_model_id)
    {
      dataset_id = dba::helpers::get_incremente_counter_and_notify("datasets");

      auto collection_counter = dba::helpers::get_incremente_counter_and_notify(dba::Collections::GENE_EXPRESSIONS());
      gene_model_id = "gx" + utils::integer_to_string(collection_counter);

      DocumentBuilder gene_model_metadata_builder{};
      gene_model_metadata_builder.append(KVP("_id", gene_model_id));
      gene_model_metadata_builder.append(KVP(dba::KeyMapper::DATASET(), dataset_id));
      gene_model_metadata_builder.append(KVP("sample_id", sample_id));
      gene_model_metadata_builder.append(KVP("replica", replica));
      gene_model_metadata_builder.append(KVP("extra_metadata", extra_metadata_obj));

      if (format == "cufflinks") {
        const auto& cufflinks_format = parser::FileFormat::cufflinks_format();
        gene_model_metadata_builder.append(KVP("format", cufflinks_format.format()));
        gene_model_metadata_builder.append(KVP("columns", cufflinks_format.to_array()));
      } else if (format == "grape2") {
        const auto& grape2_format = parser::FileFormat::grape2_format();
        gene_model_metadata_builder.append(KVP("format", grape2_format.format()));
        gene_model_metadata_builder.append(KVP("columns", grape2_format.to_array()));
      } else if (format == "salmon") {
        const auto& salmon_format = parser::FileFormat::salmon_format();
        gene_model_metadata_builder.append(KVP("format", salmon_format.format()));
        gene_model_metadata_builder.append(KVP("columns", salmon_format.to_array()));
      } else {
        throw deepblue_user_exception(ERR_GENE_EXPRESSION_FORMAT_INVALID, format);
      }


      gene_model_metadata_builder.append(KVP("project", project));
      gene_model_metadata_builder.append(KVP("norm_project", norm_project));

      datatypes::Metadata sample_data = dba::info::get_sample_by_id(sample_id, true);

      DocumentBuilder sample_builder{};
      for (auto it = sample_data.begin(); it != sample_data.end(); ++it) {
        if ((it->first != "_id") && (it->first != "user")) {
          sample_builder.append(KVP(it->first, it->second));
        }
      }
      gene_model_metadata_builder.append(KVP("sample_info", sample_builder.extract()));

      return gene_model_metadata_builder.extract();
    }

    Document AbstractExpressionType::build_upload_info(const datatypes::User& user,
        const std::string &client_address, const std::string &content_format)
    {
      DocumentBuilder upload_info_builder;

      upload_info_builder.append(KVP("user", user.id()));
      upload_info_builder.append(KVP("content_format", content_format));
      upload_info_builder.append(KVP("done", false));
      upload_info_builder.append(KVP("client_address", client_address));
      upload_info_builder.append(KVP("upload_start", Date{std::chrono::system_clock::now()}));

      return upload_info_builder.extract();
    }


    const Document AbstractExpressionType::build_list_expressions_query(const datatypes::User& user,
        const std::vector<serialize::ParameterPtr> &sample_ids, const std::vector<serialize::ParameterPtr> &replicas,
        const std::vector<serialize::ParameterPtr> &projects)
    {
      DocumentBuilder args_builder;

      if (!sample_ids.empty()) {
        args_builder.append(KVP("sample_id",
          MakeDocument(KVP("$in", bson_utils::parameters_vector_to_array(sample_ids)))
        ));
      }


      if (!replicas.empty()) {
        args_builder.append(KVP("replica",
          MakeDocument(KVP("$in", bson_utils::parameters_long_vector_to_array(replicas)))
        ));
      }


      // TODO: move to a more generic function in projects
      if (!projects.empty()) {
        // Filter the projects that are available to the user
        std::vector<std::string> filtered_projects;
        for (const auto& project : projects) {
          std::string project_name = project->as_string();
          std::string norm_project = utils::normalize_name(project_name);
          bool found = false;
          for (const auto& user_project : user.projects()) {
            std::string norm_user_project = utils::normalize_name(user_project);
            if (norm_project == norm_user_project) {
              filtered_projects.push_back(norm_project);
              found = true;
              break;
            }
          }

          if (!found) {
            throw deepblue_user_exception(ERR_INVALID_PROJECT, project_name);
          }
        }
        args_builder.append(KVP("norm_project",
          MakeDocument(KVP("$in", utils::build_normalized_array(filtered_projects)))
        ));
      } else {
        args_builder.append(KVP("norm_project",
          MakeDocument(KVP("$in", utils::build_normalized_array(user.projects())))
        ));
      }

      return args_builder.extract();
    }

  }
}
