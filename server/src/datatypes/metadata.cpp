//
//  metadata.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 07.10.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "../extras/utils.hpp"
#include "../typedefs/bson.hpp"

#include "../extras/bson_utils.hpp"

#include "metadata.hpp"

namespace epidb {
  namespace datatypes {
    Document metadata_to_document(const Metadata &extra_metadata)
    {
      DocumentBuilder extra_metadata_builder;
      Metadata::const_iterator cit;
      for (cit = extra_metadata.begin(); cit != extra_metadata.end(); ++cit) {
        extra_metadata_builder.append(KVP(cit->first, cit->second));
      }
      return extra_metadata_builder.extract();
    }


    Metadata document_to_metadata(const View view)
    {
      Metadata extra_metadata;

      for (Element ele : view) {
        std::string key(bson_utils::get_key(ele));
        extra_metadata[key] = utils::element_to_string(ele.get_value());
      }

      return extra_metadata;
    }
  }
}