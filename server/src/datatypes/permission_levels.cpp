//
//  permission_levels.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 08.11.20
//  Copyright (c) 2020 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//



#include "permission_levels.hpp"
#include "../extras/utils.hpp"

namespace epidb {
  namespace datatypes {

    std::string permission_level_to_string(PermissionLevel pl)
    {
      switch (pl) {
      case datatypes::ADMIN:
        return "ADMIN";
      case datatypes::INCLUDE_COLLECTION_TERMS:
        return "INCLUDE_COLLECTION_TERMS";
      case datatypes::INCLUDE_EXPERIMENTS:
        return "INCLUDE_EXPERIMENTS";
      case datatypes::INCLUDE_ANNOTATIONS:
        return "INCLUDE_ANNOTATIONS";
      case datatypes::GET_DATA:
        return "GET_DATA";
      case datatypes::LIST_COLLECTIONS:
        return "LIST_COLLECTIONS";
      case datatypes::NONE:
        return "NONE";
      default:
        return "Unknown permission level: " + utils::integer_to_string(pl);
      }
    }
  }
}
