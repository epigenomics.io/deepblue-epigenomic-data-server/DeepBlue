//
//  hub.cpp
//  DeepBlue Epigenomic Data Server

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Code from https://github.com/temporaer/MDBQ/
// adapted to DeepBlue by Felipe Albrecht on 22.01.2015

#include <chrono>
#include <iomanip>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

#include "hub.hpp"

#include "../config/config.hpp"

#include "../dba/collections.hpp"

#include "../dba/helpers.hpp"
#include "../dba/users.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../errors.hpp"
#include "../log.hpp"


namespace epidb {
  namespace mdbq {
    struct HubImpl {
      unsigned int m_interval;
      std::string  m_prefix;
      std::auto_ptr<boost::asio::deadline_timer> m_timer;

      void update_check(Hub *c, const boost::system::error_code &error)
      {
        // search for jobs which have failed and reschedule them
        FindOptions opts{};
        opts.projection(MakeDocument(KVP("_id", 1),
                                  KVP("owner", 1),
                                  KVP("nfailed", 1)));

        COLLECTION(jobs, dba::Collections::JOBS())
        auto p = jobs.find(MakeDocument(
                            KVP("state", TS_FAILED),
                            KVP("nfailed", MakeDocument(KVP("$lt", 1)) /* first time failure only */
                          )),
                          opts);

        for (const auto& f: p) {
          if (!f["nfailed"].get_int32().value)
            continue;

          std::cerr << "HUB: warning: task `"
                    << bson_utils::get_string(f, "_id") << "' on `"
                    << f["owner"].get_utf8().value << "' failed, rescheduling" << std::endl;

          auto query = bson_utils::id_doc(bson_utils::get_string(f, "_id"));
          auto update = MakeDocument(KVP("$inc", MakeDocument(KVP("nfailed", 1))),
                                     KVP("$set", MakeDocument(KVP("state", TS_NEW),
                                                      KVP("book_time",  Undefined{}),
                                                      KVP("refresh_time", Undefined{}))));
          jobs.update_one(query.view(), update.view());
        }

        if (!error) {
          m_timer->expires_at(m_timer->expires_at() + boost::posix_time::seconds(m_interval));
          m_timer->async_wait(boost::bind(&HubImpl::update_check, this, c, boost::asio::placeholders::error));
        } else {
          EPIDB_LOG_ERR("HUB: error_code!=0, failing!");
          return;
        }
      }
    };

    Hub::Hub(const std::string &url, const std::string &prefix)
      : m_prefix(prefix)
    {
      m_ptr.reset(new HubImpl());
      m_ptr->m_prefix = prefix;
    }

    bool Hub::exists_job(const View &job, std::string &id, bool update)
    {
      COLLECTION(jobs_collection, dba::Collections::JOBS())
      const auto maybe_ret = jobs_collection.find_one(job);

      if (maybe_ret) {
        auto ret = maybe_ret->view();
        if (update && (is_cleared(ret) || is_canceled(ret))) {
          reprocess_job(ret);
        }
        id = bson_utils::get_string(ret, "_id");
        return true;
      } else {
        return false;
      }
    }

    const std::string Hub::insert_job(const View &job, unsigned int timeout, const int version_value)
    {
      int r_id = dba::helpers::get_increment_counter("request");
      std::string id = "r" + epidb::utils::integer_to_string(r_id);

      auto now = std::chrono::system_clock::now();

      COLLECTION(jobs, dba::Collections::JOBS())
      jobs.insert_one(MakeDocument(
                  KVP( "_id"         , id),
                  KVP("timeout"      , static_cast<int32_t>(timeout)),
                  KVP("version"      , static_cast<int32_t>(version_value)),
                  KVP("create_time"  , Date(now)),
                  KVP("finish_time"  , Undefined{}),
                  KVP("book_time"    , Undefined{}),
                  KVP("refresh_time" , Undefined{}),
                  KVP("misc"         , job),
                  KVP("nfailed"      , static_cast<int32_t>(0)),
                  KVP("state"        , static_cast<int32_t>(TS_NEW)),
                  KVP("version"      , static_cast<int32_t>(0))
                )
              );

      return id;
    }

    bool Hub::reprocess_job(const View &job)
    {
      const std::string& _id = bson_utils::get_string(job, "_id");

      auto now = std::chrono::system_clock::now();
      COLLECTION(jobs, dba::Collections::JOBS())

      auto OPTS = mongocxx::options::find_one_and_update();
      OPTS.return_document(mongocxx::options::return_document::k_after);

      auto updated = jobs.find_one_and_update(
        bson_utils::id_doc(_id),
        MakeDocument(KVP("$set",
                      MakeDocument(KVP("state", TS_REPROCESS),
                                   KVP("refresh_time", Date(now))
                                  ))
                    ),
        OPTS
      );

      if (!updated) {
        std::cerr << "No request '" + _id + "' available";
        return false;
      }

      return true;
    }

    void Hub::reg(boost::asio::io_service &io_service, unsigned int interval)
    {
      m_ptr->m_interval = interval;
      m_ptr->m_timer.reset(new boost::asio::deadline_timer(io_service, boost::posix_time::seconds(interval)));
      m_ptr->m_timer->async_wait(boost::bind(&HubImpl::update_check, m_ptr.get(), this, boost::asio::placeholders::error));
    }

    MaybeDocument Hub::get_job(const std::string &id, bool update_access)
    {
      COLLECTION(jobs, dba::Collections::JOBS())

      if (update_access) {
        auto now = std::chrono::system_clock::now();

        auto query = bson_utils::id_doc(id);
        auto update =   MakeDocument(
                                  KVP("$set", MakeDocument(KVP("last_access", Date(now)))),
                                  KVP("$inc", MakeDocument(KVP("times_accessed", static_cast<int32_t>(1)))
                                  ));

        auto updated = jobs.find_one_and_update(
                              query.view(),
                              update.view());
        return updated;
      }

      return jobs.find_one(bson_utils::id_doc(id));
    }

    std::list<Document> Hub::get_jobs(const mdbq::TaskState& state, const std::string &user_id)
    {
      std::list<Document> ret;

      COLLECTION(jobs, dba::Collections::JOBS())
      if (state == mdbq::_TS_END) {
        auto cursor = jobs.find(MakeDocument(KVP("misc.user_id", user_id)));

        for (const auto o: cursor) {
          ret.emplace_back(Document{o});
        }

      } else {
        auto cursor = jobs.find(MakeDocument(
                                  KVP("state", state),
                                  KVP("misc.user_id", user_id)
                                ));

        for (const auto o: cursor) {
          ret.emplace_back(Document{o});
        }
      }

      return ret;
    }

    bool Hub::job_has_user_id(const std::string& request_id, const std::string& user_id)
    {
      COLLECTION(jobs, dba::Collections::JOBS())
      auto count = jobs.count_documents(MakeDocument(
                        KVP("_id", request_id),
                        KVP("misc.user_id", user_id)));
      return count > 0;
    }

    mdbq::TaskState Hub::state_number(const std::string& name)
    {
      if (name == "new") {
        return TS_NEW;
      } else if (name == "running") {
        return TS_RUNNING;
      } else if (name == "done") {
        return TS_DONE;
      } else if (name == "failed") {
        return TS_FAILED;
      } else if (name == "canceled") {
        return TS_CANCELLED;
      } else if (name == "removed") {
        return TS_REMOVED;
      } else if (name == "renew") {
        return TS_RENEW;
      } else if (name == "cleared") {
        return TS_CLEARED;
      } else if (name == "reprocess") {
        return TS_REPROCESS;
      } else {
        return _TS_END;
      }
    }

    std::string Hub::state_name(const View &o)
    {
      return state_name(o["state"].get_int32().value);
    }

    std::string Hub::state_name(const int state)
    {
      switch (state) {
      case TS_NEW:
        return "new";
      case TS_RUNNING:
        return "running";
      case TS_DONE:
        return "done";
      case TS_FAILED:
        return "failed";
      case TS_CANCELLED:
        return "canceled";
      case TS_REMOVED:
        return "removed";
      case TS_RENEW:
        return "renew";
      case TS_CLEARED:
        return "cleared";
      case TS_REPROCESS:
        return "reprocess";
      default :
        return "Invalid State: " + epidb::utils::integer_to_string(state);
      }
    }

    std::string Hub::state_message(const View &o)
    {
      int state = o["state"].get_int32().value;

      switch (state) {
      case TS_NEW:
      case TS_RUNNING:
      case TS_DONE:
      case TS_CANCELLED:
      case TS_REMOVED:
      case TS_RENEW:
      case TS_CLEARED:
      case TS_REPROCESS:
        return "";
      case TS_FAILED:
        return bson_utils::get_string(o, "error");
      default :
        return "Invalid State: " + epidb::utils::integer_to_string(state);
      }
    }

    std::chrono::time_point<std::chrono::system_clock> Hub::get_create_time(const View& o)
    {
      return o["create_time"].get_date();
    }

    std::chrono::time_point<std::chrono::system_clock> Hub::get_finish_time(const View& o)
    {
      return o["finish_time"].get_date();
    }

    Document Hub::get_misc(const View& o)
    {
      return Document{o["misc"].get_document().value};
    }

    std::string Hub::get_id(const View& o)
    {
      return bson_utils::get_string(o, "_id");
    }

    bool Hub::is(const View &o, const mdbq::TaskState state)
    {
      TaskState task_state = (TaskState) o["state"].get_int32().value;
      return task_state == state;
    }

    bool Hub::is_done(const View &o)
    {
      return Hub::is(o, TS_DONE);
    }

    bool Hub::is_failed(const View &o)
    {
      return Hub::is(o, TS_FAILED);
    }

    bool Hub::is_cleared(const View &o)
    {
      return Hub::is(o, TS_CLEARED);
    }

    bool Hub::is_canceled(const View &o)
    {
      return Hub::is(o, TS_CANCELLED);
    }
  }
}
