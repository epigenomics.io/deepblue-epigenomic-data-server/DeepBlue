//
//  cleaner.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 24.05.17.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <chrono>
#include <string>

#include "../config/config.hpp"

#include "../datatypes/user.hpp"

#include "../dba/collections.hpp"

#include "../dba/helpers.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../storage/storage.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "common.hpp"
#include "cleaner.hpp"

#include "../exceptions.hpp"
#include "../errors.hpp"
#include "../log.hpp"

namespace epidb {
  namespace mdbq {
    bool remove_request_data(const std::string& request_id, TaskState state)
    {
      // This function has 3 steps:
      // 1. Mark the request as removed.
      // 2. Mark the processing as removed, but keep it for future reference.
      // 3. Delete the data from gridfs

      // 1.
      auto OPTS = mongocxx::options::find_one_and_update();
      OPTS.return_document(mongocxx::options::return_document::k_after);

      auto cmd = MakeDocument(KVP("$set", MakeDocument(
                                    KVP("state", state),
                                    KVP("refresh_time", Date(std::chrono::system_clock::now()))
                                  )));

      COLLECTION(jobs, dba::Collections::JOBS())
      auto updated = jobs.find_one_and_update(
                            bson_utils::id_doc(request_id),
                            cmd.view(),
                            OPTS);
      if (!updated) {
        throw deepblue_user_exception(ERR_REQUEST_ID_INVALID, request_id);
      }


      // 2.
      COLLECTION(processing, dba::Collections::PROCESSING())
      processing.update_one(
                          bson_utils::id_doc(request_id),
                          MakeDocument(KVP("$set", MakeDocument(
                                    KVP("status", state))))
      );

      // 3.
      remove_result(request_id);

      return true;
    }

    bool cancel_request(const datatypes::User& user, const std::string& request_id)
    {
      DocumentBuilder builder;
      builder.append(KVP("_id", request_id));

      if (!user.is_admin()) {
        builder.append(KVP("misc.user_id", user.id()));
      }

      auto query = builder.extract();
      auto update = MakeDocument(KVP("$set", MakeDocument(
                                KVP("finish_time",  Date(std::chrono::system_clock::now())),
                                KVP("state", static_cast<int32_t>(TS_CANCELLED)),
                                KVP("refresh_time", Date(std::chrono::system_clock::now()))
                              )));


      auto OPTS = mongocxx::options::find_one_and_update();
      OPTS.return_document(mongocxx::options::return_document::k_after);
      COLLECTION(jobs, dba::Collections::JOBS())

      auto maybe_task_info = jobs.find_one_and_update(query.view(), update.view(), OPTS);
      if (!maybe_task_info) {
        throw deepblue_user_exception(ERR_REQUEST_ID_INVALID, request_id);
      }

      TaskState task_state = static_cast<TaskState>(maybe_task_info->view()["state"].get_int32().value);

      if (task_state == TS_DONE || task_state == TS_FAILED) {
        remove_request_data(request_id, TS_REMOVED);
      }

      return true;
    }

    void remove_result(const std::string request_id)
    {
      EPIDB_LOG_DBG("Removing data for the request: " + request_id);

      storage::remove(request_id);
    }

  }
}