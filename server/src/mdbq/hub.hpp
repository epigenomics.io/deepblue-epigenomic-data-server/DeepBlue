//
//  hub.hpp
//  DeepBlue Epigenomic Data Server

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Code from https://github.com/temporaer/MDBQ/
// adapted to DeepBlue by Felipe Albrecht on 22.01.2015

#ifndef __MDBQ_HUB_HPP__
#define __MDBQ_HUB_HPP__

#include <list>
#include <memory>

#include <boost/asio.hpp>

#include "../datatypes/user.hpp"
#include "../mdbq/common.hpp"


namespace epidb {
  class StringBuilder;
}

namespace epidb {
  namespace mdbq {
    struct HubImpl;

    /**
     * MongoDB Queue Hub
     *
     * derive from this class to implement your own job generator
     */
    class Hub {
    private:
      /// pointer to implementation
      std::shared_ptr<HubImpl> m_ptr;

      /// database plus queue prefix (db.queue)
      const std::string m_prefix;

      static bool is(const View &o, const mdbq::TaskState state);

    public:
      /**
       * ctor.
       *
       * @param url how to connect to mongodb
       */
      Hub(const std::string &url, const std::string &prefix);


      /**
       * Check if job with the given parameters already exists.
       */
      bool exists_job(const View &job, std::string &id, bool update = false);

      /**
       * insert job
       *
       * @param job the job description
       * @param timeout the timeout in seconds
       * @param version the DeepBlue version when the job was inserted
       */
      const std::string insert_job(const View &job, unsigned int timeout, const int version_value);


      MaybeDocument get_job(const std::string& id, bool update_access = false);

      /*
       * \brief Get all jobs in given state owned by given user. If no valid mdbq::TaskState is given,
       *        all jobs are returned.
       * \param state     State searched for
       *        user_id   ID of owning user
       * \return          Requested jobs
       */
      std::list<Document> get_jobs(const mdbq::TaskState& state, const std::string &user_id);

      bool job_has_user_id(const std::string& request_id, const std::string& user_id);


      /**
       *
       */
      bool reprocess_job(const View &job);

      /**
       * register with the main loop
       *
       * @param interval querying interval
       */
      void reg(boost::asio::io_service &io_service, unsigned int interval);

      static mdbq::TaskState state_number(const std::string& name);

      static std::string state_name(const View& o);

      static std::string state_name(const int state);

      static std::string state_message(const View& o);

      /*
      * \brief Get the time at which a request was created
      */
      static std::chrono::time_point<std::chrono::system_clock> get_create_time(const View& o);

      /*
      * \brief Get the time at which a request was completed
      */
      static std::chrono::time_point<std::chrono::system_clock> get_finish_time(const View& o);

      /*
      * \brief Get misc info for a request
      */
      static Document get_misc(const View& o);

      /*
      * \brief Get the id of a request
      */
      static std::string get_id(const View& o);

      static bool is_done(const View& o);

      static bool is_cleared(const View& o);

      static bool is_failed(const View& o);

      static bool is_canceled(const View& o);
    };
  }
}
#endif /* __MDBQ_HUB_HPP__ */
