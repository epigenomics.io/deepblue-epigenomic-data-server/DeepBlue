//
//  janitor.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 24.05.17.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

#include "../config/config.hpp"

#include "../dba/collections.hpp"
#include "../dba/helpers.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/database.hpp"

#include "janitor.hpp"
#include "common.hpp"
#include "cleaner.hpp"

#include "../log.hpp"

namespace epidb {
  namespace mdbq {

    void Janitor::run()
    {
      EPIDB_LOG_TRACE("Starting Janitor");
      reg(ios);
      ios.run();
    }

    void Janitor::reg(boost::asio::io_service &io_service)
    {
      unsigned long long interval = epidb::config::get_janitor_periodicity();

      m_timer.reset(new boost::asio::deadline_timer(io_service,
                    boost::posix_time::seconds(interval)));
      m_timer->async_wait(boost::bind(&Janitor::clean_oldest, this, boost::asio::placeholders::error));
    }

    void Janitor::update(config::ConfigSubject & subject)
    {
      reset();
    }

    void Janitor::reset()
    {
      m_timer->cancel();
      unsigned long long interval = epidb::config::get_janitor_periodicity();
      m_timer.reset(new boost::asio::deadline_timer(ios, boost::posix_time::seconds(interval)));
      m_timer->async_wait(boost::bind(&Janitor::clean_oldest, this, boost::asio::placeholders::error));
    }

    bool Janitor::clean_oldest(const boost::system::error_code &error)
    {
      EPIDB_LOG("RUN Janitor");
      if (error == boost::asio::error::operation_aborted) {
        EPIDB_LOG_DBG("clean_oldest canceled, probably it was reset");
        return false;
      }

      DocumentBuilder bob;

      auto now = std::chrono::system_clock::now();
      std::chrono::time_point<std::chrono::system_clock> prev = now + std::chrono::seconds(config::get_old_request_age_in_sec());

      ArrayBuilder ab;
      ab.append(MakeDocument(KVP("misc.command", "score_matrix")));
      ab.append(MakeDocument(KVP("misc.command", "get_regions")));

      bob.append(KVP("state", TaskState::TS_DONE));
      bob.append(KVP("$or", ab.extract()));
      bob.append(KVP("finish_time", MakeDocument(KVP("$lte", Date{prev}))));

      GET_ORDER(opts, "finish_time", 1)

      COLLECTION(jobs_collection, dba::Collections::JOBS())
      auto cursor = jobs_collection.find(bob.extract(), opts);

      for (const auto& job: cursor) {
        EPIDB_LOG_DBG("CLEARING JOB " << ToJSON(job));
        const std::string& id = bson_utils::get_string(job, "_id");

        bool o = remove_request_data(id, TS_CLEARED);
        if (o) {
          EPIDB_LOG_DBG("Job " << id << " cleared with success");
        } else {
          EPIDB_LOG_DBG("Problem clearing job: " << id << " - ");
        }
      }

      unsigned long long interval = epidb::config::get_janitor_periodicity();
      m_timer->expires_at(m_timer->expires_at() + boost::posix_time::seconds(interval));
      m_timer->async_wait(boost::bind(&Janitor::clean_oldest, this, boost::asio::placeholders::error));

      return true;
    }
  }
}