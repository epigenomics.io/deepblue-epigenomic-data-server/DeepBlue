//
//  client.cpp
//  DeepBlue Epigenomic Data Server

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Code from https://github.com/temporaer/MDBQ/
// adapted to DeepBlue by Felipe Albrecht on 22.01.2015
// Last update on 28.04.2021

#include <chrono>
#include <random>
#include <vector>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

#include "client.hpp"
#include "common.hpp"

#include "../config/config.hpp"

#include "../dba/collections.hpp"
#include "../dba/helpers.hpp"

#include "../engine/engine.hpp"

#include "../extras/bson_utils.hpp"

#include "../storage/storage.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../errors.hpp"

#include "../exceptions.hpp"

namespace epidb {
  namespace mdbq {

    struct ClientImpl {
      MaybeDocument            m_current_task;
      std::chrono::system_clock::time_point m_current_task_timeout_time;
      long long int            m_running_nr;
      float                    m_interval;
      std::auto_ptr<boost::asio::deadline_timer> m_timer;

      void update_check(Client *c, const boost::system::error_code &error)
      {
        std::string _id;
        auto task = c->get_next_task(_id);

        if (task) {
          try {
            c->handle_task(_id, task->view());
          } catch (const deepblue_user_exception& de) {
            const auto& msg = de.msg();
            EPIDB_LOG_ERR(msg);
          }
        }

        if (!error) {
          unsigned int ms;
          if (m_interval <= 1.f) {
            ms = 1000 * (m_interval / 2 + drand48() * (m_interval / 2));
          } else {
            ms = 1000 * (1 + drand48() * (m_interval - 1));
          }
          m_timer->expires_at(m_timer->expires_at() + boost::posix_time::millisec(ms));
          m_timer->async_wait(boost::bind(&ClientImpl::update_check, this, c, boost::asio::placeholders::error));
        }
      }
    };

    Client::Client(const std::string &url, const std::string &prefix)
      : m_fscol(prefix + ".fs")
      , m_verbose(false)
    {
      m_ptr.reset(new ClientImpl());
      m_db = prefix;
    }

    MaybeDocument Client::get_next_task(std::string &_id)
    {
      auto now = std::chrono::system_clock::now();

      char hostname_c[256];
      gethostname(hostname_c, 256);
      std::string hostname(hostname_c, strlen(hostname_c));

      std::string hostname_pid = fmt::format("{}:{}", hostname, getpid());

      ArrayBuilder ab;
      ab.append(TS_NEW, TS_RENEW, TS_REPROCESS);

      DocumentBuilder queryb;
      queryb.append(KVP("state", MakeDocument(
        KVP("$in",  ab.extract()))));

      auto update = MakeDocument(KVP("$set", MakeDocument(
                                        KVP("book_time", Date{now}),
                                        KVP("state", TS_RUNNING),
                                        KVP("refresh_time", Date{now}),
                                        KVP("owner", hostname_pid))));

      COLLECTION(jobs_collection, dba::Collections::JOBS())
      auto res = jobs_collection.find_one_and_update(queryb.extract(), update.view());

      if (!res) {
          return EMPTY_DOCUMENT;
      }

      m_ptr->m_current_task = *res;

      auto current_task_view = m_ptr->m_current_task->view();

      int timeout_s = INT_MAX;
      if (bson_utils::has_key(current_task_view, "timeout")) {
        timeout_s = current_task_view["timeout"].get_int32().value;
      }

      m_ptr->m_current_task_timeout_time = now + std::chrono::seconds(timeout_s);
      m_ptr->m_running_nr = 0;

      _id  = bson_utils::get_string(current_task_view, "_id");

      return Document(res->view()["misc"].get_document().value);
    }

    void Client::finish(const View &result, bool ok)
    {
      if (!m_ptr->m_current_task) {
        throw_runtime_error("Get a task first before you finish!");
      }
      auto finish_time = std::chrono::system_clock::now();

      auto task_view = m_ptr->m_current_task->view();
      int version = task_view["version"].get_int32().value;

      auto _id = bson_utils::get_string(task_view, "_id");

      if (ok) {
        auto query = MakeDocument(KVP("_id", _id),
                                  KVP("version", version));

        auto update = MakeDocument(KVP("$set", MakeDocument(
                                   KVP("state", TS_DONE),
                                   KVP("version", version + 1),
                                   KVP("finish_time", Date{finish_time}),
                                   KVP("result", result))));

        COLLECTION(jobs_collection, dba::Collections::JOBS())
        jobs_collection.find_one_and_update(query.view(), update.view());

      } else {
        auto maybe_job = dba::helpers::get_one_by_id(dba::Collections::JOBS(), _id);
        if (!maybe_job) {
          throw deepblue_user_exception(ERR_REQUEST_ID_INVALID, _id);
        }

        auto job = maybe_job->view();

        if (bson_utils::has_key(job, "state") &&
            (job["state"].get_int32().value == mdbq::TS_CANCELLED ||
             job["state"].get_int32().value == mdbq::TS_REMOVED)) {
          epidb::Engine::instance().remove_request_data(_id, static_cast<TaskState>(job["state"].get_int32().value));
        } else {
          auto query = MakeDocument(KVP("_id", _id),
                                    KVP("version", version));

          auto update = MakeDocument(KVP("$set", MakeDocument(
                                        KVP("state", TS_FAILED),
                                        KVP("version", version + 1),
                                        KVP("failure_time", Date{finish_time}),
                                        KVP("error", bson_utils::get_string(result, "__error__")))));

          COLLECTION(jobs_collection, dba::Collections::JOBS())
          jobs_collection.find_one_and_update(query.view(), update.view());
        }
      }
      m_ptr->m_current_task = EMPTY_DOCUMENT;
    }

    void Client::reg(boost::asio::io_service &io_service, unsigned int interval)
    {
      m_ptr->m_interval = interval;
      m_ptr->m_timer.reset(new boost::asio::deadline_timer(io_service,
                           boost::posix_time::seconds(interval) +
                           boost::posix_time::millisec((int)(1000 * (interval - (int)interval)))));
      m_ptr->m_timer->async_wait(boost::bind(&ClientImpl::update_check, m_ptr.get(), this, boost::asio::placeholders::error));
    }


    void Client::handle_task(const std::string& _id, const View &o)
    {
      throw_runtime_error("MDBQC: WARNING: got a task, but no handler defined!");
    }

    Client::~Client() { }


    std::string Client::store_result(const uint8_t *ptr, size_t len)
    {
      const auto& maybe_ct = m_ptr->m_current_task;
      if (!maybe_ct) {
        throw_runtime_error("MDBQC: get a task first before you store something about it!");
      }

      auto ct = maybe_ct->view();
      std::string filename = bson_utils::get_string(ct, "_id");

      storage::store(filename, ptr, len);

      return filename;
    }

  }
}
