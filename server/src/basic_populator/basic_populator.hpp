//
//  basic_pipulator.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.05.21.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EPIDB_BASIC_POPULATOR_HPP
#define EPIDB_BASIC_POPULATOR_HPP

#include "../datatypes/user.hpp"

namespace epidb {
  namespace basic_populator {

    void columns(const datatypes::User& user);
    void indexes(const datatypes::User& user);


  } // namespace basic_populator
} // namespace epidb

#endif