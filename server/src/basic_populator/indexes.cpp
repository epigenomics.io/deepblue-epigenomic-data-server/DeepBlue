//
//  indexes.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 13.05.21.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#include <string>
#include <vector>
#include <tuple>

#include "basic_populator.hpp"

#include "../dba/collections.hpp"

#include "../datatypes/user.hpp"

#include "../extras/bson_utils.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "../log.hpp"

namespace epidb {
  namespace basic_populator {

    void indexes(const datatypes::User& user) {
        EPIDB_LOG("Start creating indexes")
        COLLECTION(text_search_collection, dba::Collections::TEXT_SEARCH())

        auto x = text_search_collection.create_index(MakeDocument(
                                                        KVP("epidb_id", 1)));

        x = text_search_collection.create_index(MakeDocument(
                                                    KVP("$**", "text"),
                                                    KVP("name", "text"),
                                                    KVP("description", "text")));

        x = text_search_collection.create_index(MakeDocument(
                                                    KVP("type", 1),
                                                    KVP("go_id", 1)));
        EPIDB_LOG("Indexes created")
    }
  }
}
