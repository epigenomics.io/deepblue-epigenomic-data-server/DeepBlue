## Users


## Tables
# Create table:
(JOBS(), ())

### Indexes
## Collection ## Key
(USERS(), ("key", 1))
(USERS(), ("key", "hashed"))

(BIOSOURCES(), ("norm_name", "hashed"))
(BIOSOURCES(), ("extra_metadata.ontology_id", "hashed"))
(BIOSOURCES(), ("$**", "text"))

(TEXT_SEARCH(), ("norm_biosource_name", "hashed"))
(TEXT_SEARCH(), ("epidb_id", "hashed"))
(TEXT_SEARCH(), ("sample_info_norm_biosource_name", "hashed"))

(BIOSOURCE_SYNONYM_NAMES(), ("norm_synonym", "hashed"))

(BIOSOURCE_SYNONYMS(), ("name", "hashed"))
(BIOSOURCE_SYNONYMS(), ("norm_name", "hashed"))
(BIOSOURCE_SYNONYMS(), ("synonym", "hashed"))

(BIOSOURCE_EMBRACING(), ("norm_biosource_name", "hashed"))
(BIOSOURCE_EMBRACING(), ("norm_biosource_embracing", "hashed"))

(TECHNIQUES(), ("norm_name", "hashed"))

(EXPERIMENTS(), ("name", "hashed"))
(EXPERIMENTS(), ("norm_name", "hashed"))
(EXPERIMENTS(), ("norm_genome", "hashed"))
(EXPERIMENTS(), ("norm_epigenetic_mark", "hashed"))
(EXPERIMENTS(), ("sample_id", "hashed"))
(EXPERIMENTS(), ("norm_technique", "hashed"))
(EXPERIMENTS(), ("norm_project", "hashed"))
(EXPERIMENTS(), ("upload_info.upload_end", -1))

(EXPERIMENTS(), ("$**", "text"))

(QUERIES(), ("query_args.norm_experiment_name", 1))
(QUERIES(), (("type", 1), ("user", 1), ("derived_from", 1)))
(QUERIES(), (("type", 1), ("user", 1), ("query_args.norm_experiment_name", 1))

(PROJECTS(), ("$**", "text"))

(SAMPLES(), ("$**", "text"))

(ANNOTATIONS(), ("$**", "text"))
(COLUMN_TYPES(), ("$**", "text"))
(EPIGENETIC_MARKS(), ("$**", "text"))
(GENES(), ("$**", "text"))
(GENOMES(), ("$**", "text"))


#### Full text search:
## c->createIndex(helpers::collection_name(Collections::TEXT_SEARCH()),
##    mongo::fromjson("{\"epidb_id\": 1}"));
##
## c->createIndex(helpers::collection_name(Collections::TEXT_SEARCH()),
##    mongo::fromjson("{\"$**\": \"text\", \"name\":\"text\", \"description\":\"text\"}"));
#### Full text
##  index_name.append("type", 1);
##  index_name.append("go_id", 1);
##  c->createIndex(helpers::collection_name(Collections::TEXT_SEARCH()), index_name.obj());


#### Genes:
##        {
##          DocumentBuilder index_name;
##          index_name.append(KVP(KeyMapper::CHROMOSOME(), 1));
##          index_name.append(KVP(KeyMapper::START(), 1));
##          index_name.append(KVP(KeyMapper::DATASET(), 1));
##          c->createIndex(helpers::collection_name(Collections::GENES()), index_name.obj());
##
##          index_name.append(KeyMapper::ATTRIBUTES() + ".gene_id", 1);
##          c->createIndex(helpers::collection_name(Collections::GENES()), index_name.obj());
##

#### Gene ontlogy:
##
##          index_name.append("go_id", "hashed");
##          c->createIndex(helpers::collection_name(Collections::GENE_ONTOLOGY()), index_name.obj());
##
##          index_name.append("go_label", "hashed");
##          c->createIndex(helpers::collection_name(Collections::GENE_ONTOLOGY()), index_name.obj());
##
##          index_name.append("go_namespace", "hashed");
##          c->createIndex(helpers::collection_name(Collections::GENE_ONTOLOGY()), index_name.obj());
##
##          index_name.append("go_annotation.go_id", 1);
##          c->createIndex(helpers::collection_name(Collections::GENES()), index_name.obj());
##
##          index_name.append("go_annotation.go_namespace", 1);
##          c->createIndex(helpers::collection_name(Collections::GENES()), index_name.obj());
##
##          index_name.append("go_annotation.go_id", 1);
##          c->createIndex(helpers::collection_name(Collections::GENES()), index_name.obj());

