//
//  columns.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.05.21.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#include <string>
#include <vector>
#include <tuple>

#include "basic_populator.hpp"

#include "../dba/column_types.hpp"
#include "../datatypes/user.hpp"

#include "../extras/utils.hpp"

#include "../log.hpp"

namespace epidb {
  namespace basic_populator {

    const std::vector<std::vector<std::string> > _columns = {
      {"SIMPLE", "CHROMOSOME", "Chromosome name column. This column should be used to store the Chromosome value in all experiments and annotations", "string"},
      {"SIMPLE", "START", "Region Start column. This column should be used to store the Region Start position all experiments and annotations", "integer"},
      {"SIMPLE", "END", "Region End column. This column should be used to store the Region End position all experiments and annotations", "integer"},
      {"SIMPLE", "VALUE", "Region Value. This column should be used to store the Wig Files Region Values", "double"},
      {"SIMPLE", "GTF_SCORE", "A floating point score. The type is a string because the value '.' is also acceptable.", "string"},
      {"SIMPLE", "FEATURE", "A floating point score. The type is a string because the value '.' is also acceptable.", "string"},
      {"SIMPLE", "SOURCE", "Name of the program that generated this feature, or the data source {database or project name},.", "string"},
      {"SIMPLE", "FRAME", "One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on.. The type is a string because the value '.' is also acceptable.", "string"},
      {"SIMPLE", "GTF_ATTRIBUTES", "A semicolon-separated list of tag-value pairs, providing additional information about each feature.", "string"},
      {"SIMPLE", "TRACKING_ID", "ID used for tracking data.", "string"},
      {"SIMPLE", "GENE_SHORT_NAME", "Gene short name.", "string"},
      {"SIMPLE", "GENE_ID", "Gene ID.", "string"},
      {"SIMPLE", "FPKM", "Fragments Per Kilobase of transcript per Million mapped reads. In RNA-Seq, the relative expression of a transcript is proportional to the number of cDNA fragments that originate from it.", "double"},
      {"SIMPLE", "FPKM_CONF_LO", "FPKM confidence interval - low value.", "double"},
      {"SIMPLE", "FPKM_CONF_HI", "FPKM confidence interval - high value.", "double"},
      {"SIMPLE", "FPKM_STATUS", "FPKM status.", "string"},
      {"SIMPLE", "TRANSCRIPT_IDS", "IDs of the transcripts.", "string"},
      {"SIMPLE", "EFFECTIVE_LENGTH", "Effective length.", "double"},
      {"SIMPLE", "EXPECTED_COUNT", "Expected count.", "double"},
      {"SIMPLE", "TPM", "Transcripts per kilobase million", "double"},
      {"SIMPLE", "POSTERIOR_MEAN_COUNT", "Posterior mean count.", "double"},
      {"SIMPLE", "POSTERIOR_STANDARD_DEVIATION_OF_COUNT", "Posterior standard deviation of count.", "double"},
      {"SIMPLE", "PME_TPM", "Posterior mean estimate - Transcripts per kilobase million.", "double"},
      {"SIMPLE", "PME_FPKM", "Posterior mean estimate - Fragments Per Kilobase of transcript per Million mapped reads.", "double"},
      {"SIMPLE", "TPM_CI_LOWER_BOUND", "TPM - Credibility interval - lower bound.", "double"},
      {"SIMPLE", "TPM_CI_UPPER_BOUND", "TPM - Credibility interval - upper bound.", "double"},
      {"SIMPLE", "FPKM_CI_LOWER_BOUND", "FPKM - Credibility interval - lower bound.", "double"},
      {"SIMPLE", "FPKM_CI_UPPER_BOUND", "FPKM - Credibility interval - upper bound.", "double"},
      {"SIMPLE", "NUM_READS", "Number of Reads for TPM or FPKM calculation.", "double"}
    };


    void columns(const datatypes::User& user) {
      for (const auto& column: _columns) {

          auto c = column[0];
          auto name = column[1];
          auto norm_name = utils::normalize_name(name);
          auto desc = column[2];
          auto norm_desc = utils::normalize_name(desc);
          auto type = column[3];

          dba::columns::create_column_type_simple(user, name, norm_name, desc, norm_desc, type);
      }
      EPIDB_LOG("Columns created.");
    }

  }
}
