//
//  tiling_regions.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Fabian Reinartz on 05.03.2014
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <set>

#include "../dba/dba.hpp"
#include "../dba/exists.hpp"
#include "../dba/genomes.hpp"
#include "../dba/queries.hpp"
#include "../datatypes/user.hpp"

#include "../extras/serialize.hpp"

#include "../engine/commands.hpp"

#include "../typedefs/bson.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace command {

    class TilingRegionsCommand: public Command {

    private:
      static CommandDescription desc_()
      {
        return CommandDescription(categories::OPERATIONS, "Generate tiling regions across the genome chromosomes. The idea is to \"bin\" genomic regions systematically in order to obtain discrete regions over which one can aggregate. Using the 'score_matrix' command, these bins (tiles) can be compared directly across experiments.");
      }

      static  Parameters parameters_()
      {
        return {
          Parameter("size", serialize::INTEGER, "tiling size"),
          parameters::Genome,
          Parameter("chromosome", serialize::STRING, "chromosome name(s)", true),
          parameters::UserKey
        };
      }

      static Parameters results_()
      {
        return {
          Parameter("id", serialize::STRING, "query id")
        };
      }

    public:
      TilingRegionsCommand() : Command("tiling_regions", parameters_(), results_(), desc_()) {}

      virtual bool run(const std::string &ip,
                       const serialize::Parameters &parameters, serialize::Parameters &result) const
      {
        std::vector<serialize::ParameterPtr> chromosomes;
        parameters[2]->children(chromosomes);

        const int64_t size = parameters[0]->as_long();
        std::string genome = parameters[1]->as_string();
        const std::string user_key = parameters[3]->as_string();

        std::string msg;
        datatypes::User user;

        if (!check_permissions(user_key, datatypes::GET_DATA, user, msg )) {
          result.add_error(msg);
          return false;
        }

        std::string norm_genome = utils::normalize_name(genome);
        if (!dba::exists::genome(norm_genome)) {
          result.add_error(Error::m(ERR_INVALID_GENOME_NAME, genome));
          return false;
        }

        DocumentBuilder args_builder;
        args_builder.append(KVP("genome", genome));
        args_builder.append(KVP("norm_genome", norm_genome));
        args_builder.append(KVP("size", size));

        std::set<std::string> chroms;
        if (!chromosomes.empty()) {
          for (auto parameter_ptr : chromosomes) {
            chroms.insert(parameter_ptr->as_string());
          }
          args_builder.append(KVP("chromosomes", [&chroms](SubArray chromosomes) {
            for (const auto &c: chroms) {
              chromosomes.append(c);
            }
          }));
        }

        std::string query_id = dba::query::store_query(user, "tiling", args_builder.extract());

        result.add_string(query_id);
        return true;
      }

    } tilingRegionsCommand;
  }
}
