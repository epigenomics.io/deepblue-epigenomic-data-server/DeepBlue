//
//  preview_regions.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 29.06.16.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "../engine/commands.hpp"

#include "../dba/dba.hpp"
#include "../dba/experiments.hpp"
#include "../dba/exists.hpp"
#include "../dba/genomes.hpp"
#include "../dba/queries.hpp"
#include "../dba/list.hpp"
#include "../dba/retrieve.hpp"

#include "../datatypes/user.hpp"

#include "../extras/utils.hpp"
#include "../extras/serialize.hpp"
#include "../extras/stringbuilder.hpp"

#include "../processing/processing.hpp"

#include "../typedefs/bson.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace command {

    class PreviewExperimentCommand: public Command {

    private:
      static CommandDescription desc_()
      {
        return CommandDescription(categories::EXPERIMENTS, " List the DeepBlue Experiments that matches the search criteria defined by this command parameters.");
      }

      static Parameters parameters_()
      {
        return {
          Parameter("experiment_name", serialize::STRING, "name(s) of selected experiment(s)"),
          parameters::UserKey
        };
      }

      static Parameters results_()
      {
        return {
          Parameter("experiment", serialize::STRING, "experiment's regions")
        };
      }

    public:
      PreviewExperimentCommand() : Command("preview_experiment", parameters_(), results_(), desc_()) {}

      virtual bool run(const std::string &ip,
                       const serialize::Parameters &parameters, serialize::Parameters &result) const
      {
        const std::string experiment_name_or_id = parameters[0]->as_string();
        const std::string user_key = parameters[1]->as_string();

        std::string msg;
        datatypes::User user;

        if (!check_permissions(user_key, datatypes::GET_DATA, user, msg )) {
          result.add_error(msg);
          return false;
        }

        auto name = dba::experiments::get_experiment_name(experiment_name_or_id);
        auto experiment_name = name.first;
        auto experiment_norm_name = name.second;

        if (!dba::exists::experiment(experiment_norm_name)) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, experiment_name);
        }

        auto user_projects_names = user.projects();

        auto maybe_genome = dba::experiments::get_genome(experiment_norm_name);
        if (!maybe_genome) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, experiment_name);
        }
        auto norm_genome = utils::normalize_name(*maybe_genome);

        std::set<std::string> chroms = dba::genomes::get_chromosomes(norm_genome);

        DocumentBuilder args_builder;
        args_builder.append(KVP("experiment_name", experiment_name));
        args_builder.append(KVP("norm_experiment_name", experiment_norm_name));
        args_builder.append(KVP("project", utils::build_array(user_projects_names)));
        args_builder.append(KVP("norm_project", utils::build_normalized_array(user_projects_names)));

        auto args = args_builder.extract();
        auto regions_query = dba::query::build_experiment_query(user, args.view());

        std::vector<std::string> chroms_vector;
        std::copy(chroms.begin(), chroms.end(), std::back_inserter(chroms_vector));

        ChromosomeRegionsList chromosomeRegionsList;
        size_t chrom_pos = 0;
        do {
          Regions regions = dba::retrieve::get_regions_preview(norm_genome, chroms_vector[chrom_pos], regions_query.view());
          if (!regions.empty()) {
            Regions preview_regions;
            size_t count = std::min(regions.size(), static_cast<size_t>(5));
            preview_regions.insert(preview_regions.end(), std::make_move_iterator(regions.begin()), std::make_move_iterator(regions.begin() + count));
            chromosomeRegionsList.emplace_back(chroms_vector[chrom_pos], std::move(preview_regions));
          }
        } while(chromosomeRegionsList.empty() && ++chrom_pos < chroms_vector.size());

        auto maybe_experiment = dba::experiments::by_name(experiment_norm_name);
        if (!maybe_experiment) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT, experiment_norm_name);
        }

        processing::StatusPtr status = processing::build_dummy_status();
        const auto& experiment_view = maybe_experiment->view();
        std::string experiment_format{experiment_view["format"].get_utf8().value};

        const StringBuilder& sb = epidb::processing::format_regions(experiment_format, chromosomeRegionsList, status);

        std::replace( experiment_format.begin(), experiment_format.end(), ',', '\t');

        std::string output = experiment_format + "\n" + sb.to_string();

        result.add_string(output);
        return true;
      }
    } previewExperimentCommand;
  }
}
