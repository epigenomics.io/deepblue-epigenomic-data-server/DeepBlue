//
//  insert.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 29.05.13.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "../datatypes/user.hpp"

#include "../dba/dba.hpp"
#include "../dba/exists.hpp"
#include "../dba/insert.hpp"
#include "../dba/list.hpp"

#include "../engine/commands.hpp"

#include "../extras/utils.hpp"
#include "../extras/serialize.hpp"

#include "../parser/parser_factory.hpp"
#include "../parser/bedgraph_parser.hpp"
#include "../parser/wig_parser.hpp"
#include "../parser/wig.hpp"

#include "../errors.hpp"

namespace epidb {
  namespace command {

    class AddExperimentCommand: public Command {

    private:
      static CommandDescription desc_()
      {
        return CommandDescription(categories::EXPERIMENTS, "Add an Experiment in DeepBlue. An Experiment describes the characteristics of a specific Epigenetic Mark with respect to a single sample. The technology used and project must be informed as well. Extra metadata can be specified in addition to the mandatory meta information.");
      }

      static Parameters parameters_()
      {
        return {
          Parameter("name", serialize::STRING, "experiment name"),
          parameters::Genome,
          Parameter("epigenetic_mark", serialize::STRING, "epigenetic mark of the experiment"),
          Parameter("sample", serialize::STRING, "id of the used sample"),
          Parameter("technique", serialize::STRING, "technique used by this experiment"),
          Parameter("project", serialize::STRING, "the project name"),
          Parameter("description", serialize::STRING, "description of the experiment"),
          Parameter("data", serialize::DATASTRING, "the BED formated data"),
          Parameter("format", serialize::STRING, "format of the provided data"),
          parameters::AdditionalExtraMetadata,
          parameters::UserKey
        };
      }

      static Parameters results_()
      {
        return {
          Parameter("id", serialize::STRING, "id of the newly inserted experiment")
        };
      }

    public:
      AddExperimentCommand() : Command("add_experiment", parameters_(), results_(), desc_()) {}

      // TODO: Check user
      virtual bool run(const std::string &ip,
                       const serialize::Parameters &parameters, serialize::Parameters &result) const
      {
        const std::string name = parameters[0]->as_string();
        const std::string genome = parameters[1]->as_string();
        const std::string epigenetic_mark = parameters[2]->as_string();
        const std::string sample = parameters[3]->as_string();
        const std::string technique = parameters[4]->as_string();
        const std::string project = parameters[5]->as_string();
        const std::string description = parameters[6]->as_string();
        const std::string data = parameters[7]->as_string();
        const std::string format = parameters[8]->as_string();
        const std::string user_key = parameters[10]->as_string();

        std::string msg;
        datatypes::User user;

        if (!check_permissions(user_key, datatypes::INCLUDE_EXPERIMENTS, user, msg )) {
          result.add_error(msg);
          return false;
        }

        if (name.empty()) {
          result.add_error("Experiment name cannot be empty.");
          return false;
        }

        datatypes::Metadata extra_metadata;
        if (!read_metadata(parameters[9], extra_metadata, msg)) {
          result.add_error(msg);
          return false;
        }

        std::string norm_name = utils::normalize_name(name);
        std::string norm_genome = utils::normalize_name(genome);
        std::string norm_description = utils::normalize_name(description);
        std::string norm_epigenetic_mark = utils::normalize_epigenetic_mark(epigenetic_mark);
        std::string norm_project = utils::normalize_name(project);
        std::string norm_technique = utils::normalize_name(technique);

        if (description.size() > 2048) {
          result.add_error("Description is too long.");
          return false;
        }

        if (dba::exists::experiment(norm_name)) {
          std::string s = Error::m(ERR_DUPLICATED_EXPERIMENT_NAME, name);
          result.add_error(s);
          return false;
        }

        if (!dba::exists::genome(norm_genome)) {
          result.add_error("Invalid genome '" + genome + "'");
          return false;
        }

        if (!dba::exists::epigenetic_mark(norm_epigenetic_mark)) {
          std::vector<utils::IdName> names = dba::list::similar_epigenetic_marks(epigenetic_mark);
          std::stringstream ss;
          ss << "Invalid epigenetic mark: ";
          ss << epigenetic_mark;
          ss << ".";
          if (!names.empty()) {
            ss << " It is suggested the following names: ";
            ss << utils::vector_to_string(names);
          }
          result.add_error(ss.str());
          return false;
        }

        if (!dba::exists::technique(norm_technique)) {
          std::vector<utils::IdName> names = dba::list::similar_techniques(technique);
          std::stringstream ss;
          ss << "Invalid technique name: ";
          ss << technique;
          ss << ".";
          if (!names.empty()) {
            ss << " The following names are suggested: ";
            ss << utils::vector_to_string(names);
          }
          result.add_error(ss.str());
          return false;
        }

        // TODO: check the sample

        if (!dba::exists::project(norm_project)) {
          std::vector<utils::IdName> names = dba::list::similar_projects(project);
          std::stringstream ss;
          ss << "Invalid project name. ";
          ss << project;
          ss << ".";
          if (!names.empty()) {
            ss << " The following names are suggested: ";
            ss << utils::vector_to_string(names);
          }
          result.add_error(ss.str());
          return false;
        }

        std::unique_ptr<std::istream> _input;
        if (extra_metadata.find("__local_file__") != extra_metadata.end()) {
          std::string &file_name = extra_metadata["__local_file__"];
          _input = std::unique_ptr<std::istream>(new std::ifstream(file_name.c_str()));
          if (!_input->good()) {
            result.add_error("File " + file_name + " does not exist or it is not accessible.");
            return false;
          }
        } else {
          _input = std::unique_ptr<std::istream>(new std::stringstream(data));
        }

        if (format == "wig" || format == "bedgraph") {
          parser::WigPtr wig;
          if (format == "wig") {
            parser::WIGParser wig_parser(std::move(_input));
            if (!wig_parser.get(wig, msg)) {
              result.add_error(msg);
              return false;
            }
          } else {
            parser::BedGraphParser bedgraph_parser(std::move(_input));
            if (!bedgraph_parser.get(wig, msg)) {
              result.add_error(msg);
              return false;
            }
          }

          std::string id = dba::insert_experiment(user, name, norm_name, genome, norm_genome, epigenetic_mark, norm_epigenetic_mark, sample,
                                            technique, norm_technique, project, norm_project, description, norm_description,
                                            extra_metadata, ip, wig);
          result.add_string(id);
        } else {

          parser::ChromosomeRegionsMap map_regions;

          parser::FileFormat fileFormat = parser::FileFormatBuilder::build(format);

          parser::Parser parser(std::move(_input), fileFormat);
          parser.validate_format();

          while (!parser.eof()) {
            auto bed_line = parser.parse_line();
            // Ignore Empty Line Error
            if (!bed_line) {
              continue;
            }
            map_regions.insert(std::move(*bed_line));
          }

          map_regions.finish();

          std::string id = dba::insert_experiment(user, name, norm_name, genome, norm_genome, epigenetic_mark, norm_epigenetic_mark, sample,
                                            technique, norm_technique, project, norm_project, description, norm_description,
                                            extra_metadata, ip, map_regions, fileFormat);
          result.add_string(id);
        }
        return true;
      }

    } addExperimentCommand;

  }
}
