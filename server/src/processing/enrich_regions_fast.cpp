//
//  signature.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 21.10.17.
//  Copyright (c) 2021 Max Planck Institute for Informatics. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <bitset>
#include <future>
#include <string>
#include <sstream>
#include <thread>

#include "../datatypes/user.hpp"

#include "../dba/collections.hpp"
#include "../dba/experiments.hpp"
#include "../dba/genomes.hpp"
#include "../dba/helpers.hpp"
#include "../dba/queries.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/math.hpp"
#include "../extras/utils.hpp"

#include "../threading/semaphore.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include <boost/serialization/bitset.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <bsoncxx/exception/exception.hpp>

#include "enrichment_result.hpp"

#include "../log.hpp"
#include "../exceptions.hpp"

#define BITMAP_SIZE (1024 * 1024)

namespace epidb {

  namespace processing {

    void store(const std::string &id, const std::bitset<BITMAP_SIZE>& bitmap, processing::StatusPtr status);

    const bool exists(const std::string &id, processing::StatusPtr status);

    const std::bitset<BITMAP_SIZE> load(const std::string &id, processing::StatusPtr status);

    ProcessOverlapResult compare_to(const datatypes::User& user,
                                    const std::bitset<BITMAP_SIZE>& query_bitmap,
                                    const utils::IdName& exp,
                                    const ChromosomeRegionsList& bitmap_regions,
                                    processing::StatusPtr status, threading::SemaphorePtr sem);

    const ChromosomeRegionsList get_bitmap_regions(const datatypes::User& user, const std::string &query_id,
                            processing::StatusPtr status);

    const std::bitset<BITMAP_SIZE> process_bitmap_query(const datatypes::User& user, const std::string &query_id,
                                    const ChromosomeRegionsList& bitmap_regions, processing::StatusPtr status);

    const std::bitset<BITMAP_SIZE> process_bitmap_experiment(const datatypes::User& user, const std::string &id,
                                   const ChromosomeRegionsList& bitmap_regions, processing::StatusPtr status);


    const Document enrich_regions_fast(const datatypes::User& user, const std::string& query_id, const std::vector<utils::IdName>& names,
                             processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST, {});

      ChromosomeRegionsList bitmap_regions = get_bitmap_regions(user, query_id, status);

      std::bitset<BITMAP_SIZE> query_bitmap;
      if (exists(query_id, status)) {
        query_bitmap = load(query_id, status);
      } else {
        query_bitmap = process_bitmap_query(user, query_id, bitmap_regions, status);
        store(query_id, query_bitmap, status);
      }
      
      std::vector<std::future<ProcessOverlapResult > > threads;

      runningOp.set_total_steps(names.size());

      threading::SemaphorePtr sem = threading::build_semaphore(16);
      for (const auto& exp: names) {
        utils::IdNameCount result;

        sem->down();
        auto t = std::async(std::launch::async, &compare_to,
                            std::ref(user),
                            std::ref(query_bitmap), std::ref(exp),
                            std::ref(bitmap_regions),
                            status, sem);

        threads.emplace_back(std::move(t));
      }

      std::vector<std::tuple<std::string, size_t>> datasets_support;
      std::vector<std::tuple<std::string, double>> datasets_log_score;
      std::vector<std::tuple<std::string, double>> datasets_odds_score;

      std::vector<ProcessOverlapResult> results;

      for (size_t i = 0; i < threads.size(); ++i) {
        threads[i].wait();
        auto result = threads[i].get();
        runningOp.increment_step();
        std::string dataset = std::get<0>(result);

        // --------------------- [0] dataseset, [1] biosource, [2] epi mark, [3] description, [4] size, [5] database_name, [6] ///negative_natural_log, [5] odds_score, [6] a, [7] b, [8] c, [9] d)
        datasets_log_score.push_back(std::make_tuple(dataset, std::get<6>(result)));
        datasets_odds_score.push_back(std::make_tuple(dataset, std::get<7>(result)));
        datasets_support.push_back(std::make_tuple(dataset, std::get<8>(result)));

        results.emplace_back(std::move(result));
      }

      std::vector<std::shared_ptr<ExperimentResult>> experiment_results =
            sort_results(results, datasets_support, datasets_log_score, datasets_odds_score);

      DocumentBuilder bob;

      ArrayBuilder ab;
      bool has_data = false;
      for (const auto& er: experiment_results) {
        ab.append(er->toDocument());
      }

      bob.append(KVP("results", ab.extract()));

      return bob.extract();
    }

    ProcessOverlapResult compare_to(const datatypes::User& user,
                                    const std::bitset<BITMAP_SIZE>& query_bitmap,
                                    const utils::IdName& exp,
                                    const ChromosomeRegionsList& bitmap_regions,
                                    processing::StatusPtr status, threading::SemaphorePtr sem)
    {
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_COMPARE_TO, {});
      IS_PROCESSING_CANCELLED(status)

      std::bitset<BITMAP_SIZE> exp_bitmap;
      
      if (exists(exp.id, status)) {
         exp_bitmap = load(exp.id, status);
      } else {
        exp_bitmap = process_bitmap_experiment(user, exp.id, bitmap_regions, status);
        store(exp.id, exp_bitmap, status);
      }

      auto maybe_experiment = dba::experiments::by_name(exp.name);
      if (!maybe_experiment) {
        throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_NAME, exp.name);
      }

      auto obj_view = maybe_experiment->view();

      std::string biosource = StringViewToString(obj_view["sample_info"]["biosource_name"].get_utf8().value);
      std::string epigenetic_mark = StringViewToString(obj_view["epigenetic_mark"].get_utf8().value);
      std::string description = StringViewToString(obj_view["description"].get_utf8().value);
      size_t count = (query_bitmap & exp_bitmap).count();

      double a = count;
      double b = exp_bitmap.count() - a;

      if (b < 0) {
        throw deepblue_user_exception(ERR_LOLA_INVALID_NEGATIVE_VALUE_B);
      }

      double c = query_bitmap.count()  - a;
      double d = BITMAP_SIZE - a - b - c;

      double p_value = math::fisher_test(a, b, c, d);

      double negative_natural_log = abs(log10(p_value));

      double a_b = a/b;
      double c_d = c/d;
      double odds_score;
      // Handle when 'b' and 'd' are 0.
      if (a_b == c_d) {
        odds_score = 1;
      } else {
        odds_score = a_b/c_d;
      }

      return std::make_tuple(exp.name, biosource, epigenetic_mark, "", BITMAP_SIZE, "", negative_natural_log, odds_score, a, b, c, d);
    }

    void store(const std::string &id, const std::bitset<BITMAP_SIZE>& bitmap, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_STORE_BITMAP, {});

      std::stringstream stream;
      boost::archive::binary_oarchive ar(stream, boost::archive::no_header);
      ar & bitmap;

      const uint32_t size = stream.str().size();
      const char* data = stream.str().data();

      DocumentBuilder bob;
      bob.append(KVP("_id", id));
      bob.append(KVP("data",
                  Binary{bsoncxx::binary_sub_type::k_binary, size, reinterpret_cast<const uint8_t*>(data)}));

      COLLECTION(signatures_collection, dba::Collections::SIGNATURES())
      try {
        signatures_collection.insert_one(bob.extract());
      } catch (const bsoncxx::exception& e) {
        // TODO: http://mongocxx.org/api/current/namespacemongocxx.html#a36960f75ff06296d1ecd18b9393174f1acbbd1f0cb429c23d59a99d76eebfc245
        EPIDB_LOG_TRACE("Error while inserting the bitmap for the ID:" + id + ". It was already inserted.");
      }
      
    }

    const bool exists(const std::string &id, processing::StatusPtr status) {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_EXIST_BITMAP, {});
      
      return dba::helpers::check_exist(dba::Collections::SIGNATURES(), bson_utils::id_doc(id));
    }

    const std::bitset<BITMAP_SIZE> load(const std::string &id, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_LOAD_BITMAP, {});

      MaybeDocument maybe_result = dba::helpers::get_one_by_id(dba::Collections::SIGNATURES(), id);
      if (!maybe_result) {
        throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, id);
      }

      auto result = maybe_result->view();
      
      const auto binary = result["data"].get_binary();
      std::istringstream ss(std::string(binary.bytes, binary.bytes+binary.size));

      boost::archive::binary_iarchive ar(ss, boost::archive::no_header);
      std::bitset<BITMAP_SIZE> bitset;
      ar & bitset;

      return bitset;
    }


    void build_bitmap(const Regions& ranges, const Regions& data, 
                      std::bitset<BITMAP_SIZE>& bitmap, size_t &pos, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_BUILD_BITMAP, {});

      auto it_ranges = ranges.begin();
      auto it_data_begin = data.begin();

      while (it_ranges != ranges.end()) {
        bool overlap = false;
        while ( it_data_begin != data.end()
                && (*it_data_begin)->end() < (*it_ranges)->start() )  {
          it_data_begin++;
        }

        auto it_data = it_data_begin;
        while (it_data != data.end() &&
               (*it_ranges)->end() >= (*it_data)->start() ) {

          if (((*it_ranges)->start() <= (*it_data)->end()) && ((*it_ranges)->end() >= (*it_data)->start())) {
            overlap = true;
          }
          it_data++;
        }
        if (overlap) {
          if (pos >= BITMAP_SIZE) {
            throw deepblue_user_exception(ERR_BITMAP_INVALID_POS, pos);
          }
          bitmap.set(pos);
        }

        pos++;
        it_ranges++;
      }
    }

    const ChromosomeRegionsList get_bitmap_regions(const datatypes::User& user, const std::string &query_id,                            
                            processing::StatusPtr status)
    {
      auto&& runningOp =   status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_GET_BITMAP_REGIONS, {});
      IS_PROCESSING_CANCELLED(status)

      std::string norm_genome;

      ;
      const std::string em_field_name = "norm_genome";
      auto vector_genome = dba::query::get_main_experiment_data(user, query_id, em_field_name, status);

      if (vector_genome.empty()) {
        throw deepblue_user_exception(ERR_IMPOSSIBLE_QUERY_GENOME, query_id);
      }

      norm_genome = vector_genome[0];
      auto chromosomes = dba::genomes::get_chromosomes_info(norm_genome);

      size_t total_genome_size = 0;
      for (const auto &chromosome : chromosomes) {
        total_genome_size += chromosome.size;
      }

      size_t d = total_genome_size / BITMAP_SIZE;
      size_t tiling_size = d + chromosomes.size() + 1;

      auto tiling_query = MakeDocument(
                            KVP("args", MakeDocument(
                              KVP("norm_genome", norm_genome),
                              KVP("size", static_cast<int32_t>(tiling_size))
                            )));
      return dba::query::retrieve_tiling_query(tiling_query, status);
    }

    const std::bitset<BITMAP_SIZE> process_bitmap_query(const datatypes::User& user, const std::string &query_id,
                              const ChromosomeRegionsList& bitmap_regions, processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_BITMAP_QUERY, {});

      std::bitset<BITMAP_SIZE> bitmap;
      size_t pos = 0;
      ChromosomeRegionsList data_regions = dba::query::retrieve_query(user, query_id, status, true);      

      for (auto &chromosome : bitmap_regions) {
        bool found = false;
        for (auto &datum: data_regions) {
          if (chromosome.first == datum.first) {
            build_bitmap(chromosome.second, datum.second, bitmap, pos, status);
            for (const auto& r : datum.second) {
              status->subtract_size(r->size());
            }
            found = true;
          }
        }
        if (!found) {
          Regions empty_data;
          build_bitmap(chromosome.second, empty_data, bitmap, pos, status);
        }
      }
      return bitmap;
    }

    const std::bitset<BITMAP_SIZE> process_bitmap_experiment(const datatypes::User& user, const std::string &id,
                                   const ChromosomeRegionsList& bitmap_regions,
                                   processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(processing::PROCESS_ENRICH_REGIONS_FAST_BITMAP_EXPERIMENT, {});

      MaybeDocument maybe_experiment = dba::experiments::by_id(id);
      if (!maybe_experiment) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_ID, id);
      }

      auto experiment_view = maybe_experiment->view();

      const std::string& norm_exp_name = bson_utils::get_string(experiment_view, "norm_name");
      const std::string& norm_genome = bson_utils::get_string(experiment_view, "norm_genome");

      std::bitset<BITMAP_SIZE> bitmap;
      size_t pos = 0;
      for (auto &chromosome : bitmap_regions) {
        auto regions_query = dba::query::build_experiment_query(-1, -1, norm_exp_name);

        auto data = dba::retrieve::get_regions(norm_genome, chromosome.first, regions_query.view(), false, status);
        
        build_bitmap(chromosome.second, data, bitmap, pos, status);

        for (const auto& r : data) {
          status->subtract_size(r->size());
        }
      }

      return true;
    }
  };
};
