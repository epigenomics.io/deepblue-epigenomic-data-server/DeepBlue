//
//  processing.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 02.06.15.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <atomic>
#include <chrono>
#include <memory>
#include <string>

#include "../dba/collections.hpp"
#include "../dba/helpers.hpp"

#include "../engine/engine.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/utils.hpp"

#include "../mdbq/common.hpp"

#include "../typedefs/bson.hpp"
#include "../typedefs/database.hpp"

#include "processing.hpp"
#include "running_cache.hpp"

#include "../errors.hpp"
#include "../macros.hpp"

namespace epidb {

  namespace processing {

    const std::map<OP, std::string> create_OP_names_map()
    {
      std::map<OP, std::string> m;

      m[PROCESS_QUERY]                                  = "" STR(PROCESS_QUERY);
      m[GET_EXPERIMENT_BY_QUERY]                        = "" STR(GET_EXPERIMENT_BY_QUERY);
      m[COUNT_REGIONS]                                  = "" STR(COUNT_REGIONS);
      m[RETRIEVE_EXPERIMENT_SELECT_QUERY]               = "" STR(RETRIEVE_EXPERIMENT_SELECT_QUERY);
      m[RETRIEVE_ANNOTATION_SELECT_QUERY]               = "" STR(RETRIEVE_ANNOTATION_SELECT_QUERY);
      m[RETRIEVE_INTERSECTION_QUERY]                    = "" STR(RETRIEVE_INTERSECTION_QUERY);
      m[RETRIEVE_OVERLAP_QUERY]                         = "" STR(RETRIEVE_OVERLAP_QUERY);
      m[RETRIEVE_EXTEND_QUERY]                          = "" STR(RETRIEVE_EXTEND_QUERY);
      m[RETRIEVE_MERGE_QUERY]                           = "" STR(RETRIEVE_MERGE_QUERY);
      m[RETRIEVE_FIND_MOTIF_QUERY]                      = "" STR(RETRIEVE_FIND_MOTIF_QUERY);
      m[RETRIEVE_FILTER_QUERY]                          = "" STR(RETRIEVE_FILTER_QUERY);
      m[RETRIEVE_FILTER_MOTIF_QUERY]                    = "" STR(RETRIEVE_FILTER_MOTIF_QUERY);
      m[RETRIEVE_TILING_QUERY]                          = "" STR(RETRIEVE_TILING_QUERY);
      m[RETRIEVE_GENES_DATA]                            = "" STR(RETRIEVE_GENES_DATA);
      m[PROCESS_AGGREGATE]                              = "" STR(PROCESS_AGGREGATE);
      m[PROCESS_DISTINCT]                               = "" STR(PROCESS_DISTINCT);
      m[PROCESS_BINNING]                                = "" STR(PROCESS_BINNING);
      m[PROCESS_ENRICH_REGIONS_OVERLAP]                 = "" STR(PROCESS_ENRICH_REGIONS_OVERLAP);
      m[PROCESS_CALCULATE_GO_ENRICHMENT]                = "" STR(PROCESS_CALCULATE_GO_ENRICHMENT);
      m[PROCESS_COUNT]                                  = "" STR(PROCESS_COUNT);
      m[PROCESS_COVERAGE]                               = "" STR(PROCESS_COVERAGE);
      m[PROCESS_GET_EXPERIMENTS_BY_QUERY]               = "" STR(PROCESS_GET_EXPERIMENTS_BY_QUERY);
      m[PROCESS_GET_REGIONS]                            = "" STR(PROCESS_GET_REGIONS);
      m[PROCESS_SCORE_MATRIX]                           = "" STR(PROCESS_SCORE_MATRIX);
      m[PROCESS_SCORE_MATRIX_THREAD]                    = "" STR(PROCESS_SCORE_MATRIX_THREAD);
      m[RETRIEVE_QUERY_REGION_SET]                      = "" STR(RETRIEVE_QUERY_REGION_SET);
      m[RETRIEVE_FLANK_QUERY]                           = "" STR(RETRIEVE_FLANK_QUERY);
      m[RETRIEVE_EXPRESSIONS_DATA]                      = "" STR(RETRIEVE_EXPRESSIONS_DATA);
      m[FORMAT_OUTPUT]                                  = "" STR(FORMAT_OUTPUT);
      // Enrichment fast
      m[PROCESS_ENRICH_REGIONS_FAST]                    = "" STR(PROCESS_ENRICH_REGIONS_FAST);
      m[PROCESS_ENRICH_REGIONS_FAST_COMPARE_TO]         = "" STR(PROCESS_ENRICH_REGIONS_FAST_COMPARE_TO);
      m[PROCESS_ENRICH_REGIONS_FAST_STORE_BITMAP]       = "" STR(PROCESS_ENRICH_REGIONS_FAST_STORE_BITMAP);
      m[PROCESS_ENRICH_REGIONS_FAST_EXIST_BITMAP]       = "" STR(PROCESS_ENRICH_REGIONS_FAST_EXIST_BITMAP);
      m[PROCESS_ENRICH_REGIONS_FAST_LOAD_BITMAP]        = "" STR(PROCESS_ENRICH_REGIONS_FAST_LOAD_BITMAP);
      m[PROCESS_ENRICH_REGIONS_FAST_BUILD_BITMAP]       = "" STR(PROCESS_ENRICH_REGIONS_FAST_BUILD_BITMAP);
      m[PROCESS_ENRICH_REGIONS_FAST_GET_BITMAP_REGIONS] = "" STR(PROCESS_ENRICH_REGIONS_FAST_GET_BITMAP_REGIONS);
      m[PROCESS_ENRICH_REGIONS_FAST_BITMAP_QUERY]       = "" STR(PROCESS_ENRICH_REGIONS_FAST_BITMAP_QUERY);
      m[PROCESS_ENRICH_REGIONS_FAST_BITMAP_EXPERIMENT]  = "" STR(PROCESS_ENRICH_REGIONS_FAST_BITMAP_EXPERIMENT);

      return m;
    }

    std::string& op_name(const OP& op)
    {
      return OP_names[op];
    }

    RunningOp::RunningOp(const std::string& processing_id, const OP& op, const View& param):
      _id(ObjectID{}),
      _processing_id(processing_id),
      _op(op),
      _start_time(std::chrono::system_clock::now()),
      _steps(0),
      _actual_step(0)
    {

      MaybeDocument insert;
      if (param.empty()) {
        insert = MakeDocument(
                      KVP("_id", _id),
                      KVP("processing_id", _processing_id),
                      KVP("op", _op),
                      KVP("op_name", op_name(op)),
                      KVP("s", Date(_start_time)));
      } else {
        insert = MakeDocument(
                      KVP("_id", _id),
                      KVP("processing_id", _processing_id),
                      KVP("op", _op),
                      KVP("op_name", op_name(op)),
                      KVP("params", param),
                      KVP("s", Date(_start_time)));
      }
      COLLECTION(processing_ops_collection, dba::Collections::PROCESSING_OPS())
      processing_ops_collection.insert_one(insert->view());
    }


    RunningOp::~RunningOp()
    {
      auto now = std::chrono::system_clock::now();
      auto total = now - _start_time;
      auto query = bson_utils::id_doc(_processing_id);
      auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                              KVP("e", Date(now)),
                              KVP("t", static_cast<int64_t>(total.count())))));

      COLLECTION(processing_ops_collection, dba::Collections::PROCESSING_OPS())
      processing_ops_collection.update_one(query.view(), update_value.view());
    }

    void RunningOp::set_total_steps(size_t steps)
    {
      _steps = steps;

      auto query = bson_utils::id_doc(_processing_id);
      auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                                KVP("total_steps", static_cast<int64_t>(_steps)))));

      COLLECTION(processing_ops_collection, dba::Collections::PROCESSING_OPS())
      processing_ops_collection.update_one(query.view(), update_value.view());
    }

    void RunningOp::increment_step()
    {
      ++_actual_step;

      auto query = MakeDocument(KVP("_id",_processing_id));
      auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                                KVP("completed_steps", static_cast<int64_t>(_actual_step)))));

      COLLECTION(processing_ops_collection, dba::Collections::PROCESSING_OPS())
      processing_ops_collection.update_one(query.view(), update_value.view());
    }


    Status::Status(const std::string &request_id, const long long maximum_memory) :
      _request_id(request_id),
      _maximum_memory(maximum_memory),
      _canceled(false),
      _total_regions(0),
      _total_size(0),
      _total_stored_data(0),
      _total_stored_data_compressed(0),
      _last_update(std::chrono::duration_cast< std::chrono::seconds >( std::chrono::system_clock::now().time_since_epoch())),
      _update_time_out(1),
      _running_cache(std::make_unique<RunningCache>())
    {
      if (_request_id != DUMMY_REQUEST) {
        std::string msg;
        int id = dba::helpers::get_incremente_counter_and_notify("processing");
        _processing_id = "pc" + utils::integer_to_string(id);
        auto b = MakeDocument(KVP("_id", _processing_id),
                              KVP("request_id", _request_id));

        COLLECTION(processing_collection, dba::Collections::PROCESSING())
        processing_collection.insert_one(b.view());
      }
    }

    Status::~Status()
    {
      if (_request_id != DUMMY_REQUEST) {
        auto query = MakeDocument(KVP("_id",_processing_id));
        auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                                KVP("total_regions", static_cast<int64_t>(_total_regions.load())),
                                KVP("total_size", static_cast<int64_t>(_total_size.load())))));

        COLLECTION(processing_collection, dba::Collections::PROCESSING())
        processing_collection.update_one(query.view(), update_value.view());
      }
    }

    const std::string Status::request_id()
    {
      return this->_request_id;
    }

    const std::string Status::processing_id()    {
      return this->_processing_id;
    }

    RunningOp Status::start_operation(OP op, const View& params)
    {
      return std::move(RunningOp(_processing_id, op, params));
    }

    void Status::update_values_in_db()
    {
      auto current_second = std::chrono::duration_cast< std::chrono::seconds >(std::chrono::system_clock::now().time_since_epoch());
      if (current_second - _last_update > _update_time_out) {
        _last_update = current_second;

        auto query = MakeDocument(KVP("_id",_processing_id));
        auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                                KVP("total_regions", static_cast<int64_t>(_total_regions.load())),
                                KVP("total_size", static_cast<int64_t>(_total_size.load())))));

        COLLECTION(processing_collection, dba::Collections::PROCESSING())
        processing_collection.update_one(query.view(), update_value.view());
      }
    }

    void Status::sum_regions(const long long qtd)
    {
      _total_regions += qtd;
      update_values_in_db();
    }

    void Status::subtract_regions(const long long qtd)
    {
      if (_total_regions == 0) {
        return;
      }

      size_t _total_regions_size_t = _total_regions;
      size_t new_total = _total_regions_size_t - qtd;

      if (new_total > _total_regions_size_t) {
        _total_regions = 0;
      } else {
        _total_regions = new_total;
      }
      update_values_in_db();
    }

    long long Status::sum_size(const long long size)
    {
      _total_size += size;
      update_values_in_db();

      return _total_size;
    }

    bool Status::is_allowed_size(size_t output_size)
    {
      if (_request_id == DUMMY_REQUEST) {
        return true;
      }
      return (output_size <= static_cast<size_t>(_maximum_memory));
    }


    bool Status::sum_and_check_size(size_t to_sum)
    {
      sum_size(to_sum);
      return is_allowed_size(_total_size);
    }

    long long Status::subtract_size(const long long size)
    {
      if (_total_size == 0) {
        return _total_size;
      }

      size_t _total_size_size_t = _total_size;
      size_t new_size = _total_size_size_t - size;

      if (new_size > _total_size_size_t) {
        _total_size = 0;
      } else {
        _total_size = new_size;
      }
      update_values_in_db();

      return _total_size;
    }

    void Status::set_total_stored_data(const long long size)
    {
      _total_stored_data = size;

      auto query = MakeDocument(KVP("_id",_processing_id));
      auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                              KVP("total_stored_data", static_cast<int64_t>(_total_stored_data.load())))));

      COLLECTION(processing_collection, dba::Collections::PROCESSING())
      processing_collection.update_one(query.view(), update_value.view());
    }

    void Status::set_total_stored_data_compressed(const long long size)
    {
      _total_stored_data_compressed = size;

      auto query = MakeDocument(KVP("_id",_processing_id));
      auto update_value = MakeDocument(
                            KVP("$set", MakeDocument(
                              KVP("total_stored_data_compressed", static_cast<int64_t>(_total_stored_data_compressed.load())))));

      COLLECTION(processing_collection, dba::Collections::PROCESSING())
      processing_collection.update_one(query.view(), update_value.view());
    }

    long long Status::total_regions()
    {
      return _total_regions;
    }

    long long Status::total_size()
    {
      return _total_size;
    }

    long long Status::maximum_size()
    {
      return _maximum_memory;
    }

    boost::posix_time::ptime last_checked;
    const bool Status::is_canceled()
    {
      if (_request_id == DUMMY_REQUEST) {
        return false;
      }

      auto current_second = std::chrono::duration_cast< std::chrono::seconds >(std::chrono::system_clock::now().time_since_epoch());
      if (current_second - _last_update > _update_time_out) {
        _last_update = current_second;

        auto maybe_result = dba::helpers::get_one_by_id(dba::Collections::JOBS(), _request_id);
        if (!maybe_result) { // If request was not found, let's assume that it is canceled.
          return true;
        }
        auto result = maybe_result->view();
        return (result["state"].get_int32().value == mdbq::TS_CANCELLED);
      }

      return false;
    }

    std::unique_ptr<RunningCache>& Status::running_cache()
    {
      return _running_cache;
    }

    typedef std::shared_ptr<Status> StatusPtr;

    StatusPtr build_status(const std::string& _id, const long long maximum_memory)
    {
      return std::make_shared<Status>(_id, maximum_memory);
    }

    StatusPtr build_dummy_status()
    {
      return std::make_shared<Status>(DUMMY_REQUEST, 0);
    }

    std::string DUMMY_REQUEST = "dummy";
    std::map<OP, std::string> OP_names = create_OP_names_map();
  }

}
