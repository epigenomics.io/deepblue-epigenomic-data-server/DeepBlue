//
//  binning.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 29.12.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <iterator>
#include <string>

#include "../cache/column_dataset_cache.hpp"

#include "../dba/queries.hpp"

#include "processing.hpp"

namespace epidb {
  namespace processing {
    const Document binning(const datatypes::User& user,
                    const std::string& query_id,
                    const std::string& column_name, const int bars,
                    processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(PROCESS_BINNING, {});

      if (bars <= 0) {
        throw deepblue_user_exception(ERR_BINNING_NO_BARS);
      }

      if (bars >= 65536) {
        throw deepblue_user_exception(ERR_BINNING_TOO_MANY_BARS);
      }

      ChromosomeRegionsList chromosomeRegionsList = dba::query::retrieve_query(user, query_id, status);

      size_t total_size = 0;
      for (const auto& chromosomeRegions: chromosomeRegionsList) {
        total_size += chromosomeRegions.second.size();
      }

      Regions all_regions(total_size);

      for (const auto& chromosomeRegions: chromosomeRegionsList) {
        Regions to_move = std::move(chromosomeRegions.second);
        all_regions.insert(all_regions.end(),
                           std::make_move_iterator(to_move.begin()), std::make_move_iterator(to_move.end())
                          );
      }

      std::vector<Score> scores;
      std::vector<long> bar_counts(bars);

      if (all_regions.empty() ) {
        DocumentBuilder bob;
        bob.append(KVP("ranges", utils::build_array(scores)));
        bob.append(KVP("counts", utils::build_array(bar_counts)));
        return bob.extract();
      }

      std::sort(all_regions.begin(), all_regions.end(),
        [column_name](const RegionPtr & a, const RegionPtr & b) -> bool {
          auto pos_a = cache::get_column_position_from_dataset(a->dataset_id(), column_name);
          if (pos_a == -1) {
            throw deepblue_user_exception(ERR_INVALID_COLUMN_NAME, column_name);
          }
          auto pos_b = cache::get_column_position_from_dataset(b->dataset_id(), column_name);
          if (pos_a == -1) {
            throw deepblue_user_exception(ERR_INVALID_COLUMN_NAME, column_name);
          }

          return a->value(pos_a) <  b->value(pos_b);
        }
      );

      auto min_column_pos = cache::get_column_position_from_dataset(all_regions.front()->dataset_id(), column_name);
      Score min = all_regions.front()->value(min_column_pos);

      auto max_column_pos = cache::get_column_position_from_dataset(all_regions.back()->dataset_id(), column_name);
      Score max = all_regions.back()->value(max_column_pos);

      Score step = (max - min) / bars;

      int actual_bar = 0;
      int actual_count = 0;


      for (const RegionPtr& region: all_regions) {
        auto column_pos = cache::get_column_position_from_dataset(region->dataset_id(), column_name);
        Score value = region->value(column_pos);

        if (value <= (min + (step  * (actual_bar + 1)))) {
          actual_count++;
        } else {
          bar_counts[actual_bar] = actual_count;

          actual_bar = static_cast<int>(floor((value - min) / step));

          actual_count = 1;
        }
      }
      bar_counts[actual_bar] = actual_count;

      for (int i = 0; i <= bars; i++) {
        scores.push_back((min + (step * i)));
      }

      DocumentBuilder bob;
      bob.append(KVP("ranges", utils::build_array(scores)));
      bob.append(KVP("counts", utils::build_array(bar_counts)));
      return bob.extract();
    }
  }
}