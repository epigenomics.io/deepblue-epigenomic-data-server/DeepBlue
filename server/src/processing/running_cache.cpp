//
//  running_cache.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 01.11.16.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <memory>
#include <boost/regex.hpp>
#include <string>

#include "../dba/genomes.hpp"
#include "../dba/retrieve.hpp"
#include "../dba/sequence_retriever.hpp"
#include "../dba/key_mapper.hpp"

#include "processing.hpp"

#include "running_cache.hpp"

#include "../exceptions.hpp"

namespace epidb {
  namespace processing {

    void DatasetCache::load_sequence(const std::string& chromosome)
    {
      size_t size = dba::genomes::chromosome_size(_genome, chromosome);
      _sequence = dba::retrieve::SequenceRetriever::singleton().retrieve(_genome, chromosome, 0, size);
    }

    const size_t DatasetCache::count_regions(const std::string& pattern,
                                     const std::string& chromosome, const Position start, const Position end)
    {
      if (pattern.empty()) {
        throw deepblue_user_exception(ERR_USER_MOTIF_MISSING);
      }

      size_t count = 0;

      if (chromosome != current_chromosome) {
        load_sequence(chromosome);
        current_chromosome = chromosome;
      }

      std::string sub = _sequence.substr(start, end - start);

      boost::regex e(pattern);
      boost::match_results<std::string::const_iterator> what;
      std::string::const_iterator begin_it = sub.begin();
      std::string::const_iterator end_it = sub.end();
      count = 0;
      while (boost::regex_search(begin_it, end_it, what, e)) {
        begin_it = what[0].second;
        ++count;
      }

      return count;
    }

    const std::string DatasetCache::get_sequence(const std::string& chromosome, const Position start, const Position end)
    {
      if (chromosome != current_chromosome) {
        load_sequence(chromosome);
        current_chromosome = chromosome;
      }

      return _sequence.substr(start, end - start);
    }


    const std::string RunningCache::get_sequence(const std::string & norm_genome, const std::string & chromosome,
                                    const Position start, const Position end, StatusPtr status)
    {
      if (caches.find(norm_genome) == caches.end()) {
        caches[norm_genome] = std::make_unique<DatasetCache>(norm_genome, status);
      }

      return caches[norm_genome]->get_sequence(chromosome, start, end);
    }

    const size_t RunningCache::count_regions(const std::string & norm_genome, const std::string & chromosome, const std::string & pattern,
                                     const Position start, const Position end, StatusPtr status)
    {
      if (caches.find(norm_genome) == caches.end()) {
        caches[norm_genome] = std::make_unique<DatasetCache>(norm_genome, status);
      }

      return caches[norm_genome]->count_regions(pattern, chromosome, start, end);
    }
  }
}
