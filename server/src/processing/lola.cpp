//
//  lola.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 23.06.2017.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm> // for min and max
#include <future>
#include <iterator>
#include <string>
#include <thread>

#include "../algorithms/algorithms.hpp"

#include "../dba/experiments.hpp"
#include "../dba/genomes.hpp"
#include "../dba/genes.hpp"
#include "../dba/gene_ontology.hpp"
#include "../dba/queries.hpp"
#include "../dba/retrieve.hpp"

#include "../extras/bson_utils.hpp"
#include "../extras/math.hpp"
#include "../extras/utils.hpp"

#include "../typedefs/bson.hpp"

#include "../threading/semaphore.hpp"

#include "enrichment_result.hpp"


namespace epidb {
  namespace processing {

    ProcessOverlapResult process_overlap(const datatypes::User& user,
                                         const std::string& genome, const std::vector<std::string>& chromosomes,
                                         const ChromosomeRegionsList &redefined_universe_overlap_query, const long count_redefined_universe_overlap_query,
                                         const std::string dataset_name, const std::string description,
                                         const std::string database_name,
                                         const ChromosomeRegionsList &universe_regions, const long total_universe_regions,
                                         processing::StatusPtr status, threading::SemaphorePtr sem)
    {
      ChromosomeRegionsList database_regions;

      std::string biosource;
      std::string epigenetic_mark;

      if (utils::is_id(dataset_name, "q")) {
        database_regions = dba::query::retrieve_query(user, dataset_name, status);

        const std::string bs_field_name = "sample_info.biosource_name";
        std::vector<std::string> exp_biosources = dba::query::get_main_experiment_data(user, dataset_name, bs_field_name, status);

        const std::string em_field_name = "epigenetic_mark";
        std::vector<std::string> exp_epigenetic_marks = dba::query::get_main_experiment_data(user, dataset_name, em_field_name, status);

        biosource = utils::vector_to_string(exp_biosources);
        epigenetic_mark = utils::vector_to_string(exp_epigenetic_marks);
      } else {
        auto regions_query = dba::query::build_experiment_query(-1, -1, utils::normalize_name(dataset_name));

        database_regions = dba::retrieve::get_regions(genome, chromosomes, regions_query, false, status, /* reduced_mode */ true);

        auto maybe_experiment = dba::experiments::by_name(dataset_name);
        if (!maybe_experiment) {
          throw deepblue_user_exception(ERR_INVALID_EXPERIMENT_NAME, dataset_name);
        }

        auto obj_view = maybe_experiment->view();
        biosource = StringViewToString(obj_view["sample_info"]["biosource_name"].get_utf8().value);
        epigenetic_mark = StringViewToString(obj_view["epigenetic_mark"].get_utf8().value);
      }

      auto count_database_regions = count_regions(database_regions);


      // This will become "support" -- the number of regions in the
      // userSet (which I implicitly assume is ALSO the number of regions
      // in the universe) that overlap anything in each database set.
      // Turn results into an overlap matrix. It is
      // dbSets (rows) by userSets (columns), counting overlap.
      auto query_overlap_total = algorithms::intersect_count(redefined_universe_overlap_query, database_regions);

      double a = query_overlap_total;


      // b = the # of items *in the universe* that overlap each dbSet,
      // less the support; This is the number of items in the universe
      // that are in the dbSet ONLY (not in userSet)
      auto universe_overlap_with_database_total = algorithms::intersect_count(universe_regions, database_regions);
      double b = universe_overlap_with_database_total - a;


      // c = the size of userSet, less the support; This is the opposite:
      // Items in the userSet ONLY (not in the dbSet)
      double c = count_redefined_universe_overlap_query - a;


      //#universe_regions - a - b - c = #universe_regions that do NOT overlap with query_set and that do NOT overlap with dataset_regions
      double d = total_universe_regions - a - b - c;


      if (b < 0) {
        throw deepblue_user_exception(ERR_LOLA_INVALID_NEGATIVE_VALUE_B, dataset_name);
      }

      if (d < 0) {
        throw deepblue_user_exception(ERR_LOLA_INVALID_NEGATIVE_VALUE_D, dataset_name);
      }

      double p_value = math::fisher_test(a, b, c, d);
      double negative_natural_log = abs(log10(p_value));

      double a_b = a/b;
      double c_d = c/d;
      double odds_score;
      // Handle when 'b' and 'd' are 0.
      if (a_b == c_d) {
        odds_score = 1;
      } else {
        odds_score = a_b/c_d;
      }

      status->subtract_regions(count_database_regions);
      size_t total_size_to_remove = 0;

      for (const auto& chr : database_regions) {
        for (const auto& rs :chr.second) {
          total_size_to_remove += rs->size();
        }
      }

      status->subtract_size(total_size_to_remove);

      sem->up();
      return std::make_tuple(dataset_name, biosource, epigenetic_mark, description, count_database_regions, database_name, negative_natural_log, odds_score, a, b, c, d);
    }


    const Document lola(const datatypes::User& user,
              const std::string& query_id, const std::string& universe_query_id,
              const View& databases, const std::string& genome,
              processing::StatusPtr status)
    {
      IS_PROCESSING_CANCELLED(status);
      auto&& runningOp = status->start_operation(PROCESS_ENRICH_REGIONS_OVERLAP, {});

      ChromosomeRegionsList query_regions = dba::query::retrieve_query(user, query_id, status, /* reduced_mode */ true);

      size_t total_query_regions = count_regions(query_regions);
      long times = clock();
      ChromosomeRegionsList universe_regions = dba::query::retrieve_query(user, universe_query_id, status, /* reduced_mode */ true);
      size_t total_universe_regions = count_regions(universe_regions);

      ChromosomeRegionsList disjoin_set = algorithms::disjoin(std::move(universe_regions));
      size_t disjoin_set_count = count_regions(disjoin_set);

      universe_regions = std::move(disjoin_set);
      ChromosomeRegionsList redefined_universe_overlap_query = algorithms::intersect(universe_regions, query_regions, status);

      size_t count_redefined_universe_overlap_query = count_regions(redefined_universe_overlap_query);

      const std::string& norm_genome = utils::normalize_name(genome);
      auto chromosomes_set = dba::genomes::get_chromosomes(norm_genome);
      std::vector<std::string> chromosomes(chromosomes_set.begin(), chromosomes_set.end());

      std::vector<std::tuple<std::string, size_t>> datasets_support;
      std::vector<std::tuple<std::string, double>> datasets_log_score;
      std::vector<std::tuple<std::string, double>> datasets_odds_score;

      std::vector<std::future<ProcessOverlapResult > > threads;

      threading::SemaphorePtr sem = threading::build_semaphore(128);

      std::vector<std::pair<Document, std::string>> all_datasets;


      for (const auto& database: databases ) {
        const std::string database_name = bson_utils::get_key(database);
        const auto& datasets = database.get_array().value;

        for (const auto& dataset: datasets) {
          all_datasets.push_back(std::pair<Document, std::string>(dataset.get_document().value, database_name));
        }
      }

      // sort using a lambda expression
      std::sort(all_datasets.begin(), all_datasets.end(),
        [](const std::pair<Document, std::string>& a, const std::pair<Document, std::string>& b) {
          return bson_utils::get_string(a.first, "name").compare(bson_utils::get_string(b.first, "name")) > 0;
      });



      for (const auto& dataset_database : all_datasets) {
        const auto dataset = dataset_database.first;
        const auto database_name = dataset_database.second;
        const auto dataset_name = bson_utils::get_string(dataset, "name");
        const auto description = bson_utils::get_string(dataset, "description");

        sem->down();
        auto t = std::async(std::launch::async, &process_overlap,
                            std::ref(user),
                            std::ref(genome), std::ref(chromosomes),
                            std::ref(redefined_universe_overlap_query), count_redefined_universe_overlap_query,
                            dataset_name, description, database_name,
                            std::ref(universe_regions), total_universe_regions,
                            status, sem);

        threads.emplace_back(std::move(t));

      }

      std::vector<ProcessOverlapResult> results;

      for (size_t i = 0; i < threads.size(); ++i) {
        threads[i].wait();
        auto result = threads[i].get();

        std::string dataset = std::get<0>(result);

        datasets_log_score.push_back(std::make_tuple(dataset, std::get<6>(result)));
        datasets_odds_score.push_back(std::make_tuple(dataset, std::get<7>(result)));
        datasets_support.push_back(std::make_tuple(dataset, std::get<8>(result)));

        results.emplace_back(std::move(result));
      }

      std::vector<std::shared_ptr<ExperimentResult>> experiment_results =
            sort_results(results, datasets_support, datasets_log_score, datasets_odds_score);

      DocumentBuilder bob;
      bob.append(KVP("count_query_regions", static_cast<int64_t>(total_query_regions)));
      bob.append(KVP("count_universe_regions", static_cast<int64_t>(total_universe_regions)));

      ArrayBuilder ab;
      for (const auto& er: experiment_results) {
        ab.append(er->toDocument());
      }
      bob.append(KVP("results", ab.extract()));

      return bob.extract();
    }
  }
}