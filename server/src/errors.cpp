//
//  errors.cpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 23.06.14.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <string>
#include <vector>

#include "errors.hpp"

namespace epidb {

  const Error ERR_USER_USER_MISSING(User, "User is missing. Please, specify the User.");
  const Error ERR_INSUFFICIENT_PERMISSION(User, "Insufficient permission. Permission {} is required.");
  const Error ERR_USER_EXPERIMENT_MISSING(User, "Experiment is missing. Please, specify the Experiment.");
  const Error ERR_USER_ANNOTATION_MISSING(User, "Annotation is missing. Please, specify the Annotation.");
  const Error ERR_USER_SAMPLE_MISSING(User, "Sample is missing. Please, specify the Sample.");
  const Error ERR_USER_BIOSOURCE_MISSING(User, "BioSource is missing. Please, specify the BioSource.");
  const Error ERR_USER_EPIGNETIC_MARK_MISSING(User, "Epigenetic Mark is missing. Please, specify the Epigenetic Mark.");
  const Error ERR_USER_TECHNIQUE_MISSING(User, "Technique is missing. Please, specify the Technique.");
  const Error ERR_USER_PROJECT_MISSING(User, "Project is missing. Please, specify the Project.");
  const Error ERR_USER_DATA_MISSING(User, "Data is missing. Please, specify the Data.");
  const Error ERR_USER_FORMAT_MISSING(User, "Format is missing. Please, specify the Format.");
  const Error ERR_USER_GENOME_MISSING(User, "Genome is missing. Please, specify the Genome.");
  const Error ERR_USER_GENE_MISSING(User, "Gene is missing. Please, specify the Gene.");
  const Error ERR_USER_GENE_MODEL_MISSING(User, "Gene Model is missing. Please, specify the Gene Model.");
  const Error ERR_USER_MOTIF_MISSING(User, "Motif is missing. Please, specify the Motif.");

  const Error ERR_FORMAT_CHROMOSOME_MISSING(User, "The CHROMOSOME column is missing in the format. Please, inform the CHROMOSOME column in the Format.");
  const Error ERR_FORMAT_START_MISSING(User, "The START column is missing in the format. Please, inform the START column in the Format.");
  const Error ERR_FORMAT_END_MISSING(User, "The END column is missing in the format. Please, inform the END column in the Format.");
  const Error ERR_FORMAT_COLUMN_NAME_MISSING(User, "A column is missing in the format. Please, inform the column name in the Format. (Maybe there are double commas (',,') or an extra comma at the end of the format.)");
  const Error ERR_FORMAT_COLUMN_NUMERICAL_VALUES(User, "Column '{}' ({}) in the experiment '{}' does not contain numerical values.");

  const Error ERR_GENE_EXPRESSION_FORMAT_INVALID(User, "The Gene Expression format name '{}' is invalid.");

  const Error ERR_PARSING_TOKENS_COUNT(User, "Error while reading the BED file. Line: '{}'. The number of tokens ({}) is different from the format size ({}) - {}");
  const Error ERR_TOKENS_COUNT(User, "Number of line tokens '{}' doesn't match the file format size '{}'.");
  const Error ERR_PARSING_START_POSITION(User, "{} is not a valid start position");
  const Error ERR_PARSING_END_POSITION(User, "{} is not a valid end position");
  const Error ERR_PARSING_EMPTY_FILE(User, "File content is empty");
  const Error ERR_PARSING_LINE(User, "Failed to parse line '{}': {}");
  const Error ERR_PARSING_LINE_FIELD(User, "Error at line '{}'. The field '{}' with the value '{}' is invalid.");

  const Error ERR_INVALID_CONTROLLED_VABULARY(User, "Controlled vocabulary '{}' does not exist. Available controlled vocabularies: types, {}, {}, {}, {}, {}, {}");

  const Error ERR_INVALID_USER_NAME(User, "Invalid User name '{}'.");
  const Error ERR_INVALID_USER_ID(User, "Invalid User ID '{}'.");
  const Error ERR_INVALID_USER_KEY(User, "Invalid User Key.");
  const Error ERR_INVALID_USER_EMAIL_PASSWORD(User, "Invalid User email and password combination.");

  const Error ERR_DUPLICATED_EXPERIMENT_NAME(User, "Experiment name '{}' is already being used.");

  const Error ERR_DUPLICATED_GENE_MODEL_NAME(User, "Gene Model '{}' is already being used.");
  const Error ERR_INVALID_GENE_MODEL_ID(User, "Unable to find Gene Model ID '{}'.");
  const Error ERR_INVALID_GENE_MODEL_NAME(User, "Unable to find Gene Model name '{}'.");

  const Error ERR_DUPLICATE_EXPRESSION(User, "A Expression of the type '{}' with sample_id '{}' and replica '{}' already exists.");
  const Error ERR_INVALID_EXPRESSION_ID(User, "Unable to find Gene Expression ID '{}'.");
  const Error ERR_INVALID_EXPRESSION_TYPE(User, "Unable to find Expression Type '{}'.");

  const Error ERR_INVALID_GENE_NAME(User, "Gene Name '{}' was not found in the Gene Model '{}'.");
  const Error ERR_INVALID_GENE_ID(User, "Gene ID '{}' was not found in the Gene Model '{}'.");
  const Error ERR_INVALID_GENE_LOCATION(User, "There are no gene in the chromosome '{}' location {} - {} in the Gene Model '{}'.");
  const Error ERR_INVALID_GENE_ATTRIBUTE(User, "Gene '{}' does not have the attribute '{}'.");
  const Error ERR_INVALID_GENE_TRACKING_ID(User, "Gene tracking_id '{}' with gene name: '{}' not found in the gene model '{}'.");

  const Error ERR_INVALID_GENE_ONTOLOGY_TERM_ID(User, "Invalid Gene Ontology term ID '{}'.");
  const Error ERR_DUPLICATED_GENE_ONTOLOGY_TERM_ID(User, "Duplicated Gene Ontology term ID '{}'. GO term with this ID already exists.");
  const Error ERR_DUPLICATED_GENE_ONTOLOGY_TERM_LABEL(User, "Duplicated Gene Ontology term label '{}'. GO term with this label already exists.");
  const Error ERR_INVALID_GENE_ONTOLOGY_NAMESPACE(User, "Invalid Gene Ontology namespace '{}'. The valid names are: 'cellular component', 'biological process', 'molecular const function'");
  const Error ERR_ALREADY_CONECTED_GENE_ONTOLOGY_TERM(User, "'{}' and '{}' are already connected.");

  const Error ERR_INVALID_BIOSOURCE_NAME(User, "Unable to find BioSource '{}'. No BioSource or Synonym was defined with this name.");
  const Error ERR_INVALID_BIOSOURCE_ID(User, "Uable to find BioSource ID '{}'. No BioSource or Synonym was defined with this ID.");
  const Error ERR_DUPLICATED_BIOSOURCE_NAME(User, "Duplicated BioSource Name '{}'. BioSource or Synonym with this name already exists.");
  const Error ERR_MORE_EMBRACING_BIOSOURCE_NAME(User, "'{}' is already more embracing than '{}'.");
  const Error ERR_ALREADY_PARENT_BIOSOURCE_NAME(User, "'{}' is already parent of '{}'.");

  const Error ERR_ALREADY_CONECTED_BIOSOURCE_NAME(User, "'{}' and '{}' are already connected.");
  const Error ERR_BIOSOURCE_SAME(User, "'{}' and '{}' are synonyms.");

  const Error ERR_INVALID_BIOSOURCE_SYNONYM(User, "Invalid BioSource Synonym '{}'. A BioSource or a synonym with this name already exists.");
  const Error ERR_SYNONYM_NAME_NOT_FOUND(System, "It was not possible to find the name of '{}'.");
  const Error ERR_BIOSOURCE_SYNONYM_ROOT_NOT_FOUND(System, "It was not possible to find the biosource root for {}.");

  const Error ERR_DUPLICATED_EPIGENETIC_MARK_NAME(User, "Duplicated Epigenetic Mark Name '{}'." );
  const Error ERR_INVALID_EPIGENETIC_MARK(User, "Epigenetic Mark ID '{}' does not exists." );
  const Error ERR_INVALID_EPIGENETIC_MARK_ID(User, "Unable to find Epigenetic Mark ID '{}'." );

  const Error ERR_INVALID_PROJECT(User, "Project '{}' does not exist.");
  const Error ERR_DUPLICATED_PROJECT_NAME(User, "Duplicated Project Name '{}'.");
  const Error ERR_INVALID_PROJECT_ID(User, "Unable to find the project ID '{}'.");
  const Error ERR_PERMISSION_PROJECT(User, "You are not the project '{}' owner and neither an administrator.");
  const Error ERR_SET_PUBLIC_PROJECT(User, "Error setting the Project id '{}' public.");

  const Error ERR_DUPLICATED_GENOME_NAME(User, "Duplicated Genome name '{}'.");
  const Error ERR_INVALID_GENOME_NAME(User, "Unable to find the genome '{}'.");
  const Error ERR_INVALID_GENOME_ID(User, "Unable to find the genome ID '{}'.");

  const Error ERR_INVALID_CHROMOSOME_NAME(User, "Unable to find the chromosome '{}'.");
  const Error ERR_INVALID_CHROMOSOME_NAME_GENOME(User, "Unable to find the chromosome '{}' in the genome '{}'.");
  const Error ERR_INVALID_CHROMOSOME_LIST(User, "Chromosome name is empty.");
  const Error ERR_INVALID_CHROMOSOME_POSITION(User, "Invalid region: '{}' - '{}'. It is beyond the length of the chromosome '{}'.");

  const Error ERR_INVALID_TECHNIQUE(User, "Technique '{}' does not exist.");
  const Error ERR_INVALID_TECHNIQUE_ID(User, "Unable to find the technique ID '{}'.");
  const Error ERR_DUPLICATED_TECHNIQUE_NAME(User, "Duplicated Genome Name '{}'.");

  const Error ERR_INVALID_EXPERIMENT(User, "Experiment '{}' does not exists.");
  const Error ERR_INVALID_EXPERIMENT_ID(User, "Unable to find the experiment ID '{}'.");
  const Error ERR_INVALID_EXPERIMENT_NAME(User, "Unable to find the experiment name '{}'.");
  const Error ERR_INVALID_EXPERIMENT_COLUMN(User, "Experiment '{}' does not have the column '{}'.");

  const Error ERR_INVALID_SAMPLE_ID(User, "Unable to find the sample ID '{}'.");

  const Error ERR_INVALID_QUERY_ID(User, "Unable to find the query ID '{}'.");
  const Error ERR_INVALID_UNIVERSE_ID(User, "Unable to find the universe ID '{}'.");
  const Error ERR_PERMISSION_QUERY(User, "You are not the query ID '{}' owner and neither an administrator.");
  const Error ERR_IMPOSSIBLE_QUERY_GENOME(User, "It is not possible to obtain the genome of the query '{}'");

  const Error ERR_INVALID_COLLECTION_NAME(User, "Invalid collection '{}'. The valid types are: {}.");

  const Error ERR_INVALID_ANNOTATION_NAME(User, "Unable to find the annotation '{}' in the genome {}.");
  const Error ERR_INVALID_ANNOTATION_ID(User, "Unable to find the annotation ID '{}'.");

  const Error ERR_INVALID_PRA_PROCESSED_ANNOTATION_NAME(User, "There is not {} annotation for the patterns '{}' for the genome '{}'.");

  const Error ERR_INVALID_COLUMN_NAME(User, "Column name '{}' does not exist.");
  const Error ERR_INVALID_COLUMN_ID(User, "Unable to find the column ID '{}'.");
  const Error ERR_INVALID_COLUMN_TYPE(System, "Invalid column type '{}'.");
  const Error ERR_INVALID_COLUMN_TYPE_SIMPLE(System, "Invalid column type '{}' for a simple column. The acceptable values for a column type are: string, integer, double.");
  const Error ERR_DUPLICATED_COLUMN_NAME(User, "Duplicated column name '{}'.");
  const Error ERR_INVALID_VALUE_FOR_COLUMN(User, "Invalid value '{}' for column '{}'.");
  const Error ERR_FIELD_INTEGER_VALUE_INVALID(User, "Field '{}' is an integer, but the value '{}' is not a valid integer.");
  const Error ERR_FIELD_DOUBLE_VALUE_INVALID(User, "Field '{}' is an double, but the value '{}' is not a valid double.");
  const Error ERR_USAGE_COLUMN_RANGE(User, "Range Columns should be created first and them loaded.");
  const Error ERR_USAGE_COLUMN_CATEGORY(User, "Category Columns should be created first and them loaded.");
  const Error ERR_INVALID_COLUMN_IN_DATASET(User, "Column '{}' is not present in the dataset '{}'.");

  const Error ERR_INVALID_COLUMN_TYPE_ID(User, "Unable to find the column type ID '{}'." );

  const Error ERR_INVALID_META_COLUMN_NAME(User, "Meta-column '{}' does not exist.");

  const Error ERR_CLONE_INVALID_COLUMNS_COUNT(User, "The new format has fewer columns ('{}') than the original format ('{}').");
  const Error ERR_CLONE_IMUTABLE_COLUMNS(User, "Column '{}' cannot be renamed. Columns CHROMOSOME,START, and END are immutable.");
  const Error ERR_CLONE_INCOMPATIBLE_COLUMN_TYPES(User, "The column '{}' (type: '{}') is incompatible with the original column '{}' (type: '{}').");
  const Error ERR_CLONE_NEW_COLUMN_CALCULATED(User, "Column '{}' should be calculated. It is not possible to include new columns into cloned experiment or annotation that are not calculated.");

  const Error ERR_REQUEST_CANCELED(User, "Request ID '{}' was canceled.");
  const Error ERR_REQUEST_ID_INVALID(User, "Request ID '{}' is invalid.");
  const Error ERR_REQUEST_ERROR(User, "Request ID '{}' failed error: '{}'");
  const Error ERR_REQUEST_CLEARED(User, "Tequest ID '{}' was cleared. We are going to reprocess this request. Please, check its status.");
  const Error ERR_REQUEST_NOT_FINISHED(User, "Request ID '{}' was not finished. Please, check its status.");

  const Error ERR_NAME_NOT_FOUND(User, "Element name {} of the collection {} not found.");

  const Error ERR_UNKNOW_QUERY_TYPE(System, "Unknown query type '{}'");

  const Error ERR_COLUMN_TYPE_MISSING(System, "Column Type '{}' does not exist.");
  const Error ERR_COLUMN_TYPE_NAME_MISSING(System, "Column Type '{}' does not exist.");

  const Error ERR_DATASET_NOT_FOUND(System, "Dataset id '{}' not found.");

  const Error ERR_INVALID_TILING_REGIONS_ID(User, "Unable to find tiling regions ID '{}'.");

  const Error ERR_INVALID_IDENTIFIER(User, "Invalid identifier '{}'.");
  const Error ERR_INVALID_IDENTIFIER_VALIDS(User, "Invalid identifier '{}'. It is accepted only data ID from {}.");

  const Error ERR_INVALID_START(User, "Invalid starting position '{}'.");
  const Error ERR_INVALID_LENGTH(User, "Invalid length '{}'.");
  const Error ERR_NO_PERMISSION_TO_REMOVE(User, "You do not have permission to delete the entity '{}'.");

  const Error ERR_INVALID_GSM_IDENTIFIER(User, "Invalid GSM identifier '{}'.");

  const Error ERR_INVALID_INPUT_TYPE(User, "Invalid input type. It is '{}' but must be a '{}'");

  const Error ERR_INVALID_INPUT_SUB_ITEM_SIZE(User, "Invalid sub-item size {}. It must be of size {}");
  const Error ERR_INVALID_INPUT_SUB_ITEM_TYPE(User, "Invalid sub-item type at position {}. Its type is '{}' but must be a '{}'");

  const Error ERR_DATABASE_CONNECTION(Database, "MongoDB connection error: '{}'.");
  const Error ERR_DATABASE_EXCEPTION(Database, "MongoDB exception at operation '{}': '{}'.");
  const Error ERR_DATABASE_INVALID_BIOSOURCE(Database, "BioSource '{}' not found.");

  const Error ERR_BSON_EXCEPTION(System, "Execption when handling bson content: {}");

  const Error ERR_INVALID_INTERNAL_NAME(System, "Unable to retrieve the name of the internal name '{}'.");

  const Error ERR_SCORE_MATRIX_TOO_MANY_CELLS(User, "This score_matrix request exceeds the limit of '{}' cells. It requests '{}' cells, being from '{}' rows (ranges) and '{}' experiments");

  const Error ERR_INVALID_LUA_CODE(User, "Invalid Lua code '{}'. Error: '{}'.");
  const Error ERR_INVALID_LUA_STATUS(System, "Invalid internal Lua Status. Check your code: '{}'.");
  const Error ERR_INVALID_LUA_MAX_NUMBER_INSTRUCTIONS_EXECUTED(User, "Maximum number of instructions has been reached.");

  const Error ERR_GENOME_BEING_USED_BY_EXPERIMENT(User, "This genome is being used by the experiment: '{}'.");
  const Error ERR_GENOME_BEING_USED_BY_ANNOTATION(User, "This genome is being used by the annotation: '{}'.");

  const Error ERR_PROJECT_BEING_USED_BY_EXPERIMENT(User, "This project is being used by experiments.");
  const Error ERR_SAMPLE_BEING_USED_BY_EXPERIMENT(User, "This sample is being used by experiments.");
  const Error ERR_EPIGENETIC_MARK_BEING_USED_BY_EXPERIMENT(User, "This epigenetic mark is being used by experiments.");
  const Error ERR_TECHNIQUE_BEING_USED_BY_EXPERIMENT(User, "This technique is being used by experiments.");

  const Error ERR_BIOSOURCE_BEING_USED_BY_SAMPLES(User, "This biosource is being used by samples.");
  const Error ERR_BIOSOURCE_SCOPE_CONTENT(User, "This biosource has terms into his scope and cannot be removed.");

  const Error ERR_DUPLICATED_FIELD(User, "Field {} already exists.");
  const Error ERR_DUPLICATED_SEQUENCE(User, "Sequence for chromosome '{}' of '{}' was already uploaded.");
  const Error ERR_CHROMOSOME_SEQUENCE_NOT_FOUND(User, "There is not sequence for the chromosomes '{}' of the genome '{}'. Please upload using 'upload_chromosome' command.");


  const Error ERR_EXPERIMENT_SET(User, "Experiment set '{}' already exists.");
  const Error ERR_EXPERIMENT_SET_ID_NOT_FOUND(User, "Experiment set '{}' not found.");

  const Error ERR_INVALID_DATA_TYPE(User, "Datatype with '{}' not accepted for this function.");

  const Error ERR_INVALID_OBJECT_OWNER(System, "Object ID '{}' does not have a valid user owner.");

  const Error ERR_INVALID_COUNTER(System, "Invalid counter name '{}'.");

  const Error ERR_FILTER_INVALID_COMMAND(User, "Invalid filter command '{}'.");
  const Error ERR_FILTER_EMPTY_VALUE(User, "The given value for '{}' is empty.");
  const Error ERR_FILTER_INVALID_VALUE(User, "The value '{}' for '{}' is invalid. Please, do not use '$'.");
  const Error ERR_FILTER_INVALID_TYPE(User, "Invalid type. Valid types are: string, number, integer, double.");
  const Error ERR_FILTER_INVALID_STRING_COMMAND(User, "Only equals (==) or not equals (!=) are available for type 'string'.");

  const Error ERR_FILTER_BY_MOTIF_AGGREGATED(User, "It is (yet) not possible to filter by DNA Motif on aggregated regions.");

  const Error ERR_INVALID_AGG_FUNCTION(User, "Aggregation function '{}' is invalid.");

  const Error ERR_CALCULATED_FIELD_DATA(User, "Calculated field does not have data.");

  const Error ERR_MEMORY_EXAUSTED(User, "Memory exhausted. Used {} bytes of {} bytes allowed. Please, select a smaller initial dataset, for example, selecting fewer chromosomes.");
  const Error ERR_INVALID_STATE(System, "Invalid state");
  const Error ERR_STRING_BUILDER_MEMORY_EXAUSTED(User, "The output string ({} MBytes) is bigger than the size that you are allowed to use: '{}  MBytes'. We recomend you to select fewer experiments, chromosomes, or check the metafields that you are using, for example the @SEQUENCE metafield.");

  const Error ERR_CHUNCK_SIZE_TOO_BIG(User, "Internal Error. Chuck size is too big: '{}'. Please, ask the administrator to check the logs.");

  const Error ERR_BINNING_NO_BARS(User, "There must be at least one bar in the binning");
  const Error ERR_BINNING_TOO_MANY_BARS(User, "There must be at no more than 65536 bars");

  const Error ERR_LOLA_INVALID_NEGATIVE_VALUE_B(User, "Negative b entry in table. This means either: 1) Your user sets contain items outside your universe; or 2) your universe has a region that overlaps multiple user set regions, interfering with the universe set overlap calculation. Dataset: '{}'");
  const Error ERR_LOLA_INVALID_NEGATIVE_VALUE_D(User, "Negative d entry in table. This means either: 1) Your user sets contain items outside your universe; or 2) your universe has a region that overlaps multiple user set regions, interfering with the universe set overlap calculation. Dataset: '{}'");

  const Error ERR_BITMAP_INVALID_POS(User, "Invalid position '{}'");

  const Error ERR_WORKAROUND_STRING_ERROR(User, "Error not defined: '{}'");
}
