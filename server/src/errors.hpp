//
//  errors.hpp
//  DeepBlue Epigenomic Data Server
//  File created by Felipe Albrecht on 23.06.14.
//  Copyright (c) 2021 Felipe Albrecht. All rights reserved.
//  Copyright (c) 2016 Max Planck Institute for Informatics. All rights reserved.

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#ifndef EPIDB_ERRORS_HPP
#define EPIDB_ERRORS_HPP

#include <iostream>

#include <string>
#include <fmt/core.h>

namespace epidb {

  enum ErrorLevel {
    User,
    System,
    Database
  };

  class Error {
  private:
    ErrorLevel code_value;
    std::string err_fmt;
  public:
    Error(const ErrorLevel &c, const std::string &f) :
      code_value(c),
      err_fmt(f) {}


    ErrorLevel code() const {
      return code_value;
    }

    bool operator==(const Error& other) const noexcept
    {
      return this == &other;
    }

    template<typename... Args>
    static std::string m(const Error e, const Args & ... args)
    {
      return fmt::format(e.err_fmt, args...);
    }
  };

  extern const Error ERR_USER_USER_MISSING;
  extern const Error ERR_INSUFFICIENT_PERMISSION;
  extern const Error ERR_USER_EXPERIMENT_MISSING;
  extern const Error ERR_USER_ANNOTATION_MISSING;
  extern const Error ERR_USER_MOTIF_MISSING;
  extern const Error ERR_USER_SAMPLE_MISSING;
  extern const Error ERR_USER_BIOSOURCE_MISSING;
  extern const Error ERR_USER_EPIGNETIC_MARK_MISSING;
  extern const Error ERR_USER_TECHNIQUE_MISSING;
  extern const Error ERR_USER_PROJECT_MISSING;
  extern const Error ERR_USER_DATA_MISSING;
  extern const Error ERR_USER_FORMAT_MISSING;
  extern const Error ERR_USER_GENOME_MISSING;
  extern const Error ERR_USER_GENE_MISSING;
  extern const Error ERR_USER_GENE_MODEL_MISSING;

  extern const Error ERR_FORMAT_CHROMOSOME_MISSING;
  extern const Error ERR_FORMAT_START_MISSING;
  extern const Error ERR_FORMAT_END_MISSING;
  extern const Error ERR_FORMAT_COLUMN_NAME_MISSING;
  extern const Error ERR_FORMAT_COLUMN_NUMERICAL_VALUES;
  extern const Error ERR_GENE_EXPRESSION_FORMAT_INVALID;

  extern const Error ERR_PARSING_TOKENS_COUNT;
  extern const Error ERR_TOKENS_COUNT;
  extern const Error ERR_PARSING_START_POSITION;
  extern const Error ERR_PARSING_END_POSITION;
  extern const Error ERR_PARSING_EMPTY_FILE;
  extern const Error ERR_PARSING_LINE;
  extern const Error ERR_PARSING_LINE_FIELD;

  extern const Error ERR_INVALID_CONTROLLED_VABULARY;

  extern const Error ERR_INVALID_EXPERIMENT;
  extern const Error ERR_INVALID_EXPERIMENT_ID;
  extern const Error ERR_INVALID_EXPERIMENT_NAME;
  extern const Error ERR_INVALID_EXPERIMENT_COLUMN;

  extern const Error ERR_INVALID_ANNOTATION_NAME;
  extern const Error ERR_INVALID_ANNOTATION_ID;

  extern const Error ERR_INVALID_PRA_PROCESSED_ANNOTATION_NAME;

  extern const Error ERR_INVALID_PROJECT;
  extern const Error ERR_INVALID_PROJECT_ID;
  extern const Error ERR_PERMISSION_PROJECT;
  extern const Error ERR_SET_PUBLIC_PROJECT;

  extern const Error ERR_INVALID_TECHNIQUE;
  extern const Error ERR_INVALID_TECHNIQUE_ID;

  extern const Error ERR_INVALID_BIOSOURCE_ID;
  extern const Error ERR_INVALID_EPIGENETIC_MARK;
  extern const Error ERR_INVALID_EPIGENETIC_MARK_ID;
  extern const Error ERR_INVALID_GENE_MODEL_ID;
  extern const Error ERR_INVALID_TILING_REGIONS_ID;
  extern const Error ERR_INVALID_COLUMN_TYPE_ID;

  extern const Error ERR_INVALID_GENE_MODEL_NAME;

  extern const Error ERR_INVALID_COLUMN_TYPE;
  extern const Error ERR_INVALID_COLUMN_TYPE_SIMPLE;

  extern const Error ERR_INVALID_COLLECTION_NAME;

  extern const Error ERR_INVALID_CHROMOSOME_NAME;
  extern const Error ERR_INVALID_CHROMOSOME_NAME_GENOME;
  extern const Error ERR_INVALID_CHROMOSOME_LIST;
  extern const Error ERR_INVALID_CHROMOSOME_POSITION;

  extern const Error ERR_INVALID_GENOME_NAME;
  extern const Error ERR_INVALID_GENOME_ID;

  extern const Error ERR_INVALID_GENE_ID;
  extern const Error ERR_INVALID_GENE_NAME;
  extern const Error ERR_INVALID_GENE_LOCATION;
  extern const Error ERR_INVALID_GENE_ATTRIBUTE;
  extern const Error ERR_INVALID_GENE_TRACKING_ID;

  extern const Error ERR_DUPLICATED_GENE_ONTOLOGY_TERM_ID;
  extern const Error ERR_INVALID_GENE_ONTOLOGY_TERM_ID;
  extern const Error ERR_DUPLICATED_GENE_ONTOLOGY_TERM_LABEL;
  extern const Error ERR_INVALID_GENE_ONTOLOGY_NAMESPACE;
  extern const Error ERR_ALREADY_CONECTED_GENE_ONTOLOGY_TERM;

  extern const Error ERR_INVALID_EXPRESSION_TYPE;
  extern const Error ERR_INVALID_EXPRESSION_ID;
  extern const Error ERR_DUPLICATE_EXPRESSION;

  extern const Error ERR_INVALID_SAMPLE_ID;

  extern const Error ERR_INVALID_BIOSOURCE_NAME;
  extern const Error ERR_INVALID_QUERY_ID;
  extern const Error ERR_INVALID_UNIVERSE_ID;
  extern const Error ERR_PERMISSION_QUERY;
  extern const Error ERR_IMPOSSIBLE_QUERY_GENOME;

  extern const Error ERR_INVALID_COLUMN_NAME;
  extern const Error ERR_INVALID_COLUMN_ID;
  extern const Error ERR_DUPLICATED_COLUMN_NAME;
  extern const Error ERR_INVALID_VALUE_FOR_COLUMN;
  extern const Error ERR_FIELD_INTEGER_VALUE_INVALID;
  extern const Error ERR_FIELD_DOUBLE_VALUE_INVALID;
  extern const Error ERR_USAGE_COLUMN_RANGE;
  extern const Error ERR_USAGE_COLUMN_CATEGORY;
  extern const Error ERR_INVALID_COLUMN_IN_DATASET;

  extern const Error ERR_INVALID_META_COLUMN_NAME;

  extern const Error ERR_CLONE_INVALID_COLUMNS_COUNT;
  extern const Error ERR_CLONE_IMUTABLE_COLUMNS;
  extern const Error ERR_CLONE_INCOMPATIBLE_COLUMN_TYPES;
  extern const Error ERR_CLONE_NEW_COLUMN_CALCULATED;

  extern const Error ERR_DUPLICATED_BIOSOURCE_NAME;
  extern const Error ERR_DUPLICATED_EXPERIMENT_NAME;
  extern const Error ERR_DUPLICATED_GENE_MODEL_NAME;

  extern const Error ERR_MORE_EMBRACING_BIOSOURCE_NAME;
  extern const Error ERR_ALREADY_PARENT_BIOSOURCE_NAME;

  extern const Error ERR_INVALID_BIOSOURCE_SYNONYM;
  extern const Error ERR_SYNONYM_NAME_NOT_FOUND;
  extern const Error ERR_BIOSOURCE_SYNONYM_ROOT_NOT_FOUND;

  extern const Error ERR_ALREADY_CONECTED_BIOSOURCE_NAME;
  extern const Error ERR_ALREADY_PARENT_BIOSOURCE_NAME;
  extern const Error ERR_BIOSOURCE_SAME;

  extern const Error ERR_INVALID_USER_KEY;
  extern const Error ERR_INVALID_USER_NAME;
  extern const Error ERR_INVALID_USER_ID;
  extern const Error ERR_INVALID_USER_EMAIL_PASSWORD;

  extern const Error ERR_DUPLICATED_EPIGENETIC_MARK_NAME;
  extern const Error ERR_DUPLICATED_PROJECT_NAME;
  extern const Error ERR_DUPLICATED_GENOME_NAME;
  extern const Error ERR_DUPLICATED_TECHNIQUE_NAME;

  extern const Error ERR_INVALID_START;
  extern const Error ERR_INVALID_LENGTH;

  extern const Error ERR_NO_PERMISSION_TO_REMOVE;

  extern const Error ERR_UNKNOW_QUERY_TYPE;

  extern const Error ERR_COLUMN_TYPE_MISSING;
  extern const Error ERR_COLUMN_TYPE_NAME_MISSING;

  extern const Error ERR_INVALID_IDENTIFIER;
  extern const Error ERR_INVALID_IDENTIFIER_VALIDS;
  extern const Error ERR_INVALID_GSM_IDENTIFIER;

  extern const Error ERR_INVALID_INPUT_TYPE;

  extern const Error ERR_INVALID_INPUT_SUB_ITEM_SIZE;
  extern const Error ERR_INVALID_INPUT_SUB_ITEM_TYPE;

  extern const Error ERR_DATASET_NOT_FOUND;

  extern const Error ERR_NAME_NOT_FOUND;

  extern const Error ERR_DATABASE_CONNECTION;
  extern const Error ERR_DATABASE_EXCEPTION;
  extern const Error ERR_DATABASE_INVALID_BIOSOURCE;

  extern const Error ERR_BSON_EXCEPTION;

  extern const Error ERR_REQUEST_CANCELED;
  extern const Error ERR_REQUEST_ID_INVALID;
  extern const Error ERR_REQUEST_ERROR;
  extern const Error ERR_REQUEST_CLEARED;
  extern const Error ERR_REQUEST_NOT_FINISHED;

  extern const Error ERR_INVALID_INTERNAL_NAME;

  extern const Error ERR_SCORE_MATRIX_TOO_MANY_CELLS;

  extern const Error ERR_INVALID_LUA_CODE;
  extern const Error ERR_INVALID_LUA_STATUS;
  extern const Error ERR_INVALID_LUA_MAX_NUMBER_INSTRUCTIONS_EXECUTED;

  extern const Error ERR_GENOME_BEING_USED_BY_EXPERIMENT;
  extern const Error ERR_GENOME_BEING_USED_BY_ANNOTATION;

  extern const Error ERR_PROJECT_BEING_USED_BY_EXPERIMENT;
  extern const Error ERR_SAMPLE_BEING_USED_BY_EXPERIMENT;
  extern const Error ERR_EPIGENETIC_MARK_BEING_USED_BY_EXPERIMENT;
  extern const Error ERR_TECHNIQUE_BEING_USED_BY_EXPERIMENT;

  extern const Error ERR_BIOSOURCE_BEING_USED_BY_SAMPLES;
  extern const Error ERR_BIOSOURCE_SCOPE_CONTENT;

  extern const Error ERR_DUPLICATED_FIELD;
  extern const Error ERR_DUPLICATED_SEQUENCE;
  extern const Error ERR_CHROMOSOME_SEQUENCE_NOT_FOUND;

  extern const Error ERR_EXPERIMENT_SET;
  extern const Error ERR_EXPERIMENT_SET_ID_NOT_FOUND;

  extern const Error ERR_INVALID_DATA_TYPE;

  extern const Error ERR_INVALID_OBJECT_OWNER;

  extern const Error ERR_INVALID_COUNTER;

  extern const Error ERR_PROCESSING_CANCELED;

  extern const Error ERR_FILTER_INVALID_COMMAND;
  extern const Error ERR_FILTER_EMPTY_VALUE;
  extern const Error ERR_FILTER_INVALID_VALUE;
  extern const Error ERR_FILTER_INVALID_TYPE;
  extern const Error ERR_FILTER_INVALID_STRING_COMMAND;

  extern const Error ERR_FILTER_BY_MOTIF_AGGREGATED;

  extern const Error ERR_INVALID_AGG_FUNCTION;

  extern const Error ERR_CALCULATED_FIELD_DATA;

  extern const Error ERR_MEMORY_EXAUSTED;
  extern const Error ERR_STRING_BUILDER_MEMORY_EXAUSTED;

  extern const Error ERR_INVALID_STATE;

  extern const Error ERR_CHUNCK_SIZE_TOO_BIG;

  extern const Error ERR_BINNING_NO_BARS;
  extern const Error ERR_BINNING_TOO_MANY_BARS;

  extern const Error ERR_LOLA_INVALID_NEGATIVE_VALUE_B;
  extern const Error ERR_LOLA_INVALID_NEGATIVE_VALUE_D;

  extern const Error ERR_BITMAP_INVALID_POS;

  extern const Error ERR_WORKAROUND_STRING_ERROR;
}

#endif
